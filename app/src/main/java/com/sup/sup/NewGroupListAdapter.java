package com.sup.sup;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

/**
 * Created by anshudwibhashi on 02/06/17.
 */
public class NewGroupListAdapter  extends BaseAdapter {

    ArrayList<String> names, photos, usernames;
    private LayoutInflater mInflater;

    Context context;
    public NewGroupListAdapter(Context context, ArrayList<String> names, ArrayList<String> usernames, ArrayList<String> photos) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.names = names; this.photos = photos; this.usernames = usernames;
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public String getItem(int position) {
        return null; // useless to us
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<String> selectedUsers = new ArrayList<>();

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  mInflater.inflate(R.layout.list_item_group_participant, null);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.usernameTV = (TextView) convertView.findViewById(R.id.usernameTV);
            holder.profilePictureCIV = (ImageView) convertView.findViewById(R.id.profilePictureCIV);
            holder.addChecker = (CheckBox) convertView.findViewById(R.id.addChecker);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(selectedUsers.contains(usernames.get(position))) {
            holder.addChecker.setChecked(true);
        }

        try {
            holder.nameTV.setText(names.get(position));
            holder.usernameTV.setText("@"+usernames.get(position));

            Glide.with(context).load(photos.get(position)).asBitmap().centerCrop()
                    .placeholder(R.drawable.ic_default_picture)
                    .override(100, 100).into(new BitmapImageViewTarget(holder.profilePictureCIV) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.profilePictureCIV.setImageDrawable(circularBitmapDrawable);
                }
            });

            holder.addChecker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked) {
                        selectedUsers.add(usernames.get(position));
                    } else {
                        selectedUsers.remove(usernames.get(position));
                    }
                }
            });

            convertView.setEnabled(false);
            convertView.setOnClickListener(null);

            return convertView;
        } catch (Exception e) {e.printStackTrace(); return null;}
    }

    public static class ViewHolder {
        public ImageView profilePictureCIV;
        public TextView nameTV;
        public TextView usernameTV;
        public CheckBox addChecker;
    }

}