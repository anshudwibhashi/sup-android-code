package com.sup.sup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;

public class NewGroupActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean pictureChanged = false;
    private Bitmap picture;
    private ImageView profilePicture;
    private String groupName;
    NewGroupListAdapter adapter;

    private boolean editing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);
        if (getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            Utils.populateMainDataSet(this); Utils.populateFeedDataSet(this);
        }

        ArrayList<String> names = new ArrayList<>(), usernames = new ArrayList<>(), photos = new ArrayList<>();

        for (int i = 0; i < Utils.MainDataSet.names.size(); i++) {
            if (Utils.MainDataSet.tokens.get(i).startsWith("CHANNEL_") || Utils.MainDataSet.tokens.get(i).startsWith("GROUP_")) {
                continue;
            }
            names.add(Utils.MainDataSet.names.get(i));
            usernames.add(Utils.MainDataSet.usernames.get(i));
            photos.add(Utils.MainDataSet.photos.get(i));
        }

        adapter = new NewGroupListAdapter(this, names, usernames, photos);

        findViewById(R.id.changePictureButton).setOnClickListener(this);
        findViewById(R.id.removePictureButton).setOnClickListener(this);

        profilePicture = (ImageView) findViewById(R.id.profilePicture);

        Intent intent = getIntent();
        if (intent.getBooleanExtra("editing", false)) {
            setTitle("Edit Group");
            groupName = intent.getStringExtra("groupName");
            SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
            Cursor result = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{groupName});
            result.moveToFirst();
            try {
                JSONArray participants = new JSONArray(result.getString(result.getColumnIndexOrThrow("username")));
                for(int i = 0; i < participants.length(); i++) {
                    adapter.selectedUsers.add(participants.getString(i));
                }
            } catch (Exception e) {e.printStackTrace();}

            // Now we load the image
            Glide.with(this)
                    .load(groupName+"_photo.png")
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(this))
                    .placeholder(R.mipmap.ic_group_generic_huge)
                    .into(profilePicture);

            ((EditText) findViewById(R.id.nameET)).setText(result.getString(result.getColumnIndexOrThrow("name")));
            ((EditText) findViewById(R.id.descET)).setText(result.getString(result.getColumnIndexOrThrow("tagline")));
            pictureUrl = result.getString(result.getColumnIndexOrThrow("photoUrl"));
            editing = true;
        } else {
            groupName = "GROUP_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis());

            findViewById(R.id.removePictureButton).setEnabled(true);
            findViewById(R.id.removePictureButton).setAlpha(1f);
        }

        ((ListView) findViewById(R.id.contactsLV)).setAdapter(adapter);
        picture = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_group_generic_huge);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changePictureButton:
                changePicture();
                break;
            case R.id.removePictureButton:
                removePicture();
                break;
        }
    }

    private void changePicture() {
        /**
         * Allows user to pick a new picture.
         */
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Choose picture") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .limit(1) // max images can be selected (999 by default)
                .start(Constants.REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    String pictureUrl = "";
    private void removePicture() {
        picture = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_group_generic_huge);
        profilePicture.setImageBitmap(picture);
        pictureUrl = "";


        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(new File(this.getFilesDir(), groupName+"_photo.png"));
            picture.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        findViewById(R.id.removePictureButton).setEnabled(false);
        findViewById(R.id.removePictureButton).setAlpha(.5f);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<com.nguyenhoanglam.imagepicker.model.Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);

            // Now we load the image
            Glide.with(this)
                    .load(images.get(0).getPath())
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(this))
                    .placeholder(R.mipmap.ic_group_generic_huge)
                    .into(profilePicture);
            picture = BitmapFactory.decodeFile(images.get(0).getPath());
            pictureChanged = true;

            findViewById(R.id.removePictureButton).setEnabled(true);
            findViewById(R.id.removePictureButton).setAlpha(1f);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_profile:
                saveProfile();
                return true;
            case android.R.id.home:
                overrideNavigation();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        overrideNavigation();
    }

    private void overrideNavigation() {
        if(getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            finish();
            startActivity(new Intent(this, HomeActivity.class));
        }
    }

    ProgressDialog pd;
    String groupDispName, groupDesc, url;
    private void saveProfile(){
        if(pictureChanged) {
            // Save new picture in self_photo.png
            FileOutputStream outputStream;
            try {
                File file = new File(getFilesDir(), groupName+"_photo.png");
                file.createNewFile();
                outputStream = new FileOutputStream(file);
                picture.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        groupDispName = ((EditText) findViewById(R.id.nameET)).getText().toString().trim();
        groupDesc = ((EditText) findViewById(R.id.descET)).getText().toString().trim();

        pd = new ProgressDialog(this);
        pd.setMessage("Saving");
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        if(!Utils.isNetworkAvailable(this)){
            Utils.showToast(this, "No internet connection");
            pd.dismiss();
            return;
        }

        if(groupDispName.isEmpty() || groupDesc.isEmpty()){
            Utils.showToast(this, "Your group must have a name, and a description");
            pd.dismiss();
            return;
        }

        if(groupDispName.length() > 25){
            Utils.showToast(this,"Name must be shorter than 25 characters");
            pd.dismiss();
            return;
        }
        if (groupDesc.length() > 100){
            Utils.showToast(this,"Description must be shorter than 100 characters");
            pd.dismiss();
            return;
        }
        ArrayList<String> participantsList = adapter.selectedUsers;
        final JSONArray participants = new JSONArray();
        for (int i = 0; i < participantsList.size(); i++) {
            participants.put(participantsList.get(i));
        }
        String endpoint;
        if(editing) {
            endpoint = "edit_group";
        } else {
            endpoint = "create_group";
        }
        url = "http://black-abode-2709.appspot.com/"+endpoint+"?groupID="+ URLEncoder.encode(groupName)+
                "&groupName="+URLEncoder.encode(groupDispName) +
                "&groupDesc="+URLEncoder.encode(groupDesc)+
                "&owner="+URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", ""));

        // Now upload it to FCS. Filenames are of format "<groupID>_photo.png"

        if(pictureChanged || !editing) {
            // either the picture has changed or they're creating a profile for the first time

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] data = baos.toByteArray();

            Utils.uploadToFCS(this, "Saving your picture", groupName + "_photo.png", data, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    url += "&photoUrl=" + result;
                    final String photoUrl = (String) result;
                    // Now Volley magic
                    if (!editing) {
                        // they're creating the group for the first time, add the owner's username to participants list
                        participants.put(Utils.getStringFromSharedPrefs("self_username", ""));
                        url += "&participants="+URLEncoder.encode(participants.toString());
                    }
                    Utils.volleyStringCall(NewGroupActivity.this, url, new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            Utils.updateGroup(NewGroupActivity.this, groupName, groupDispName, groupDesc, participants.toString(), photoUrl, new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    pd.dismiss();
                                    finish();
                                }
                            });
                        }
                    });
                }
            });

            // Small pic for fast loading
            ByteArrayOutputStream baosmini = new ByteArrayOutputStream();
            Bitmap mini = Utils.resize(picture, 100, 100);
            mini.compress(Bitmap.CompressFormat.PNG, 100, baosmini);
            byte[] data_mini = baosmini.toByteArray();

            Utils.uploadToFCS(this, null, groupName + "_photo_mini.png", data_mini, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    // Don't care...
                }
            });
        } else {
            // picture hasn't changed, and they're editing
            url += "&participants="+URLEncoder.encode(participants.toString());
            Utils.volleyStringCall(NewGroupActivity.this, url+"&photoUrl=" + pictureUrl, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    Utils.updateGroup(NewGroupActivity.this, groupName, groupDispName, groupDesc, participants.toString(), "", new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            pd.dismiss();
                            finish();
                        }
                    });
                }
            });
        }
    }
}
