package com.sup.sup;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class EmptyAdapter  extends BaseAdapter {
    private LayoutInflater mInflater;
    Context context;
    public boolean feed, comments;
    public String title, message;

    public EmptyAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        feed = false;
        comments = false;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public String getItem(int position) {
        return null; // useless to us
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (feed) {
            convertView = mInflater.inflate(R.layout.list_item_empty2, null);
        } else {
            convertView = mInflater.inflate(R.layout.list_item_empty, null);
        }
        if (comments) {
            convertView.setBackgroundColor(Color.WHITE);
        }
        ((TextView) convertView.findViewById(R.id.titleTV)).setText(title);
        ((TextView) convertView.findViewById(R.id.messageTV)).setText(message);

        return convertView;
    }

}