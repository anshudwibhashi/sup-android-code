package com.sup.sup;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

public class MLBPBPAdapter extends BaseAdapter {
    Context context; JSONArray pbp;
    public MLBPBPAdapter(Context context, JSONObject pbp) {
        this.context = context; this.pbp = processPbP(pbp);
    }
    @Override
    public int getCount() {
        try {
            return pbp.length();
        } catch (Exception e) {e.printStackTrace(); return 0;}
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  ((Activity)context).getLayoutInflater().inflate(R.layout.list_item_mlb_plays, null);
            holder.battingTeam = (TextView) convertView.findViewById(R.id.battingTeamTV);
            holder.inning = (TextView) convertView.findViewById(R.id.inningTV);
            holder.inningHalf = (TextView) convertView.findViewById(R.id.inningHalfTV);
            holder.playDisp = (TextView) convertView.findViewById(R.id.playDispTV);
            holder.line1 = (TextView) convertView.findViewById(R.id.line1TV);
            holder.line2 = (TextView) convertView.findViewById(R.id.line2TV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            JSONObject play = pbp.getJSONObject(pbp.length() - position - 1); // for reverse
            holder.battingTeam.setText(play.getString("batTeam"));
            holder.inning.setText(play.getString("inning"));
            holder.inningHalf.setText(play.getString("inningHalf"));
            holder.playDisp.setText(play.getString("playType"));
            holder.line1.setText(play.getString("line1"));
            holder.line2.setText(play.getString("line2"));
        } catch (Exception e) { e.printStackTrace();}

        return convertView;
    }

    public static class ViewHolder {
        public TextView battingTeam, inning, inningHalf, playDisp, line1, line2;
    }

    private JSONArray processPbP(JSONObject pbp) {
        try {
            JSONArray majorPlays = pbp.getJSONObject("gameplaybyplay").getJSONObject("atBats").getJSONArray("atBat");
            JSONArray plays = new JSONArray();
            for (int i = 0; i < majorPlays.length(); i++) {
                String innings = majorPlays.getJSONObject(i).getString("inning");
                String inningHalf = majorPlays.getJSONObject(i).getString("inningHalf");
                String batTeam = majorPlays.getJSONObject(i).getJSONObject("battingTeam").getString("Abbreviation");
                for(int j = 0; j < majorPlays.getJSONObject(i).getJSONArray("atBatPlay").length(); j++) {
                    JSONObject currentPlay = majorPlays.getJSONObject(i).getJSONArray("atBatPlay").getJSONObject(j);
                    String playType = currentPlay.keys().next();
                    String line1 = "", line2 = "", playTypeDisplay="";
                    switch (playType) {
                        case "batterUp":
                            playTypeDisplay = "Batter Up";
                            line1 = currentPlay.getJSONObject("batterUp").getJSONObject("battingPlayer").getString("FirstName")+
                                    " "+currentPlay.getJSONObject("batterUp").getJSONObject("battingPlayer").getString("LastName");
                            line2 = "";
                            break;
                        case "pitch":
                            playTypeDisplay = "Pitch";
                            line1 = currentPlay.getJSONObject("pitch").getJSONObject("pitchingPlayer").getString("LastName")+" to "+
                                    currentPlay.getJSONObject("pitch").getJSONObject("battingPlayer").getString("LastName");
                            line2 = processCase(currentPlay.getJSONObject("pitch").getString("result"));
                            break;
                        case "pickoffAttempt":
                            playTypeDisplay = "Pickoff Attempt";
                            line1 = "Pitched by "+currentPlay.getJSONObject("pickoffAttempt").getJSONObject("pitchingPlayer").getString("LastName")+
                                    ", ran by "+currentPlay.getJSONObject("pickoffAttempt").getJSONObject("runningPlayer").getString("LastName")+
                                    ", caught by "+currentPlay.getJSONObject("pickoffAttempt").getJSONObject("catchingPlayer").getString("LastName");
                            line2 = (Boolean.valueOf(currentPlay.getJSONObject("pickoffAttempt").getString("isSuccessful"))?"Successfully":"Unsuccessfully")
                                    +" to base "+currentPlay.getJSONObject("pickoffAttempt").getString("toBase");
                            break;
                        case "hit":
                            playTypeDisplay = "Hit";
                            line1 = "Pitched by "+currentPlay.getJSONObject("hit").getJSONObject("pitchingPlayer").getString("LastName")+
                                    " to "+currentPlay.getJSONObject("hit").getJSONObject("battingPlayer").getString("LastName")+
                                    " for a "+processCase(currentPlay.getJSONObject("hit").getString("hitType"));
                            line2 = (Boolean.valueOf(currentPlay.getJSONObject("hit").getString("isOut"))?("Out"+
                                    (Boolean.valueOf(currentPlay.getJSONObject("hit").getString("isCaughtOut"))?" (Caught)":"")
                            ):"Safe");
                            break;
                        case "baseRunAttempt":
                            playTypeDisplay = "Run Attempt";
                            line1 = currentPlay.getJSONObject("baseRunAttempt").getJSONObject("runningPlayer").getString("LastName")+
                                    " from "+currentPlay.getJSONObject("baseRunAttempt").getString("fromBase")+" to "+
                                    currentPlay.getJSONObject("baseRunAttempt").getString("toBase");
                            line2 = (Boolean.valueOf(currentPlay.getJSONObject("baseRunAttempt").getString("isSafe"))?"Safe, ":"Unsafe, ")+
                                    (Boolean.valueOf(currentPlay.getJSONObject("baseRunAttempt").getString("isRunScored"))?"Run scored, ":"No run scored, ")+
                                    (Boolean.valueOf(currentPlay.getJSONObject("baseRunAttempt").getString("isForcedOut"))?"Forced out, ":"Not forced out")+
                                    (Boolean.valueOf(currentPlay.getJSONObject("baseRunAttempt").getString("isWalk"))?", Walk ":"")+
                                    (Boolean.valueOf(currentPlay.getJSONObject("baseRunAttempt").getString("isWalkIntentional"))?"(Intentional)":"");
                            break;
                        case "baseStealAttempt":
                            playTypeDisplay = "Base Steal Attempt";
                            line1 = currentPlay.getJSONObject("baseStealAttempt").getJSONObject("stealingPlayer").getString("LastName")+
                                    " attempted to steal base "+currentPlay.getJSONObject("baseStealAttempt").getString("base");
                            line2 = (Boolean.valueOf(currentPlay.getJSONObject("baseStealAttempt").getString("isSuccessful"))?"Successful ":"Unsuccessful");
                            break;
                        case "ballThrow":
                            playTypeDisplay = "Ball Throw";
                            line1 = currentPlay.getJSONObject("ballThrow").getJSONObject("fromPlayer").getString("LastName")+
                                    (currentPlay.getJSONObject("ballThrow").has("fromBase")?" (base "+currentPlay.getJSONObject("ballThrow").getString("fromBase")+")":"")
                                    +" to "+
                                    currentPlay.getJSONObject("ballThrow").getJSONObject("toPlayer").getString("LastName")+
                                    (currentPlay.getJSONObject("ballThrow").has("toBase")?" (base "+currentPlay.getJSONObject("ballThrow").getString("toBase")+")":"");
                            line2 = (currentPlay.getJSONObject("ballThrow").has("runningPlayer")?(currentPlay.getJSONObject("ballThrow").getJSONObject("runningPlayer").getString("LastName")+
                                    (Boolean.valueOf(currentPlay.getJSONObject("ballThrow").getString("isRunningPlayerOut"))?" out":" not out")):"");
                            break;
                        case "playerSubstitution":
                            playTypeDisplay = "Substitution";
                            line1 = currentPlay.getJSONObject("playerSubstitution").getJSONObject("team").getString("Abbreviation");
                            line2 = "Outgoing: "+currentPlay.getJSONObject("playerSubstitution").getJSONObject("outgoingPlayer").getString("LastName")+
                            ", incoming: "+currentPlay.getJSONObject("playerSubstitution").getJSONObject("incomingPlayer").getString("LastName");
                            break;
                    }

                    JSONObject play = new JSONObject();
                    play.put("playType", playTypeDisplay);
                    play.put("inning", innings);
                    play.put("inningHalf", processCase(inningHalf));
                    play.put("batTeam", batTeam);
                    play.put("line1", line1);
                    play.put("line2", line2);
                    plays.put(play);
                }
            }
            return plays;
        } catch (Exception e) {e.printStackTrace(); return null;}
    }
    private String processCase(String s) {
        s = s.replace("_", " ");
        final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
        // to be capitalized

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray()) {
            c = (capNext)
                    ? Character.toUpperCase(c)
                    : Character.toLowerCase(c);
            sb.append(c);
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0); // explicit cast not needed
        }
        return sb.toString();
    }
}
