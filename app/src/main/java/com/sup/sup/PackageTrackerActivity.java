package com.sup.sup;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;

import java.net.URLEncoder;

public class PackageTrackerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_tracker);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        try {
            JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("package_tracking_ids", "[]"));
            if(pre.length() > 0) {
                // we have some tickers
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                ListView lv = (ListView) findViewById(R.id.tickerLV);
                lv.setAdapter(new TopicsAdapter(this));
            }
        } catch(Exception e) {e.printStackTrace();}
    }

    public void addTrackingID(View view) {

        new MaterialDialog.Builder(this)
                .title("Enter Tracking Code")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                        try {
                            final String keywords = input.toString();
                            if(keywords != null && !keywords.isEmpty()) {
                                // send keyword to server and have it setup the webhook
                                Utils.volleyStringCall(PackageTrackerActivity.this, "http://black-abode-2709.appspot.com/newTrackingID?id="+ URLEncoder.encode(keywords) + "&fcm_token=" + FirebaseInstanceId.getInstance().getToken() , new VolleyCallback() {
                                    @Override
                                    public void onResponse(Object result) {
                                        try {
                                            if (((String)result).equals("OK")) {
                                                JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("package_tracking_ids", "[]"));
                                                pre.put(keywords);
                                                Utils.putStringInSharedPrefs("package_tracking_ids", pre.toString());
                                                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                                                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                                                ListView lv = (ListView) findViewById(R.id.tickerLV);
                                                lv.setAdapter(new TopicsAdapter(PackageTrackerActivity.this));
                                            } else {
                                                new MaterialDialog.Builder(PackageTrackerActivity.this)
                                                        .title("Error")
                                                        .content("Something's wrong. Check your tracking code and try again.")
                                                        .cancelListener(new DialogInterface.OnCancelListener() {
                                                            @Override
                                                            public void onCancel(DialogInterface dialog) {
                                                                finish();
                                                            }
                                                        })
                                                        .show();
                                            }
                                        } catch (Exception e) {e.printStackTrace();}
                                    }
                                });
                            }
                        } catch (Exception e) {e.printStackTrace();}
                    }
                }).show();
    }
}
