package com.sup.sup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLEncoder;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private SignInButton mSignInButton;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    private void signIn() {
        if(Utils.isNetworkAvailable(this)) {
            // First create a progress box
            pd = new ProgressDialog(LoginActivity.this);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.setMessage("Please wait...");
            pd.setIndeterminate(true);
            pd.show();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, Constants.RC_SIGN_IN);
        } else {
            Utils.showToast(this, "No internet connection");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constants.RC_SIGN_IN:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
                break;
        }
    }

    private void clearAll(){
        // clear everything
        SharedPreferences.Editor editor = Utils.mSharedPreferences.edit();
        editor.clear();
        editor.apply();

        // Now delete all that's in the db
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
        db.delete("connections", null, null);

        // Now delete all files in internal storage
        File dir = getFilesDir();
        String[] children = dir.list();
        for (String child : children) {
            new File(dir, child).delete();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            // Now obtain the user's details and save data
            String name = acct.getDisplayName(); if(name==null) name = "";
            String username = acct.getEmail().substring(0, acct.getEmail().indexOf('@'));
            final String token = String.valueOf(acct.getId());

            Utils.volleyStringCall(this, "http://black-abode-2709.appspot.com/logoutFromOtherDevices?token=" + token, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {

                }
            });

            clearAll();

            String fcm_token = FirebaseInstanceId.getInstance().getToken(); if(fcm_token == null) fcm_token = "";
            String photoUrl;
            Uri photoUri = acct.getPhotoUrl(); if(photoUri==null) photoUrl = ""; else photoUrl = photoUri.toString();

            // Send to our app servers
            String url = "http://black-abode-2709.appspot.com/login?name=" + URLEncoder.encode(name) + "&token=" + URLEncoder.encode(token) + "&fcm_token=" + URLEncoder.encode(fcm_token) + "&username=" + URLEncoder.encode(username) + "&photoUrl=" + URLEncoder.encode(photoUrl);
            Utils.volleyStringCall(this, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    handleLoginResponse((String)result, token);
                }
            });
        } else {
            // Show error toast
            Utils.showToast(this, "Sign in failed");
        }
    }

    private void handleLoginResponse(final String result, final String token){
        Utils.downloadProfile(LoginActivity.this, token, new VolleyCallback() {
            @Override
            public void onResponse(Object profile) {
                processProfile(result, (JSONObject) profile, token);
            }
        });
    }

    private ProgressDialog pd;
    private void processProfile(final String response, final JSONObject profile, final String token) {

        try {
            profile.put("following", profile.getString("following").replaceAll("u'","'")); // clean up
            Utils.saveSelfProfile(profile.getString("name"), token, profile.getString("photoUrl"), profile.getString("tagline"), profile.getString("following"), profile.getString("username"), profile.getString("settings"),
                    profile.getString("phone_number"));

            // Now download this user's contacts
            Utils.downloadContacts(LoginActivity.this, profile, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    // Manage concurrency:
                    try {
                        if (Utils.getConnectionsCount(LoginActivity.this) != new JSONArray(profile.getString("following")).length()) {
                            return; // we still haven't downloaded all contacts
                        }
                    } catch (Exception e) {e.printStackTrace();}

                    // Now download this user's groups
                    Utils.downloadGroups(LoginActivity.this, token, new VolleyCallback(){
                        @Override
                        public void onResponse(Object result) {
                            savePhoto(response, profile, token);
                        }
                    });
                }
            });
        } catch(Exception e) { e.printStackTrace(); }
    }

    private void savePhoto(final String response, JSONObject profile, String token) {
        try {
            // Save the user's photo to self_photo.png
            if (profile.getString("photoUrl") != null && !profile.getString("photoUrl").equals("")) {
                Utils.downloadImageAndSave(LoginActivity.this, profile.getString("photoUrl"), "self_photo.png", new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        redirect(response);
                    }
                });
            } else {
                // Save default profile picture (drawable) to internal storage
                FileOutputStream outputStream;
                try {
                    outputStream = LoginActivity.this.openFileOutput("self_photo.png", Context.MODE_PRIVATE);
                    BitmapFactory.decodeResource(LoginActivity.this.getResources(), R.drawable.ic_default_picture).compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                redirect(response);
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    private void redirect(String response) {
        if (response.equals("recurring")) {
            Utils.populateMainDataSet(LoginActivity.this);
            pd.dismiss();
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
        } else {
            pd.dismiss();
            startActivity(new Intent(LoginActivity.this, CreateProfileActivity.class));
            finish();
        }
        Utils.putBooleanInSharedPrefs("signed_in", true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Make the sign in button wider
        mSignInButton = (SignInButton) findViewById(R.id.sign_in_button);
        mSignInButton.setSize(SignInButton.SIZE_WIDE);

        // Setup Google sign in
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestId()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Set an OnClickListener for the Google sign in button
        findViewById(R.id.sign_in_button).setOnClickListener(this);

        TextView textView = ((TextView)findViewById(R.id.text));
        textView.setClickable(true);
        String linkTxt=getResources().getString(R.string.link_text);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(Html.fromHtml(linkTxt));
    }
}
