package com.sup.sup;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

public class NHLPBPAdapter extends BaseAdapter {
    Context context; JSONArray pbp;
    public NHLPBPAdapter(Context context, JSONObject pbp) {
        this.context = context; this.pbp = processPbP(pbp);
    }
    @Override
    public int getCount() {
        try {
            return pbp.length();
        } catch (Exception e) {e.printStackTrace(); return 0;}
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  ((Activity)context).getLayoutInflater().inflate(R.layout.list_item_nhl_plays, null);
            holder.periodTV = (TextView) convertView.findViewById(R.id.periodTV);
            holder.timeTV = (TextView) convertView.findViewById(R.id.timeTV);
            holder.descTV = (TextView) convertView.findViewById(R.id.descTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            JSONObject play = pbp.getJSONObject(pbp.length() - position - 1); // for reverse
            holder.descTV.setText(play.getString("desc"));
            holder.timeTV.setText(play.getString("time"));
            holder.periodTV.setText(play.getString("period"));
        } catch (Exception e) { e.printStackTrace();}

        return convertView;
    }

    public static class ViewHolder {
        public TextView descTV, timeTV, periodTV;
    }

    private JSONArray processPbP(JSONObject pbp) {
        try {
            JSONArray majorPlays = pbp.getJSONObject("gameplaybyplay").getJSONObject("plays").getJSONArray("play");
            JSONArray plays = new JSONArray();
            for (int i = 0; i < majorPlays.length(); i++) {
                String time = majorPlays.getJSONObject(i).getString("time");
                String period = majorPlays.getJSONObject(i).getString("period");
                String desc = "";
                String playType = "";
                Iterator<String> iter = majorPlays.getJSONObject(i).keys();
                while(iter.hasNext()){
                    String key = iter.next();
                    if(!key.equals("time") && !key.equals("period")) {
                        playType = key;
                        break;
                    }
                }

                switch (playType) {
                    case "faceoff":
                        desc = "Face-off between " + majorPlays.getJSONObject(i).getJSONObject("faceoff").getJSONObject("homePlayer").getString("LastName")+
                                " and " +majorPlays.getJSONObject(i).getJSONObject("faceoff").getJSONObject("awayPlayer").getString("LastName")+" · Won by "+
                                majorPlays.getJSONObject(i).getJSONObject("faceoff").getString("wonBy");
                        break;
                    case "shot":
                        desc = "Shot by "+majorPlays.getJSONObject(i).getJSONObject("shot").getJSONObject("shooter").getString("LastName")+" ("+
                                majorPlays.getJSONObject(i).getJSONObject("shot").getString("teamAbbreviation")+")";
                        break;
                    case "hit":
                        desc = "Hit by "+majorPlays.getJSONObject(i).getJSONObject("hit").getJSONObject("player").getString("LastName")+" ("+
                                majorPlays.getJSONObject(i).getJSONObject("hit").getString("teamAbbreviation")+")";
                        break;
                    case "goal":
                        desc = "Goal by "+majorPlays.getJSONObject(i).getJSONObject("goal").getJSONObject("goalScorer").getString("LastName")+" ("+
                                majorPlays.getJSONObject(i).getJSONObject("goal").getString("teamAbbreviation")+")"+
                                (!majorPlays.getJSONObject(i).getJSONObject("goal").isNull("assist1Player")?(" · Assisted by "+
                                        majorPlays.getJSONObject(i).getJSONObject("goal").getJSONObject("assist1Player").getString("LastName")+
                                        ((!majorPlays.getJSONObject(i).getJSONObject("goal").isNull("assist2Player"))?" and "+
                                        majorPlays.getJSONObject(i).getJSONObject("goal").getJSONObject("assist2Player").getString("LastName")
                                        :"")
                                ):"");
                        break;
                    case "penalty":
                        desc = "Penalty awarded to "+majorPlays.getJSONObject(i).getJSONObject("penalty").getJSONObject("penalizedPlayer").getString("LastName")+" ("+
                                majorPlays.getJSONObject(i).getJSONObject("penalty").getString("teamAbbreviation")+")"+
                                (!majorPlays.getJSONObject(i).getJSONObject("penalty").isNull("durationMinutes")?" for "+majorPlays.getJSONObject(i).getJSONObject("penalty").getString("durationMinutes")
                                        +" minutes":"")+
                                (!majorPlays.getJSONObject(i).getJSONObject("penalty").isNull("type")?" · "+majorPlays.getJSONObject(i).getJSONObject("penalty").getString("type"):"");
                        break;
                    case "goalieChange":
                        desc = "Goalie change ("+
                                majorPlays.getJSONObject(i).getJSONObject("goalieChange").getString("teamAbbreviation")+")" +
                                (!majorPlays.getJSONObject(i).getJSONObject("goalieChange").isNull("incomingGoalie")?" · Incoming: "+
                                        majorPlays.getJSONObject(i).getJSONObject("goalieChange").getJSONObject("incomingGoalie").getString("LastName"):"") +
                                (!majorPlays.getJSONObject(i).getJSONObject("goalieChange").isNull("outgoingGoalie")?" · Outgoing: "+
                                        majorPlays.getJSONObject(i).getJSONObject("goalieChange").getJSONObject("outgoingGoalie").getString("LastName"):"");
                        break;
                    case "penaltyShot":
                        desc = majorPlays.getJSONObject(i).getJSONObject("penaltyShot").getJSONObject("shooter").getString("LastName")+" ("+
                                majorPlays.getJSONObject(i).getJSONObject("penaltyShot").getString("teamAbbreviation")+") takes penalty shot · "+
                                majorPlays.getJSONObject(i).getJSONObject("penaltyShot").getString("outcome");
                        break;
                    case "shootoutAttempt":
                        desc = "Shootout attempt by "+majorPlays.getJSONObject(i).getJSONObject("shootoutAttempt").getJSONObject("shooter").getString("LastName")+" ("+
                                majorPlays.getJSONObject(i).getJSONObject("shootoutAttempt").getString("teamAbbreviation")+") · "+
                                majorPlays.getJSONObject(i).getJSONObject("shootoutAttempt").getString("outcome");
                        break;
                    default:
                        desc = playType;
                }

                JSONObject play = new JSONObject();
                play.put("time", time);
                play.put("period", period);
                play.put("desc", desc);
                plays.put(play);

            }
            return plays;
        } catch (Exception e) {Log.d("Here", "ded");e.printStackTrace(); return null;}
    }
}
