package com.sup.sup;

import android.content.DialogInterface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.ToolbarWidgetWrapper;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;

public class TwitterChannelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitter_channel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        try {
            JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("twitter_handles", "[]"));
            if(pre.length() > 0) {
                // we have some tickers
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                ListView lv = (ListView) findViewById(R.id.tickerLV);
                lv.setAdapter(new TopicsAdapter(this));
            }
        } catch(Exception e) {e.printStackTrace();}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (Utils.getBooleanFromSharedPrefs("show_twitter_channel_notifications", true)) {
            getMenuInflater().inflate(R.menu.menu_channels, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_channels2, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_toggle_notifs:
                toggleNotifs();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void toggleNotifs() {
        Utils.putBooleanInSharedPrefs("show_twitter_channel_notifications",
                !Utils.getBooleanFromSharedPrefs("show_twitter_channel_notifications", true) );
        if (Utils.getBooleanFromSharedPrefs("show_twitter_channel_notifications", true)) {
            Utils.showToast(this, "Notifications on");
        } else {
            Utils.showToast(this, "Notifications off");
        }invalidateOptionsMenu();
    }

    public void addHandle(View view) {

        new MaterialDialog.Builder(this)
                .title("Enter Twitter handle")
                .content("Without the @")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                        final String keywords = input.toString();
                        // Send keyword to server
                        Utils.volleyStringCall(TwitterChannelActivity.this, "http://35.202.114.198/sup/?operation=add&value=" + keywords, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                FirebaseMessaging.getInstance().subscribeToTopic("twitter_"+keywords); // Subscribe to FCM topic
                            }
                        });
                        if(keywords != null && !keywords.isEmpty()) {
                            try {
                                JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("twitter_handles", "[]"));
                                pre.put(keywords);
                                Utils.putStringInSharedPrefs("twitter_handles", pre.toString());
                                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                                ListView lv = (ListView) findViewById(R.id.tickerLV);
                                lv.setAdapter(new TopicsAdapter(TwitterChannelActivity.this));
                            } catch (Exception e){e.printStackTrace();}
                        }
                    }
                }).show();
    }
}
