package com.sup.sup;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.utils.DateFormatter;

import org.json.JSONObject;

import rm.com.audiowave.AudioWaveView;

/*
 * Created by troy379 on 05.04.17.
 */
public class IncomingVoiceMessageViewHolder
        extends MessageHolders.IncomingTextMessageViewHolder<Smalltalk.MyMessage> {

    private TextView tvTime; private ImageView playButton, cancelButton;
    private AudioWaveView wave;

    public IncomingVoiceMessageViewHolder(View itemView) {
        super(itemView);
        tvTime = (TextView) itemView.findViewById(R.id.time);
        playButton = (ImageView) itemView.findViewById(R.id.play_action);
        cancelButton = (ImageView) itemView.findViewById(R.id.cancel_action3);
        wave = (AudioWaveView) itemView.findViewById(R.id.wave);
    }

    @Override
    public void onBind(Smalltalk.MyMessage message) {
        super.onBind(message);
        tvTime.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
        try {
            Utils.renderAudioResponse(wave, playButton, cancelButton, new JSONObject().put("response", message.getImgPath()));
        } catch (Exception e) {e.printStackTrace();}
    }
}