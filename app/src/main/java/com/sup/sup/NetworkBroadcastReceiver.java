package com.sup.sup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;

public class NetworkBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if(Utils.isNetworkAvailable(context)) {
            try{
                JSONArray urls = new JSONArray(Utils.getStringFromSharedPrefs("pending_requests", "[]"));
                for(int i = 0; i < urls.length(); i++){
                    Utils.volleyStringCall(context, urls.getString(i), new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            // Don't care.
                        }
                    });
                }
                // Now delete everything from pending_requests
                Utils.putStringInSharedPrefs("pending_requests", "[]");
            } catch(Exception e) {e.printStackTrace();}
        }
    }
}