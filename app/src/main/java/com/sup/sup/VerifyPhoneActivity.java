package com.sup.sup;

import android.os.Handler;
import androidx.core.app.NavUtils;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.lamudi.phonefield.PhoneInputLayout;

import java.net.URLEncoder;
import java.util.Random;

public class VerifyPhoneActivity extends AppCompatActivity {

    PhoneInputLayout input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Verify Phone");

        input = (PhoneInputLayout) findViewById(R.id.phone_input_layout);
        input.setDefaultCountry(this.getResources().getConfiguration().locale.getCountry());
        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (input.isValid()) {
                    Utils.showToast(VerifyPhoneActivity.this, "Verification code sent");
                    VerifyPhoneActivity.this.findViewById(R.id.NumberLayout).setVisibility(View.GONE);
                    VerifyPhoneActivity.this.findViewById(R.id.VerifyLayout).setVisibility(View.VISIBLE);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            VerifyPhoneActivity.this.findViewById(R.id.resendButton).setVisibility(View.VISIBLE);
                        }
                    }, 5000);
                    phoneNumber = input.getPhoneNumber();
                    sendVerificationCode();
                } else {
                    Utils.showToast(VerifyPhoneActivity.this, "Invalid number");
                }
            }
        });
        findViewById(R.id.resendButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendVerificationCode();
            }
        });

        ((PinView)findViewById(R.id.pinView)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String enteredValue = ((PinView)findViewById(R.id.pinView)).getText().toString();
                    if(enteredValue.equals(String.valueOf(rand))) {
                        // success
                        Utils.showToast(VerifyPhoneActivity.this, "Phone verified");
                        finish();
                        Utils.putStringInSharedPrefs("self_phone_number", phoneNumber);
                        Utils.volleyStringCall(VerifyPhoneActivity.this,
                                "http://black-abode-2709.appspot.com/addPhoneNumber?phone=" +
                                        URLEncoder.encode(phoneNumber) + "&token=" + Utils.getStringFromSharedPrefs("self_token",""),
                                new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {

                            }
                        });
                    } else {
                        // failure
                        Utils.showToast(VerifyPhoneActivity.this, "Incorrect code");
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private int rand; private String phoneNumber;
    private void sendVerificationCode() {
        Random r = new Random( System.currentTimeMillis() );
        rand = 10000 + r.nextInt(20000);
        Utils.volleyStringCall(this, "http://black-abode-2709.appspot.com/sendVerificationCode?phone=" + URLEncoder.encode(phoneNumber) + "&code=" + String.valueOf(rand), new VolleyCallback() {
            @Override
            public void onResponse(Object result) {

            }
        });
        Utils.showToast(this, "Code Sent");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
        }
        return true;
    }
}