package com.sup.sup;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.utils.DateFormatter;

import org.json.JSONObject;

/*
 * Created by troy379 on 05.04.17.
 */
public class OutgoingVideoMessageViewHolder
        extends MessageHolders.OutcomingTextMessageViewHolder<Smalltalk.MyMessage> {

    private TextView tvTime; private ImageView playButton2, imageView;

    public OutgoingVideoMessageViewHolder(View itemView) {
        super(itemView);
        tvTime = (TextView) itemView.findViewById(R.id.time);
        playButton2 = (ImageView) itemView.findViewById(R.id.playButton2);
        imageView = (ImageView) itemView.findViewById(R.id.imageResponseIV);
    }

    @Override
    public void onBind(Smalltalk.MyMessage message) {
        super.onBind(message);
        tvTime.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
        try {
            Utils.renderVideoResponse(imageView, new JSONObject().put("response", message.getText()), false, playButton2);
        } catch (Exception e) {e.printStackTrace();}
    }
}