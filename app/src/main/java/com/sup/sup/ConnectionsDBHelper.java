package com.sup.sup;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * The Connections DB stores all the people that this person is connected to
 */
public class ConnectionsDBHelper extends SQLiteOpenHelper{
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Connections.db";

    public ConnectionsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS connections " +
                "(token TEXT, " + // Just a token to identify users
                "name TEXT, " + // Name of the user
                "tagline TEXT, " + // The user's tagline
                "sup_count INTEGER, " + // The number of sups this person has sent you
                "response TEXT, " + // This person's response to your sups. See schema at the end of this file
                "smalltalk TEXT, " + // Small talk conversation.
                "microtext_read INT," + // Effectively a boolean. Whether or not you've read this microtext. True if no microtext.
                "last_modified TEXT, " + // The timestamp for last interaction with this user. For sorting purposes.
                "username TEXT, " + // @username
                "online_status TEXT," + // 0 means online, -1 means offline (no last seen available), and other number is the timestamp of last seen.
                "photoUrl TEXT);"); // The photoUrl, just to see whether or not to round the photo for MainAdapter.

        // REMINDER: All profile pictures stored in internal storage as <token>_photo.png
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private static ConnectionsDBHelper instance;

    public static synchronized ConnectionsDBHelper getHelper(Context context)
    {
        if (instance == null)
            instance = new ConnectionsDBHelper(context);

        return instance;
    }
}

/*
 * Response schema for single-users: A JSONObject: {'type':<type_of_response>, 'response':<the text or media>}
 * Response schema for groups: A JSONObject: {'sup_sender':<username>, 'responses':[<A list of responses with the same schema as above, but also including sender>]}
 */