package com.sup.sup;

import android.content.DialogInterface;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;

public class RedditChannelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reddit_channel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            Utils.populateMainDataSet(this); Utils.populateFeedDataSet(this);
        }
        try {
            JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("subreddits", "[]"));
            if(pre.length() > 0) {
                // we have some tickers
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                ListView lv = (ListView) findViewById(R.id.tickerLV);
                lv.setAdapter(new TopicsAdapter(this));
            }
        } catch(Exception e) {e.printStackTrace();}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (Utils.getBooleanFromSharedPrefs("show_reddit_channel_notifications", true)) {
            getMenuInflater().inflate(R.menu.menu_channels, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_channels2, menu);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        overrideNavigation();
    }

    private void overrideNavigation() {
        if(getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            finish();
            startActivity(new Intent(this, HomeActivity.class));
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_toggle_notifs:
                toggleNotifs();
                return true;
            case android.R.id.home:
                overrideNavigation();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void toggleNotifs() {
        Utils.putBooleanInSharedPrefs("show_reddit_channel_notifications",
               !Utils.getBooleanFromSharedPrefs("show_reddit_channel_notifications", true) );
        if (Utils.getBooleanFromSharedPrefs("show_reddit_channel_notifications", true)) {
            Utils.showToast(this, "Notifications on");
        } else {
            Utils.showToast(this, "Notifications off");
        }invalidateOptionsMenu();
    }

    public void addSubreddit(View view) {

        new MaterialDialog.Builder(this)
                .title("Enter Subreddit")
                .content("Without the /r/")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                        String keywords = input.toString();
                        if(keywords != null && !keywords.isEmpty()) {
                            try {
                                JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("subreddits", "[]"));
                                pre.put(keywords);
                                Utils.putStringInSharedPrefs("subreddits", pre.toString());
                                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                                ListView lv = (ListView) findViewById(R.id.tickerLV);
                                lv.setAdapter(new TopicsAdapter(RedditChannelActivity.this));
                            } catch (Exception e){e.printStackTrace();}
                        }
                    }
                }).show();
    }
}
