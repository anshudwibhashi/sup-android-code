package com.sup.sup;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import org.json.JSONArray;

public class ParticipantsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participants);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        try {
            JSONArray results = new JSONArray(getIntent().getStringExtra("participants"));
            ((ListView) findViewById(R.id.participantsLV)).setAdapter(new SearchAdapter(this, results, null));
        }catch (Exception e){e.printStackTrace();}
    }
}
