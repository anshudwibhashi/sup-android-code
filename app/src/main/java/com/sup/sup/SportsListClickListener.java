package com.sup.sup;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import org.json.JSONArray;

/**
 * Created by anshudwibhashi on 08/10/17.
 */

public class SportsListClickListener implements AdapterView.OnItemClickListener {
    JSONArray sports; Context context;
    public SportsListClickListener(JSONArray sports, Context context) {
        this.sports = sports;
        this.context = context;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            context.startActivity(new Intent(context, SportDetailActivity.class).putExtra("sport", sports.getJSONObject(position).toString()));
        } catch (Exception e) {e.printStackTrace();}
    }
}
