package com.sup.sup;

import android.app.Activity;
import android.content.Context;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by anshudwibhashi on 11/10/17.
 */

public class SportsResultsAdapter extends RecyclerView.Adapter<SportsResultsAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    JSONArray results;

    @Override
    public int getItemCount() {
        return results.length();
    }

    Context context;
    public SportsResultsAdapter(Context context, JSONArray results) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View convertView =  mInflater.inflate(R.layout.list_item_sports_result, parent, false);
        ViewHolder holder = new ViewHolder(convertView);
        holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
        holder.abbrTV = (TextView) convertView.findViewById(R.id.abbrTV);
        holder.leagueTV = (TextView) convertView.findViewById(R.id.leagueTV);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.nameTV.setText(results.getJSONObject(position).getString("city") + " " + results.getJSONObject(position).getString("name"));
            holder.leagueTV.setText(results.getJSONObject(position).getString("league"));
            holder.abbrTV.setText(results.getJSONObject(position).getString("abbreviation"));

            holder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // now subscribe
                    try {
                        JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("sports_teams_abbrs", "[]"));
                        pre.put(results.getJSONObject(position).getString("abbreviation"));
                        Utils.putStringInSharedPrefs("sports_teams_abbrs", pre.toString());

                        pre = new JSONArray(Utils.getStringFromSharedPrefs("sports_teams_leagues", "[]"));
                        pre.put(results.getJSONObject(position).getString("league"));
                        Utils.putStringInSharedPrefs("sports_teams_leagues", pre.toString());

                        ((Activity)context).findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                        ((Activity)context).findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                        ListView lv = (ListView) ((Activity)context).findViewById(R.id.tickerLV);
                        lv.setAdapter(new TopicsAdapter(context));

                        // If they've just add their first ticker, create a new contact
                        if (pre.length() == 1) {
                            JSONObject profile = new JSONObject();
                            profile.put("token", "CHANNEL_SPORTS");
                            profile.put("name", "");
                            profile.put("tagline", "");
                            profile.put("photoUrl", "");
                            profile.put("username", "");
                            profile.put("online_status", "");
                            Utils.dbAdd(context, profile, "CHANNEL_SPORTS", new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {

                                }
                            });
                        }

                        if (dialog != null)
                            dialog.dismiss();
                    } catch (Exception e) {e.printStackTrace();}
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public MaterialDialog dialog;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, abbrTV, leagueTV;
        public View convertView;
        public ViewHolder(View v) {
            super(v);
            convertView = v;
        }
    }

}