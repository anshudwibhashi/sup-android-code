package com.sup.sup;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.IBinder;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.sup.sup.CustomFirebaseMessagingService.SERVICE_MESSAGE;
import static com.sup.sup.CustomFirebaseMessagingService.SERVICE_RESULT;
import static com.sup.sup.CustomFirebaseMessagingService.SERVICE_TOKEN;

public class RedditService extends Service {
    public RedditService() {
    }

    LocalBroadcastManager broadcaster;
    @Override
    public void onCreate() {
        super.onCreate();
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Log.d("", "Starting Async RedditService");
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.d("", "Ending Async RedditService");
            }

            @Override
            protected Void doInBackground(Void... params) {
                Pusher pusher = new Pusher("50ed18dd967b455393ed");
                pusher.connect();

                while(true) {
                    try {
                        JSONArray subreddits = new JSONArray(Utils.getStringFromSharedPrefs("subreddits", "[]"));
                        JSONArray subscribed_subreddits = new JSONArray(Utils.getStringFromSharedPrefs("subscribed_subreddits", "[]"));
                        for(int i = 0; i < subreddits.length(); i++) {
                            if(!contains(subscribed_subreddits, subreddits.getString(i))) {
                                // New subreddit. Subscribe!
                                Channel channel = pusher.subscribe(subreddits.getString(i));
                                channel.bind("new-listing", new SubscriptionEventListener() {
                                    @Override
                                    public void onEvent(String channelName, String eventName, final String data_str) {
                                        try {
                                            SQLiteDatabase broadcastsDBR = BroadcastsDBHelper.getHelper(RedditService.this).getReadableDatabase();
                                            JSONObject data = new JSONObject(data_str);
                                            Cursor result = broadcastsDBR.rawQuery("SELECT * FROM broadcasts WHERE id=?", new String[]{"CHANNEL_REDDIT_" + data.getString("id")});
                                            result.moveToFirst();
                                            if (result.getCount() == 0) {
                                                // add to broadcastsDB
                                                ContentValues cv = new ContentValues();

                                                String time = System.currentTimeMillis() + "";
                                                cv.put("timestamp", time);
                                                cv.put("id", "CHANNEL_REDDIT_" + data.getString("id"));
                                                cv.put("text", data_str);

                                                SQLiteDatabase broadcastsDBW = BroadcastsDBHelper.getHelper(RedditService.this).getWritableDatabase();
                                                broadcastsDBW.insert("broadcasts", null, cv);

                                                // refresh UI
                                                Intent intent = new Intent(SERVICE_RESULT);
                                                intent.putExtra(SERVICE_MESSAGE, "update_ui2");
                                                intent.putExtra(SERVICE_TOKEN, "");

                                                try {
                                                    if(Utils.getBooleanFromSharedPrefs("show_reddit_channel_notifications", true)) {
                                                        NotificationCompat.Builder mBuilder = CustomFirebaseMessagingService.buildNotification(RedditService.this, "Sup Reddit Update",
                                                                "New post: \"" + new JSONObject(data_str).getString("title") + "\"",
                                                                BitmapFactory.decodeResource(getResources(), R.drawable.ic_reddit_channel));

                                                        CustomFirebaseMessagingService._notify(mBuilder, Constants.PACKAGE_NOTIFICATION_ID, RedditService.this);
                                                    }
                                                } catch ( Exception e) {e.printStackTrace();}

                                                broadcaster.sendBroadcast(intent);
                                            }
                                        } catch (Exception e) {e.printStackTrace();}
                                    }
                                });
                            }
                        }
                        JSONArray newSubscribedSubreddits = new JSONArray();
                        for(int i = 0; i < subscribed_subreddits.length(); i++) {
                            if(!contains(subreddits, subscribed_subreddits.getString(i))) {
                                pusher.unsubscribe(subscribed_subreddits.getString(i));
                            } else {
                                newSubscribedSubreddits.put(subscribed_subreddits.getString(i));
                            }
                        }
                        Utils.putStringInSharedPrefs("subscribed_subreddits", newSubscribedSubreddits.toString());
                        Thread.sleep(10000); // Sleep for ten seconds
                    } catch (Exception e) {}
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return START_STICKY;
    }

    private boolean contains(JSONArray a, Object b) {
        try {
            for (int i = 0; i < a.length(); i++) {
                if (a.get(i).equals(b))
                    return true;
            }
        } catch (Exception e){}
        return false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
