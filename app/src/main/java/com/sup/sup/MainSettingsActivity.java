package com.sup.sup;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;

public class MainSettingsActivity extends AppCompatActivity {

    @Override
    protected void onPause() {
        super.onPause();
        Utils.updateSettingsToServer(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String[] data = new String[] {"Every hour", "Every 30 minutes", "Every 15 minutes", "Every 10 minutes", "As soon as I close Sup"};
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_single_choice, data));
        listView.setItemsCanFocus(true);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setItemChecked(Utils.getIntegerFromSharedPrefs("delete_smalltalks_frequency", 0), true);
        listView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Utils.putIntegerInSharedPrefs("delete_smalltalks_frequency", position);
            }
        });
        ((CheckBox)findViewById(R.id.toggler1)).setChecked(!Utils.getBooleanFromSharedPrefs("save_custom_texts", true));
        ((CheckBox)findViewById(R.id.toggler2)).setChecked(!Utils.getBooleanFromSharedPrefs("keep_sup_count", true));
    }

    public void deleteHistory(View view) {
        Utils.putStringInSharedPrefs("text_history", "[]");
        Utils.showToast(this, "Custom text history cleared");
    }

    public void toggleSaveCustomTexts(View view) {
        Utils.putBooleanInSharedPrefs("save_custom_texts", !Utils.getBooleanFromSharedPrefs("save_custom_texts", true));
    }

    public void toggleSupCount(View view) {
        Utils.putBooleanInSharedPrefs("keep_sup_count", !Utils.getBooleanFromSharedPrefs("keep_sup_count", true));
    }
}
