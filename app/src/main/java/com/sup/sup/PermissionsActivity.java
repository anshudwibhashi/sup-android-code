package com.sup.sup;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PermissionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        findViewById(R.id.grantButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                Utils.showLongToast(PermissionsActivity.this, "Click \"Permissions\"");
            }
        });

        List<Map<String, String>> data = new ArrayList<>();

        Map<String, String> datum = new HashMap<>(2);
        datum.put("First Line", "Take pictures and record video");
        datum.put("Second Line", "For image or video responses, attachments, and broadcasts.");
        data.add(datum);

        datum = new HashMap<>(2);
        datum.put("First Line", "Record audio");
        datum.put("Second Line", "For audio responses, attachments, and broadcasts.");
        data.add(datum);

        datum = new HashMap<>(2);
        datum.put("First Line", "Access device location");
        datum.put("Second Line", "For location responses, attachments and broadcasts.");
        data.add(datum);

        datum = new HashMap<>(2);
        datum.put("First Line", "Access photos, media and files on your device");
        datum.put("Second Line", "To save media on your device. Requested before capturing images and videos.");
        data.add(datum);

        SimpleAdapter adapter = new SimpleAdapter(this, data,
                R.layout.list_item_permissions,
                new String[] {"First Line", "Second Line" },
                new int[] {R.id.tv1, R.id.tv2 });

        ((ListView) findViewById(R.id.listView)).setEnabled(false);
        ((ListView) findViewById(R.id.listView)).setAdapter(adapter);
    }
}
