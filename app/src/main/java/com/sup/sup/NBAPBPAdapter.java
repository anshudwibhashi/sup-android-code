package com.sup.sup;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

public class NBAPBPAdapter extends BaseAdapter {
    Context context; JSONArray pbp;
    public NBAPBPAdapter(Context context, JSONObject pbp) {
        this.context = context; this.pbp = processPbP(pbp);
    }
    @Override
    public int getCount() {
        try {
            return pbp.length();
        } catch (Exception e) {e.printStackTrace(); return 0;}
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  ((Activity)context).getLayoutInflater().inflate(R.layout.list_item_nhl_plays, null); // Reusing NHL's layout
            holder.periodTV = (TextView) convertView.findViewById(R.id.periodTV);
            holder.timeTV = (TextView) convertView.findViewById(R.id.timeTV);
            holder.descTV = (TextView) convertView.findViewById(R.id.descTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            JSONObject play = pbp.getJSONObject(pbp.length() - position - 1); // for reverse
            holder.descTV.setText(play.getString("desc"));
            holder.timeTV.setText(play.getString("time"));
            holder.periodTV.setText(play.getString("period"));
        } catch (Exception e) { e.printStackTrace();}

        return convertView;
    }

    public static class ViewHolder {
        public TextView descTV, timeTV, periodTV;
    }

    private JSONArray processPbP(JSONObject pbp) {
        try {
            JSONArray majorPlays = pbp.getJSONObject("gameplaybyplay").getJSONObject("plays").getJSONArray("play");
            JSONArray plays = new JSONArray();
            for (int i = 0; i < majorPlays.length(); i++) {
                String time = majorPlays.getJSONObject(i).getString("time");
                String period = "Q"+majorPlays.getJSONObject(i).getString("quarter");
                String desc = "";
                String playType = "";
                Iterator<String> iter = majorPlays.getJSONObject(i).keys();
                while(iter.hasNext()){
                    String key = iter.next();
                    if(!key.equals("time") && !key.equals("quarter")) {
                        playType = key;
                        break;
                    }
                }

                switch (playType) {
                    case "fieldGoalAttempt":
                        desc = majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").getString("teamAbbreviation") + " · Field goal attempt by "+
                                majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").getJSONObject("shootingPlayer").getString("LastName") + " · "+
                                majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").getString("shotType") +
                                (!majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").isNull("assistingPlayer")?(" · Assisted by "+
                                        majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").getJSONObject("assistingPlayer").getString("LastName")):"")+
                                (!majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").isNull("blockingPlayer")?(" · Blocked by "+
                                        majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").getJSONObject("blockingPlayer").getString("LastName")):"") +
                                " · " +majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").getString("Points") + " points" + " · " +
                                processCase(majorPlays.getJSONObject(i).getJSONObject("fieldGoalAttempt").getString("outcome"));
                        break;
                    case "freeThrowAttempt":
                        desc = majorPlays.getJSONObject(i).getJSONObject("freeThrowAttempt").getString("teamAbbreviation") + " · Free throw attempt by "+
                                majorPlays.getJSONObject(i).getJSONObject("freeThrowAttempt").getJSONObject("shootingPlayer").getString("LastName") + " · Attempt "+
                                majorPlays.getJSONObject(i).getJSONObject("freeThrowAttempt").getString("attemptNum") + " of " +
                                majorPlays.getJSONObject(i).getJSONObject("freeThrowAttempt").getString("totalAttempts") + " · " +
                                processCase(majorPlays.getJSONObject(i).getJSONObject("freeThrowAttempt").getString("outcome"));
                        break;
                    case "rebound":
                        desc = majorPlays.getJSONObject(i).getJSONObject("rebound").getString("teamAbbreviation") +
                                (!majorPlays.getJSONObject(i).getJSONObject("rebound").isNull("retrievingPlayer")?( " · Rebound retrieved by "+
                                majorPlays.getJSONObject(i).getJSONObject("rebound").getJSONObject("retrievingPlayer").getString("LastName")):"") + " · "+
                                (!majorPlays.getJSONObject(i).getJSONObject("rebound").isNull("offensiveOrDefensive")?(
                                processCase(majorPlays.getJSONObject(i).getJSONObject("rebound").getString("offensiveOrDefensive"))+"ensive"):"");
                        break;
                    case "turnover":
                        desc = majorPlays.getJSONObject(i).getJSONObject("turnover").getString("teamAbbreviation") + " · Turnover · Ball lost by "+
                                majorPlays.getJSONObject(i).getJSONObject("turnover").getJSONObject("lostByPlayer").getString("LastName") + " · "+
                                majorPlays.getJSONObject(i).getJSONObject("turnover").getString("turnoverType") +
                                (Boolean.valueOf(majorPlays.getJSONObject(i).getJSONObject("turnover").getString("isStolen"))?" · Stolen by " +
                                        majorPlays.getJSONObject(i).getJSONObject("turnover").getJSONObject("stolenByPlayer").getString("LastName")
                                :"");
                        break;
                    case "foul":
                        desc = majorPlays.getJSONObject(i).getJSONObject("foul").getString("teamAbbreviation") + " · Foul by "+
                                majorPlays.getJSONObject(i).getJSONObject("foul").getJSONObject("penalizedPlayer").getString("LastName") + " · "+
                                majorPlays.getJSONObject(i).getJSONObject("foul").getString("foulType");
                        break;
                    case "violation":
                        desc = majorPlays.getJSONObject(i).getJSONObject("violation").getString("teamAbbreviation") + " · Violation"+
                                (majorPlays.getJSONObject(i).getJSONObject("violation").getString("playerOrTeam").equals("PLAYER")?" by " +
                                        majorPlays.getJSONObject(i).getJSONObject("violation").getJSONObject("violatingPlayer").getString("LastName")
                                        :"")+" · "+majorPlays.getJSONObject(i).getJSONObject("violation").getString("violationType");
                        break;
                    case "jumpBall":
                        desc = "Jump ball between "+majorPlays.getJSONObject(i).getJSONObject("jumpBall").getJSONObject("homePlayer").getString("LastName")+" (home) and "+
                                majorPlays.getJSONObject(i).getJSONObject("jumpBall").getJSONObject("awayPlayer").getString("LastName")+" (away) · Won by "+
                                processCase(majorPlays.getJSONObject(i).getJSONObject("jumpBall").getString("wonBy"))+
                                (!majorPlays.getJSONObject(i).getJSONObject("jumpBall").isNull("tippedToPlayer")?(" · Tipped to "+
                                        majorPlays.getJSONObject(i).getJSONObject("jumpBall").getJSONObject("tippedToPlayer").getString("LastName")):"");
                        break;
                    case "substitution":
                        desc = "Substitution · "+majorPlays.getJSONObject(i).getJSONObject("substitution").getString("teamAbbreviation")+
                                (!majorPlays.getJSONObject(i).getJSONObject("substitution").isNull("incomingPlayer")?(" · Incoming player: "+
                                        majorPlays.getJSONObject(i).getJSONObject("substitution").getJSONObject("incomingPlayer").getString("LastName")):"")+
                                (!majorPlays.getJSONObject(i).getJSONObject("substitution").isNull("outgoingPlayer")?(" · Outgoing player "+
                                        majorPlays.getJSONObject(i).getJSONObject("substitution").getJSONObject("outgoingPlayer").getString("LastName")):"");
                        break;
                    default:
                        desc = playType;
                }

                JSONObject play = new JSONObject();
                play.put("time", time);
                play.put("period", period);
                play.put("desc", desc);
                plays.put(play);

            }
            return plays;
        } catch (Exception e) {Log.d("Here", "ded");e.printStackTrace(); return null;}
    }

    private String processCase(String s) {
        s = s.replace("_", " ");
        final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
        // to be capitalized

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray()) {
            c = (capNext)
                    ? Character.toUpperCase(c)
                    : Character.toLowerCase(c);
            sb.append(c);
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0); // explicit cast not needed
        }
        return sb.toString();
    }
}
