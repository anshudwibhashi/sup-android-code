package com.sup.sup;

import java.util.ArrayList;

/**
 * Created by anshudwibhashi on 03/06/17.
 */

public class Comments {
    public ArrayList<Commenter> users;
    public Comments(){
        users = new ArrayList<>();
    }

    public static class Commenter {
        public String name, username, comment, timeStamp, photoUrl, token;
        public Commenter(){
            name = "";
            username = "";
            comment= "";
            timeStamp = "";
            photoUrl = "";
            token = "";
        }
    }
}
