package com.sup.sup;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.tomash.androidcontacts.contactgetter.entity.ContactData;
import com.tomash.androidcontacts.contactgetter.main.FieldType;
import com.tomash.androidcontacts.contactgetter.main.contactsGetter.ContactsGetterBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class HomeActivity extends AppCompatActivity{

    // START: functions and variables to periodically delete smalltalk messsages older than an hour
    private int mInterval = 60*1000; // every 10 seconds
    private Handler mHandler;

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();

        // Clear sports info, bc it won't be up to date when they open the app again
        ContentValues cv = new ContentValues();
        cv.put("response", "{}");
        ConnectionsDBHelper.getHelper(this).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_SPORTS"});
        Utils.populateMainDataSet(this);
    }

    Runnable periodicSweeper = new Runnable() {
        @Override
        public void run() {
            // Delete Smalltalk messages older than an hour (in a background thread)
            // Also delete broadcasts older than 24 hours

            SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(HomeActivity.this).getReadableDatabase();
            SQLiteDatabase dbW = ConnectionsDBHelper.getHelper(HomeActivity.this).getWritableDatabase();

            // Delete smalltalks
            deleteSmalltalks(dbR, dbW, false);

            Utils.populateMainDataSet(HomeActivity.this);

            if(activeAdapter!= null) {
                activeAdapter.clear();
                activeMessageList.getRecycledViewPool().clear();
                activeAdapter.notifyDataSetChanged();
                Utils.refreshChatAdapter(activeAdapter, Utils.MainDataSet.smallTalkMessages.get(activePosition));
            }

            // Delete old broadcasts:
            dbR = BroadcastsDBHelper.getHelper(HomeActivity.this).getReadableDatabase();
            dbW = BroadcastsDBHelper.getHelper(HomeActivity.this).getWritableDatabase();
            Cursor results = dbR.rawQuery("SELECT * FROM broadcasts", null);
            for(results.moveToFirst(); !results.isAfterLast(); results.moveToNext()) {
                String id = results.getString(results.getColumnIndexOrThrow("id"));
                String media = results.getString(results.getColumnIndexOrThrow("media"));
                Long created = new Date(Long.parseLong(results.getString(results.getColumnIndexOrThrow("timestamp")))).getTime();
                if(created < (System.currentTimeMillis()-87000*1000)) {
                    // if the broadcast is older than a day + 600 seconds grace period
                    dbW.delete(
                            "broadcasts",
                            "id=?",
                            new String[]{id});

                    if(id.startsWith("CHANNEL_NEWS_")) {
                        // if it's a news broadcast, also delete it from the newsarticlesdb
                        try {
                            ArrayList<String> names = Utils.JSONArrayToArrayList(new JSONArray(Utils.getStringFromSharedPrefs("newsArticlesPossessed", "[]")));
                            names.remove(new JSONObject(results.getString(results.getColumnIndexOrThrow("text"))).getString("title"));
                            Utils.putStringInSharedPrefs("newsArticlesPossessed", Utils.ArrayListToJSONArray(names).toString());
                        } catch (Exception e) {e.printStackTrace();}
                    }

                    // now delete any associated media
                    if(media != null && !media.isEmpty()) {
                        // firstly delete from GCS (If it errs, that means some other person already deleted)
                        Utils.deleteFromFCS(media, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                // don't care
                            }
                        });

                        // now delete from device
                        if (media.endsWith(".png")) {
                            new File(HomeActivity.this.getFilesDir(), media).delete();
                        } else if (media.endsWith(".mp4")) {
                            new File(HomeActivity.this.getFilesDir(), media).delete();
                            new File(HomeActivity.this.getFilesDir(), "snapshot_" + media.replace(".mp4", ".png")).delete();
                        } else if (media.endsWith(".3gpp")) {
                            new File(HomeActivity.this.getFilesDir(), media).delete();
                        } else {
                            // location
                            new File(HomeActivity.this.getFilesDir(), media + ".png").delete();
                        }
                    }
                }
            }
            Utils.populateFeedDataSet(HomeActivity.this);
            ListView feed = (ListView) HomeActivity.this.findViewById(R.id.mainLV2);
            if(feed != null) {
                ((BaseAdapter)feed.getAdapter()).notifyDataSetChanged();
            }

            mHandler.postDelayed(periodicSweeper, mInterval);
        }
    };

    private void deleteSmalltalks(SQLiteDatabase dbR, SQLiteDatabase dbW, boolean exiting) {
        // Delete smalltalks older than an hour:
        Cursor results = dbR.rawQuery("SELECT * FROM connections", null);
        for(results.moveToFirst(); !results.isAfterLast(); results.moveToNext()) {
            String token = results.getString(results.getColumnIndexOrThrow("token"));
            Smalltalk.SmalltalkMessage smalltalkMessage = (Smalltalk.SmalltalkMessage) Utils.deserialize(results.getString(results.getColumnIndexOrThrow("smalltalk")), Smalltalk.SmalltalkMessage.class);

            int delete_window = 60*60*1000; // default 1 hour
            switch (Utils.getIntegerFromSharedPrefs("delete_smalltalks_frequency", 0)) {
                case 0:
                    delete_window = 60*60*1000; // default 1 hour
                    break;
                case 1:
                    delete_window = 30*60*1000; // 30 mins
                    break;
                case 2:
                    delete_window = 15*60*1000; // 15 mins
                    break;
                case 3:
                    delete_window = 10*60*1000; // 10 mins
                    break;
                case 4:
                    delete_window = 1000*1000*1000; // don't delete it here. delete in onDestroy()
            }

            if (exiting)
                delete_window = 0; // delete on exit

            for (Iterator<Smalltalk.MyMessage> iterator = smalltalkMessage.messages.getMessages().iterator(); iterator.hasNext(); ) {
                Smalltalk.MyMessage message = iterator.next();
                if(message.getCreatedAt().getTime() < (System.currentTimeMillis()-delete_window)) {
                    // This message was created more than an hour ago (or however long the user wants to delete sups after).
                    String type = message.getType();
                    if(!type.equals("text")) {
                        // delete any associated media
                        if (type.startsWith("image") || type.startsWith("video") || type.startsWith("audio")) {
                            new File(HomeActivity.this.getFilesDir(), message.getText()).delete();
                            // also delete fcm. (This will throw an error if not a group smalltalk, but non fatal, so can ignore
                            Utils.deleteFromFCS(message.getText(), new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    // don't care
                                }
                            });
                            // If it's a video, there's an additional snapshot image file to delete
                            if (type.startsWith("video")) {
                                new File(HomeActivity.this.getFilesDir(), "snapshot_" + message.getText().replace(".mp4", ".png")).delete();
                            }
                        } else if (type.startsWith("location")) {
                            new File(HomeActivity.this.getFilesDir(), message.getText() + ".png").delete();
                        }
                    }
                    iterator.remove();
                }
            }

            ContentValues values = new ContentValues();
            values.put("smalltalk", Utils.serialize(smalltalkMessage));
            if(smalltalkMessage.messages.getMessages().size() == 0) {
                values.put("microtext_read", 1);
            }
            dbW.update(
                    "connections",
                    values,
                    "token=?",
                    new String[]{token});
        }
    }

    void startRepeatingTask() {
        periodicSweeper.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(periodicSweeper);
    }
    // END

    @Override
    protected void onResume() {
        super.onResume();

        String url = "http://black-abode-2709.appspot.com/update_online_status?token=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", "")) + "&online_status=0";
        Utils.volleyStringCall(this, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {

            }
        });
        url = "http://black-abode-2709.appspot.com/update_profile?token=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", "")) + "&online_status=0";
        Utils.volleyStringCall(this, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {

            }
        });

        if(Utils.getBooleanFromSharedPrefs("kill", false)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("You've been logged out")
                    .setMessage("Since you logged in from another device, you'll now be logged out of this one.")
                    .setNeutralButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            logout(HomeActivity.this);
                        }
                    })
                    .setCancelable(false)
                    .show();
        }

        if (((ListView) findViewById(R.id.mainLV)) != null) {
            ((BaseAdapter) ((ListView) findViewById(R.id.mainLV)).getAdapter()).notifyDataSetInvalidated();
            ((BaseAdapter) ((ListView) findViewById(R.id.mainLV)).getAdapter()).notifyDataSetChanged();
        }
    }

    public LinearLayout activeLL = null;

    @Override
    protected void onPause() {
        super.onPause();
        String status;
        Utils.putStringInSharedPrefs("smalltalk_open", "");
        if (Utils.getBooleanFromSharedPrefs("last_seen", true)) {
            status = ""+System.currentTimeMillis();
        } else {
            status = "-1";
        }
        String url = "http://black-abode-2709.appspot.com/update_online_status?token="+ URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", ""))+"&online_status="+status;
        Utils.volleyStringCall(this, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {

            }
        });
        url = "http://black-abode-2709.appspot.com/update_profile?token="+ URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", ""))+"&online_status="+status;
        Utils.volleyStringCall(this, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {

            }
        });
        if (Utils.getBooleanFromSharedPrefs("update_weather_last_seen", false)) {
            ContentValues cv = new ContentValues();
            cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
            ConnectionsDBHelper.getHelper(this).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_WEATHER"});
            Utils.putBooleanInSharedPrefs("update_weather_last_seen", false);
        }
        if (Utils.getBooleanFromSharedPrefs("update_stocks_last_seen", false)) {
            ContentValues cv = new ContentValues();
            cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
            ConnectionsDBHelper.getHelper(this).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_STOCKS"});
            Utils.putBooleanInSharedPrefs("update_stocks_last_seen", false);
        }
        if (Utils.getBooleanFromSharedPrefs("update_flights_last_seen", false)) {
            ContentValues cv = new ContentValues();
            cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
            ConnectionsDBHelper.getHelper(this).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_FLIGHTS"});
            Utils.putBooleanInSharedPrefs("update_flights_last_seen", false);
        }
    }

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public MessagesListAdapter<Smalltalk.MyMessage> activeAdapter = null;
    public MessagesList activeMessageList = null;
    public int activePosition = 0;
    public TextView activeNameTV=null, activeOnlineStatusTV= null, activeHeaderTV=null;
    public ImageView activePhotoIV=null;
    public ListView activeLikesLV=null, activeCommentsLV=null;
    int activeLikesPosition=-1, activeCommentsPosition=-1;

    Bundle extra;
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Start periodically downloading news articles
        if (!Utils.getBooleanFromSharedPrefs("startedNewsFetching", false)) {
            NewsArticlesJob.scheduleJob();
            Utils.putBooleanInSharedPrefs("startedNewsFetching", true);
        } else {
        }

        RedditJob.scheduleJob();

        if (extra != null && extra.getString("delete_broadcast_notification") != null) {
            CustomFirebaseMessagingService.broadcastNotificationBuilders.remove(extra.getString("delete_broadcast_notification"));
            CustomFirebaseMessagingService.broadcastNotificationIds.remove(extra.getString("delete_broadcast_notification"));

            // Take user to the liked post
            mViewPager.setCurrentItem(1);
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
                    int position;
                    for(position = 0; !Utils.FeedDataSet.ids.get(position).equals(extra.getString("delete_broadcast_notification")); position++);
                    ((ListView)findViewById(R.id.mainLV2)).smoothScrollToPosition(position);
                    mViewPager.removeOnPageChangeListener(this);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Utils.initChannels(this);

        if (getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            addContact();
        }

        Utils.mSharedPreferences = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        mHandler = new Handler();
        startRepeatingTask();

        Utils.populateFeedDataSet(HomeActivity.this);

        Utils.populateMainDataSet(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                animateFab(tab.getPosition(), fab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        if (getIntent().getBooleanExtra("show_feed", false)) {
            tabLayout.getTabAt(1).select();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(tabLayout.getSelectedTabPosition()){
                    case 0:
                        addContact();
                        break;
                    case 1:
                        broadcast();
                        break;
                }
            }
        });

        // START: Activated through a notification
        extra = getIntent().getBundleExtra("extra");
        if (extra != null && extra.getString("delete_sup_notification") != null) {
            CustomFirebaseMessagingService.supNotificationsBuilders.remove(extra.getString("delete_sup_notification"));
            CustomFirebaseMessagingService.supNotificationsIds.remove(extra.getString("delete_sup_notification"));
        }
        if (extra != null && extra.getString("delete_response_notification") != null) {
            CustomFirebaseMessagingService.responseNotificationsBuilders.remove(extra.getString("delete_response_notification"));
            CustomFirebaseMessagingService.responseNotificationsIds.remove(extra.getString("delete_response_notification"));
        }
        if (extra != null && extra.getString("delete_microtext_notification") != null) {
            CustomFirebaseMessagingService.smalltalkNotificationBuilders.remove(extra.getString("delete_microtext_notification"));
            CustomFirebaseMessagingService.smalltalkNotificationsIds.remove(extra.getString("delete_microtext_notification"));
        }
        // END

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s = intent.getStringExtra(CustomFirebaseMessagingService.SERVICE_MESSAGE);
                switch (s) {
                    case "update_ui":
                        Utils.populateMainDataSet(HomeActivity.this);
                        if (((ListView) findViewById(R.id.mainLV)).getAdapter() instanceof HomeAdapter) {
                            final HomeAdapter adapter = new HomeAdapter(HomeActivity.this);
                            ((ListView) findViewById(R.id.mainLV)).setAdapter(adapter);
                            ((BaseAdapter) ((ListView) findViewById(R.id.mainLV)).getAdapter()).notifyDataSetInvalidated();
                            ((BaseAdapter) ((ListView) findViewById(R.id.mainLV)).getAdapter()).notifyDataSetChanged();
                        } else {
                            final ListView view = ((ListView) findViewById(R.id.mainLV));
                            final HomeAdapter adapter = new HomeAdapter(HomeActivity.this);
                            view.setAdapter(adapter);
                            view.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                                @Override
                                public boolean onItemLongClick(AdapterView<?> parent, View view2, final int position, long id) {
                                    if(Utils.MainDataSet.names.isEmpty()) {
                                        return false;
                                    }
                                    if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
                                        if (Utils.getGroupOwner(Utils.MainDataSet.tokens.get(position)).equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
                                            File photo = new File(Utils.MainDataSet.photos.get(position));
                                            Utils.deleteGroupFromServer(HomeActivity.this, Utils.MainDataSet.tokens.get(position), null, photo);
                                        } else {
                                            File photo = new File(Utils.MainDataSet.photos.get(position));
                                            Utils.leaveGroup(HomeActivity.this, Utils.MainDataSet.tokens.get(position), null, photo);
                                        }
                                    } else {
                                        Utils.deleteContact(HomeActivity.this, Utils.MainDataSet.tokens.get(position), adapter, view);
                                    }
                                    return true;
                                }

                            });

                            view.setOnItemClickListener(new ContactScreenClickListener(HomeActivity.this));
                        }
                        break;
                    case "update_ui2":
                        // token is actually the ID
                        String token = intent.getStringExtra(CustomFirebaseMessagingService.SERVICE_TOKEN);
                        Utils.populateFeedDataSet(HomeActivity.this);
                        if (((ListView) findViewById(R.id.mainLV2)).getAdapter() instanceof FeedAdapter) {
                            ((BaseAdapter) ((ListView) findViewById(R.id.mainLV2)).getAdapter()).notifyDataSetChanged();

                            if(!token.isEmpty()) {
                                if (activeLikesLV != null && token.equals(Utils.FeedDataSet.ids.get(activeLikesPosition))) {
                                    if (Utils.FeedDataSet.likes.get(activeLikesPosition).users.size() == 0) {
                                        EmptyAdapter adapter = new EmptyAdapter(HomeActivity.this);
                                        adapter.comments = true;
                                        adapter.title = "Nothing to see here!";
                                        adapter.message = "Nobody has liked this broadcast yet.";
                                        activeLikesLV.setAdapter(adapter);
                                    } else {
                                        activeLikesLV.setAdapter(new LikesAdapter(HomeActivity.this, Utils.FeedDataSet.likes.get(activeLikesPosition)));
                                    }
                                    activeHeaderTV.setText("People who like this · " + Utils.FeedDataSet.likes.get(activeLikesPosition).users.size());
                                }
                                if (activeCommentsLV != null && token.equals(Utils.FeedDataSet.ids.get(activeCommentsPosition))) {
                                    activeCommentsLV.setAdapter(new CommentsAdapter(HomeActivity.this, Utils.FeedDataSet.comments.get(activeCommentsPosition)));
                                    activeHeaderTV.setText("Comments · " + Utils.FeedDataSet.comments.get(activeCommentsPosition).users.size());
                                }
                            }
                        } else {
                            final ListView view = ((ListView) findViewById(R.id.mainLV2));
                            final FeedAdapter adapter = new FeedAdapter(HomeActivity.this);
                            view.setAdapter(adapter);
                        }
                        break;
                    case "update_ui3":
                        token = intent.getStringExtra(CustomFirebaseMessagingService.SERVICE_TOKEN);
                        Utils.populateMainDataSet(HomeActivity.this);
                        if(HomeActivity.this.activeAdapter != null && token.equals(Utils.MainDataSet.tokens.get(HomeActivity.this.activePosition))) {
                            activeAdapter.clear();
                            activeMessageList.getRecycledViewPool().clear();
                            activeAdapter.notifyDataSetChanged();
                            Utils.refreshChatAdapter(activeAdapter, Utils.MainDataSet.smallTalkMessages.get(activePosition));

                            activeNameTV.setText(Utils.MainDataSet.names.get(activePosition));
                            Glide.with(context).load(Utils.MainDataSet.photos.get(activePosition)).centerCrop()
                                    .override(100, 100)
                                    .transform(new CircleTransform(context))
                                    .placeholder(R.drawable.ic_default_picture).into(activePhotoIV);

                            String online_status = Utils.MainDataSet.online_statuses.get(activePosition);
                            switch (online_status) {
                                case "0":
                                    activeOnlineStatusTV.setText("Online");
                                    break;
                                case "-1":
                                    activeOnlineStatusTV.setText("Offline");
                                    break;
                                default:
                                    activeOnlineStatusTV.setText("Last seen at " + SimpleDateFormat.getTimeInstance(DateFormat.SHORT).format(new Date(Long.parseLong(online_status))));
                            }
                        }
                        if((((ListView) findViewById(R.id.mainLV)) != null))
                            ((BaseAdapter) ((ListView) findViewById(R.id.mainLV)).getAdapter()).notifyDataSetChanged(); // for tint

                        break;
                }
            }
        };

        if(Utils.getStringFromSharedPrefs("self_phone_number", "0").equals("0") &&
                !Utils.getBooleanFromSharedPrefs("asked_for_number",false)) {
            Utils.confirmDialog(this, "Verify Phone?", "This will allow your friends to find you on Sup.", new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    if((Boolean) result) {
                        startActivity(new Intent(HomeActivity.this, VerifyPhoneActivity.class));
                    }

                    Utils.putBooleanInSharedPrefs("asked_for_number", true);
                    // Tutorial: First welcome them with a dialog box
                    if(!Utils.getBooleanFromSharedPrefs("tutorial1_shown", false)) {
                        Utils.confirmDialog2(HomeActivity.this, "Welcome aboard!",
                                "Sup is a lot more fun with friends! Click the button on the bottom right to see which of your contacts is on Sup, or invite them!",
                                new VolleyCallback() {
                                    @Override
                                    public void onResponse(Object result) {
                                        // Now that they've seen this, tell 'em about broadcasts
                                        Snackbar.make(findViewById(R.id.main_content), "Swipe right to broadcast your status to your feed!", Snackbar.LENGTH_LONG).show();
                                        Utils.putBooleanInSharedPrefs("tutorial1_shown", true);
                                    }
                                });
                    }
                }
            });
        }
    }

    private String caption = "";

    public void getCaption(final JSONObject broadcast) {
        // Show caption bottom sheet
        LinearLayout ll = (LinearLayout) HomeActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet_caption, null);
        Button noCaption = (Button) ll.findViewById(R.id.noButton);
        EditText capET = (EditText) ll.findViewById(R.id.capET);

        final Dialog mBottomSheetDialog = new Dialog (HomeActivity.this,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (ll);
        mBottomSheetDialog.setCancelable (false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);

        capET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    try {
                        caption = v.getText().toString();
                        doneWithCaption = true;
                        mBottomSheetDialog.dismiss();
                        if (!broadcast.getString("type").equals("text_broadcast")) {
                            if (caption != null && !caption.trim().isEmpty()) {
                                broadcast.put("text", caption);
                                caption = "";
                            }
                            Utils.sendMediaBroadcast(broadcast, HomeActivity.this);
                            Utils.sendBroadcast(HomeActivity.this, broadcast);
                        }
                    } catch (Exception e) {e.printStackTrace();}
                }
                return false;
            }
        });

        noCaption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Send without caption
                try {
                    doneWithCaption = true;
                    mBottomSheetDialog.dismiss();
                    if (!broadcast.getString("type").equals("text_broadcast")) {
                        if (caption != null && !caption.trim().isEmpty()) {
                            broadcast.put("text", caption);
                            caption = "";
                        }
                        Utils.sendMediaBroadcast(broadcast, HomeActivity.this);
                        Utils.sendBroadcast(HomeActivity.this, broadcast);
                    }
                } catch (Exception e) {e.printStackTrace();}
            }
        });

        mBottomSheetDialog.show ();
    }

    private boolean doneWithCaption = false;

    private void broadcast() {
        if (!Utils.isNetworkAvailable(this)) {
            return;
        }
        Utils.obtainResponseFromUser(this, "your followers", "Broadcast", new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                if (!Utils.isNetworkAvailable(HomeActivity.this)) {
                    Utils.showToast(HomeActivity.this, "No internet connection");
                    return;
                }

                try {
                    final JSONObject broadcast = (JSONObject) result;
                    if (!broadcast.getString("type").equals("text_broadcast")) {
                        if (caption != null && !caption.trim().isEmpty()) {
                            broadcast.put("text", caption);
                            caption = "";
                        }
                    } else {
                        Utils.sendTextBroadcast(broadcast, HomeActivity.this);
                        Utils.sendBroadcast(HomeActivity.this, broadcast);
                    }
                } catch (Exception e) {e.printStackTrace();}
            }
        }, null);
    }

    private void loadAddressBook(View view) {
        List <ContactData> cd = new ContactsGetterBuilder(this)
                .onlyWithPhones()
                .addField(FieldType.NAME_DATA)
                .buildList();

        // Contact data protocol:
        // [{'name':'', 'phone':'', 'onsup':0, 'pic':'', 'username':''}, {...}, {...}, ...]

        final JSONArray contactsData = new JSONArray();

        for (int i = 0; i < cd.size(); i++) {
            JSONObject contactData = new JSONObject();
            if((cd.get(i).getNameData().getFirstName()+cd.get(i).getNameData().getSurname()).trim().isEmpty())
                continue; // This person doesn't even have a name on their phone
            String phone = Utils.standardisePhone(this, cd.get(i).getPhoneList().get(0).getMainData());
            if (phone.isEmpty())
                continue; // invalid phone
            try {
                contactData.put("name", cd.get(i).getCompositeName());
                contactData.put("phone", phone);
                contactData.put("onsup", 0);
                contactData.put("pic", "");
                contactData.put("username", "");
            } catch (Exception e) {e.printStackTrace();}
            contactsData.put(contactData);
        }

        //Log.d("Contacts data", contactsData.toString());
        // Result: [{"name":"Aakash","phone":"+108041169163","onsup":0,"pic":"","username":""},{"name":"Aashutosh","phone":"+917795826126","onsup":0,"pic":"","username":""},{"name":"Aayush Sahay","phone":"+919845198710","onsup":0,"pic":"","username":""},{"name":"Aayush Shah","phone":"+19741138970","onsup":0,"pic":"","username":""},{"name":"Abhilash","phone":"+919980554671","onsup":0,"pic":"","username":""},{"name":"Abirami","phone":"+919535876340","onsup":0,"pic":"","username":""},{"name":"Adhit 9","phone":"+918296606230","onsup":0,"pic":"","username":""},{"name":"Aditya Mehrotra","phone":"+918041528953","onsup":0,"pic":"","username":""},{"name":"Adwait Nair","phone":"+919633306259","onsup":0,"pic":"","username":""},{"name":"Agni","phone":"+19663692033","onsup":0,"pic":"","username":""},{"name":"Akash Reshan","phone":"+19972097231","onsup":0,"pic":"","username":""},{"name":"Akhilesh Budhiraja","phone":"+19818867127","onsup":0,"pic":"","username":""},{"name":"Alex Bendeck","phone":"+15618662383","onsup":0,"pic":"","username":""},{"name":"Ali Kristine Thursland","phone":"+15162440135","onsup":0,"pic":"","username":""},{"name":"Alok","phone":"+918884644302","onsup":0,"pic":"","username":""},{"name":"Ameya #Vietnam","phone":"+109930967221","onsup":0,"pic":"","username":""},{"name":"Ananya Tripathi","phone":"+918765752233","onsup":0,"pic":"","username":""},{"name":"Anirudh Kaushik ILMUNC AD","phone":"+19884983595","onsup":0,"pic":"","username":""},{"name":"Anusha Gupta","phone":"+918041259852","onsup":0,"pic":"","username":""},{"name":"Anusha Gupta","phone":"+919148170854","onsup":0,"pic":"","username":""},{"name":"Arati","phone":"+919886497977","onsup":0,"pic":"","username":""},{"name":"Arin Gerald","phone":"+19036092170","onsup":0,"pic":"","username":""},{"name":"Arjun Govind","phone":"+919901399867","onsup":0,"pic":"","username":""},{"name":"Arune Chellaram","phone":"+919886888251","onsup":0,"pic":"","username":""},{"name":"Ashankh","phone":"+919741851448","onsup":0,"pic":"","username":""},{"name":"Ashwin Shanbhag","phone":"+919535567600","onsup":0,"pic":"","username":""},{"name":"Bashir","phone":"+919901647309","onsup":0,"pic":"","username":""},{"name":"Bharath Guru","phone":"+18971964590","onsup":0,"pic":"","username":""},{"name":"Bharath Raikar","phone":"+919972610543","onsup":0,"pic":"","username":""},{"name":"Bilal Suleman","phone":"+919167692209","onsup":0,"pic":"","username":""},{"name":"Chandana Sathish","phone":"+18792898864","onsup":0,"pic":"","username":""},{"name":"Chiranth J","phone":"+919902034010","onsup":0,"pic":"","username":""},{"name":"Dad","phone":"+19900177990","onsup":0,"pic":"","username":""},{"name":"David Rein","phone":"+18178000794","onsup":0,"pic":"","username":""},{"name":"Deeptanshu","phone":"+917899704969","onsup":0,"pic":"","username":""},{"name":"Dhruv","phone":"+919591585796","onsup":0,"pic":"","username":""},{"name":"Dhruv Jatti","phone":"+917760161751","onsup":0,"pic":"","username":""},{"name":"Distress Number","phone":"+1112","onsup":0,"pic":"","username":""},{"name":"Dr Anjali Shetty","phone":"+19886034333","onsup":0,"pic":"","username":""},{"name":"Driving School Venkatesh","phone":"+17349110715","onsup":0,"pic":"","username":""},{"name":"Ekta Chawan","phone":"+19036531490","onsup":0,"pic":"","username":""},{"name":"Elizabeth Bueti","phone":"+16026809348","onsup":0,"pic":"","username":""},{"name":"Eric Chang","phone":"+15184950712","onsup":0,"pic":"","username":""},{"name":"Faiz Sait","phone":"+19920825649","onsup":0,"pic":"","username":""},{"name":"Germany","phone":"+19945380172","onsup":0,"pic":"","username":""},{"name":"Goutham (Gym)","phone":"+919731433091","onsup":0,"pic":"","username":""},{"name":"Grandma","phone":"+919071320641","onsup":0,"pic":"","username":""},{"name":"Grandpa","phone":"+918088539097","onsup":0,"pic":"","username":""},{"name":"Hans","phone":"+19148580612","onsup":0,"pic":"","username":""},{"name":"Harry Han","phone":"+15712879766","onsup":0,"pic":"","username":""},{"name":"Hema Malini Ma'am","phone":"+918971237584","onsup":0,"pic":"","

        final ListView lv = (ListView) view.findViewById(R.id.resultsLV);
        final ProgressBar pb = (ProgressBar) view.findViewById(R.id.searchProgress);
        lv.setVisibility(View.VISIBLE);
        view.findViewById(R.id.contactsPayload).setVisibility(View.GONE);

        if (checkAddressBookCache()) {
            try {
                JSONArray contactsDataSynced = new JSONArray(Utils.getStringFromSharedPrefs("address_book_cache", "[]"));
                lv.setAdapter(new AddressBookAdapter(HomeActivity.this, contactsDataSynced));
            } catch (Exception e) {e.printStackTrace();}
        }

        if(Utils.isNetworkAvailable(this)) {
            // There's internet, download data from the server

            new AsyncTask<Void, Void, Void>(){
                private JSONArray result;
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        result = new JSONArray(Utils.syncAddressBook(contactsData.toString()));
                        Utils.putStringInSharedPrefs("address_book_cache", Utils.syncAddressBook(contactsData.toString()));
                    } catch (Exception e) {e.printStackTrace();}
                    return null;
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    pb.setVisibility(View.VISIBLE);
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    lv.setAdapter(new AddressBookAdapter(HomeActivity.this, result));
                    pb.setVisibility(View.INVISIBLE);
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // Nothing to do
            Utils.showToast(this, "Can't sync contacts without internet");
            // Show list pretending no one's on sup

            lv.setAdapter(new AddressBookAdapter(HomeActivity.this, contactsData));
        }
    }

    private boolean checkAddressBookCache() {
        // Returns false if cache is emp
        return !Utils.getStringFromSharedPrefs("address_book_cache", "[]").equals("[]");
    }

    public void allowReadContacts(View view) {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.READ_CONTACTS},
                Constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        contactsBottomSheet = view.getRootView();
    }
    private View contactsBottomSheet;

    private void addContact(){
        // Add contact by username
        final View view = getLayoutInflater ().inflate (R.layout.bottom_sheet_find_friends, null);
        final ProgressBar pb = (ProgressBar) view.findViewById(R.id.searchProgress);
        final EditText et = (EditText) view.findViewById(R.id.qET);

        final Dialog mBottomSheetDialog = new Dialog (this,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
        mBottomSheetDialog.show ();

        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {
            loadAddressBook(view);
        }

        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if(!Utils.isNetworkAvailable(HomeActivity.this)){
                        Utils.showToast(HomeActivity.this, "No internet connection");
                        return false;
                    }

                    String q = et.getText().toString().trim();

                    if(q == null || q.isEmpty()) {
                        return false;
                    }

                    pb.setVisibility(View.VISIBLE);

                    Utils.volleyJSONCall2(HomeActivity.this, "http://black-abode-2709.appspot.com/search?q=" + URLEncoder.encode(q.toLowerCase()), new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            try {
                                ((TextView)mBottomSheetDialog.findViewById(R.id.tvAboveLVInResults)).setText("Results:");
                                JSONArray results = (JSONArray) result;
                                SearchAdapter adapter = new SearchAdapter(HomeActivity.this, results, mBottomSheetDialog);

                                ListView lv = (ListView) view.findViewById(R.id.resultsLV);
                                lv.setVisibility(View.VISIBLE);
                                mBottomSheetDialog.findViewById(R.id.contactsPayload).setVisibility(View.GONE);
                                lv.setAdapter(adapter);

                                if(results.length() == 0){
                                    Utils.showToast(HomeActivity.this, "No results found");
                                }

                                pb.setVisibility(View.INVISIBLE);

                            }catch (Exception e){e.printStackTrace();}
                        }
                    });

                    return true;
                }
                return false;
            }
        });
    }

    private void addContactToDb(String username) {
        if (username.equals(Utils.getStringFromSharedPrefs("self_username", ""))) {
            Utils.showToast(this, "Can't add yourself");
        } else {
            // Follow this person, if not already following

            final ProgressDialog pd = new ProgressDialog(this);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.setMessage("Please wait...");
            pd.setIndeterminate(true);
            pd.show();

            Utils.downloadProfileByUsername(this, username, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    try {
                        final JSONObject profile = (JSONObject) result;
                        if (profile.length() == 0) {
                            Utils.showToast(HomeActivity.this, "User doesn't exist");
                            pd.dismiss();
                        } else {
                            if (!Utils.getStringFromSharedPrefs("self_following", "[]").contains(profile.getString("token"))) {
                                Utils.follow(HomeActivity.this, profile, profile.getString("token"), true, new VolleyCallback() {
                                    @Override
                                    public void onResponse(Object result) {
                                        try {
                                            Utils.showToast(HomeActivity.this, "Added " + profile.getString("name"));
                                            Utils.populateMainDataSet(HomeActivity.this);
                                            pd.dismiss();

                                            // Now update UI
                                            ListView lv = (ListView) findViewById(R.id.mainLV);
                                            BaseAdapter adapter = ((BaseAdapter)(lv).getAdapter());
                                            if (adapter instanceof  HomeAdapter) {
                                                adapter.notifyDataSetChanged();
                                            } else {
                                                lv.setAdapter(new HomeAdapter(HomeActivity.this));
                                            }
                                        } catch (Exception e) {
                                        }
                                    }
                                });
                            } else {
                                Utils.showToast(HomeActivity.this, "Already added");
                                pd.dismiss();
                            }
                        }
                    } catch (Exception e) {}
                }
            });
        }
    }


    // Method to animate changing of the FAB when switching between tabs
    protected void animateFab(final int position, final FloatingActionButton fab) {
        final int iconIntArray[] = {R.mipmap.ic_person_add_white, R.mipmap.ic_broadcast_light};

        fab.clearAnimation();
        // Scale down animation
        ScaleAnimation shrink =  new ScaleAnimation(1f, 0.2f, 1f, 0.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(150);     // animation duration in milliseconds
        shrink.setInterpolator(new DecelerateInterpolator());
        shrink.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // Change FAB icon
                fab.setImageBitmap(BitmapFactory.decodeResource(getResources(), iconIntArray[position]));

                // Scale up animation
                ScaleAnimation expand =  new ScaleAnimation(0.2f, 1f, 0.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                expand.setDuration(100);     // animation duration in milliseconds
                expand.setInterpolator(new AccelerateInterpolator());
                fab.startAnimation(expand);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fab.startAnimation(shrink);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_logout:
                logout(this);
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_channels:
                startActivity(new Intent(this, ChannelsMenuActivity.class));
                return true;
            case R.id.action_group:
                startActivity(new Intent(this, NewGroupActivity.class));
                return true;
            case R.id.action_premium:
                showPremiumDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showPremiumDialog() {
        new MaterialDialog.Builder(this)
                .title("All users get 6 months of Sup Premium for free!")
                .content("Then, for just $1 a year, Sup Premium includes access to all SupBots, and an ad-free browsing experience.")
                .show();
    }

    public static void logout(Activity context){
        // First delete their FCM token from server
        Utils.volleyStringCall(context, "http://black-abode-2709.appspot.com/update_profile?token=" + Utils.getStringFromSharedPrefs("self_token", "") + "&fcm_token=+", new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                // Don't care...
            }
        });

        // Now delete all that's stored in SharedPrefs
        SharedPreferences.Editor editor = Utils.mSharedPreferences.edit();
        editor.clear();
        editor.apply();

        // Now delete all that's in the db
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
        db.delete("connections", null, null);

        db = BroadcastsDBHelper.getHelper(context).getWritableDatabase();
        db.delete("broadcasts", null, null);

        // Now delete all files in internal storage
        File dir = context.getFilesDir();
        String[] children = dir.list();
        for (String child : children) {
            new File(dir, child).delete();
        }

        // Now take them to the login screen
        context.startActivity(new Intent(context, LoginActivity.class));
        context.finish();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            switch(position){
                case 0:
                    // Home screen
                    return HomeScreenFragment.newInstance();
                case 1:
                    // Feed screen
                    return FeedScreenFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 1 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "HOME";
                case 1:
                    return "FEED";
            }
            return null;
        }
    }

    public boolean saveMedia = false;
    public boolean forBroadcast = false;
    public String usersName = null;
    public int requestingPosition = -1;
    public HomeAdapter.ViewHolder holder = null;
    public HomeAdapter.ViewHolder5 groupHolder = null;

    public VolleyCallback refreshCallback = null;

    private File fileToBeDeleted;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final String fileName;
        if (forBroadcast) {
            fileName = "broadcast_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis() % 100000000);
        } else {
            fileName = "response_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis() % 100000000);
        }

        if (requestCode == Constants.REQUEST_CAPTURE_MEDIA) {
            if (resultCode == RESULT_OK) {
                String path = data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH);
                path = path.substring(path.indexOf("/storage"));
                File result = new File(path);
                String mime = Utils.getMimeType(Uri.fromFile(result), HomeActivity.this);
                if (mime.startsWith("image/")) {
                    fileToBeDeleted = result;
                    try {

                        Uri editedImageUri = Uri.fromFile(result);
                        InputStream is = getContentResolver().openInputStream(editedImageUri);
                        final File finalFile = new File(getFilesDir(), fileName);
                        finalFile.createNewFile();
                        Utils.copyInputStreamToFile(is, finalFile);
                        byte[] bytes = Utils.fileToBytes(finalFile);

                        if (saveMedia) {
                            saveMedia = false;
                            File f = new File(getFilesDir(), fileName + ".png");
                            FileOutputStream os = new FileOutputStream(f);
                            os.write(bytes);
                            os.close();
                        }

                        String message;
                        if (usersName != null) {
                            message = "Sending image to " + usersName;
                        } else {
                            message = null;
                        }

                        if (purpose.startsWith("Attach") && refreshCallback != null) {
                            refreshCallback.onResponse(Utils.packageResponse("image_response", fileName + ".png"));
                        }
                        final JSONObject broadcast = Utils.packageBroadcast("image_broadcast", "", fileName + ".png");

                        if (!forBroadcast && !purpose.startsWith("Attach")) {
                            if (holder != null)
                                HomeAdapter.animateOut(this, holder, requestingPosition);
                            else if (groupHolder != null)
                                HomeAdapter.animateOut(this, groupHolder, requestingPosition);
                        } else if (!purpose.startsWith("Attach")){
                            HomeActivity.this.getCaption(broadcast);
                        }
                        Utils.uploadToFCS(this, message, fileName + ".png", bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                if (forBroadcast) {
                                    Utils.activity_result_callback.onResponse(broadcast);
                                } else {
                                    Utils.activity_result_callback.onResponse(Utils.packageResponse("image_response", fileName + ".png"));
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (fileToBeDeleted != null) {
                        fileToBeDeleted.delete();
                    }
                } else if (mime.startsWith("video/")) {
                    try {
                        File f = new File(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                        byte[] bytes = Utils.fileToBytes(f);
                        f.delete();

                        if (saveMedia) {
                            saveMedia = false;
                            f = new File(getFilesDir(), fileName + ".mp4");
                            FileOutputStream os = new FileOutputStream(f);
                            os.write(bytes);
                            os.close();
                        }

                        File output = new File(HomeActivity.this.getFilesDir(), "snapshot_" + fileName+ ".png");
                        output.createNewFile();
                        Utils.snapshot(new File(HomeActivity.this.getFilesDir(), fileName+".mp4"), output, HomeActivity.this);

                        if (purpose.startsWith("Attach") && refreshCallback != null) {
                            refreshCallback.onResponse(Utils.packageResponse("video_response", fileName + ".mp4"));
                        }

                        final JSONObject broadcast = Utils.packageBroadcast("video_broadcast", "", fileName + ".mp4");
                        if (!forBroadcast && !purpose.startsWith("Attach")) {
                            if (holder != null)
                                HomeAdapter.animateOut(this, holder, requestingPosition);
                            else if (groupHolder != null)
                                HomeAdapter.animateOut(this, groupHolder, requestingPosition);
                        } else if (!purpose.startsWith("Attach")){
                            HomeActivity.this.getCaption(broadcast);
                        }

                        String message;
                        if (usersName != null) {
                            message = "Sending video to " + usersName;
                        } else {
                            message = null;
                        }

                        Utils.uploadToFCS(this, message, fileName + ".mp4", bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                if (forBroadcast) {
                                    Utils.activity_result_callback.onResponse(broadcast);
                                } else {
                                    Utils.activity_result_callback.onResponse(Utils.packageResponse("video_response", fileName + ".mp4"));
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (data != null) {
                Bundle bundle = data.getExtras();
                for (String key : bundle.keySet()) {
                    Object value = bundle.get(key);

                }
            }
        } else if (requestCode == Constants.ADOBE_IMAGE_EDITOR) {

        } else if (requestCode == Constants.PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                LatLng ll = PlacePicker.getPlace(data, this).getLatLng();
                final double lat = ll.latitude; final double lon = ll.longitude;
                try {
                    final ProgressDialog pd = new ProgressDialog(this);
                    pd.setCancelable(false);
                    pd.setCanceledOnTouchOutside(false);
                    pd.setIndeterminate(true);
                    pd.setMessage("Downloading map...");
                    pd.show();
                    Utils.downloadImageAndSave((Activity) this, Utils.prepareStaticMapUrl(this, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            pd.dismiss();
                            if (purpose.startsWith("Attach") && refreshCallback != null) {
                                refreshCallback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                            }

                            JSONObject broadcast = Utils.packageBroadcast("location_broadcast", "", lat + "_" + lon);

                            if (forBroadcast) {
                                getCaption(broadcast);
                                Utils.activity_result_callback.onResponse(broadcast);
                            } else {
                                Utils.activity_result_callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                            }
                        }
                    });
                } catch (Exception e) {e.printStackTrace();}
            }
        }
    }


    public static View layoutForAttach = null; public static String purpose= "";
    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_PERMISSIONS_REQUEST_IMAGE_RESPONSE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED || (grantResults.length == 4 && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED)) {
                new SandriosCamera((Activity) HomeActivity.this, Constants.REQUEST_CAPTURE_MEDIA)
                        .setShowPicker(true)
                        .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                        .enableImageCropping(false) // Default is false.
                        .launchCamera();
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        } else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_VIDEO_RESPONSE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED || (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                new SandriosCamera((Activity) HomeActivity.this, Constants.REQUEST_CAPTURE_MEDIA)
                        .setShowPicker(true)
                        .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                        .enableImageCropping(false) // Default is false.
                        .launchCamera();
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        } else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_AUDIO_RESPONSE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED || (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                Utils.recordAudio(layoutForAttach, usersName, this, purpose, null);
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        } else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_LOCATION_FOR_WEATHER) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                if (activeLL != null)
                    Utils.fetchWeather(HomeActivity.this, activeLL);
            }
        } else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                Location location = Utils.getLastKnownLocation(this);
                if(location == null) {
                    Utils.showToast(this, "Location Unavailable");
                } else {
                    final double lat = location.getLatitude();
                    final double lon = location.getLongitude();

                    if (!saveMedia) {
                        Utils.activity_result_callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                    } else {
                        final ProgressDialog pd = new ProgressDialog(this);
                        pd.setCancelable(false);
                        pd.setCanceledOnTouchOutside(false);
                        pd.setIndeterminate(true);
                        pd.setMessage("Downloading map...");
                        pd.show();
                        Utils.downloadImageAndSave((Activity) this, Utils.prepareStaticMapUrl(this, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                pd.dismiss();
                                try {
                                    if (forBroadcast) {
                                        JSONObject broadcast = Utils.packageBroadcast("location_broadcast", "", lat + "_" + lon);
                                        getCaption(broadcast);
                                        Utils.activity_result_callback.onResponse(broadcast);
                                    } else {
                                        Utils.activity_result_callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        }  else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE2) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    this.startActivityForResult(builder.build(this), Constants.PLACE_PICKER_REQUEST);
                } catch (Exception e) {e.printStackTrace();}
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        } else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                loadAddressBook(contactsBottomSheet);
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Toast.makeText(this, "Can't read contacts", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * The following two functions are for registering and unregistering the receiver
     * that helps us receive ui_update requests from the FCM service
     */
    BroadcastReceiver receiver; // Initialized at the end of onCreate()
    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(CustomFirebaseMessagingService.SERVICE_RESULT)
        );
    }
    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
        if (Utils.getIntegerFromSharedPrefs("delete_smalltalks_frequency", 0) == 4) {
            SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(HomeActivity.this).getReadableDatabase();
            SQLiteDatabase dbW = ConnectionsDBHelper.getHelper(HomeActivity.this).getWritableDatabase();

            // Delete smalltalks
            deleteSmalltalks(dbR, dbW, true);
        }
    }

}
