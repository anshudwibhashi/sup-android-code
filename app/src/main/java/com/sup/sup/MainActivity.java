package com.sup.sup;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * MainActivity is the fork activity that the app launches with.
 * Also serves as login screen.
 */
public class MainActivity extends AppCompatActivity{

    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Global initializations
        FirebaseApp.initializeApp(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Utils.mSharedPreferences = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        Utils.mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .build();

        // Clear Glide cache on startup if there's internet
        if(Utils.isNetworkAvailable(this)) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    Log.d("", "Starting Async Glide refresh 2");
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    Glide.get(MainActivity.this).clearDiskCache();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    Glide.get(MainActivity.this).clearMemory();
                    Log.d("", "Ending Async Glide refresh 2");
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }


        if(Utils.getBooleanFromSharedPrefs("signed_in", false)){
            // If the user is logged in, take them to HomeActivity
            startActivity(new Intent(this, HomeActivity.class));
            this.overridePendingTransition(0, 0);
        } else {
            // Else take them to the login screen
            startActivity(new Intent(this, LoginActivity.class));
            this.overridePendingTransition(0, 0);
        }

        finish();
    }
}
