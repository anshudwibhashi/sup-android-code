package com.sup.sup;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class SettingsAdapter extends BaseAdapter {
    String texts[] = {"Invite Friends",
            "Privacy",
            "Notifications",
            "Sups and Smalltalks",
            "Delete Account",
            "Verify Phone",
            "Permissions",
            "FAQ",
            "Terms and Conditions",
            "Contact Us",
            "License Notices",
            "About Sup"};
    int icons[] = {R.mipmap.ic_person_add_white,
            R.drawable.ic_visibility_white_48dp,
            R.drawable.ic_notifications_white_48dp,
            R.mipmap.ic_question_answer_white_24dp,
            R.drawable.ic_delete_white_48dp,
            R.drawable.baseline_phone_white_48,
            R.drawable.ic_lock_white_48dp,
            R.mipmap.ic_help_white_48dp,
            R.mipmap.ic_done_white_48dp,
            R.drawable.ic_email_white_48dp,
            R.drawable.ic_assignment_white_48dp,
            R.drawable.ic_error_white_48dp};

    SettingsActivity context;
    public SettingsAdapter(SettingsActivity context) {
        this.context = context;
    }
    @Override
    public int getCount() {
        return 12;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null || convertView.getTag() == null) {
            holder = new ViewHolder();
            convertView =  context.getLayoutInflater().inflate(R.layout.list_item_settings, parent, false);
            holder.icon = (ImageView) convertView.findViewById(R.id.iconIV);
            holder.text = (TextView) convertView.findViewById(R.id.textTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(texts[position]);
        holder.icon.setImageResource(icons[position]);

        return convertView;
    }

    public static class ViewHolder {
        public ImageView icon;
        public TextView text;
    }
}
