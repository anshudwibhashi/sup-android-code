package com.sup.sup;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;

public class NewsKeywordsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_keywords);
        if (getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            Utils.populateMainDataSet(this); Utils.populateFeedDataSet(this);
        }
        try {
            JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("news_keywords", "[]"));
            if(pre.length() > 0) {
                // we have some keywords
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                findViewById(R.id.topicsLV).setVisibility(View.VISIBLE);
                ListView lv = (ListView) findViewById(R.id.topicsLV);
                lv.setAdapter(new TopicsAdapter(this));
            }
        } catch(Exception e) {e.printStackTrace();}
    }

    public void addTopic(View v) {
        new MaterialDialog.Builder(this)
                .title("Enter keywords")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                        try {
                            String keywords = input.toString();
                            if(keywords != null && !keywords.isEmpty()) {
                                JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("news_keywords", "[]"));
                                pre.put(keywords);
                                Utils.putStringInSharedPrefs("news_keywords", pre.toString());
                                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                                findViewById(R.id.topicsLV).setVisibility(View.VISIBLE);
                                ListView lv = (ListView) findViewById(R.id.topicsLV);
                                lv.setAdapter(new TopicsAdapter(NewsKeywordsActivity.this));
                                NewsArticlesJob.scheduleJob();
                                Utils.putBooleanInSharedPrefs("startedNewsFetching", true);
                            }
                        } catch (Exception e) {e.printStackTrace();}
                    }
                }).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                overrideNavigation();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        overrideNavigation();
    }

    private void overrideNavigation() {
        if(getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            finish();
            startActivity(new Intent(this, HomeActivity.class));
        } else {
            super.onBackPressed();
        }
    }


}
