package com.sup.sup;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by anshudwibhashi on 11/10/17.
 */

public class YouTubeSearchResultsAdapter extends RecyclerView.Adapter<YouTubeSearchResultsAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    ArrayList<String> imageUrls, names, descs, ids;

    @Override
    public int getItemCount() {
        return imageUrls.size();
    }

    Context context;
    public YouTubeSearchResultsAdapter(Context context, ArrayList<String> imageUrls, ArrayList<String> names, ArrayList<String> descs, ArrayList<String> ids) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.imageUrls = imageUrls;
        this.names = names;
        this.descs = descs;
        this.ids = ids;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View convertView =  mInflater.inflate(R.layout.list_item_youtube_results, parent, false);
        ViewHolder holder = new ViewHolder(convertView);
        holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
        holder.iconIV = (ImageView) convertView.findViewById(R.id.iconIV);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.nameTV.setText(names.get(position));

            Glide.with(context).load(imageUrls.get(position)).asBitmap().centerCrop()
                    .placeholder(R.drawable.ic_default_picture)
                    .override(100, 100).into(new BitmapImageViewTarget(holder.iconIV) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.iconIV.setImageDrawable(circularBitmapDrawable);
                }
            });
            holder.convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // now subscribe
                    try {
                        JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("youtube_channel_ids", "[]"));
                        pre.put(ids.get(position));
                        Utils.putStringInSharedPrefs("youtube_channel_ids", pre.toString());

                        pre = new JSONArray(Utils.getStringFromSharedPrefs("youtube_channel_names", "[]"));
                        pre.put(names.get(position));
                        Utils.putStringInSharedPrefs("youtube_channel_names", pre.toString());

                        ((Activity)context).findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                        ((Activity)context).findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                        ListView lv = (ListView) ((Activity)context).findViewById(R.id.tickerLV);
                        lv.setAdapter(new TopicsAdapter(context));

                        FirebaseMessaging.getInstance().subscribeToTopic("youtube_"+ids.get(position));
                        Utils.volleyStringCall(context, "http://black-abode-2709.appspot.com/youtubesubscribe?channelId=" + ids.get(position), new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                // don't care
                            }
                        });
                        if (dialog != null)
                            dialog.dismiss();
                    } catch (Exception e) {e.printStackTrace();}
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public MaterialDialog dialog;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView iconIV;
        public TextView nameTV;
        public View convertView;
        public ViewHolder(View v) {
            super(v);
            convertView = v;
        }
    }

}