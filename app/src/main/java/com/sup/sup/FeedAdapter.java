package com.sup.sup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.thefinestartist.finestwebview.FinestWebView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import rm.com.audiowave.AudioWaveView;

/**
 * Created by anshudwibhashi on 03/06/17.
 */
public class FeedAdapter  extends BaseAdapter {
    private LayoutInflater mInflater;
    Context context;
    private final static int BROADCAST = 0;
    private final static int CHANNEL_NEWS = 1;
    private final static int CHANNEL_PACKAGE = 2;
    private final static int CHANNEL_YOUTUBE = 3;
    private final static int CHANNEL_REDDIT = 4;
    private final static int CHANNEL_TWITTER = 5;

    public FeedAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public int getCount() {
        return Utils.FeedDataSet.names.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (Utils.FeedDataSet.ids.get(position).startsWith("CHANNEL_NEWS_")) {
            return CHANNEL_NEWS;
        } else if(Utils.FeedDataSet.ids.get(position).startsWith("CHANNEL_PACKAGE_")) {
            return CHANNEL_PACKAGE;
        } else if(Utils.FeedDataSet.ids.get(position).startsWith("CHANNEL_YOUTUBE_")) {
            return CHANNEL_YOUTUBE;
        } else if(Utils.FeedDataSet.ids.get(position).startsWith("CHANNEL_REDDIT_")) {
            return CHANNEL_REDDIT;
        } else if(Utils.FeedDataSet.ids.get(position).startsWith("CHANNEL_TWITTER_")) {
            return CHANNEL_TWITTER;
        }  else {
            return BROADCAST;
        }
    }

    @Override
    public String getItem(int position) {
        return null; // useless to us
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Housekeeping stuff
        int size = Utils.FeedDataSet.names.size();

        while (!((Utils.FeedDataSet.names.size()==size)&&
                (Utils.FeedDataSet.ids.size()==size)&&
                (Utils.FeedDataSet.timestamps.size()==size)&&
                (Utils.FeedDataSet.owners.size()==size)&&
                (Utils.FeedDataSet.texts.size()==size)&&
                (Utils.FeedDataSet.media.size()==size)&&
                (Utils.FeedDataSet.profilePics.size()==size)&&
                (Utils.FeedDataSet.liked.size()==size)&&
                (Utils.FeedDataSet.comments.size()==size)&&
                (Utils.FeedDataSet.likes.size()==size)));

        switch (this.getItemViewType(position)) {
            case BROADCAST:
                convertView = caseBroadcast(convertView, position);
                break;
            case CHANNEL_NEWS:
                convertView = caseNewsCard(convertView, position);
                break;
            case CHANNEL_PACKAGE:
                convertView = casePackageCard(convertView, position);
                break;
            case CHANNEL_YOUTUBE:
                convertView = caseYoutubeCard(convertView, position);
                break;
            case CHANNEL_REDDIT:
                convertView = caseRedditCard(convertView, position);
                break;
            case CHANNEL_TWITTER:
                convertView = caseTwitterCard(convertView, position);
                break;
        }
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 6;
    }

    private View caseTwitterCard(View convertView, final int position) {
        final ViewHolder6 holder;
        if (convertView == null) {
            holder = new ViewHolder6();
            convertView =  mInflater.inflate(R.layout.list_item_twitter_card, null);
            holder.timestampTV = (TextView) convertView.findViewById(R.id.timestampTV);
            holder.usernameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            holder.udcTV = (TextView) convertView.findViewById(R.id.udcTV);
            holder.viewButton = (Button) convertView.findViewById(R.id.viewButton);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder6) convertView.getTag();
        }

        Resources r = context.getResources();
        int dpToPixel = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10,
                r.getDisplayMetrics()
        );

        if(position == 0) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, dpToPixel, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        } else {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, 0, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        }

        try {
            final JSONObject post = new JSONObject(Utils.FeedDataSet.texts.get(position));
            holder.usernameTV.setText(post.getJSONObject("user").getString("name")+" tweeted");
            holder.titleTV.setText(post.getString("text"));
            holder.timestampTV.setText(Utils.timeFromTimeStamp(Long.valueOf(Utils.FeedDataSet.timestamps.get(position)))+" · @"+post.getJSONObject("user").getString("screen_name"));
            int ret = post.getInt("retweet_count"); int fav = post.getInt("favorite_count"); int rep = post.getInt("reply_count");
            String udc = ""+ret;
            if(ret == 1) {
                udc = udc.concat(" RETWEET · ")+fav;
            } else {
                udc = udc.concat(" RETWEETS · ")+fav;
            }

            if(fav == 1) {
                udc = udc.concat(" FAVORITE · ")+rep;
            } else {
                udc = udc.concat(" FAVORITES · ")+rep;
            }

            if(rep == 1) {
                udc = udc.concat(" REPLY");
            } else {
                udc = udc.concat(" REPLIES");
            }

            holder.udcTV.setText(udc);

            holder.viewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        new FinestWebView.Builder(v.getContext())
                                .swipeRefreshColor(v.getContext().getResources().getColor(R.color.colorAccent))
                                .show("https://twitter.com/"+post.getJSONObject("user").getString("screen_name")+"/status/"+post.getString("id_str"));
                    } catch (Exception e) {}
                }
            });

        } catch (Exception e) {e.printStackTrace();}

        return convertView;
    }

    private View caseRedditCard(View convertView, final int position) {
        final ViewHolder5 holder;
        if (convertView == null) {
            holder = new ViewHolder5();
            convertView =  mInflater.inflate(R.layout.list_item_reddit_card, null);
            holder.timestampTV = (TextView) convertView.findViewById(R.id.timestampTV);
            holder.usernameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.subredditTV = (TextView) convertView.findViewById(R.id.subredditTV);
            holder.titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            holder.udcTV = (TextView) convertView.findViewById(R.id.udcTV);
            holder.viewButton = (Button) convertView.findViewById(R.id.viewButton);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder5) convertView.getTag();
        }

        Resources r = context.getResources();
        int dpToPixel = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10,
                r.getDisplayMetrics()
        );

        if(position == 0) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, dpToPixel, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        } else {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, 0, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        }

        holder.timestampTV.setText(Utils.timeFromTimeStamp(Long.valueOf(Utils.FeedDataSet.timestamps.get(position))));

        try {
            final JSONObject post = new JSONObject(Utils.FeedDataSet.texts.get(position));
            holder.usernameTV.setText("/u/"+post.getString("author"));
            holder.subredditTV.setText("Posted on /r/"+post.getString("subreddit"));
            holder.titleTV.setText(post.getString("title"));
            int ups = post.getInt("ups"); int downs = post.getInt("downs"); int num_comments = post.getInt("num_comments");
            String udc = ""+ups;
            if(ups == 1) {
                udc = udc.concat(" UPVOTE · ")+downs;
            } else {
                udc = udc.concat(" UPVOTES · ")+downs;
            }

            if(downs == 1) {
                udc = udc.concat(" DOWNVOTE · ")+num_comments;
            } else {
                udc = udc.concat(" DOWNVOTES · ")+num_comments;
            }

            if(num_comments == 1) {
                udc = udc.concat(" COMMENT");
            } else {
                udc = udc.concat(" COMMENTS");
            }

            holder.udcTV.setText(udc);

            holder.viewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        new FinestWebView.Builder(v.getContext())
                                .swipeRefreshColor(v.getContext().getResources().getColor(R.color.colorAccent))
                                .show(post.getString("url"));
                    } catch (Exception e) {}
                }
            });

        } catch (Exception e) {e.printStackTrace();}

        return convertView;
    }

    private View caseYoutubeCard(View convertView, final int position) {
        final ViewHolder4 holder;
        if (convertView == null) {
            holder = new ViewHolder4();
            convertView =  mInflater.inflate(R.layout.list_item_youtube_card, null);
            holder.timestampTV = (TextView) convertView.findViewById(R.id.timestampTV);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.titleTV = (TextView) convertView.findViewById(R.id.titleTV);
            holder.youtube_thumbnail = (ImageView) convertView.findViewById(R.id.youtube_thumbnail);
            holder.playButton2 = (ImageView) convertView.findViewById(R.id.playButton2);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder4) convertView.getTag();
        }

        Resources r = context.getResources();
        int dpToPixel = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10,
                r.getDisplayMetrics()
        );

        if(position == 0) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, dpToPixel, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        } else {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, 0, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        }

        holder.timestampTV.setText(Utils.timeFromTimeStamp(Long.valueOf(Utils.FeedDataSet.timestamps.get(position))));

        try {
            final JSONObject video = new JSONObject(Utils.FeedDataSet.texts.get(position));

            JSONArray ids = new JSONArray(Utils.getStringFromSharedPrefs("youtube_channel_ids", "[]"));
            JSONArray names = new JSONArray(Utils.getStringFromSharedPrefs("youtube_channel_names", "[]"));
            holder.titleTV.setText("Uploaded \""+video.getString("title")+"\"");
            for (int i = 0; i < ids.length(); i++) {
                if (ids.getString(i).equals(video.getString("channelid"))) {
                    holder.nameTV.setText(names.getString(i));
                }
            }

            Glide.with(context)
                    .load("https://img.youtube.com/vi/"+video.getString("videoid")+"/maxresdefault.jpg")
                    .asBitmap()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            holder.youtube_thumbnail.setImageBitmap(resource);
                        }
                    });

            holder.playButton2.setVisibility(View.VISIBLE);
            holder.playButton2.setOnClickListener(new YouTubePlayListener(holder, video.getString("videoid"), convertView));
            holder.youtube_thumbnail.setOnClickListener(new YouTubePlayListener(holder, video.getString("videoid"), convertView));

        } catch (Exception e) {e.printStackTrace();}

        return convertView;
    }


    private class YouTubePlayListener implements View.OnClickListener {
        ViewHolder4 holder;

        private String VIDEO_ID;


        public YouTubePlayListener(ViewHolder4 holder, String VIDEO_ID, View parent) {
            this.holder = holder;
            this.VIDEO_ID = VIDEO_ID;
        }


        @Override
        public void onClick(View v) {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity)context, "AIzaSyAbs_8UVdh-MptE7k0u7Iq369o-XKTzD6w", VIDEO_ID);
            context.startActivity(intent);
        }

    }


    private View casePackageCard(View convertView, final int position) {
        final ViewHolder3 holder;
        if (convertView == null) {
            holder = new ViewHolder3();
            convertView =  mInflater.inflate(R.layout.list_item_package_update, null);
            holder.timestampTV = (TextView) convertView.findViewById(R.id.timestampTV);
            holder.asofTV = (TextView) convertView.findViewById(R.id.asofTV);
            holder.carrierTV = (TextView) convertView.findViewById(R.id.carrierTV);
            holder.statusTV = (TextView) convertView.findViewById(R.id.statusTV);
            holder.estTV = (TextView) convertView.findViewById(R.id.estTV);
            holder.trackingTV = (TextView) convertView.findViewById(R.id.trackingTV);
            holder.detailsButton = (Button) convertView.findViewById(R.id.detailsButton);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder3) convertView.getTag();
        }

        Resources r = context.getResources();
        int dpToPixel = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10,
                r.getDisplayMetrics()
        );

        if(position == 0) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, dpToPixel, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        } else {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, 0, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        }

        try {
            final JSONObject info = new JSONObject(Utils.FeedDataSet.texts.get(position));
            holder.timestampTV.setText(Utils.timeFromTimeStamp(Long.valueOf(Utils.FeedDataSet.timestamps.get(position))));

            Date date = new Date(Long.valueOf(Utils.FeedDataSet.timestamps.get(position)));
            DateFormat formatter = new SimpleDateFormat("MMMM dd, yyyy (h:mm aaa)");
            String dateFormatted = formatter.format(date);

            holder.asofTV.setText("As of " + dateFormatted);
            holder.carrierTV.setText(info.getString("carrier"));
            holder.statusTV.setText(info.getString("message"));
            holder.estTV.setText(info.getString("estdelivery"));
            holder.trackingTV.setText(info.getString("trackingid"));
            holder.detailsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        new FinestWebView.Builder(v.getContext())
                                .swipeRefreshColor(v.getContext().getResources().getColor(R.color.colorAccent))
                                .show(info.getString("public_url"));
                    } catch (Exception e) {}
                }
            });
        } catch (Exception e) {e.printStackTrace();}

        return convertView;
    }

    private View caseNewsCard(View convertView, final int position) {
        final ViewHolder2 holder;
        if (convertView == null) {
            holder = new ViewHolder2();
            convertView =  mInflater.inflate(R.layout.list_item_news_feed, null);
            holder.headline = (TextView) convertView.findViewById(R.id.headlineTV);
            holder.desc = (TextView) convertView.findViewById(R.id.descTV);
            holder.source = (TextView) convertView.findViewById(R.id.sourceTV);
            holder.newsThumbnail = (ImageView) convertView.findViewById(R.id.thumbnailIV);
            holder.mainCard = (LinearLayout) convertView.findViewById(R.id.mainCard);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder2) convertView.getTag();
        }

        Resources r = context.getResources();
        int dpToPixel = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10,
                r.getDisplayMetrics()
        );

        if(position == 0) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, dpToPixel, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        } else {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, 0, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        }

        try {
            final JSONObject article = new JSONObject(Utils.FeedDataSet.texts.get(position));
            holder.headline.setText(Html.fromHtml(article.getString("title")));
            holder.desc.setText(Html.fromHtml(article.getString("snippet")));
            holder.source.setText(Html.fromHtml(article.getString("source")+" &middot; Published "+Utils.timeFromTimeStamp(Long.valueOf(article.getString("datePublished")))));
            if (Utils.isNetworkAvailable(context) && !article.getString("image").isEmpty()) {
                holder.newsThumbnail.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(article.getString("image"))
                        .override(100, 100)
                        .centerCrop()
                        .into(holder.newsThumbnail);
            } else {
                holder.newsThumbnail.setVisibility(View.GONE);
            }
            holder.mainCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        new FinestWebView.Builder(v.getContext())
                                .titleDefault(article.getString("title"))
                                .swipeRefreshColor(v.getContext().getResources().getColor(R.color.colorAccent))
                                .show(article.getString("url"));
                    } catch (Exception e) {}
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private View caseBroadcast(View convertView, final int position) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  mInflater.inflate(R.layout.list_item_feed, null);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.timestampTV = (TextView) convertView.findViewById(R.id.timestampTV);
            holder.textTV = (TextView) convertView.findViewById(R.id.textTV);
            holder.mediaIV = (ImageView) convertView.findViewById(R.id.mediaIV);
            holder.photoIV = (ImageView) convertView.findViewById(R.id.photoIV);
            holder.playButton2 = (ImageView) convertView.findViewById(R.id.playButton2);
            holder.audioResponsePayload = (LinearLayout) convertView.findViewById(R.id.audioResponsePayload);

            holder.likeButton = (ImageButton) convertView.findViewById(R.id.likeButton);
            holder.likesButton = (Button) convertView.findViewById(R.id.likesButton);
            holder.commentsButton = (ImageButton) convertView.findViewById(R.id.commentsButton);
            holder.playButton = (ImageView) convertView.findViewById(R.id.play_action);
            holder.cancelButton = (ImageView) convertView.findViewById(R.id.cancel_action3);

            holder.wave = (AudioWaveView) convertView.findViewById(R.id.wave);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Resources r = context.getResources();
        int dpToPixel = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                10,
                r.getDisplayMetrics()
        );


        if(position == 0) {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, dpToPixel, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        } else {
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(dpToPixel, 0, dpToPixel, dpToPixel);
            convertView.findViewById(R.id.mainCard).setLayoutParams(llp);
        }

        holder.nameTV.setText(Utils.FeedDataSet.names.get(position));
        holder.timestampTV.setText(Utils.timeFromTimeStamp(Long.valueOf(Utils.FeedDataSet.timestamps.get(position))));

        String text = Utils.FeedDataSet.texts.get(position);

        if (text.isEmpty()) {
            holder.textTV.setVisibility(View.GONE);
        } else {
            holder.textTV.setVisibility(View.VISIBLE);
            holder.textTV.setText(Html.fromHtml(text.replaceAll("\\@([a-zA-Z0-9\\._\\-]+)", "<font color=\"#E040FB\">@$1</font>")));
        }

        /*Typeface font = Typeface.createFromAsset(context.getAssets(), "RobotoCondensed-Light.ttf");
        holder.textTV.setTypeface(font);*/

        // Now we load the image in background if not in cache
        if (Utils.FeedDataSet.names.get(position).equals(Utils.getStringFromSharedPrefs("self_name", ""))) {
            // Load self_photo.png

            Glide.with(context)
                    .load(new File(context.getFilesDir(), "self_photo.png").getAbsolutePath())
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(context))
                    .placeholder(R.drawable.ic_default_picture)
                    .into(holder.photoIV);
        } else {

            Glide.with(context)
                    .load(Utils.FeedDataSet.profilePics.get(position))
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(context))
                    .placeholder(R.drawable.ic_default_picture)
                    .into(holder.photoIV);

            // Load token_photo.png
        }

        final String media = Utils.FeedDataSet.media.get(position);
        if (media.isEmpty()) {
            ((View)holder.mediaIV.getParent().getParent()).setVisibility(View.GONE);
            holder.playButton2.setVisibility(View.GONE);
            holder.audioResponsePayload.setVisibility(View.GONE);
        } else {
            try {
                if (media.endsWith(".png")) {
                    ((View)holder.mediaIV.getParent().getParent()).setVisibility(View.VISIBLE);
                    holder.playButton2.setVisibility(View.GONE);
                    holder.audioResponsePayload.setVisibility(View.GONE);
                    Utils.renderImageResponse(holder.mediaIV, new JSONObject().put("response", media), true);
                } else if (media.endsWith(".mp4")) {
                    ((View)holder.mediaIV.getParent().getParent()).setVisibility(View.VISIBLE);
                    holder.playButton2.setVisibility(View.VISIBLE);
                    holder.audioResponsePayload.setVisibility(View.GONE);
                    Utils.renderVideoResponse(holder.mediaIV, new JSONObject().put("response", media), true, holder.playButton2);
                } else if (media.endsWith(".3gpp")) {
                    holder.audioResponsePayload.setVisibility(View.VISIBLE);
                    ((View)holder.mediaIV.getParent().getParent()).setVisibility(View.GONE);
                    holder.playButton2.setVisibility(View.GONE);
                    Utils.renderAudioResponse(holder.wave, holder.playButton, holder.cancelButton, new JSONObject().put("response", media));
                } else {
                    holder.audioResponsePayload.setVisibility(View.GONE);
                    ((View)holder.mediaIV.getParent().getParent()).setVisibility(View.VISIBLE);
                    holder.playButton2.setVisibility(View.GONE);
                    Utils.renderLocationResponse(holder.mediaIV, new JSONObject().put("response", media), true);
                }
            } catch (Exception e) {e.printStackTrace();}
        }

        if(Utils.FeedDataSet.liked.get(position)) {
            holder.likeButton.setColorFilter(Color.parseColor("#E040FB"));
        } else {
            holder.likeButton.setColorFilter(Color.parseColor("#757575"));
        }

        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.toggleLike(context, (ImageView) v, Utils.FeedDataSet.ids.get(position), Utils.FeedDataSet.owners.get(position), Utils.FeedDataSet.liked.get(position), Utils.FeedDataSet.likes.get(position), position);
            }
        });

        holder.commentsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showCommentsDialog(context, Utils.FeedDataSet.comments.get(position), Utils.FeedDataSet.ids.get(position), Utils.FeedDataSet.owners.get(position), position);
            }
        });

        int likes = Utils.FeedDataSet.likes.get(position).users.size();
        if (likes == 1) {
            holder.likesButton.setText(likes+" like");
        } else {
            holder.likesButton.setText(likes+" likes");
        }
        holder.likesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showLikesDialog(context, Utils.FeedDataSet.likes.get(position), position);
            }
        });
        return convertView;
    }

    public static class ViewHolder { // ViewHolder for Broadcasts
        public ImageView photoIV, mediaIV, playButton2;
        public TextView nameTV, timestampTV, textTV;
        public ImageButton likeButton, commentsButton;
        public ImageView playButton, cancelButton;
        public AudioWaveView wave;
        public LinearLayout audioResponsePayload;
        public Button likesButton;
    }

    public static class ViewHolder2 { // ViewHolder for news cards
        public TextView headline, desc, source;
        public ImageView newsThumbnail;
        public LinearLayout mainCard;
    }

    public static class ViewHolder3 { // for package cards
        public TextView timestampTV, statusTV, asofTV, carrierTV, estTV, trackingTV;
        public Button detailsButton;
    }

    public static class ViewHolder4 { // for youtube cards
        public TextView timestampTV, nameTV, titleTV;
        public ImageView youtube_thumbnail, playButton2;
    }

    public static class ViewHolder5 { // for reddit cards
        public TextView usernameTV, timestampTV, titleTV, udcTV, subredditTV;
        public Button viewButton;
    }

    public static class ViewHolder6 { // for twitter cards
        public TextView usernameTV, timestampTV, titleTV, udcTV;
        public Button viewButton;
    }

}
