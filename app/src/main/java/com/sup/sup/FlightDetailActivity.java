package com.sup.sup;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class FlightDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        try {
            JSONObject flight = new JSONObject(intent.getStringExtra("flight"));
            getSupportActionBar().setTitle(flight.getString("flight_code"));
            ((ProgressBar)findViewById(R.id.progress_bar)).getProgressDrawable().setColorFilter(
                    new PorterDuffColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY));
            ((TextView) findViewById(R.id.airlineTV)).setText(flight.getString("airline"));
            String status;
            switch(flight.getString("status")) {
                case "A":
                    status = "IN FLIGHT";
                    break;
                case "C":
                    status = "CANCELLED";
                    break;
                case "D":
                    status = "DIVERTED";
                    break;
                case "L":
                    status = "LANDED";
                    break;
                case "S":
                    status = "SCHEDULED";
                    break;
                default:
                    status = "UNKNOWN";
            }
            if ((status.equals("SCHEDULED") && !flight.isNull("dep_delay"))
                    || (status.equals("IN FLIGHT") && !flight.isNull("arr_delay"))) {
                status = "DELAYED";
            }
            ((TextView) findViewById(R.id.statusTV)).setText(status);
            ((TextView) findViewById(R.id.routeTV)).setText(flight.getString("dep_city")+" to "+flight.getString("arr_city"));
            ((TextView) findViewById(R.id.dep_code)).setText(flight.getString("dep_code"));
            ((TextView) findViewById(R.id.arr_code)).setText(flight.getString("arr_code"));

            DateFormat dateFormatiso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            DateFormat dateFormatiso2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");


            DateFormat formatter = new SimpleDateFormat("h:mm a");

            long sch_dep_utc = dateFormatiso2.parse(flight.getString("sch_dep_utc")).getTime();
            long sch_arr_utc = dateFormatiso2.parse(flight.getString("sch_arr_utc")).getTime();
            long now_utc = Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis();

            if (status.equals("SCHEDULED")) {
                ((ProgressBar) findViewById(R.id.progress_bar)).setProgress(0);
            } else if (status.equals("LANDED")) {
                ((ProgressBar) findViewById(R.id.progress_bar)).setProgress(100);
            } else {
                int progress = (int) ((((float) (now_utc - sch_dep_utc))
                        / ((float) (sch_arr_utc - sch_dep_utc))) * 100);
                ((ProgressBar) findViewById(R.id.progress_bar)).setProgress(progress);
            }

            String schDep = formatter.format(dateFormatiso.parse(flight.getString("sch_dep")));
            String schArr = formatter.format(dateFormatiso.parse(flight.getString("sch_arr")));

            String estDep;
            try {
                estDep = formatter.format(dateFormatiso.parse(flight.getString("est_dep")));
            } catch (Exception e) {
                estDep = " - ";
            }
            String estArr;
            try {
                estArr = formatter.format(dateFormatiso.parse(flight.getString("est_arr")));
            } catch (Exception e){
                estArr = " - ";
            }
            String actDep;
            try {
                actDep = formatter.format(dateFormatiso.parse(flight.getString("act_dep")));
            } catch (Exception e) {
                actDep = " - ";
            }
            String actArr;
            try {
                actArr = formatter.format(dateFormatiso.parse(flight.getString("act_arr")));
            } catch (Exception e){
                actArr = " - ";
            }

            ((TextView) findViewById(R.id.schDepTV)).setText(schDep);
            ((TextView) findViewById(R.id.schArrTV)).setText(schArr);
            ((TextView) findViewById(R.id.estDepTV)).setText(estDep);
            ((TextView) findViewById(R.id.estArrTV)).setText(estArr);
            ((TextView) findViewById(R.id.actDepTV)).setText(actDep);
            ((TextView) findViewById(R.id.actArrTV)).setText(actArr);

            String gateDep;
            if(!flight.isNull("dep_gate")) {
                gateDep = flight.getString("dep_gate");
            } else {
                gateDep = " - ";
            }
            String gateArr;
            if (!flight.isNull("arr_gate")) {
                gateArr = flight.getString("arr_gate");
            } else {
                gateArr = " - ";
            }
            String terminalDep;
            if (!flight.isNull("dep_terminal")) {
                terminalDep = flight.getString("dep_terminal");
            } else {
                terminalDep = " - ";
            }
            String terminalArr;
            if (!flight.isNull("arr_terminal")) {
                terminalArr = flight.getString("arr_terminal");
            } else {
                terminalArr = " - ";
            }

            ((TextView) findViewById(R.id.gateArrTV)).setText(gateArr);
            ((TextView) findViewById(R.id.gateDepTV)).setText(gateDep);
            ((TextView) findViewById(R.id.terminalArrTV)).setText(terminalArr);
            ((TextView) findViewById(R.id.terminalDepTV)).setText(terminalDep);

        } catch (Exception e) {e.printStackTrace();}
    }
}
