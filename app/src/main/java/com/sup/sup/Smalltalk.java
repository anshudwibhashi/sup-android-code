package com.sup.sup;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by anshudwibhashi on 28/05/17.
 */
public class Smalltalk {
    public static class SmalltalkMessage {
        /**
         * This is the class from which objects will be made, serialized, and stored in the db.
          */
        public MessageBlock messages = null;
    }

    public static class MessageBlock {
        private ArrayList<MyMessage> messages;
        public MessageBlock() {
            messages = new ArrayList<>();
        }
        public ArrayList<MyMessage> getMessages() {return messages;}
        public void addMessage(MyMessage message) {messages.add(message);}
    }

    public static class MyUser implements IUser {
        String id, name;
        public MyUser(String id, String name) {
            this.id = id; this.name = name;
        }
        public void setId(String id) {this.id = id;}
        @Override
        public String getId() {
            return id;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getAvatar() {
            return null;
        }
    }

    /* VERY IMPORTANT NOTE: This class has methods and functions that correspond to
      images. That's because they were created back when audio messages weren't around.
      The same methods are used for audio messages as well.
       */
    public static class MyMessage implements IMessage,
            MessageContentType.Image, MessageContentType {
        String id, text, type="text", imgPath=null;

        @Override
        public String getImageUrl() {
            if(type != null && (type.startsWith("image") || type.startsWith("video") || type.startsWith("location"))) {
                return imgPath;
            } else {
                return null;
            }
        }

        public void setType(String type) {this.type = type;}
        public void setImgPath(String imgPath) {this.imgPath = imgPath;}
        public String getImgPath() {return this.imgPath;}

        Date createdAt;
        MyUser user;
        public MyMessage(String id, String text, MyUser user, Date createdAt) {
            this.id = id; this.text = text; this.user = user; this.createdAt = createdAt;
            this.type="text"; imgPath="";
        }
        @Override
        public String getId() {
            return id;
        }

        @Override
        public String getText() {
            return text;
        }

        @Override
        public IUser getUser() {
            return user;
        }

        public String getType(){return this.type;}

        @Override
        public Date getCreatedAt() {
            return createdAt;
        }
    }
}
