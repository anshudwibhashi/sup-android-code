package com.sup.sup;

import android.content.DialogInterface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONObject;

public class FlightChannelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_channel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        try {
            JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("flight_codes", "[]"));
            if(pre.length() > 0) {
                // we have some tickers
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                ListView lv = (ListView) findViewById(R.id.tickerLV);
                lv.setAdapter(new TopicsAdapter(this));
            }
        } catch(Exception e) {e.printStackTrace();}
    }

    public void addFlightCode(View view) {

        new MaterialDialog.Builder(this)
                .title("Enter Flight Code")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                        String keywords = input.toString();
                        if(keywords != null && !keywords.isEmpty()) {
                            try {
                                JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("flight_codes", "[]"));
                                pre.put(keywords);
                                Utils.putStringInSharedPrefs("flight_codes", pre.toString());
                                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                                ListView lv = (ListView) findViewById(R.id.tickerLV);
                                lv.setAdapter(new TopicsAdapter(FlightChannelActivity.this));

                                // If they've just add their first ticker, create a new contact
                                if (pre.length() == 1) {
                                    JSONObject profile = new JSONObject();
                                    profile.put("token", "CHANNEL_FLIGHTS");
                                    profile.put("name", "");
                                    profile.put("tagline", "");
                                    profile.put("photoUrl", "");
                                    profile.put("username", "");
                                    profile.put("online_status", "");
                                    Utils.dbAdd(FlightChannelActivity.this, profile, "flight_codes", new VolleyCallback() {
                                        @Override
                                        public void onResponse(Object result) {

                                        }
                                    });
                                }
                            } catch (Exception e){e.printStackTrace();}
                        }
                    }
                }).show();
    }
}
