package com.sup.sup;

import android.Manifest;
import android.animation.Animator;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.firebase.FirebaseApp;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;

import rm.com.audiowave.AudioWaveView;

public class ShareActivity extends AppCompatActivity {

    String type; Uri fileUri;
    Intent intent;
    public ArrayList<String> selectedUsers; // tokens of those who've been checked

    @Override
    public void onBackPressed() {
        if (findViewById(R.id.mainCard2).getVisibility() == View.VISIBLE) {
            finish();
        } else {
            // Prepare the View for the animation
            findViewById(R.id.mainCard).setVisibility(View.VISIBLE);
            findViewById(R.id.mainCard).setAlpha(1.0f);

            // Start the animation
            findViewById(R.id.mainCard).animate()
                    .alpha(0.0f)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            animation.removeAllListeners();
                            findViewById(R.id.mainCard).setVisibility(View.GONE);
                            findViewById(R.id.mainCard2).setVisibility(View.VISIBLE);
                            findViewById(R.id.mainCard2).setAlpha(0.0f);

                            // Start the animation
                            findViewById(R.id.mainCard2).animate()
                                    .alpha(1.0f)
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {

                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            animation.removeAllListeners();
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {

                                        }
                                    });
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        setTitle("Share on Sup");
        selectedUsers = new ArrayList<>();

        // Global initializations
        FirebaseApp.initializeApp(this);
        Utils.mSharedPreferences = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        intent = getIntent();
        String action = intent.getAction();
        type = intent.getType();

        ((ListView)findViewById(R.id.contactsLV)).setAdapter(new ShareAdapter(this));
        ((ListView)findViewById(R.id.contactsLV)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                    // Prepare the View for the animation
                    ((View)ShareActivity.this.findViewById(R.id.contactsLV).getParent()).setVisibility(View.VISIBLE);
                    ((View)ShareActivity.this.findViewById(R.id.contactsLV).getParent()).setAlpha(1.0f);

                    // Start the animation
                    ((View)ShareActivity.this.findViewById(R.id.contactsLV).getParent()).animate()
                            .alpha(0.0f)
                            .setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    animation.removeAllListeners();
                                    ((View)ShareActivity.this.findViewById(R.id.contactsLV).getParent()).setVisibility(View.GONE);
                                    ShareActivity.this.findViewById(R.id.mainCard).setVisibility(View.VISIBLE);
                                    ShareActivity.this.findViewById(R.id.mainCard).setAlpha(0.0f);

                                    // Start the animation
                                    ShareActivity.this.findViewById(R.id.mainCard).animate()
                                            .alpha(1.0f).setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animation) {

                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            animation.removeAllListeners();
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {

                                        }
                                    });
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });

                } else {
                    position--;
                    if(!ShareActivity.this.selectedUsers.toString().contains(Utils.MainDataSet.tokens.get(position))) {
                        ShareActivity.this.selectedUsers.add(Utils.MainDataSet.tokens.get(position));
                        ((ImageView) view.findViewById(R.id.checkbox)).setColorFilter(Color.parseColor("#FFFFFF"));
                        ((View)(view.findViewById(R.id.checkbox)).getParent()).setBackgroundResource(R.drawable.rounded_button3);
                    } else {
                        Iterator<String> it = ShareActivity.this.selectedUsers.iterator();
                        while (it.hasNext()) {
                            String token = it.next();
                            if (token.equals(Utils.MainDataSet.tokens.get(position))) {
                                it.remove();
                            }
                        }
                        ((ImageView) view.findViewById(R.id.checkbox)).setColorFilter(Color.parseColor("#512DA8"));
                        ((View)(view.findViewById(R.id.checkbox)).getParent()).setBackgroundResource(R.drawable.rounded_button2);
                    }
                }
            }
        });

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent);
            } else {
                fileUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
                String type2 = Utils.getMimeType(fileUri, ShareActivity.this);
                type = (type2 !=  null ? type2 : type);

                if (!(ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);
                } else {
                    if (type.startsWith("image/")) {
                        handleSendImage(fileUri, intent);
                    } else if (type.startsWith("video/")) {
                        handleSendVideo(fileUri);
                    } else if (type.startsWith("audio/")) {
                        handleSendAudio(fileUri);
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (type.startsWith("image/")) {
                        handleSendImage(fileUri, intent);
                    } else if (type.startsWith("video/")) {
                        handleSendVideo(fileUri);
                    } else if (type.startsWith("audio/")) {
                        handleSendAudio(fileUri);
                    }
                } else {
                    Utils.showToast(this, "Permission denied");
                    finish();
                }
                return;
            }
        }
    }

    void handleSendText(final Intent intent) {
        findViewById(R.id.audioResponsePayload).setVisibility(View.GONE);
        findViewById(R.id.mediaIV).setVisibility(View.GONE);
        findViewById(R.id.mediaPayload).setVisibility(View.GONE);
        final String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            ((EditText)findViewById(R.id.captionET)).setHint("Text");
            ((EditText)findViewById(R.id.captionET)).setText(sharedText);

            findViewById(R.id.broadcast).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    JSONObject broadcast = Utils.packageBroadcast("text_broadcast", ((EditText)findViewById(R.id.captionET)).getText().toString(), "");
                    try {
                        broadcast.put("id", Utils.getStringFromSharedPrefs("self_token", "") + "_" + System.currentTimeMillis());
                        Utils.sendTextBroadcast(broadcast, ShareActivity.this);
                        Utils.sendBroadcast(ShareActivity.this, broadcast);
                    } catch (Exception e) {e.printStackTrace();}
                }
            });

            findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Send smalltalk to all those who were selected
                    finish();

                    // First prepare message:
                    Smalltalk.MyUser user = new Smalltalk.MyUser("sender", ""); // third param null, because we don't want do display our own pic
                    Smalltalk.MyMessage message = new Smalltalk.MyMessage("" + new Random().nextInt(), intent.getStringExtra(Intent.EXTRA_TEXT), user, new Date());

                    for (String token : selectedUsers) {
                        SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(ShareActivity.this).getReadableDatabase();
                        Cursor c = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                        c.moveToFirst();
                        Smalltalk.SmalltalkMessage smalltalk = (Smalltalk.SmalltalkMessage)Utils.deserialize(c.getString(c.getColumnIndexOrThrow("smalltalk")), Smalltalk.SmalltalkMessage.class);
                        if(token.startsWith("GROUP_")) {
                            smalltalk = Utils.addMessageToGroupSmalltalk(smalltalk, message);
                        } else {
                            smalltalk = Utils.addMessageToSmalltalk(smalltalk, message);
                        }

                        // update db
                        ContentValues cv = new ContentValues();
                        cv.put("smalltalk", new Gson().toJson(smalltalk));
                        SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(ShareActivity.this).getWritableDatabase();
                        dbWritable.update("connections", cv, "token=?", new String[]{token});

                        // send
                        Utils.sendSmalltalkMessage(sharedText, "text", token, ShareActivity.this);
                        if(token.startsWith("GROUP_")) {
                            Utils.sendSmalltalkToGroup(sharedText, "text", token, ShareActivity.this);
                        } else {
                            Utils.sendSmalltalkMessage(sharedText, "text", token, ShareActivity.this);
                        }
                    }
                }
            });
        }
    }

    void handleSendImage(Uri imageUri, final Intent intent) {
        findViewById(R.id.mediaIV).setVisibility(View.VISIBLE);
        findViewById(R.id.mediaPayload).setVisibility(View.VISIBLE);
        findViewById(R.id.audioResponsePayload).setVisibility(View.GONE);

        if (imageUri != null) {
            final String fileName = "broadcast_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis() % 100000000) + ".png";

            try {

                InputStream is = getContentResolver().openInputStream(imageUri);
                final File ourCopy = new File(getFilesDir(), fileName);
                ourCopy.createNewFile();
                Utils.copyInputStreamToFile(is, ourCopy);

                final byte[] bytes = Utils.fileToBytes(ourCopy);

                JSONObject response = new JSONObject();
                response.put("response", fileName);
                Utils.renderImageResponse(((ImageView) findViewById(R.id.mediaIV)), response, true);

                findViewById(R.id.broadcast).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                        Utils.uploadToFCS(ShareActivity.this, "Send image to your followers", fileName, bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                JSONObject broadcast = Utils.packageBroadcast("image_broadcast", ((EditText) findViewById(R.id.captionET)).getText().toString(), fileName);
                                try {
                                    broadcast.put("id", Utils.getStringFromSharedPrefs("self_token", "") + "_" + System.currentTimeMillis());
                                    Utils.sendMediaBroadcast(broadcast, ShareActivity.this);
                                    Utils.sendBroadcast(ShareActivity.this, broadcast);
                                } catch ( Exception e) {e.printStackTrace();}
                            }
                        });
                    }
                });

                findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Send smalltalk to all those who were selected

                        Utils.uploadToFCS(ShareActivity.this, "Send image to selected users", fileName, bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                // prepare message:
                                finish();
                                Smalltalk.MyUser user = new Smalltalk.MyUser("sender", ""); // third param null, because we don't want do display our own pic
                                final Smalltalk.MyMessage message = new Smalltalk.MyMessage("" + new Random().nextInt(), fileName, user, new Date());
                                message.setType("image_response");
                                message.setImgPath(fileName);

                                for (String token : selectedUsers) {
                                    SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(ShareActivity.this).getReadableDatabase();
                                    Cursor c = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                                    c.moveToFirst();
                                    Smalltalk.SmalltalkMessage smalltalk = (Smalltalk.SmalltalkMessage)Utils.deserialize(c.getString(c.getColumnIndexOrThrow("smalltalk")), Smalltalk.SmalltalkMessage.class);
                                    if(token.startsWith("GROUP_")) {
                                        smalltalk = Utils.addMessageToGroupSmalltalk(smalltalk, message);
                                    } else {
                                        smalltalk = Utils.addMessageToSmalltalk(smalltalk, message);
                                    }

                                    // update db
                                    ContentValues cv = new ContentValues();
                                    cv.put("smalltalk", new Gson().toJson(smalltalk));
                                    SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(ShareActivity.this).getWritableDatabase();
                                    dbWritable.update("connections", cv, "token=?", new String[]{token});

                                    // send
                                    if(token.startsWith("GROUP_")) {
                                        Utils.sendSmalltalkToGroup(fileName, "image_response", token, ShareActivity.this);
                                    } else {
                                        Utils.sendSmalltalkMessage(fileName, "image_response", token, ShareActivity.this);
                                    }
                                }
                            }
                        });
                    }
                });

            } catch (Exception e) {e.printStackTrace();
                Utils.showDialogMessage(this, "Sorry", "Sup couldn't process your file");
                finish();}
        }
    }

    void handleSendVideo(Uri videoUri) {
        findViewById(R.id.mediaIV).setVisibility(View.VISIBLE);
        findViewById(R.id.mediaPayload).setVisibility(View.VISIBLE);
        findViewById(R.id.audioResponsePayload).setVisibility(View.GONE);
        if (videoUri != null) {
            final String fileName = "broadcast_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis() % 100000000);

            try {

                InputStream is = getContentResolver().openInputStream(videoUri);
                final File ourCopy = new File(getFilesDir(), fileName+".mp4");
                ourCopy.createNewFile();
                Utils.copyInputStreamToFile(is, ourCopy);

                File output = new File(getFilesDir(), "snapshot_" + fileName+ ".png");
                output.createNewFile();
                Utils.snapshot(ourCopy, output, ShareActivity.this);

                final byte[] bytes = Utils.fileToBytes(ourCopy);

                JSONObject response = new JSONObject();
                response.put("response", fileName+".mp4");
                Utils.renderVideoResponse(((ImageView) findViewById(R.id.mediaIV)), response, true, ((ImageView) findViewById(R.id.playButton2)));

                findViewById(R.id.broadcast).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.uploadToFCS(ShareActivity.this, "Send video to your followers", fileName+".mp4", bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                finish();
                                JSONObject broadcast = Utils.packageBroadcast("video_broadcast", ((EditText) findViewById(R.id.captionET)).getText().toString(), fileName+".mp4");
                                try {
                                    broadcast.put("id", Utils.getStringFromSharedPrefs("self_token", "") + "_" + System.currentTimeMillis());
                                    Utils.sendMediaBroadcast(broadcast, ShareActivity.this);
                                    Utils.sendBroadcast(ShareActivity.this, broadcast);
                                } catch (Exception e) {e.printStackTrace();}
                            }
                        });
                    }
                });

                findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Send smalltalk to all those who were selected
                        finish();

                        Utils.uploadToFCS(ShareActivity.this, "Send video to selected users", fileName+".mp4", bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                // prepare message:
                                Smalltalk.MyUser user = new Smalltalk.MyUser("sender", ""); // third param null, because we don't want do display our own pic
                                final Smalltalk.MyMessage message = new Smalltalk.MyMessage("" + new Random().nextInt(), fileName+".mp4", user, new Date());
                                message.setType("video_response");
                                message.setImgPath("snapshot_" + fileName+".png");

                                for (String token : selectedUsers) {
                                    SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(ShareActivity.this).getReadableDatabase();
                                    Cursor c = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                                    c.moveToFirst();
                                    Smalltalk.SmalltalkMessage smalltalk = (Smalltalk.SmalltalkMessage)Utils.deserialize(c.getString(c.getColumnIndexOrThrow("smalltalk")), Smalltalk.SmalltalkMessage.class);
                                    if(token.startsWith("GROUP_")) {
                                        smalltalk = Utils.addMessageToGroupSmalltalk(smalltalk, message);
                                    } else {
                                        smalltalk = Utils.addMessageToSmalltalk(smalltalk, message);
                                    }

                                    // update db
                                    ContentValues cv = new ContentValues();
                                    cv.put("smalltalk", new Gson().toJson(smalltalk));
                                    SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(ShareActivity.this).getWritableDatabase();
                                    dbWritable.update("connections", cv, "token=?", new String[]{token});

                                    // send
                                    if(token.startsWith("GROUP_")) {
                                        Utils.sendSmalltalkToGroup(fileName+".mp4", "video_response", token, ShareActivity.this);
                                    } else {
                                        Utils.sendSmalltalkMessage(fileName+".mp4", "video_response", token, ShareActivity.this);
                                    }
                                }
                            }
                        });
                    }
                });
            } catch (Exception e) {e.printStackTrace();
                Utils.showDialogMessage(this, "Sorry", "Sup couldn't process your file");
                finish();}
        }
    }

    void handleSendAudio(Uri audioUri){
        findViewById(R.id.audioResponsePayload).setVisibility(View.VISIBLE);
        findViewById(R.id.mediaPayload).setVisibility(View.GONE);
        findViewById(R.id.mediaIV).setVisibility(View.GONE);

        if (audioUri != null) {
            final String fileName = "broadcast_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis() % 100000000) + ".3gpp";

            try {
                InputStream is = getContentResolver().openInputStream(audioUri);
                final File ourCopy = new File(getFilesDir(), fileName);
                ourCopy.createNewFile();
                Utils.copyInputStreamToFile(is, ourCopy);

                final byte[] bytes = Utils.fileToBytes(ourCopy);

                JSONObject response = new JSONObject();
                response.put("response", fileName);

                Utils.renderAudioResponse((AudioWaveView)findViewById(R.id.audioResponsePayload).findViewById(R.id.wave), findViewById(R.id.audioResponsePayload).findViewById(R.id.play_action), findViewById(R.id.audioResponsePayload).findViewById(R.id.cancel_action3), response);

                findViewById(R.id.broadcast).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                        Utils.uploadToFCS(ShareActivity.this, "Send audio to your followers", fileName, bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                JSONObject broadcast = Utils.packageBroadcast("audio_broadcast", ((EditText) findViewById(R.id.captionET)).getText().toString(), fileName);
                                try {
                                    broadcast.put("id", Utils.getStringFromSharedPrefs("self_token", "") + "_" + System.currentTimeMillis());
                                    Utils.sendMediaBroadcast(broadcast, ShareActivity.this);
                                    Utils.sendBroadcast(ShareActivity.this, broadcast);
                                } catch ( Exception e){e.printStackTrace();}
                            }
                        });
                    }
                });

                findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Send smalltalk to all those who were selected
                        finish();

                        Utils.uploadToFCS(ShareActivity.this, "Send audio to selected users", fileName, bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                // prepare message:
                                Smalltalk.MyUser user = new Smalltalk.MyUser("sender", ""); // third param null, because we don't want do display our own pic
                                final Smalltalk.MyMessage message = new Smalltalk.MyMessage("" + new Random().nextInt(), fileName, user, new Date());
                                message.setType("audio_response");
                                message.setImgPath(fileName);

                                for (String token : selectedUsers) {
                                    SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(ShareActivity.this).getReadableDatabase();
                                    Cursor c = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                                    c.moveToFirst();
                                    Smalltalk.SmalltalkMessage smalltalk = (Smalltalk.SmalltalkMessage)Utils.deserialize(c.getString(c.getColumnIndexOrThrow("smalltalk")), Smalltalk.SmalltalkMessage.class);
                                    if(token.startsWith("GROUP_")) {
                                        smalltalk = Utils.addMessageToGroupSmalltalk(smalltalk, message);
                                    } else {
                                        smalltalk = Utils.addMessageToSmalltalk(smalltalk, message);
                                    }

                                    // update db
                                    ContentValues cv = new ContentValues();
                                    cv.put("smalltalk", new Gson().toJson(smalltalk));
                                    SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(ShareActivity.this).getWritableDatabase();
                                    dbWritable.update("connections", cv, "token=?", new String[]{token});

                                    // send
                                    if(token.startsWith("GROUP_")) {
                                        Utils.sendSmalltalkToGroup(fileName, "audio_response", token, ShareActivity.this);
                                    } else {
                                        Utils.sendSmalltalkMessage(fileName, "audio_response", token, ShareActivity.this);
                                    }
                                }
                            }
                        });
                    }
                });
            } catch (Exception e) {e.printStackTrace();
                Utils.showDialogMessage(this, "Sorry", "Sup couldn't process your file");
                finish();}
        }
    }
}
