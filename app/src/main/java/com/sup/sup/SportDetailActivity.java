package com.sup.sup;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

public class SportDetailActivity extends AppCompatActivity {
    private JSONObject sport; private Handler handler = new Handler();

    private Runnable runnable = new Runnable() {
        // Updates sports scores every minute
        @Override
        public void run() {
            updateSportInfo();

            // Now load the play-by-play info
            try {
                String url = "http://black-abode-2709.appspot.com/queryPbP?league=%22" + sport.getString("league") +
                        "%22&team1=%22" + sport.getJSONObject("game").getJSONObject("homeTeam").getString("Abbreviation") + "%22&team2=%22"
                        + sport.getJSONObject("game").getJSONObject("awayTeam").getString("Abbreviation") + "%22";
                Utils.volleyJSONCall(SportDetailActivity.this, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        findViewById(R.id.pbpProgress).setVisibility(View.GONE);
                        ((ListView) findViewById(R.id.playByPlayLV)).setAdapter(new NBAPBPAdapter(SportDetailActivity.this, (JSONObject) result));
                    }
                });

            } catch (Exception e){e.printStackTrace();}
            handler.postDelayed(this, 60*1000);
        }
    };

    private void updateSportInfo(){
        Log.d("sport update", "sport update");
        try {
            Utils.getSportsScoresFromServer2(this, sport.getString("league"), sport.getJSONObject("game").getJSONObject("homeTeam").getString("Abbreviation"), new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    try {
                        sport = ((JSONArray) result).getJSONObject(0);
                        String league = sport.getString("league");
                        switch (league) {
                            case "MLB":
                                setContentView(R.layout.activity_sports_detail_mlb);
                                renderMLB();
                                break;
                            case "NFL":
                                setContentView(R.layout.activity_sports_detail_nfl);
                                renderNFL();
                                break;
                            case "NHL":
                                setContentView(R.layout.activity_sports_detail_nhl);
                                renderNHL();
                                break;
                            case "NBA":
                                setContentView(R.layout.activity_sports_detail_nba);
                                renderNBA();
                                break;

                        }
                    } catch (Exception e){e.printStackTrace();}
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler.postDelayed(runnable, 60*1000); // every minute

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        try {
            sport = new JSONObject(intent.getStringExtra("sport"));
            Log.d("sport", sport.toString());
            setTitle(sport.getJSONObject("game").getJSONObject("homeTeam").getString("Abbreviation")+" v. "+sport.getJSONObject("game").getJSONObject("awayTeam").getString("Abbreviation"));
            String league = sport.getString("league");
            switch (league) {
                case "MLB":
                    setContentView(R.layout.activity_sports_detail_mlb);
                    renderMLB();
                    break;
                case "NFL":
                    setContentView(R.layout.activity_sports_detail_nfl);
                    renderNFL();
                    break;
                case "NHL":
                    setContentView(R.layout.activity_sports_detail_nhl);
                    renderNHL();
                    break;
                case "NBA":
                    setContentView(R.layout.activity_sports_detail_nba);
                    renderNBA();
                    break;

            }
        } catch (Exception e) {e.printStackTrace();}
    }

    private void renderNBA() {
        try {
            String homeTeam = sport.getJSONObject("game").getJSONObject("homeTeam").getString("Name");
            String homeCity = sport.getJSONObject("game").getJSONObject("homeTeam").getString("City");
            String homeAbbr = sport.getJSONObject("game").getJSONObject("homeTeam").getString("Abbreviation");
            String awayTeam = sport.getJSONObject("game").getJSONObject("awayTeam").getString("Name");
            String awayCity = sport.getJSONObject("game").getJSONObject("awayTeam").getString("City");
            String awayAbbr = sport.getJSONObject("game").getJSONObject("awayTeam").getString("Abbreviation");
            String homeScore = (sport.has("homeScore")?sport.getString("homeScore"):" N/A ");
            String awayScore = (sport.has("awayScore")?sport.getString("awayScore"):" N/A ");
            String venue = sport.getJSONObject("game").getString("location");
            String date  = sport.getJSONObject("game").getString("time");
            if(Boolean.valueOf(sport.getString("isUnplayed"))) {
                date = "Starts at "+date;
            } else {
                date = "Started at "+date;
            }
            ((TextView) findViewById(R.id.homeCityNameTV)).setText(homeCity);
            ((TextView) findViewById(R.id.homeTeamNameTV)).setText(homeTeam);
            ((TextView) findViewById(R.id.homeTeamAbbrTV)).setText(homeAbbr);
            ((TextView) findViewById(R.id.awayCityNameTV)).setText(awayCity);
            ((TextView) findViewById(R.id.awayTeamNameTV)).setText(awayTeam);
            ((TextView) findViewById(R.id.awayTeamAbbrTV)).setText(awayAbbr);
            ((TextView) findViewById(R.id.awayTeamScoreTV)).setText(awayScore);
            ((TextView) findViewById(R.id.homeTeamScoreTV)).setText(homeScore);
            ((TextView) findViewById(R.id.dateTV)).setText(date);
            ((TextView) findViewById(R.id.venueTV)).setText(venue);

            TextView[] awayQuarterTVs = new TextView[] {
                    ((TextView) findViewById(R.id.awayquarter1TV)),
                    ((TextView) findViewById(R.id.awayquarter2TV)),
                    ((TextView) findViewById(R.id.awayquarter3TV)),
                    ((TextView) findViewById(R.id.awayquarter4TV))
            };
            TextView[] homeQuarterTVs = new TextView[] {
                    ((TextView) findViewById(R.id.homequarter1TV)),
                    ((TextView) findViewById(R.id.homequarter2TV)),
                    ((TextView) findViewById(R.id.homequarter3TV)),
                    ((TextView) findViewById(R.id.homequarter4TV)),
            };

            if(sport.has("quarterSummary") && !sport.isNull("quarterSummary")) {
                for (int i = 0; i < sport.getJSONObject("quarterSummary").getJSONArray("quarter").length(); i++) {
                    homeQuarterTVs[i].setText(sport.getJSONObject("quarterSummary").getJSONArray("quarter").getJSONObject(i).getString("homeScore"));
                    awayQuarterTVs[i].setText(sport.getJSONObject("quarterSummary").getJSONArray("quarter").getJSONObject(i).getString("awayScore"));
                }

                // Now load the play-by-play info

                String url = "http://black-abode-2709.appspot.com/queryPbP?league=%22NBA%22&team1=%22"+homeAbbr+"%22&team2=%22"+awayAbbr+"%22";
                Utils.volleyJSONCall(this, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        findViewById(R.id.pbpProgress).setVisibility(View.GONE);
                        ((ListView)findViewById(R.id.playByPlayLV)).setAdapter(new NBAPBPAdapter(SportDetailActivity.this, (JSONObject) result));
                    }
                });
            } else {
                findViewById(R.id.playByPlayCard).setVisibility(View.GONE);
            }
        } catch(Exception e){e.printStackTrace();}
    }

    private void renderNHL(){
        try {
            String homeTeam = sport.getJSONObject("game").getJSONObject("homeTeam").getString("Name");
            String homeCity = sport.getJSONObject("game").getJSONObject("homeTeam").getString("City");
            String homeAbbr = sport.getJSONObject("game").getJSONObject("homeTeam").getString("Abbreviation");
            String awayTeam = sport.getJSONObject("game").getJSONObject("awayTeam").getString("Name");
            String awayCity = sport.getJSONObject("game").getJSONObject("awayTeam").getString("City");
            String awayAbbr = sport.getJSONObject("game").getJSONObject("awayTeam").getString("Abbreviation");
            String homeScore = (sport.has("homeScore")?sport.getString("homeScore"):" N/A ");
            String awayScore = (sport.has("awayScore")?sport.getString("awayScore"):" N/A ");
            ((TextView) findViewById(R.id.homeCityNameTV)).setText(homeCity);
            ((TextView) findViewById(R.id.homeTeamNameTV)).setText(homeTeam);
            ((TextView) findViewById(R.id.homeTeamAbbrTV)).setText(homeAbbr);
            ((TextView) findViewById(R.id.awayCityNameTV)).setText(awayCity);
            ((TextView) findViewById(R.id.awayTeamNameTV)).setText(awayTeam);
            ((TextView) findViewById(R.id.awayTeamAbbrTV)).setText(awayAbbr);
            ((TextView) findViewById(R.id.awayTeamScoreTV)).setText(awayScore);
            ((TextView) findViewById(R.id.homeTeamScoreTV)).setText(homeScore);
            String venue = sport.getJSONObject("game").getString("location");
            String date  = sport.getJSONObject("game").getString("time");
            if(Boolean.valueOf(sport.getString("isUnplayed"))) {
                date = "Starts at "+date;
            } else {
                date = "Started at "+date;
            }
            ((TextView) findViewById(R.id.dateTV)).setText(date);
            ((TextView) findViewById(R.id.venueTV)).setText(venue);
            TextView[] awayInningTVs = new TextView[] {
                    ((TextView) findViewById(R.id.awayperiod1TV)),
                    ((TextView) findViewById(R.id.awayperiod2TV)),
                    ((TextView) findViewById(R.id.awayperiod3TV)),
                    ((TextView) findViewById(R.id.awayperiod4TV))
            };
            TextView[] homeInningTVs = new TextView[] {
                    ((TextView) findViewById(R.id.homeperiod1TV)),
                    ((TextView) findViewById(R.id.homeperiod2TV)),
                    ((TextView) findViewById(R.id.homeperiod3TV)),
                    ((TextView) findViewById(R.id.homeperiod4TV)),
            };
            int homeSum = 0, awaySum = 0;
            if(sport.has("periodSummary") && !sport.isNull("periodSummary")) {
                for (int i = 0; i < sport.getJSONObject("periodSummary").getJSONArray("period").length(); i++) {
                    homeInningTVs[i].setText(sport.getJSONObject("periodSummary").getJSONArray("period").getJSONObject(i).getString("homeScore"));
                    homeSum += Integer.valueOf(sport.getJSONObject("periodSummary").getJSONArray("period").getJSONObject(i).getString("homeScore"));
                    awayInningTVs[i].setText(sport.getJSONObject("periodSummary").getJSONArray("period").getJSONObject(i).getString("awayScore"));
                    awaySum += Integer.valueOf(sport.getJSONObject("periodSummary").getJSONArray("period").getJSONObject(i).getString("awayScore"));
                    if (i > 2) {
                        homeInningTVs[3].setText(String.valueOf((Integer.valueOf(homeScore)) - homeSum));
                        awayInningTVs[3].setText(String.valueOf((Integer.valueOf(awayScore)) - awaySum));
                        break;
                    }
                }

                // Now load the play-by-play info

                String url = "http://black-abode-2709.appspot.com/queryPbP?league=%22NHL%22&team1=%22"+homeAbbr+"%22&team2=%22"+awayAbbr+"%22";
                Utils.volleyJSONCall(this, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        findViewById(R.id.pbpProgress).setVisibility(View.GONE);
                        ((ListView)findViewById(R.id.playByPlayLV)).setAdapter(new NHLPBPAdapter(SportDetailActivity.this, (JSONObject) result));
                    }
                });
            } else {
                findViewById(R.id.playByPlayCard).setVisibility(View.GONE);
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    private void renderNFL(){
        try {
            String homeTeam = sport.getJSONObject("game").getJSONObject("homeTeam").getString("Name");
            String homeCity = sport.getJSONObject("game").getJSONObject("homeTeam").getString("City");
            String homeAbbr = sport.getJSONObject("game").getJSONObject("homeTeam").getString("Abbreviation");
            String awayTeam = sport.getJSONObject("game").getJSONObject("awayTeam").getString("Name");
            String awayCity = sport.getJSONObject("game").getJSONObject("awayTeam").getString("City");
            String awayAbbr = sport.getJSONObject("game").getJSONObject("awayTeam").getString("Abbreviation");
            String homeScore = (sport.has("homeScore")?sport.getString("homeScore"):" N/A ");
            String awayScore = (sport.has("awayScore")?sport.getString("awayScore"):" N/A ");
            ((TextView) findViewById(R.id.homeCityNameTV)).setText(homeCity);
            ((TextView) findViewById(R.id.homeTeamNameTV)).setText(homeTeam);
            ((TextView) findViewById(R.id.homeTeamAbbrTV)).setText(homeAbbr);
            ((TextView) findViewById(R.id.awayCityNameTV)).setText(awayCity);
            ((TextView) findViewById(R.id.awayTeamNameTV)).setText(awayTeam);
            ((TextView) findViewById(R.id.awayTeamAbbrTV)).setText(awayAbbr);
            ((TextView) findViewById(R.id.awayTeamScoreTV)).setText(awayScore);
            ((TextView) findViewById(R.id.homeTeamScoreTV)).setText(homeScore);
            String venue = sport.getJSONObject("game").getString("location");
            String date  = sport.getJSONObject("game").getString("time");
            if(Boolean.valueOf(sport.getString("isUnplayed"))) {
                date = "Starts at "+date;
            } else {
                date = "Started at "+date;
            }
            ((TextView) findViewById(R.id.dateTV)).setText(date);
            ((TextView) findViewById(R.id.venueTV)).setText(venue);

            TextView[] awayQuarterTVs = new TextView[] {
                    ((TextView) findViewById(R.id.awayquarter1TV)),
                    ((TextView) findViewById(R.id.awayquarter2TV)),
                    ((TextView) findViewById(R.id.awayquarter3TV)),
                    ((TextView) findViewById(R.id.awayquarter4TV))
            };
            TextView[] homeQuarterTVs = new TextView[] {
                    ((TextView) findViewById(R.id.homequarter1TV)),
                    ((TextView) findViewById(R.id.homequarter2TV)),
                    ((TextView) findViewById(R.id.homequarter3TV)),
                    ((TextView) findViewById(R.id.homequarter4TV)),
            };

            if(sport.has("quarterSummary") && !sport.isNull("quarterSummary")) {
                for (int i = 0; i < sport.getJSONObject("quarterSummary").getJSONArray("quarter").length(); i++) {
                    homeQuarterTVs[i].setText(sport.getJSONObject("quarterSummary").getJSONArray("quarter").getJSONObject(i).getString("homeScore"));
                    awayQuarterTVs[i].setText(sport.getJSONObject("quarterSummary").getJSONArray("quarter").getJSONObject(i).getString("awayScore"));
                }

                // Now load the play-by-play info

                String url = "http://black-abode-2709.appspot.com/queryPbP?league=%22NFL%22&team1=%22"+homeAbbr+"%22&team2=%22"+awayAbbr+"%22";
                Utils.volleyJSONCall(this, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        findViewById(R.id.pbpProgress).setVisibility(View.GONE);
                        ((ListView)findViewById(R.id.playByPlayLV)).setAdapter(new NFLPBPAdapter(SportDetailActivity.this, (JSONObject) result));
                    }
                });
            } else {
                findViewById(R.id.playByPlayCard).setVisibility(View.GONE);
            }
        } catch(Exception e){e.printStackTrace();}
    }

    private void renderMLB(){
        try {
            String homeTeam = sport.getJSONObject("game").getJSONObject("homeTeam").getString("Name");
            String homeCity = sport.getJSONObject("game").getJSONObject("homeTeam").getString("City");
            String homeAbbr = sport.getJSONObject("game").getJSONObject("homeTeam").getString("Abbreviation");
            String awayTeam = sport.getJSONObject("game").getJSONObject("awayTeam").getString("Name");
            String awayCity = sport.getJSONObject("game").getJSONObject("awayTeam").getString("City");
            String awayAbbr = sport.getJSONObject("game").getJSONObject("awayTeam").getString("Abbreviation");
            String homeScore = (sport.has("homeScore")?sport.getString("homeScore"):" N/A ");
            String awayScore = (sport.has("awayScore")?sport.getString("awayScore"):" N/A ");
            String venue = sport.getJSONObject("game").getString("location");
            String date  = sport.getJSONObject("game").getString("time");
            if(Boolean.valueOf(sport.getString("isUnplayed"))) {
                date = "Starts at "+date;
            } else {
                date = "Started at "+date;
            }
            ((TextView) findViewById(R.id.homeCityNameTV)).setText(homeCity);
            ((TextView) findViewById(R.id.homeTeamNameTV)).setText(homeTeam);
            ((TextView) findViewById(R.id.homeTeamAbbrTV)).setText(homeAbbr);
            ((TextView) findViewById(R.id.awayCityNameTV)).setText(awayCity);
            ((TextView) findViewById(R.id.awayTeamNameTV)).setText(awayTeam);
            ((TextView) findViewById(R.id.awayTeamAbbrTV)).setText(awayAbbr);
            ((TextView) findViewById(R.id.awayTeamScoreTV)).setText(awayScore);
            ((TextView) findViewById(R.id.homeTeamScoreTV)).setText(homeScore);
            ((TextView) findViewById(R.id.dateTV)).setText(date);
            ((TextView) findViewById(R.id.venueTV)).setText(venue);
            TextView[] awayInningTVs = new TextView[] {
                    ((TextView) findViewById(R.id.awayInning1TV)),
                    ((TextView) findViewById(R.id.awayInning2TV)),
                    ((TextView) findViewById(R.id.awayInning3TV)),
                    ((TextView) findViewById(R.id.awayInning4TV)),
                    ((TextView) findViewById(R.id.awayInning5TV)),
                    ((TextView) findViewById(R.id.awayInning6TV)),
                    ((TextView) findViewById(R.id.awayInning7TV)),
                    ((TextView) findViewById(R.id.awayInning8TV)),
                    ((TextView) findViewById(R.id.awayInning9TV)),
                    ((TextView) findViewById(R.id.awayInningETV)),
            };
            TextView[] homeInningTVs = new TextView[] {
                    ((TextView) findViewById(R.id.homeInning1TV)),
                    ((TextView) findViewById(R.id.homeInning2TV)),
                    ((TextView) findViewById(R.id.homeInning3TV)),
                    ((TextView) findViewById(R.id.homeInning4TV)),
                    ((TextView) findViewById(R.id.homeInning5TV)),
                    ((TextView) findViewById(R.id.homeInning6TV)),
                    ((TextView) findViewById(R.id.homeInning7TV)),
                    ((TextView) findViewById(R.id.homeInning8TV)),
                    ((TextView) findViewById(R.id.homeInning9TV)),
                    ((TextView) findViewById(R.id.homeInningETV)),
            };
            int homeSum = 0, awaySum = 0;
            if(sport.has("inningSummary") && !sport.isNull("inningSummary")) {
                for (int i = 0; i < sport.getJSONObject("inningSummary").getJSONArray("inning").length(); i++) {
                    homeInningTVs[i].setText(sport.getJSONObject("inningSummary").getJSONArray("inning").getJSONObject(i).getString("homeScore"));
                    homeSum += Integer.valueOf(sport.getJSONObject("inningSummary").getJSONArray("inning").getJSONObject(i).getString("homeScore"));
                    awayInningTVs[i].setText(sport.getJSONObject("inningSummary").getJSONArray("inning").getJSONObject(i).getString("awayScore"));
                    awaySum += Integer.valueOf(sport.getJSONObject("inningSummary").getJSONArray("inning").getJSONObject(i).getString("awayScore"));
                    if (i > 9) {
                        homeInningTVs[9].setText(String.valueOf((Integer.valueOf(homeScore)) - homeSum));
                        awayInningTVs[9].setText(String.valueOf((Integer.valueOf(awayScore)) - awaySum));
                        break;
                    }
                }

                // Now load the play-by-play info

                String url = "http://black-abode-2709.appspot.com/queryPbP?league=%22MLB%22&team1=%22"+homeAbbr+"%22&team2=%22"+awayAbbr+"%22";
                Utils.volleyJSONCall(this, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        findViewById(R.id.pbpProgress).setVisibility(View.GONE);
                        ((ListView)findViewById(R.id.playByPlayLV)).setAdapter(new MLBPBPAdapter(SportDetailActivity.this, (JSONObject) result));
                    }
                });
            } else {
                findViewById(R.id.playByPlayCard).setVisibility(View.GONE);
            }
        } catch (Exception e) {e.printStackTrace();}
    }
}
