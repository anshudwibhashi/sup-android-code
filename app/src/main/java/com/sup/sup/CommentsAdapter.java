package com.sup.sup;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CommentsAdapter  extends BaseAdapter {
    private LayoutInflater mInflater;
    Context context; Comments comments;

    public CommentsAdapter(Context context, Comments comments) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.comments = comments;
    }

    @Override
    public int getCount() {
        return comments.users.size();
    }

    @Override
    public String getItem(int position) {
        return null; // useless to us
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_item_comments, null);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.commentTV = (TextView) convertView.findViewById(R.id.commentTV);
            holder.usernameTV = (TextView) convertView.findViewById(R.id.usernameTV);
            holder.photoIV = (ImageView) convertView.findViewById(R.id.photoIV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.nameTV.setText(comments.users.get(position).name);
        holder.commentTV.setText(Html.fromHtml(comments.users.get(position).comment.replaceAll("\\@([a-zA-Z0-9\\._\\-]+)", "<font color=\"#E040FB\">@$1</font>")));

        Typeface font = Typeface.createFromAsset(context.getAssets(), "RobotoCondensed-Light.ttf");
        holder.commentTV.setTypeface(font);

        if (Utils.getStringFromSharedPrefs("self_following", "[]").contains(comments.users.get(position).token)) {
            // Already a contact. Load image from disk.
            int position2;
            for (position2 = 0; position2 < Utils.MainDataSet.names.size(); position2++) {
                if (Utils.MainDataSet.tokens.get(position2).equals(comments.users.get(position).token))
                    break;
            }
            Glide.with(context)
                    .load(Utils.MainDataSet.photos.get(position2))
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(context))
                    .placeholder(R.drawable.ic_default_picture)
                    .into(holder.photoIV);
        } else if(comments.users.get(position).token.equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
            Glide.with(context)
                    .load(new File(context.getFilesDir(), "self_photo.png"))
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(context))
                    .placeholder(R.drawable.ic_default_picture)
                    .into(holder.photoIV);
        } else {
            Glide.with(context)
                    .load(comments.users.get(position).photoUrl)
                    .centerCrop()
                    .transform(new CircleTransform(context))
                    .override(100, 100)
                    .placeholder(R.drawable.ic_default_picture)
                    .into(holder.photoIV);
        }

        holder.usernameTV.setText("@"+comments.users.get(position).username+" · "+SimpleDateFormat.getTimeInstance(DateFormat.SHORT).format(new Date(Long.parseLong(comments.users.get(position).timeStamp))));

        convertView.setEnabled(false);
        convertView.setOnClickListener(null);

        return convertView;
    }
    public static class ViewHolder {
        public TextView nameTV, commentTV, usernameTV;
        public ImageView photoIV;
    }
}