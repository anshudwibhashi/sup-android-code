package com.sup.sup;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import org.json.JSONArray;

/**
 * Created by anshudwibhashi on 08/10/17.
 */

public class StockPriceListingsClickListener implements AdapterView.OnItemClickListener {
    JSONArray stocks; Context context;
    public StockPriceListingsClickListener(JSONArray stocks, Context context) {
        this.stocks = stocks;
        this.context = context;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            context.startActivity(new Intent(context, ShowStockInfoActivity.class).putExtra("stock_info", stocks.getJSONObject(position).toString()));
        } catch (Exception e) {e.printStackTrace();}
    }
}
