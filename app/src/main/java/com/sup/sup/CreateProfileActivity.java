package com.sup.sup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * This class is named "Create"ProfileActivity for legacy reasons,
 * but is used for both creating and editing the users' profiles.
 */

public class CreateProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText displayNameET, taglineET, usernameET;
    private ImageView profilePicture;
    private Button changePictureButton;

    private boolean pictureChanged = false;
    private Bitmap picture;

    private boolean editing = false; // Whether or not this activity is being called for editing a profile

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        displayNameET = (EditText) findViewById(R.id.displayNameET);
        taglineET = (EditText) findViewById(R.id.taglineET);
        usernameET = (EditText) findViewById(R.id.usernameET);
        profilePicture = (ImageView) findViewById(R.id.profilePicture);
        changePictureButton = (Button) findViewById(R.id.changePictureButton);

        try {
            String filePath = this.getFilesDir() + "/" + "self_photo.png";
            File file = new File( filePath );
            picture = BitmapFactory.decodeStream(new FileInputStream(file));
        }catch(Exception e){e.printStackTrace();}

        displayNameET.setText(Utils.getStringFromSharedPrefs("self_name", ""));
        taglineET.setText(Utils.getStringFromSharedPrefs("self_tagline", "Available"));
        usernameET.setText(Utils.getStringFromSharedPrefs("self_username", ""));
        profilePicture.setImageBitmap(picture);

        changePictureButton.setOnClickListener(this);

        editing = getIntent().getBooleanExtra("editing", false);
        if(editing) getSupportActionBar().setTitle("Edit profile");
        else  getSupportActionBar().setTitle("Create profile");

        if(Utils.getStringFromSharedPrefs("self_photoUrl", "").isEmpty()){
            findViewById(R.id.removePictureButton).setEnabled(false);
            findViewById(R.id.removePictureButton).setAlpha(.5f);
        } else {
            findViewById(R.id.removePictureButton).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.changePictureButton:
                changePicture();
                break;
            case R.id.removePictureButton:
                removePicture();
                break;
        }
    }

    private void changePicture(){
        /**
         * Allows user to pick a new picture.
         */
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Choose picture") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .limit(1) // max images can be selected (999 by default)
                .start(Constants.REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    private void removePicture(){
        picture = BitmapFactory.decodeResource(getResources(), R.drawable.ic_default_picture);
        profilePicture.setImageBitmap(picture);

        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream(new File(this.getFilesDir(), "self_photo.png"));
            picture.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Utils.putStringInSharedPrefs("self_photoUrl", "");

        // Delete from FCS.
        final String token = Utils.getStringFromSharedPrefs("self_token", "");
        Utils.deleteFromFCS(token + "_photo.png", new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                // Now update profile
                Utils.volleyStringCall(CreateProfileActivity.this, "http://black-abode-2709.appspot.com/update_profile?token=" + token + "&photoUrl=+", new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        Utils.showToast(CreateProfileActivity.this, "Settings saved");
                        Utils.deleteFromFCS(token + "_photo_mini.png", new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                            }
                        });
                    }
                });
            }
        });

        findViewById(R.id.removePictureButton).setEnabled(false);
        findViewById(R.id.removePictureButton).setAlpha(.5f);
    }

    @Override
    public void onBackPressed() {
        if(editing){
            super.onBackPressed();
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            // Now we ask server to delete this profile, since they cancelled the creation of their profile
            deleteProfile();
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if(editing){
                    finish(); return true;
                } else {
                    startActivity(new Intent(this, LoginActivity.class));
                    // Now we ask server to delete this profile, since they cancelled the creation of their profile
                    deleteProfile();
                    finish();
                    return true;
                }
            case R.id.save_profile:
                saveProfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deleteProfile(){
        String url ="http://black-abode-2709.appspot.com/delete_user?token="+ URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", ""));
        Utils.volleyStringCall(this, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {

            }
        });

        // Now delete all that's stored in SharedPrefs
        SharedPreferences.Editor editor = Utils.mSharedPreferences.edit();
        editor.clear();
        editor.apply();

        // Now delete all that's in the db
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
        db.delete("connections", null, null);

        // Now delete all files in internal storage
        File dir = getFilesDir();
        String[] children = dir.list();
        for (String child : children) {
            new File(dir, child).delete();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        return true;
    }

    String url;
    ProgressDialog pd;
    String displayName, tagline, username;

    private void saveProfile(){
        displayName = ((EditText) findViewById(R.id.displayNameET)).getText().toString().trim();
        tagline = ((EditText) findViewById(R.id.taglineET)).getText().toString().trim();
        username = ((EditText) findViewById(R.id.usernameET)).getText().toString().trim();

        pd = new ProgressDialog(this);
        pd.setMessage("Saving");
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        if(!Utils.isNetworkAvailable(this)){
            Utils.showToast(this, "No internet connection");
            pd.dismiss();
            return;
        }

        if(displayName.isEmpty() || tagline.isEmpty() || username.isEmpty()){
            Utils.showToast(this, "You must have a name, a username, and a tagline");
            pd.dismiss();
            return;
        }

        if(displayName.length() > 25){
            Utils.showToast(this,"Display name must be shorter than 25 characters");
            pd.dismiss();
            return;
        }
        if (tagline.length() > 100){
            Utils.showToast(this,"Tagline must be shorter than 100 characters");
            pd.dismiss();
            return;
        }
        if(username.length() > 25) {
            Utils.showToast(this,"Username must be shorter than 25 characters");
            pd.dismiss();
            return;
        }
        Pattern p = Pattern.compile("[^a-zA-Z0-9_\\.\\-]");
        if(p.matcher(username).find()){
            Utils.showToast(this,"Username can't have special characters or spaces");
            pd.dismiss();
            return;
        }

        Utils.volleyStringCall(this, "http://black-abode-2709.appspot.com/username_taken?username=" + username+"&token="+Utils.getStringFromSharedPrefs("self_token",""), new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                if(result.equals("1")) {
                    pd.dismiss();
                    Utils.showToast(CreateProfileActivity.this, "Username taken");
                } else {
                    saveProfilePostValidation();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);
            picture = BitmapFactory.decodeFile(images.get(0).getPath());
            profilePicture.setImageBitmap(picture);
            pictureChanged = true;
        }
    }

    private void saveProfilePostValidation() {
        url = "http://black-abode-2709.appspot.com/update_profile?token="+URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", ""))+"&displayName="+URLEncoder.encode(displayName)+"&tagline="+URLEncoder.encode(tagline)+"&username="+URLEncoder.encode(username);

        if(pictureChanged) {
            // Save new picture in self_photo.png
            FileOutputStream outputStream;
            try {
                File file = new File(getFilesDir(), "self_photo.png");
                file.createNewFile();
                outputStream = new FileOutputStream(file);
                picture.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Now upload it to FCS. Filenames are of format "<token>_photo.png"

        if(pictureChanged || !editing) {
            // either the picture has changed or they're creating a profile for the first time

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] data = baos.toByteArray();

            Utils.uploadToFCS(this, "Saving your picture", Utils.getStringFromSharedPrefs("self_token", "") + "_photo.png", data, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    url += "&photoUrl=" + result;
                    final String photoUrl = (String) result;
                    // Now Volley magic
                    Utils.volleyStringCall(CreateProfileActivity.this, url, new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            Utils.updateSelfProfile(displayName, photoUrl, tagline, username);
                            pd.dismiss();
                            if (editing) {
                                finish();
                            } else {
                                startActivity(new Intent(CreateProfileActivity.this, HomeActivity.class));
                                finish();
                            }
                        }
                    });
                }
            });

            // Small pic for fast loading
            ByteArrayOutputStream baosmini = new ByteArrayOutputStream();
            Bitmap mini = Utils.resize(picture, 100, 100);
            mini.compress(Bitmap.CompressFormat.PNG, 100, baosmini);
            byte[] data_mini = baosmini.toByteArray();

            Utils.uploadToFCS(this, null, Utils.getStringFromSharedPrefs("self_token", "") + "_photo_mini.png", data_mini, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    // Don't care...
                }
            });
        } else {
            // picture hasn't changed, and they're editing
            Utils.volleyStringCall(CreateProfileActivity.this, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    Utils.updateSelfProfile(displayName, Utils.getStringFromSharedPrefs("self_photoUrl", ""), tagline, username);
                    pd.dismiss();
                    finish();
                }
            });
        }
    }
}
