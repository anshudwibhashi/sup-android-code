package com.sup.sup;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

public class NFLPBPAdapter extends BaseAdapter {
    Context context; JSONArray pbp;
    public NFLPBPAdapter(Context context, JSONObject pbp) {
        this.context = context; this.pbp = processPbP(pbp);
    }
    @Override
    public int getCount() {
        try {
            return pbp.length();
        } catch (Exception e) {e.printStackTrace(); return 0;}
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  ((Activity)context).getLayoutInflater().inflate(R.layout.list_item_nfl_plays, null);
            holder.descTV = (TextView) convertView.findViewById(R.id.descTV);
            holder.timeTV = (TextView) convertView.findViewById(R.id.timeTV);
            holder.quarterTV = (TextView) convertView.findViewById(R.id.quarterTV);
            holder.downAndYardsTV = (TextView) convertView.findViewById(R.id.downAndYardsTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            JSONObject play = pbp.getJSONObject(pbp.length() - position - 1); // for reverse
            holder.descTV.setText(play.getString("description"));
            holder.timeTV.setText(play.getString("time"));
            holder.quarterTV.setText("Q"+play.getString("quarter"));
            holder.downAndYardsTV.setText(play.getString("currentDown")+" & "+play.getString("yardsRemaining"));
        } catch (Exception e) { e.printStackTrace();}

        return convertView;
    }

    public static class ViewHolder {
        public TextView descTV, timeTV, quarterTV, downAndYardsTV;
    }

    private JSONArray processPbP(JSONObject pbp) {
        try {
            return pbp.getJSONObject("gameplaybyplay").getJSONObject("plays").getJSONArray("play");

        } catch (Exception e) {e.printStackTrace(); return null;}
    }
    private String processCase(String s) {
        s = s.replace("_", " ");
        final String ACTIONABLE_DELIMITERS = " '-/"; // these cause the character following
        // to be capitalized

        StringBuilder sb = new StringBuilder();
        boolean capNext = true;

        for (char c : s.toCharArray()) {
            c = (capNext)
                    ? Character.toUpperCase(c)
                    : Character.toLowerCase(c);
            sb.append(c);
            capNext = (ACTIONABLE_DELIMITERS.indexOf((int) c) >= 0); // explicit cast not needed
        }
        return sb.toString();
    }
}
