package com.sup.sup;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;

/**
 * Created by anshudwibhashi on 08/10/17.
 */

public class SportsListAdapter extends BaseAdapter {
    JSONArray sports; Activity activity;
    public SportsListAdapter(JSONArray sports, Activity activity) {
        this.sports = sports;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        return sports.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  activity.getLayoutInflater().inflate(R.layout.list_item_score, null);
            holder.homeScoreTV = (TextView) convertView.findViewById(R.id.homeScoreTV);
            holder.awayScoreTV = (TextView) convertView.findViewById(R.id.awayScoreTV);
            holder.leagueTV = (TextView) convertView.findViewById(R.id.leagueTV);
            holder.awayTeamTV = (TextView) convertView.findViewById(R.id.awayTeamTV);
            holder.homeTeamTV = (TextView) convertView.findViewById(R.id.homeTeamTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            if(sports.getJSONObject(position).has("awayScore"))
                holder.awayScoreTV.setText(sports.getJSONObject(position).getString("awayScore"));
            else
                holder.awayScoreTV.setText("N/A");

            if(sports.getJSONObject(position).has("homeScore"))
                holder.homeScoreTV.setText(sports.getJSONObject(position).getString("homeScore"));
            else
                holder.homeScoreTV.setText("N/A");

            holder.homeTeamTV.setText(sports.getJSONObject(position).getJSONObject("game").getJSONObject("homeTeam").getString("Name"));
            holder.awayTeamTV.setText(sports.getJSONObject(position).getJSONObject("game").getJSONObject("awayTeam").getString("Name"));

            if(sports.getJSONObject(position).getString("isInProgress").equals("true")) {
                holder.leagueTV.setText("IN PLAY");
            } else if(sports.getJSONObject(position).getString("isCompleted").equals("true")) {
                holder.leagueTV.setText("CONCLUDED");
            } else {
                holder.leagueTV.setText("SCHEDULED");
            }
        } catch (Exception e ) {e.printStackTrace();}


        return convertView;
    }

    public static class ViewHolder {
        public TextView homeScoreTV, awayScoreTV, leagueTV, awayTeamTV, homeTeamTV;
    }
}
