package com.sup.sup;

import android.app.Activity;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.synnapps.carouselview.ViewListener;

import net.vrallev.android.cat.Cat;

import org.json.JSONArray;
import org.json.JSONObject;

import rm.com.audiowave.AudioWaveView;

import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;

/**
 * Created by anshudwibhashi on 04/01/18.
 */

public class CarouselViewListener implements ViewListener {
    Activity context;
    public CarouselViewListener(Activity context) {
        try {
            this.context = context;
        } catch (Exception e){e.printStackTrace();}
    }
    public int bigListPosition = 0;
    @Override
    public View setViewForPosition(int position) {
        View customView;
        JSONArray nonIgnoredResponses = new JSONArray();
        try {
            final JSONArray responseList = Utils.MainDataSet.responses.getJSONObject(bigListPosition).getJSONArray("responseList");
            for(int i = 0; i < responseList.length(); i++) {
                if(!new JSONObject(responseList.getString(i)).getString("type").equals("ignored")) {
                    nonIgnoredResponses.put(new JSONObject(responseList.getString(i)));
                }
            }

            JSONObject response = nonIgnoredResponses.getJSONObject(position);
            String username = "You responded";
            if(response.has("sender_username")) {
                username = "@"+response.getString("sender_username")+" responded";
            }
            switch (response.getString("type")) {
                case "text_response":
                    customView = context.getLayoutInflater().inflate(R.layout.carousel_text_response, null);
                    Utils.renderTextResponse((TextView) customView.findViewById(R.id.textResponseTV), response);
                    break;
                case "image_response":
                    customView = context.getLayoutInflater().inflate(R.layout.carousel_image_response, null);
                    Utils.renderImageResponse((ImageView) customView.findViewById(R.id.imageResponseIV), response, true);
                    ((TextView) customView.findViewById(R.id.usernameTV)).setTextAppearance(context,
                            R.style.OverlayText);
                    break;
                case "audio_response":
                    customView = context.getLayoutInflater().inflate(R.layout.carousel_audio_response, null);
                    Utils.renderAudioResponse((AudioWaveView) customView.findViewById(R.id.wave), (ImageView) customView.findViewById(R.id.play_action), (ImageView) customView.findViewById(R.id.cancel_action3), response);
                    break;
                case "video_response":
                    customView = context.getLayoutInflater().inflate(R.layout.carousel_video_response, null);
                    Utils.renderVideoResponse((ImageView) customView.findViewById(R.id.imageResponseIV), response, true, (ImageView) customView.findViewById(R.id.playButton2));
                    ((TextView) customView.findViewById(R.id.usernameTV)).setTextAppearance(context,
                        R.style.OverlayText);
                    break;
                case "location_response":
                    customView = context.getLayoutInflater().inflate(R.layout.carousel_image_response, null);
                    Utils.renderLocationResponse((ImageView) customView.findViewById(R.id.imageResponseIV), response, true);
                    ((TextView) customView.findViewById(R.id.usernameTV)).setTextAppearance(context,
                            R.style.OverlayText);
                    break;
                default:
                    customView = null;
            }
            ((TextView) customView.findViewById(R.id.usernameTV)).setText(username);
            return customView;
        } catch (Exception e){e.printStackTrace();}
        return new LinearLayout(context);
    }
}
