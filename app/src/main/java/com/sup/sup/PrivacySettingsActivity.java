package com.sup.sup;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class PrivacySettingsActivity extends AppCompatActivity {

    private ArrayList selectedUsers;
    private JSONArray blockedUsers;

    @Override
    protected void onPause() {
        super.onPause();
        Utils.updateSettingsToServer(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView blockedLV = (ListView) findViewById(R.id.listView);
        selectedUsers = new ArrayList<String>();

        try {
            blockedUsers = new JSONArray(Utils.getStringFromSharedPrefs("blocked_users", "[]"));
            if (blockedUsers.length() == 0) {
                blockedLV.setVisibility(View.GONE);
            } else {
                findViewById(R.id.noblockTV).setVisibility(View.GONE);
            }

            List<String> list = new ArrayList<>();
            for (int i=0; i<blockedUsers.length(); i++) {
                list.add( blockedUsers.getString(i) );
            }

            String[] data = list.toArray(new String[list.size()]);

            ListView listView = (ListView) findViewById(R.id.listView);
            listView.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_list_item_multiple_choice, data));
            listView.setItemsCanFocus(true);
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            listView.setOnTouchListener(new View.OnTouchListener() {
                // Setting on Touch Listener for handling the touch inside ScrollView
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // Disallow the touch request for parent scroll on touch of child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        if (selectedUsers.contains(blockedUsers.getString(position))) {
                            selectedUsers.remove(blockedUsers.getString(position));
                        } else {
                            selectedUsers.add(blockedUsers.getString(position));
                        }
                    } catch (Exception e){e.printStackTrace();}
                }
            });
        } catch(Exception e) {e.printStackTrace();}

        ((CheckBox) findViewById(R.id.toggler1)).setChecked(!Utils.getBooleanFromSharedPrefs("last_seen", true));
        ((Switch) findViewById(R.id.toggler2)).setChecked(Utils.getBooleanFromSharedPrefs("restricted_profile", false));
        ((Switch) findViewById(R.id.toggler3)).setChecked(Utils.getBooleanFromSharedPrefs("private_profile", false));

    }

    public void showPrivacyPolicy(View view) {
        startActivity(new Intent(this, WebViewActivity.class).putExtra("filename", "privacy_policy.html").putExtra("title", "Privacy Policy"));
    }

    public void unblockUser(View view) {
        if (selectedUsers.isEmpty()) {
            Utils.showToast(this, "No users selected");
        } else {
            try {
                JSONArray newBlockedUsers = new JSONArray();
                for (int i=0; i<blockedUsers.length(); i++) {
                    if (!selectedUsers.contains(blockedUsers.getString(i))) {
                        newBlockedUsers.put(blockedUsers.getString(i));
                    }
                }
                Utils.putStringInSharedPrefs("blocked_users", newBlockedUsers.toString());
                Utils.showToast(this, selectedUsers.size()+" users unblocked");
                selectedUsers.clear();
                blockedUsers = newBlockedUsers;

                // refresh LV
                List<String> list = new ArrayList<>();
                for (int i=0; i<newBlockedUsers.length(); i++) {
                    list.add( newBlockedUsers.getString(i) );
                }

                String[] data = list.toArray(new String[list.size()]);

                ListView listView = (ListView) findViewById(R.id.listView);
                listView.setAdapter(new ArrayAdapter<>(PrivacySettingsActivity.this,android.R.layout.simple_list_item_multiple_choice, data));
                ((ArrayAdapter)listView.getAdapter()).notifyDataSetChanged();
                listView.invalidateViews();

                if(newBlockedUsers.length() == 0) {
                    listView.setVisibility(View.GONE);
                    findViewById(R.id.noblockTV).setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {e.printStackTrace();}
        }
    }

    public void blockUser(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter username to block");

        // Set up the input
        LinearLayout ll = new LinearLayout(this);
        ll.setPadding(25, 20, 25, 20);
        ll.setOrientation(LinearLayout.VERTICAL);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        ll.addView(input);
        TextView tv = new TextView(this);
        tv.setText("Exclude the @ from the username");
        ll.addView(tv);
        builder.setView(ll);

        // Set up the buttons
        builder.setPositiveButton("Block", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String username = input.getText().toString().trim().replace("@", "");
                if (username != null && !username.isEmpty()) {
                    try {
                        blockedUsers.put(username);
                        Utils.putStringInSharedPrefs("blocked_users", blockedUsers.toString());

                        List<String> list = new ArrayList<>();
                        for (int i=0; i<blockedUsers.length(); i++) {
                            list.add( blockedUsers.getString(i) );
                        }

                        String[] data = list.toArray(new String[list.size()]);

                        ListView listView = (ListView) findViewById(R.id.listView);
                        listView.setAdapter(new ArrayAdapter<>(PrivacySettingsActivity.this,android.R.layout.simple_list_item_multiple_choice, data));
                        ((ArrayAdapter)listView.getAdapter()).notifyDataSetChanged();
                        listView.invalidateViews();

                        if(blockedUsers.length() == 1) {
                            listView.setVisibility(View.VISIBLE);
                            findViewById(R.id.noblockTV).setVisibility(View.GONE);
                        }
                    } catch (Exception e) {e.printStackTrace();}
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void toggleLastSeen(View view) {
        Utils.putBooleanInSharedPrefs("last_seen", !Utils.getBooleanFromSharedPrefs("last_seen", true));
    }

    public void toggleRestrictedProfile(View view) {
        Utils.putBooleanInSharedPrefs("restricted_profile", !Utils.getBooleanFromSharedPrefs("restricted_profile", false));
    }

    public void togglePrivateProfile(View view) {
        Utils.putBooleanInSharedPrefs("private_profile", !Utils.getBooleanFromSharedPrefs("private_profile", false));
        Utils.volleyStringCall(this, "http://black-abode-2709.appspot.com/togglePrivate?token=" + Utils.getStringFromSharedPrefs("self_token", ""), new VolleyCallback() {
            @Override
            public void onResponse(Object result) {

            }
        });
    }
}
