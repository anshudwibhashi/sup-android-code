package com.sup.sup;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;

/**
 * Created by anshudwibhashi on 08/10/17.
 */

public class FlightListAdapter extends BaseAdapter {
    JSONArray flights; Activity activity;
    public FlightListAdapter(JSONArray flights, Activity activity) {
        this.flights = flights;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        return flights.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  activity.getLayoutInflater().inflate(R.layout.list_item_flight, null);
            holder.statusTV = (TextView) convertView.findViewById(R.id.statusTV);
            holder.routeTV = (TextView) convertView.findViewById(R.id.routeTV);
            holder.flightCodeTV = (TextView) convertView.findViewById(R.id.flightCodeTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.routeTV.setText(flights.getJSONObject(position).getString("dep_city")+" to "+flights.getJSONObject(position).getString("arr_city"));
            holder.flightCodeTV.setText(flights.getJSONObject(position).getString("flight_code"));
            String status;
            switch(flights.getJSONObject(position).getString("status")) {
                case "A":
                    status = "IN FLIGHT";
                    break;
                case "C":
                    status = "CANCELLED";
                    break;
                case "D":
                    status = "DIVERTED";
                    break;
                case "L":
                    status = "LANDED";
                    break;
                case "S":
                    status = "SCHEDULED";
                    break;
                default:
                    status = "UNKNOWN";
            }
            if ((status.equals("SCHEDULED") && !flights.getJSONObject(position).isNull("dep_delay"))
                    || (status.equals("IN FLIGHT") && !flights.getJSONObject(position).isNull("arr_delay"))) {
                status = "DELAYED";
            }
            holder.statusTV.setText(status);
        } catch (Exception e ) {e.printStackTrace();}


        return convertView;
    }

    public static class ViewHolder {
        public TextView statusTV, routeTV, flightCodeTV;
    }
}
