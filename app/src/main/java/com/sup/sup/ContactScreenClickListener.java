package com.sup.sup;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by anshudwibhashi on 08/10/17.
 */

public class ContactScreenClickListener implements AdapterView.OnItemClickListener {
    Context context;
    public ContactScreenClickListener(Context context) {
        this.context = context;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (Utils.MainDataSet.tokens.get(position).equals("CHANNEL_WEATHER")) {
            Utils.fetchWeather(context, (LinearLayout) view);
        } else if (Utils.MainDataSet.tokens.get(position).equals("CHANNEL_STOCKS")) {
            Utils.fetchStocks(context, (LinearLayout) view);
        } else if (Utils.MainDataSet.tokens.get(position).equals("CHANNEL_FLIGHTS")) {
            Utils.fetchFlights(context, (LinearLayout) view);
        } else if (Utils.MainDataSet.tokens.get(position).equals("CHANNEL_SPORTS")) {
            Utils.fetchSports(context, (LinearLayout) view);
        } else if (Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
            Utils.populateMainDataSet(context);
            if (Utils.MainDataSet.names.isEmpty()) {
                return;
            }

            try {
                String supSender = Utils.MainDataSet.responses.getJSONObject(position).getString("sup_sender");

                if (supSender.trim().isEmpty() || supSender.trim().equals(Utils.getStringFromSharedPrefs("self_username", ""))) {

                    (view.findViewById(R.id.carouselView)).setVisibility(View.GONE);
                    (view.findViewById(R.id.response_payload)).setVisibility(View.GONE);

                    Utils.sendGroupSup(context, Utils.MainDataSet.tokens.get(position), (LinearLayout) view);
                } else {
                    SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
                    Cursor results = dbR.rawQuery("SELECT * FROM Connections WHERE token=?", new String[]{Utils.MainDataSet.tokens.get(position)});
                    results.moveToFirst();
                    int participantCount = new JSONArray(results.getString(results.getColumnIndexOrThrow("username"))).length();
                    JSONArray responseList = new JSONObject(results.getString(results.getColumnIndexOrThrow("response"))).getJSONArray("responseList");
                    int ignoreCount = 0;
                    for (int i = 0; i < responseList.length(); i++) {
                        if (new JSONObject(responseList.getString(i)).getString("type").equals("ignored")) {
                            ignoreCount++;
                        }
                    }
                    if (ignoreCount >= participantCount - 1) {
                        ContentValues cv = new ContentValues();
                        JSONObject response = new JSONObject();
                        response.put("sup_sender", ""); // Done
                        response.put("responseList", new JSONArray());
                        cv.put("sup_count", 0);
                        cv.put("response", response.toString());

                        SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
                        dbWritable.update("connections", cv, "token=?", new String[]{Utils.MainDataSet.tokens.get(position)});

                        (view.findViewById(R.id.carouselView)).setVisibility(View.GONE);
                        (view.findViewById(R.id.response_payload)).setVisibility(View.GONE);

                        Utils.sendGroupSup(context, Utils.MainDataSet.tokens.get(position), (LinearLayout) view);
                    } else {
                        Log.d("Last sup sender", supSender + "__");
                        Utils.showDialogMessage(context, "Can't Sup this group", "You can't Sup a group until the person who last Supped the group sees the responses, and checks them off.");
                    }
                }
            } catch ( Exception e) {e.printStackTrace();}
        } else {
            Utils.populateMainDataSet(context);
            if (Utils.MainDataSet.names.isEmpty()) {
                return;
            }
            (view.findViewById(R.id.textResponsePayload)).setVisibility(View.GONE);
            (view.findViewById(R.id.imageResponsePayload)).setVisibility(View.GONE);
            ((TextView) (view.findViewById(R.id.textResponseTV))).setText("");
            ((ImageView) (view.findViewById(R.id.imageResponseIV))).setImageBitmap(null);
            ((view.findViewById(R.id.imageResponseIV))).setOnClickListener(null);
            (view.findViewById(R.id.response_payload)).setVisibility(View.GONE);

            Utils.sendSup(context, Utils.MainDataSet.tokens.get(position), (LinearLayout) view);
        }
    }
}
