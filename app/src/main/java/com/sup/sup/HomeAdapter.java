package com.sup.sup;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;

import androidx.percentlayout.widget.PercentRelativeLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.synnapps.carouselview.CarouselView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.net.URLEncoder;

import rm.com.audiowave.AudioWaveView;


public class HomeAdapter  extends BaseAdapter {
    private LayoutInflater mInflater;
    Context context;

    private final static int CONTACT = 0;
    private final static int CHANNEL_WEATHER = 1;
    private final static int CHANNEL_STOCKS = 2;
    private final static int CHANNEL_FLIGHTS = 3;
    private final static int GROUP = 4;
    private final static int CHANNEL_SPORTS = 5;

    public HomeAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public int getCount() {
        return Utils.MainDataSet.names.size();
    }

    @Override
    public String getItem(int position) {
        return null; // useless to us
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (Utils.MainDataSet.tokens.get(position).startsWith("CHANNEL_WEATHER")) {
            return CHANNEL_WEATHER;
        } else if(Utils.MainDataSet.tokens.get(position).startsWith("CHANNEL_STOCKS")) {
            return CHANNEL_STOCKS;
        } else if(Utils.MainDataSet.tokens.get(position).startsWith("CHANNEL_FLIGHTS")) {
            return CHANNEL_FLIGHTS;
        } else if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
            return GROUP;
        } else if(Utils.MainDataSet.tokens.get(position).startsWith("CHANNEL_SPORTS")) {
            return CHANNEL_SPORTS;
        } else {
            return CONTACT;
        }
    }

    public int getViewTypeCount() {
        return 6;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        int size = Utils.MainDataSet.names.size();
        while (!((Utils.MainDataSet.names.size()==size)&&
                (Utils.MainDataSet.taglines.size()==size)&&
                (Utils.MainDataSet.tokens.size()==size)&&
                (Utils.MainDataSet.photoUrls.size()==size)&&
                (Utils.MainDataSet.online_statuses.size()==size)&&
                (Utils.MainDataSet.photos.size()==size)&&
                (Utils.MainDataSet.supCounts.size()==size)&&
                (Utils.MainDataSet.microtextRead.size()==size)&&
                (Utils.MainDataSet.smallTalkMessages.size()==size)&&
                (Utils.MainDataSet.responses.length()==size)));

        switch (this.getItemViewType(position)) {
            case CONTACT:
                convertView = caseContact(convertView, position, parent);
                break;
            case CHANNEL_WEATHER:
                convertView = caseWeather(convertView, position);
                break;
            case CHANNEL_STOCKS:
                convertView = caseStocks(convertView, position);
                break;
            case CHANNEL_FLIGHTS:
                convertView = caseFlights(convertView, position);
                break;
            case GROUP:
                convertView = caseGroup(convertView, position);
                break;
            case CHANNEL_SPORTS:
                convertView = caseSports(convertView, position);
                break;
        }

        return convertView;
    }

    private View caseGroup(View convertView, final int position) {
        final ViewHolder5 holder;
        if (convertView == null) {
            holder = new ViewHolder5();
            convertView =  mInflater.inflate(R.layout.list_item_main_group, null);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.taglineTV = (TextView) convertView.findViewById(R.id.taglineTV);
            holder.supCountTV = (TextView) convertView.findViewById(R.id.supCountTV);
            holder.textResponseTV = (TextView) convertView.findViewById(R.id.textResponseTV);
            holder.responsePayload = (LinearLayout) convertView.findViewById(R.id.response_payload);
            holder.supPayload = (LinearLayout) convertView.findViewById(R.id.sup_payload);
            holder.textResponsePayload = (LinearLayout) convertView.findViewById(R.id.textResponsePayload);
            holder.imageResponsePayload = (PercentRelativeLayout) convertView.findViewById(R.id.imageResponsePayload);
            holder.payloadContainer = (LinearLayout) convertView.findViewById(R.id.payload_container);
            holder.supResponsePayload = (LinearLayout) holder.payloadContainer.findViewById(R.id.sup_response_payload);
            holder.photoIV = (ImageView) convertView.findViewById(R.id.photoIV);
            holder.microChatIV = (ImageView) convertView.findViewById(R.id.microChatIV);
            holder.imageResponseIV = (ImageView) convertView.findViewById(R.id.imageResponseIV);
            holder.textResponseButton = (ImageView) convertView.findViewById(R.id.text_response);
            holder.imageResponseButton = (ImageView) convertView.findViewById(R.id.image_response);
            holder.videoResponseButton = (ImageView) convertView.findViewById(R.id.video_response);
            holder.audioResponseButton = (ImageView) convertView.findViewById(R.id.audio_response);
            holder.locationResponseButton = (ImageView) convertView.findViewById(R.id.location_response);
            holder.playButton2 = (ImageView) convertView.findViewById(R.id.playButton2);
            holder.ignoreButton = (ImageView) convertView.findViewById(R.id.cancel_action);
            holder.doneButton1 = (ImageView) convertView.findViewById(R.id.doneButton1);
            holder.doneButton2 = (ImageView) convertView.findViewById(R.id.doneButton2);
            holder.doneButton3 = (ImageView) convertView.findViewById(R.id.doneButton3);
            holder.audioResponsePayload = (LinearLayout) convertView.findViewById(R.id.audioResponsePayload);
            holder.cancelButton = (ImageView) convertView.findViewById(R.id.cancel_action3);
            holder.playButton = (ImageView) convertView.findViewById(R.id.play_action);
            holder.wave = (AudioWaveView) convertView.findViewById(R.id.wave);
            holder.carouselView = (CarouselView) convertView.findViewById(R.id.carouselView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder5) convertView.getTag();
        }

        if(Utils.MainDataSet.names.isEmpty()) {
            return convertView;
        }

        try {
            if (Utils.MainDataSet.responses.getJSONObject(position).has("sup_sender")) {
                Log.d("Group sup sender", Utils.MainDataSet.responses.getJSONObject(position).getString("sup_sender")+" blah");
            } else {
            }
        } catch ( Exception e) {}

        holder.microChatIV.setColorFilter(Color.parseColor("#757575"));

        holder.nameTV.setText(Utils.MainDataSet.names.get(position));
        holder.taglineTV.setText(Utils.MainDataSet.taglines.get(position));

        // Now we load the image
        Glide.with(context)
                .load(Utils.MainDataSet.photos.get(position))
                .centerCrop()
                .override(100, 100)
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.ic_group_generic_huge)
                .into(holder.photoIV);


        if(!Utils.MainDataSet.microtextRead.get(position)) {
            holder.microChatIV.setColorFilter(Color.parseColor("#E040FB"));
        }

        renderGroupSupResponsePayload(holder, context, position);

        holder.photoIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final LinearLayout ll = (LinearLayout) ((Utils.getRequiredActivity(v))).getLayoutInflater().inflate(R.layout.bottomdrawer_group, null);
                ((TextView)ll.findViewById(R.id.statusTV)).setText(Utils.MainDataSet.taglines.get(position));
                ((TextView)ll.findViewById(R.id.statusTV)).setSelected(true);
                ((TextView)ll.findViewById(R.id.nameTV)).setText(Utils.MainDataSet.names.get(position));

                Glide.with(context)
                        .load(Utils.MainDataSet.photos.get(position))
                        .centerCrop()
                        .override(500, 500)
                        .placeholder(R.mipmap.ic_group_generic_huge)
                        .into((ImageView)ll.findViewById(R.id.profilePicture));

                Button deleteButton = (Button) ll.findViewById(R.id.deleteButton);
                Button editButton = (Button) ll.findViewById(R.id.editButton);

                try {
                    ((TextView) ll.findViewById(R.id.statusTV)).setTypeface(Typeface.createFromAsset(context.getAssets(), "RobotoCondensed-Light.ttf"));
                } catch (Exception e) {e.printStackTrace();}

                final Dialog mBottomSheetDialog = new Dialog (context,
                        R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView (ll);
                mBottomSheetDialog.setCancelable (true);
                mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
                mBottomSheetDialog.show ();

                if(Utils.getGroupOwner(Utils.MainDataSet.tokens.get(position)).equals(Utils.getStringFromSharedPrefs("self_token",""))) {
                    deleteButton.setText("DELETE GROUP");
                    editButton.setVisibility(View.VISIBLE);
                    editButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(new Intent(context, NewGroupActivity.class).putExtra("groupName", Utils.MainDataSet.tokens.get(position))
                            .putExtra("editing", true));
                        }
                    });

                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Now notify other users
                            final String token = Utils.MainDataSet.tokens.get(position);
                            File photo = new File(Utils.MainDataSet.photos.get(position));
                            Utils.deleteGroupFromServer(context, token, mBottomSheetDialog, photo);
                        }
                    });
                } else {
                    deleteButton.setText("LEAVE GROUP");
                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Now notify other users
                            final String token = Utils.MainDataSet.tokens.get(position);
                            File photo = new File(Utils.MainDataSet.photos.get(position));
                            Utils.leaveGroup(context, token, mBottomSheetDialog, photo);
                        }
                    });
                }

                try {
                    final int participantCount = new JSONArray(Utils.MainDataSet.usernames.get(position)).length();
                    ((Button) ll.findViewById(R.id.participantsButton)).setText(participantCount + " Participants");
                    ((Button) ll.findViewById(R.id.participantsButton)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!Utils.isNetworkAvailable(context)) {
                                Utils.showToast(context, "No internet connection");
                                return;
                            }
                            final ProgressDialog pd = new ProgressDialog(context);
                            pd.setCancelable(false);
                            pd.setCanceledOnTouchOutside(false);
                            pd.setMessage("Loading");
                            pd.setIndeterminate(true);
                            pd.show();
                            Utils.volleyJSONCall2(context, "http://black-abode-2709.appspot.com/get_participants?group=" + URLEncoder.encode(Utils.MainDataSet.tokens.get(position)), new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    pd.dismiss();
                                    context.startActivity(new Intent(context, ParticipantsActivity.class).putExtra("participants", result.toString()));
                                }
                            });
                        }
                    });
                } catch (Exception e) {e.printStackTrace();}
            }
        });

        holder.microChatIV.setOnClickListener(new MicroChatClickListener2(context, position, holder));

        return convertView;
    }

    public static class ViewHolder5 { // For groups
        public ImageView photoIV, imageResponseIV, microChatIV, playButton2;
        public TextView nameTV, taglineTV, supCountTV, textResponseTV;
        public LinearLayout responsePayload, supPayload, textResponsePayload;
        public LinearLayout payloadContainer, supResponsePayload, audioResponsePayload;
        public ImageView ignoreButton, textResponseButton, imageResponseButton, videoResponseButton, audioResponseButton, locationResponseButton;
        public ImageView playButton, cancelButton;
        public ImageView doneButton1, doneButton2, doneButton3;
        public AudioWaveView wave;
        public PercentRelativeLayout imageResponsePayload;
        public CarouselView carouselView;
    }

    private View caseFlights(View convertView, final int position) {
        final ViewHolder4 holder;
        if (convertView == null) {
            holder = new ViewHolder4();
            convertView =  mInflater.inflate(R.layout.list_item_flights_channel, null);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress_bar);
            holder.payloadContainer = (LinearLayout) convertView.findViewById(R.id.payload_container);
            holder.listView = (ListView) convertView.findViewById(R.id.flightListingsLV);
            holder.doneButton = (ImageView) convertView.findViewById(R.id.doneButton1);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder4) convertView.getTag();
        }
        try {
            if (Utils.MainDataSet.responses.getJSONObject(position).length() == 0) {
                holder.payloadContainer.setVisibility(View.GONE);
            } else {
                holder.payloadContainer.setVisibility(View.VISIBLE);
                Utils.renderFlights(holder, Utils.MainDataSet.responses.getJSONObject(position).getJSONArray("flights"));
            }
        } catch (Exception e){e.printStackTrace();}

        return convertView;
    }

    public static class ViewHolder4 { // for stocks
        public ProgressBar progressBar;
        public LinearLayout payloadContainer;
        public ListView listView;
        public ImageView doneButton;
    }

    private View caseSports(View convertView, final int position) {
        final ViewHolder6 holder;
        if (convertView == null) {
            holder = new ViewHolder6();
            convertView =  mInflater.inflate(R.layout.list_item_sports_channel, null);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress_bar);
            holder.payloadContainer = (LinearLayout) convertView.findViewById(R.id.payload_container);
            holder.listView = (ListView) convertView.findViewById(R.id.sportsListingsLV);
            holder.doneButton = (ImageView) convertView.findViewById(R.id.doneButton1);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder6) convertView.getTag();
        }
        try {
            if (Utils.MainDataSet.responses.getJSONObject(position).length() == 0 || Utils.MainDataSet.responses.getJSONObject(position).getJSONArray("sports").length() == 0) {
                holder.payloadContainer.setVisibility(View.GONE);
            } else {
                holder.payloadContainer.setVisibility(View.VISIBLE);
                Utils.renderSports(holder, Utils.MainDataSet.responses.getJSONObject(position).getJSONArray("sports"));
            }
        } catch (Exception e){e.printStackTrace();}

        return convertView;
    }

    public static class ViewHolder6 { // for sports
        public ProgressBar progressBar;
        public LinearLayout payloadContainer;
        public ListView listView;
        public ImageView doneButton;
    }

    private View caseStocks(View convertView, final int position) {
        final ViewHolder3 holder;
        if (convertView == null) {
            holder = new ViewHolder3();
            convertView =  mInflater.inflate(R.layout.list_item_stocks_channel, null);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress_bar);
            holder.payloadContainer = (LinearLayout) convertView.findViewById(R.id.payload_container);
            holder.listView = (ListView) convertView.findViewById(R.id.stockListingsLV);
            holder.doneButton = (ImageView) convertView.findViewById(R.id.doneButton1);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder3) convertView.getTag();
        }
        try {
            if (Utils.MainDataSet.responses.getJSONObject(position).length() == 0) {
                holder.payloadContainer.setVisibility(View.GONE);
            } else {
                holder.payloadContainer.setVisibility(View.VISIBLE);
                Utils.renderStocks(holder, Utils.MainDataSet.responses.getJSONObject(position).getJSONArray("stocks"));
            }
        } catch (Exception e){e.printStackTrace();}

        return convertView;
    }

    public static class ViewHolder3 { // for stocks
        public ProgressBar progressBar;
        public LinearLayout payloadContainer;
        public ListView listView;
        public ImageView doneButton;
    }

    private View caseWeather(View convertView, final int position) {
        final ViewHolder2 holder;
        if (convertView == null) {
            holder = new ViewHolder2();
            convertView =  mInflater.inflate(R.layout.list_item_weather_channel, null);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress_bar);
            holder.cityTV = (TextView) convertView.findViewById(R.id.cityTV);
            holder.hiloTV = (TextView) convertView.findViewById(R.id.hiloTV);
            holder.currentTempTV = (TextView) convertView.findViewById(R.id.currentTempTV);
            holder.descTV = (TextView) convertView.findViewById(R.id.descTV);
            holder.payloadContainer = (LinearLayout) convertView.findViewById(R.id.payload_container);
            holder.doneButton1 = (ImageView) convertView.findViewById(R.id.doneButton1);
            holder.weatherIV = (ImageView) convertView.findViewById(R.id.weatherIV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder2) convertView.getTag();
        }
        try {
            if (Utils.MainDataSet.responses.getJSONObject(position).length() == 0) {
                holder.payloadContainer.setVisibility(View.GONE);
            } else {
                holder.payloadContainer.setVisibility(View.VISIBLE);
                Utils.renderWeather(holder, Utils.MainDataSet.responses.getJSONObject(position));
            }
        } catch (Exception e){e.printStackTrace();}

        return convertView;
    }

    public static class ViewHolder2 { // for weather
        public ProgressBar progressBar;
        public LinearLayout payloadContainer;
        public ImageView doneButton1, weatherIV;
        public TextView hiloTV, descTV, currentTempTV, cityTV;
    }

    private View caseContact(View convertView, final int position, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  mInflater.inflate(R.layout.list_item_main, null);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.taglineTV = (TextView) convertView.findViewById(R.id.taglineTV);
            holder.supCountTV = (TextView) convertView.findViewById(R.id.supCountTV);
            holder.textResponseTV = (TextView) convertView.findViewById(R.id.textResponseTV);
            holder.responsePayload = (LinearLayout) convertView.findViewById(R.id.response_payload);
            holder.supPayload = (LinearLayout) convertView.findViewById(R.id.sup_payload);
            holder.textResponsePayload = (LinearLayout) convertView.findViewById(R.id.textResponsePayload);
            holder.imageResponsePayload = (PercentRelativeLayout) convertView.findViewById(R.id.imageResponsePayload);
            holder.payloadContainer = (LinearLayout) convertView.findViewById(R.id.payload_container);
            holder.supResponsePayload = (LinearLayout) holder.payloadContainer.findViewById(R.id.sup_response_payload);
            holder.photoIV = (ImageView) convertView.findViewById(R.id.photoIV);
            holder.microChatIV = (ImageView) convertView.findViewById(R.id.microChatIV);
            holder.imageResponseIV = (ImageView) convertView.findViewById(R.id.imageResponseIV);
            holder.textResponseButton = (ImageView) convertView.findViewById(R.id.text_response);
            holder.imageResponseButton = (ImageView) convertView.findViewById(R.id.image_response);
            holder.videoResponseButton = (ImageView) convertView.findViewById(R.id.video_response);
            holder.audioResponseButton = (ImageView) convertView.findViewById(R.id.audio_response);
            holder.locationResponseButton = (ImageView) convertView.findViewById(R.id.location_response);
            holder.playButton2 = (ImageView) convertView.findViewById(R.id.playButton2);
            holder.ignoreButton = (ImageView) convertView.findViewById(R.id.cancel_action);
            holder.doneButton1 = (ImageView) convertView.findViewById(R.id.doneButton1);
            holder.doneButton2 = (ImageView) convertView.findViewById(R.id.doneButton2);
            holder.doneButton3 = (ImageView) convertView.findViewById(R.id.doneButton3);
            holder.audioResponsePayload = (LinearLayout) convertView.findViewById(R.id.audioResponsePayload);
            holder.cancelButton = (ImageView) convertView.findViewById(R.id.cancel_action3);
            holder.playButton = (ImageView) convertView.findViewById(R.id.play_action);
            holder.wave = (AudioWaveView) convertView.findViewById(R.id.wave);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(Utils.MainDataSet.names.isEmpty()) {
            return convertView;
        }

        holder.microChatIV.setColorFilter(Color.parseColor("#757575"));

        holder.nameTV.setText(Utils.MainDataSet.names.get(position));
        holder.taglineTV.setText(Utils.MainDataSet.taglines.get(position));

        // Now we load the image
        Glide.with(context)
                .load(Utils.MainDataSet.photos.get(position))
                .centerCrop()
                .override(100, 100)
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.ic_default_picture)
                .into(holder.photoIV);


        if(!Utils.MainDataSet.microtextRead.get(position)) {
            holder.microChatIV.setColorFilter(Color.parseColor("#E040FB"));
        }

        renderSupResponsePayload(holder, context, position);

        holder.photoIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                LinearLayout ll = (LinearLayout) ((Utils.getRequiredActivity(v))).getLayoutInflater().inflate(R.layout.bottomdrawer_profile, null);
                ((TextView)ll.findViewById(R.id.statusTV)).setText(Utils.MainDataSet.taglines.get(position));
                ((TextView)ll.findViewById(R.id.statusTV)).setSelected(true);
                ((TextView)ll.findViewById(R.id.nameTV)).setText(Utils.MainDataSet.names.get(position));
                ((TextView)ll.findViewById(R.id.usernameTV)).setText("@"+Utils.MainDataSet.usernames.get(position));


                Glide.with(context)
                        .load(Utils.MainDataSet.photos.get(position))
                        .centerCrop()
                        .override(500, 500)
                        .placeholder(R.drawable.ic_default_picture)
                        .into((ImageView)ll.findViewById(R.id.profilePicture));

                Button deleteButton = (Button) ll.findViewById(R.id.deleteButton);

                try {
                    ((TextView) ll.findViewById(R.id.statusTV)).setTypeface(Typeface.createFromAsset(context.getAssets(), "RobotoCondensed-Light.ttf"));
                } catch (Exception e) {e.printStackTrace();}

                final Dialog mBottomSheetDialog = new Dialog (context,
                        R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView (ll);
                mBottomSheetDialog.setCancelable (true);
                mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
                mBottomSheetDialog.show ();
                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.deleteContact(v.getContext(), Utils.MainDataSet.tokens.get(position), HomeAdapter.this, (ListView) parent);
                        mBottomSheetDialog.dismiss();
                    }
                });
            }
        });

        holder.microChatIV.setOnClickListener(new MicroChatClickListener(context, position, holder));
        return convertView;
    }

    public static void sendSmalltalk(Context context, int position, Smalltalk.MyMessage message, MessagesListAdapter adapter, MessagesList messagesList, String result, String type){
        Smalltalk.SmalltalkMessage smalltalk = Utils.MainDataSet.smallTalkMessages.get(position); // refresh just in case
        if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
            smalltalk = Utils.addMessageToGroupSmalltalk(smalltalk, message);
        } else {
            smalltalk = Utils.addMessageToSmalltalk(smalltalk, message);
        }

        // update db
        ContentValues cv = new ContentValues();
        cv.put("smalltalk", new Gson().toJson(smalltalk));
        SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
        dbWritable.update("connections", cv, "token=?", new String[]{Utils.MainDataSet.tokens.get(position)});
        Utils.populateMainDataSet(context);

        // refresh UI
        if (adapter != null) {
            adapter.clear();
            messagesList.getRecycledViewPool().clear();
            adapter.notifyDataSetChanged();
            for (Smalltalk.MyMessage m : smalltalk.messages.getMessages()) {
                adapter.addToStart(m, true);
            }
        }
    }

    public static void sendSmalltalkToOtherUser(String result, String type, int position, Context context) {
        // send to other user
        try {
            Utils.sendSmalltalkMessage(result, type, Utils.MainDataSet.tokens.get(position), context);
        } catch(Exception e) {}
    }

    public static class ViewHolder {
        public ImageView photoIV, imageResponseIV, microChatIV, playButton2;
        public TextView nameTV, taglineTV, supCountTV, textResponseTV;
        public LinearLayout responsePayload, supPayload, textResponsePayload;
        public LinearLayout payloadContainer, supResponsePayload, audioResponsePayload;
        public ImageView ignoreButton, textResponseButton, imageResponseButton, videoResponseButton, audioResponseButton, locationResponseButton;
        public ImageView playButton, cancelButton;
        public ImageView doneButton1, doneButton2, doneButton3;
        public AudioWaveView wave;
        public PercentRelativeLayout imageResponsePayload;
    }

    private static void renderGroupSupResponsePayload(final HomeAdapter.ViewHolder5 holder, final Context context, final int position){
        try {
            final JSONArray responseList = Utils.MainDataSet.responses.getJSONObject(position).getJSONArray("responseList");
            boolean selfResponded = false;
            for(int i = 0; i < responseList.length(); i++) {
                if(new JSONObject(responseList.getString(i)).getString("sender").equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
                   selfResponded = true;
                }
            }

            if(Utils.MainDataSet.supCounts.get(position) > 0 && !selfResponded){
                holder.supPayload.setVisibility(View.VISIBLE);
                holder.supPayload.setAlpha(1.0f);
                try {
                    String username = Utils.MainDataSet.responses.getJSONObject(position).getString("sup_sender");
                    if (Utils.MainDataSet.supCounts.get(position) > 1) {
                        holder.supCountTV.setText("@"+username + " supped this group " + Utils.MainDataSet.supCounts.get(position) + " times");
                    } else {
                        holder.supCountTV.setText("@"+username + " supped this group");
                    }

                    holder.ignoreButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // notifyDataSetChanged won't work within getView()
                            holder.supPayload.setVisibility(View.VISIBLE);
                            holder.supPayload.setAlpha(1.0f);

                            // Start the animation

                            holder.supPayload.animate()
                                    .alpha(0.0f)
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            holder.supPayload.setVisibility(View.GONE);
                                            holder.supCountTV.setText("");
                                            animation.removeAllListeners();
                                            processGroupResponse(Utils.packageResponse("ignored", ""), holder, position, context);
                                            Utils.deleteSups(context, Utils.MainDataSet.tokens.get(position));
                                        }

                                        @Override
                                        public void onAnimationStart(Animator animation) {

                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animation) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animation) {

                                        }
                                    });
                        }
                    });
                } catch (Exception e) {e.printStackTrace();}

                holder.textResponseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final VolleyCallback callback = new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                animateOut(context, holder, position);
                                processGroupResponse(result, holder, position, context);
                            }
                        };

                        Utils.obtainTextResponse((Activity) context, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                callback.onResponse(Utils.packageResponse("text_response", result.toString()));
                            }
                        }, "Respond to group with");
                    }
                });

                // It's called imageResponseButton but captures all kinds of media from the camera
                holder.imageResponseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final VolleyCallback callback = new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                try {
                                    File output = new File(context.getFilesDir(), "snapshot_" + ((JSONObject) result).getString("response").replace(".mp4", ".png"));
                                    output.createNewFile();
                                    Utils.snapshot(new File(context.getFilesDir(), ((JSONObject) result).getString("response")), output, context);
                                    processGroupResponse(result, holder, position, context);
                                } catch (Exception e) {e.printStackTrace();}
                            }
                        };

                        Utils.activity_result_callback = callback;
                        ((HomeActivity)context).requestingPosition = position;
                        ((HomeActivity)context).groupHolder = holder;
                        ((HomeActivity)context).purpose = "";
                        ((HomeActivity)context).saveMedia = true;
                        ((HomeActivity) context).usersName = Utils.MainDataSet.names.get(position);

                        if(!(ContextCompat.checkSelfPermission(context,
                                android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && ContextCompat.checkSelfPermission(context,
                                android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                                && ContextCompat.checkSelfPermission(context,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                                && ContextCompat.checkSelfPermission(context,
                                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                            ActivityCompat.requestPermissions((Activity)context,
                                    new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    Constants.MY_PERMISSIONS_REQUEST_IMAGE_RESPONSE);
                        } else {
                            /*
                            new MaterialCamera((Activity)context)
                                    .saveDir(context.getFilesDir())
                                    .stillShot()
                                    .start(Constants.REQUEST_CAMERA_IMAGE);
                                    */

                            new SandriosCamera((Activity) context, Constants.REQUEST_CAPTURE_MEDIA)
                                    .setShowPicker(true)
                                    .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                                    .enableImageCropping(false) // Default is false.
                                    .launchCamera();

                            // The result will be delivered in the onActivityResult() method of HomeActivity
                        }
                    }
                });



                holder.locationResponseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final VolleyCallback callback = new VolleyCallback() {
                            @Override
                            public void onResponse(final Object result) {
                                animateOut(context, holder, position);
                                try {
                                    final float lat = Float.valueOf(((JSONObject) result).getString("response").split("_")[0]);
                                    final float lon = Float.valueOf(((JSONObject) result).getString("response").split("_")[1]);

                                    final ProgressDialog pd = new ProgressDialog(context);
                                    pd.setCancelable(false);
                                    pd.setCanceledOnTouchOutside(false);
                                    pd.setIndeterminate(true);
                                    pd.setMessage("Downloading map...");
                                    pd.show();
                                    Utils.downloadImageAndSave(context, Utils.prepareStaticMapUrl(context, lat, lon), ((JSONObject) result).getString("response") + ".png", new VolleyCallback() {
                                        @Override
                                        public void onResponse(Object r) {
                                            pd.dismiss();
                                            try {
                                                processGroupResponse(result, holder, position, context);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                } catch (Exception e){e.printStackTrace();}

                            }
                        };

                        ((HomeActivity) context).usersName = Utils.MainDataSet.names.get(position);

                        if (!(ContextCompat.checkSelfPermission(context,
                                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                                ContextCompat.checkSelfPermission(context,
                                        android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                            Utils.activity_result_callback = callback;

                            ActivityCompat.requestPermissions((Activity) context,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE);
                        } else {
                            Location location = Utils.getLastKnownLocation(context);

                            if (location != null) {
                                double lat = location.getLatitude();
                                double lon = location.getLongitude();
                                callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                            } else {
                                Utils.showToast(context, "Location Unavailable");
                            }
                        }
                    }
                });

                holder.audioResponseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final VolleyCallback callback = new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                processGroupResponse(result, holder, position, context);
                            }
                        };

                        ((HomeActivity) context).usersName = Utils.MainDataSet.names.get(position);

                        // Slide-in the audio portion
                        View pre = holder.supPayload.findViewById(R.id.attachLayout);
                        View post = holder.supPayload.findViewById(R.id.audioRecordLayout);

                        Utils.animateIn(pre, post, null);

                        Utils.requestingPosition = position;
                        Utils.groupHolder = holder;
                        Utils.obtainAudioResponse((Activity) context, Utils.MainDataSet.names.get(position), new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                callback.onResponse(result);
                            }
                        }, holder.supPayload, null, "Respond to group with");
                    }
                });

            } else {
                holder.supPayload.setVisibility(View.GONE);
            }

            JSONArray nonIgnoredResponses = new JSONArray();
            for(int i = 0; i < responseList.length(); i++) {
                if(!new JSONObject(responseList.getString(i)).getString("type").equals("ignored")) {
                    nonIgnoredResponses.put(new JSONObject(responseList.getString(i)));
                }
            }
            if (nonIgnoredResponses.length() > 0) {
                holder.responsePayload.setVisibility(View.VISIBLE);
                holder.imageResponsePayload.setVisibility(View.VISIBLE);

                // Now render response
                try {
                    holder.carouselView.setVisibility(View.VISIBLE);
                    CarouselViewListener cvListener = new CarouselViewListener((Activity) context);
                    cvListener.bigListPosition = position;
                    holder.carouselView.setViewListener(cvListener);
                    holder.carouselView.setOnTouchListener(new ListView.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            int action = event.getAction();
                            switch (action) {
                                case MotionEvent.ACTION_DOWN:
                                    // Disallow ScrollView to intercept touch events.
                                    v.getParent().requestDisallowInterceptTouchEvent(true);
                                    break;

                                case MotionEvent.ACTION_UP:
                                    // Allow ScrollView to intercept touch events.
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    break;
                            }

                            // Handle ListView touch events.
                            v.onTouchEvent(event);
                            return true;
                        }
                    });

                    holder.carouselView.setPageCount(nonIgnoredResponses.length());
                    View.OnClickListener doneListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                String groupName = Utils.MainDataSet.tokens.get(position);
                                if (Utils.MainDataSet.responses.getJSONObject(position).getString("sup_sender").equals(Utils.getStringFromSharedPrefs("self_username", ""))) {
                                    String url = "http://black-abode-2709.appspot.com/done_group_response?to_group=" + URLEncoder.encode(groupName)
                                            + "&sender="+URLEncoder.encode(Utils.getStringFromSharedPrefs("self_username", ""));

                                    Utils.volleyStringCall(context, url, new VolleyCallback() {
                                        @Override
                                        public void onResponse(Object result) {
                                        }
                                    });
                                }

                                // Undo all the rendering
                                (((Activity) context).findViewById(R.id.carouselView)).setVisibility(View.GONE);
                                (((Activity) context).findViewById(R.id.response_payload)).setVisibility(View.GONE);

                                try {
                                    Utils.deleteGroupResponse(context, Utils.MainDataSet.tokens.get(position), responseList, false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e ){e.printStackTrace();}
                        }
                    };
                    holder.doneButton1.setOnClickListener(doneListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                holder.responsePayload.setVisibility(View.GONE);
            }
        }catch (Exception e) {e.printStackTrace();}
    }

    private static void renderSupResponsePayload(final HomeAdapter.ViewHolder holder, final Context context, final int position){
        if(Utils.MainDataSet.supCounts.get(position) > 0){
            holder.supPayload.setVisibility(View.VISIBLE);
            holder.supPayload.setAlpha(1.0f);

            if(Utils.MainDataSet.supCounts.get(position) > 1){
                holder.supCountTV.setText("Supped you " + Utils.MainDataSet.supCounts.get(position) + " times");
            }

            holder.ignoreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // notifyDataSetChanged won't work within getView()
                    holder.supPayload.setVisibility(View.VISIBLE);
                    holder.supPayload.setAlpha(1.0f);

                    // Start the animation

                    holder.supPayload.animate()
                            .alpha(0.0f)
                            .setListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    holder.supPayload.setVisibility(View.GONE);
                                    holder.supCountTV.setText("Just supped you");
                                    animation.removeAllListeners();
                                    Utils.deleteSups(context, Utils.MainDataSet.tokens.get(position));
                                }

                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });
                }
            });

            holder.textResponseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final VolleyCallback callback = new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            animateOut(context, holder, position);
                            processResponse(result, holder, position, context);
                        }
                    };

                    Utils.obtainTextResponse((Activity) context, new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            callback.onResponse(Utils.packageResponse("text_response", result.toString()));
                        }
                    }, "Respond with");
                }
            });

            // It's called imageResponseButton but captures all kinds of media from the camera
            holder.imageResponseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final VolleyCallback callback = new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            processResponse(result, holder, position, context);
                        }
                    };

                    Utils.activity_result_callback = callback;
                    ((HomeActivity)context).requestingPosition = position;
                    ((HomeActivity)context).holder = holder;
                    ((HomeActivity)context).purpose = "";
                    ((HomeActivity) context).usersName = Utils.MainDataSet.names.get(position);

                    if(!(ContextCompat.checkSelfPermission(context,
                            android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(context,
                            android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(context,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(context,
                            Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                        ActivityCompat.requestPermissions((Activity)context,
                                new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                Constants.MY_PERMISSIONS_REQUEST_IMAGE_RESPONSE);
                    } else {
                        /*
                        new MaterialCamera((Activity)context)
                                .saveDir(context.getFilesDir())
                                .stillShot()
                                .start(Constants.REQUEST_CAMERA_IMAGE);
                                */

                        new SandriosCamera((Activity) context, Constants.REQUEST_CAPTURE_MEDIA)
                                .setShowPicker(true)
                                .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                                .enableImageCropping(false) // Default is false.
                                .launchCamera();

                        // The result will be delivered in the onActivityResult() method of HomeActivity
                    }
                }
            });

            holder.videoResponseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final VolleyCallback callback = new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            processResponse(result, holder, position, context);
                        }
                    };

                    Utils.activity_result_callback = callback;
                    ((HomeActivity)context).requestingPosition = position;
                    ((HomeActivity)context).holder = holder;
                    ((HomeActivity)context).purpose = "";
                    ((HomeActivity) context).usersName = Utils.MainDataSet.names.get(position);

                    if(!(ContextCompat.checkSelfPermission(context,
                            android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(context,
                            android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                        ActivityCompat.requestPermissions((Activity)context,
                                new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO},
                                Constants.MY_PERMISSIONS_REQUEST_VIDEO_RESPONSE);
                    } else {
                        /*new MaterialCamera( (Activity) context)
                                .saveDir(context.getFilesDir())
                                .countdownSeconds(10f)
                                .showPortraitWarning(false)
                                .start(Constants.REQUEST_CAMERA_VIDEO);*/
                        // The result will be delivered in the onActivityResult() method of MainActivity
                    }
                }
            });

            holder.locationResponseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final VolleyCallback callback = new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            animateOut(context, holder, position);
                            processResponse(result, holder, position, context);
                        }
                    };

                    ((HomeActivity) context).usersName = Utils.MainDataSet.names.get(position);

                    if (!(ContextCompat.checkSelfPermission(context,
                            android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(context,
                                    android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                        Utils.activity_result_callback = callback;

                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE);
                    } else {
                        Location location = Utils.getLastKnownLocation(context);

                        if (location != null) {
                            double lat = location.getLatitude();
                            double lon = location.getLongitude();
                            callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                        } else {
                            Utils.showToast(context, "Location Unavailable");
                        }
                    }
                }
            });

            holder.audioResponseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final VolleyCallback callback = new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            processResponse(result, holder, position, context);
                        }
                    };

                    ((HomeActivity) context).usersName = Utils.MainDataSet.names.get(position);

                    // Slide-in the audio portion
                    View pre = holder.supPayload.findViewById(R.id.attachLayout);
                    View post = holder.supPayload.findViewById(R.id.audioRecordLayout);

                    Utils.animateIn(pre, post, null);

                    Utils.requestingPosition = position;
                    Utils.holder = holder;
                    Utils.obtainAudioResponse((Activity) context, Utils.MainDataSet.names.get(position), new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            callback.onResponse(result);
                        }
                    }, holder.supPayload, null, "Respond with");
                }
            });
        } else {
            holder.supPayload.setVisibility(View.GONE);
        }

        try {
            if (Utils.MainDataSet.responses.getJSONObject(position).length() > 0) {
                holder.responsePayload.setVisibility(View.VISIBLE);

                // Now render response
                try {
                    final JSONObject response = Utils.MainDataSet.responses.getJSONObject(position);
                    switch (response.getString("type")) {
                        case "text_response":
                            holder.textResponsePayload.setVisibility(View.VISIBLE);
                            holder.imageResponsePayload.setVisibility(View.GONE);
                            holder.audioResponsePayload.setVisibility(View.GONE);
                            Utils.renderTextResponse(holder.textResponseTV, response);
                            holder.imageResponseIV.setImageBitmap(null);
                            holder.imageResponseIV.setOnClickListener(null);
                            holder.playButton2.setVisibility(View.GONE);
                            break;
                        case "image_response":
                            holder.textResponsePayload.setVisibility(View.GONE);
                            holder.imageResponsePayload.setVisibility(View.VISIBLE);
                            holder.audioResponsePayload.setVisibility(View.GONE);
                            Utils.renderImageResponse(holder.imageResponseIV, response, true);
                            holder.textResponseTV.setText("");
                            holder.playButton2.setVisibility(View.GONE);
                            break;
                        case "audio_response":
                            holder.textResponsePayload.setVisibility(View.GONE);
                            holder.imageResponsePayload.setVisibility(View.GONE);
                            holder.audioResponsePayload.setVisibility(View.VISIBLE);
                            Utils.renderAudioResponse(holder.wave, holder.playButton, holder.cancelButton, response);
                            holder.textResponseTV.setText("");
                            holder.imageResponseIV.setImageBitmap(null);
                            holder.imageResponseIV.setOnClickListener(null);
                            holder.playButton2.setVisibility(View.GONE);
                            break;
                        case "video_response":
                            holder.textResponsePayload.setVisibility(View.GONE);
                            holder.imageResponsePayload.setVisibility(View.VISIBLE);
                            holder.audioResponsePayload.setVisibility(View.GONE);
                            Utils.renderVideoResponse(holder.imageResponseIV, response, true, holder.playButton2);
                            holder.textResponseTV.setText("");
                            break;
                        case "location_response":
                            holder.textResponsePayload.setVisibility(View.GONE);
                            holder.imageResponsePayload.setVisibility(View.VISIBLE);
                            holder.audioResponsePayload.setVisibility(View.GONE);
                            Utils.renderLocationResponse(holder.imageResponseIV, response, true);
                            holder.playButton2.setVisibility(View.GONE);
                            holder.textResponseTV.setText("");
                            break;
                    }
                    View.OnClickListener doneListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // Undo all the rendering
                            holder.textResponsePayload.setVisibility(View.GONE);
                            holder.imageResponsePayload.setVisibility(View.GONE);
                            holder.audioResponsePayload.setVisibility(View.GONE);
                            holder.responsePayload.setVisibility(View.GONE);
                            holder.textResponseTV.setText("");
                            holder.imageResponseIV.setImageBitmap(null);
                            holder.imageResponseIV.setOnClickListener(null);
                            try {
                                Utils.deleteResponse(context, Utils.MainDataSet.tokens.get(position), response.getString("type"), response.getString("response"));
                            } catch (Exception e) {e.printStackTrace();}
                        }
                    };
                    holder.doneButton1.setOnClickListener(doneListener);
                    holder.doneButton2.setOnClickListener(doneListener);
                    holder.doneButton3.setOnClickListener(doneListener);
                } catch(Exception e) {e.printStackTrace();}
            } else {
                holder.responsePayload.setVisibility(View.GONE);
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    static boolean proceedWithDelete = true;
    public static void processResponse(Object result, final ViewHolder holder, final int position, final Context context) {
        if (!Utils.isNetworkAvailable(context)) {
            Utils.showToast(context, "No internet connection");
            return;
        }
        JSONObject data = (JSONObject) result;
        // Now send data to other device
        String url = "http://black-abode-2709.appspot.com/send_message?to_user=" + URLEncoder.encode(Utils.MainDataSet.tokens.get(position)) + "&data=" + URLEncoder.encode(data.toString());

        Utils.volleyStringCall(context, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
            }
        });
    }

    public static void processGroupResponse(Object result, final ViewHolder5 holder, final int position, final Context context) {
        if (!Utils.isNetworkAvailable(context)) {
            Utils.showToast(context, "No internet connection");
            return;
        }

        // Add this response to db
        JSONObject data = (JSONObject) result;
        String groupName = Utils.MainDataSet.tokens.get(position);

        try {
            SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
            Cursor results = dbR.rawQuery("SELECT * FROM Connections WHERE token=?", new String[]{groupName});
            results.moveToFirst();
            JSONObject response = new JSONObject(results.getString(results.getColumnIndexOrThrow("response")));
            JSONArray newResponseList = response.getJSONArray("responseList").put(data);
            response.put("responseList",newResponseList);

            SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("last_modified", String.valueOf(System.currentTimeMillis()));
            values.put("response", response.toString());
            db.update("connections", values, "token=?", new String[]{groupName});

            Utils.populateMainDataSet(context);
            ListView main = (ListView) ((Activity)context).findViewById(R.id.mainLV);
            if (main != null) {
                ((BaseAdapter)main.getAdapter()).notifyDataSetChanged();
            }

            int participantCount = new JSONArray(results.getString(results.getColumnIndexOrThrow("username"))).length();

            int ignoreCount = 0;
            for (int i = 0; i < newResponseList.length(); i++) {
                if (new JSONObject(newResponseList.getString(i)).getString("type").equals("ignored")) {
                    ignoreCount++;
                }
            }

            if (data.getString("type").equals("ignored") && ignoreCount >= participantCount - 1) {
                // All ignored
                String url = "http://black-abode-2709.appspot.com/done_group_response?to_group=" + URLEncoder.encode(groupName)
                        + "&sender=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_username", ""));

                Utils.volleyStringCall(context, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                    }
                });

                ContentValues cv = new ContentValues();
                response = new JSONObject();
                response.put("sup_sender", ""); // Done
                response.put("responseList", new JSONArray());
                cv.put("sup_count", 0);
                cv.put("response", response.toString());

                SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
                dbWritable.update("connections", cv, "token=?", new String[]{groupName});
            } else {
                // Now send data to other device
                String url = "http://black-abode-2709.appspot.com/send_group_response?to_group=" + URLEncoder.encode(groupName) + "&data=" + URLEncoder.encode(data.toString())
                        + "&sender=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_username", ""));

                Utils.volleyStringCall(context, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                    }
                });
            }

        } catch (Exception e){e.printStackTrace();}
    }

    public static void animateOut(final Context context, final ViewHolder holder, final int position) {
        /// START - Close animation

        // notifyDataSetChanged won't work within getView()
        holder.supPayload.setVisibility(View.VISIBLE);
        holder.supPayload.setAlpha(1.0f);

        // Start the animation

        holder.supPayload.animate()
                .alpha(0.0f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        holder.supPayload.setVisibility(View.GONE);
                        holder.supCountTV.setText("Just supped you");
                        animation.removeAllListeners();

                        Utils.deleteSups(context, Utils.MainDataSet.tokens.get(position));
                        /*((Activity)context).findViewById(R.id.microChatIV).setVisibility(View.VISIBLE);
                        ((Activity)context).findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);*/
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
        Utils.showToast(context, "Response sent");
    }

    public static void animateOut(final Context context, final ViewHolder5 holder, final int position) {
        /// START - Close animation

        // notifyDataSetChanged won't work within getView()
        holder.supPayload.setVisibility(View.VISIBLE);
        holder.supPayload.setAlpha(1.0f);

        // Start the animation

        holder.supPayload.animate()
                .alpha(0.0f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        holder.supPayload.setVisibility(View.GONE);
                        holder.supCountTV.setText("Just supped you");
                        animation.removeAllListeners();

                        Utils.deleteSups(context, Utils.MainDataSet.tokens.get(position));
                        /*((Activity)context).findViewById(R.id.microChatIV).setVisibility(View.VISIBLE);
                        ((Activity)context).findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);*/
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
        Utils.showToast(context, "Response sent");
    }

}