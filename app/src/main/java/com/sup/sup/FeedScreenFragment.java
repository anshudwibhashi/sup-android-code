package com.sup.sup;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.material.snackbar.Snackbar;

public class FeedScreenFragment extends Fragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FeedScreenFragment() {
    }

    public static FeedScreenFragment newInstance() {
        FeedScreenFragment fragment = new FeedScreenFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {e.printStackTrace();}
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    // Tutorial: Tell 'em about SupBots
                    if(!Utils.getBooleanFromSharedPrefs("tutorial2_shown", false)) {
                        Snackbar.make(((Activity)getContext()).findViewById(R.id.main_content), "Add SupBots to get updates from non-humans too!", Snackbar.LENGTH_LONG).show();
                        Utils.putBooleanInSharedPrefs("tutorial2_shown", true);

                        Utils.scheduleNotification(getContext(), 1000*60*60*24*1, Integer.MAX_VALUE-1, "Hey, sup?",
                                "Have you tried creating a group? Sup 'em all at once!", R.drawable.ic_group_generic,
                                NewGroupActivity.class);
                        Utils.scheduleNotification(getContext(), 1000*60*60*24*2, Integer.MAX_VALUE-2, "Love Sports?",
                                "Try the Sports SupBot!", R.drawable.ic_sports_channel,
                                SportsChannelActivity.class);
                        Utils.scheduleNotification(getContext(), 1000*60*60*24*3, Integer.MAX_VALUE-3, "Love YouTube?",
                                "Try the YouTube SupBot!", R.drawable.ic_youtube_channel,
                                YoutubeChannelActivity.class);
                        Utils.scheduleNotification(getContext(), 1000*60*60*24*4, Integer.MAX_VALUE-4, "Missing out on news?",
                                "Try the News SupBot!", R.drawable.ic_news_channel,
                                NewsKeywordsActivity.class);
                        Utils.scheduleNotification(getContext(), 1000*60*60*24*5, Integer.MAX_VALUE-5, "Love reddit?",
                                "Try the reddit SupBot!", R.drawable.ic_reddit_channel,
                                RedditChannelActivity.class);
                        Utils.scheduleNotification(getContext(), 1000*60*60*24*6, Integer.MAX_VALUE-6, "Love Sup?",
                                "Invite your friends", R.drawable.ic_launcher_alternative_hi_res,
                                HomeActivity.class);
                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    FeedAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final ListView view = (ListView)inflater.inflate(R.layout.fragment_feed, container, false);
        view.setItemsCanFocus(true);
        view.setFocusable(false);
        view.setFocusableInTouchMode(false);
        view.setLongClickable(false);

        if(Utils.FeedDataSet.names.isEmpty()) {
            view.setSelector(android.R.color.transparent);
            EmptyAdapter adapter = new EmptyAdapter(getContext());
            adapter.title = "Nothing to see here!";
            adapter.feed = true;
            adapter.message = "None of your contacts has broadcast anything in the past 24 hours. Why don't you get the ball rolling?";
            view.setAdapter(adapter);
        } else {
            adapter = new FeedAdapter(getContext());
            view.setAdapter(adapter);
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
