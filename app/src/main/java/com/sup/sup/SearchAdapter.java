package com.sup.sup;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by anshudwibhashi on 02/06/17.
 */
public class SearchAdapter  extends BaseAdapter {

    JSONArray results;
    private LayoutInflater mInflater;
    private Dialog dialog;

    Context context;
    public SearchAdapter(Context context, JSONArray results, Dialog dialog) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.results = results;
        this.dialog = dialog;
    }

    @Override
    public int getCount() {
        return results.length();
    }

    @Override
    public String getItem(int position) {
        return null; // useless to us
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  mInflater.inflate(R.layout.list_item_search, null);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.usernameTV = (TextView) convertView.findViewById(R.id.usernameTV);
            holder.profilePictureCIV = (ImageView) convertView.findViewById(R.id.profilePictureCIV);
            holder.addButton = (ImageView) convertView.findViewById(R.id.addButton);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.nameTV.setText(results.getJSONObject(position).getString("name"));
            holder.usernameTV.setText("@"+results.getJSONObject(position).getString("username"));

            Glide.with(context).load(results.getJSONObject(position).getString("photoUrl")).asBitmap().centerCrop()
                    .placeholder(R.drawable.ic_default_picture)
                    .override(100, 100).into(new BitmapImageViewTarget(holder.profilePictureCIV) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.profilePictureCIV.setImageDrawable(circularBitmapDrawable);
                }
            });

            holder.addButton.setColorFilter(Color.parseColor("#757575"));
            holder.addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addContact(position);
                }
            });

            convertView.setEnabled(false);
            convertView.setOnClickListener(null);

            return convertView;
        } catch (Exception e) {e.printStackTrace(); return null;}
    }

    private void addContact(final int position) {
        // Follow this person, if not self.
        try {
            final String token = results.getJSONObject(position).getString("token");
            if (token.equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
                Utils.showToast(context, "Can't add self");
            } else {
                // Follow this person, if not already following
                final ProgressDialog pd = new ProgressDialog(context);
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Please wait...");
                pd.setIndeterminate(true);
                pd.show();

                if (!Utils.getStringFromSharedPrefs("self_following", "[]").contains(token)) {

                    Utils.downloadProfile(context, token, new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            Utils.follow(context, (JSONObject) result, token, true, new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    try {
                                        Utils.showToast(context, "Added " + results.getJSONObject(position).getString("name"));
                                        Utils.populateMainDataSet(context);
                                        if(((ListView)((HomeActivity)context).findViewById(R.id.mainLV)).getAdapter() instanceof  EmptyAdapter) {
                                            // change to homeadapter
                                            final ListView view = ((ListView) ((HomeActivity)context).findViewById(R.id.mainLV));
                                            final HomeAdapter adapter = new HomeAdapter(context);
                                            view.setAdapter(adapter);
                                            view.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                                                @Override
                                                public boolean onItemLongClick(AdapterView<?> parent, View view2, final int position, long id) {
                                                    if(Utils.MainDataSet.names.isEmpty()) {
                                                        return false;
                                                    }
                                                    Utils.deleteContact(context, Utils.MainDataSet.tokens.get(position), adapter, view);
                                                    return true;
                                                }

                                            });

                                            view.setOnItemClickListener(new ContactScreenClickListener(context));
                                        } else {
                                            // refresh
                                            ((BaseAdapter) ((ListView) ((HomeActivity) context).findViewById(R.id.mainLV)).getAdapter()).notifyDataSetChanged();
                                        }
                                        pd.dismiss();
                                        if (dialog != null) {
                                            dialog.dismiss();
                                        }
                                    } catch (Exception e) {e.printStackTrace();}
                                }
                            });
                        }
                    });
                } else {
                    Utils.showToast(context, "Already following");
                    pd.dismiss();
                }
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    public static class ViewHolder {
        public ImageView profilePictureCIV;
        public TextView nameTV;
        public TextView usernameTV;
        public ImageView addButton;
    }

}