package com.sup.sup;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import org.json.JSONArray;

/**
 * Created by anshudwibhashi on 08/10/17.
 */

public class FlightListClickListener implements AdapterView.OnItemClickListener {
    JSONArray flights; Context context;
    public FlightListClickListener(JSONArray flights, Context context) {
        this.flights = flights;
        this.context = context;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            context.startActivity(new Intent(context, FlightDetailActivity.class).putExtra("flight", flights.getJSONObject(position).toString()));
        } catch (Exception e) {e.printStackTrace();}
    }
}
