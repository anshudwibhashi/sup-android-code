package com.sup.sup;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class YoutubeChannelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_channel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            Utils.populateMainDataSet(this); Utils.populateFeedDataSet(this);
        }
        try {
            JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("youtube_channel_names", "[]"));
            if(pre.length() > 0) {
                // we have some channels
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                ListView lv = (ListView) findViewById(R.id.tickerLV);
                lv.setAdapter(new TopicsAdapter(this));
            }
        } catch(Exception e) {e.printStackTrace();}
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (Utils.getBooleanFromSharedPrefs("show_youtube_channel_notifications", true)) {
            getMenuInflater().inflate(R.menu.menu_channels, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_channels2, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_toggle_notifs:
                toggleNotifs();
                return true;
            case android.R.id.home:
                overrideNavigation();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        overrideNavigation();
    }

    private void overrideNavigation() {
        if(getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            finish();
            startActivity(new Intent(this, HomeActivity.class));
        } else {
            super.onBackPressed();
        }
    }

    private void toggleNotifs() {
        Utils.putBooleanInSharedPrefs("show_youtube_channel_notifications",
                !Utils.getBooleanFromSharedPrefs("show_youtube_channel_notifications", true) );
        if (Utils.getBooleanFromSharedPrefs("show_youtube_channel_notifications", true)) {
            Utils.showToast(this, "Notifications on");
        } else {
            Utils.showToast(this, "Notifications off");
        }
        invalidateOptionsMenu();
    }

    public void addYouTubeChannels(View view) {
        new MaterialDialog.Builder(this)
                .title("Search for channel")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(final MaterialDialog dialog, CharSequence input) {
                        // Do something
                        try {
                            final String keywords = input.toString();
                            if(keywords != null && !keywords.isEmpty()) {
                                Utils.volleyJSONCall(YoutubeChannelActivity.this, "https://www.googleapis.com/youtube/v3/search?part=snippet&q=" + keywords + "&type=channel&key=AIzaSyAbs_8UVdh-MptE7k0u7Iq369o-XKTzD6w", new VolleyCallback() {
                                    @Override
                                    public void onResponse(Object result) {
                                        try {
                                            final JSONArray results = ((JSONObject) result).getJSONArray("items");
                                            ArrayList<String> imageUrls = new ArrayList<>();
                                            ArrayList<String> names = new ArrayList<>();
                                            ArrayList<String> descs = new ArrayList<>();
                                            ArrayList<String> ids = new ArrayList<>();

                                            for(int i=0;i<results.length();i++) {
                                                imageUrls.add(results.getJSONObject(i).getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("default").getString("url"));
                                                names.add(results.getJSONObject(i).getJSONObject("snippet").getString("title"));
                                                descs.add(results.getJSONObject(i).getJSONObject("snippet").getString("description"));
                                                ids.add(results.getJSONObject(i).getJSONObject("snippet").getString("channelId"));
                                            }

                                            dialog.dismiss();

                                            // Now show new dialog with list in it

                                            MaterialDialog dialog = new MaterialDialog.Builder(YoutubeChannelActivity.this)
                                                    .title("Search Results")
                                                    // second parameter is an optional layout manager. Must be a LinearLayoutManager or GridLayoutManager.
                                                    .adapter(new YouTubeSearchResultsAdapter(YoutubeChannelActivity.this, imageUrls, names, descs, ids), null)
                                                    .build();

                                            ((YouTubeSearchResultsAdapter)dialog.getRecyclerView().getAdapter()).dialog = dialog;

                                            dialog.show();

                                        } catch (Exception e) {e.printStackTrace();}
                                    }
                                });
                            }
                        } catch (Exception e) {e.printStackTrace();}
                    }
                }).show();
    }
}
