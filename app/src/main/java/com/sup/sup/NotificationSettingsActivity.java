package com.sup.sup;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;

public class NotificationSettingsActivity extends AppCompatActivity {

    @Override
    protected void onPause() {
        super.onPause();
        Utils.updateSettingsToServer(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((CheckBox) findViewById(R.id.toggler1)).setChecked(Utils.getBooleanFromSharedPrefs("notify_on_sup", true));
        ((CheckBox) findViewById(R.id.toggler2)).setChecked(Utils.getBooleanFromSharedPrefs("notify_on_respond", true));
        ((CheckBox) findViewById(R.id.toggler3)).setChecked(Utils.getBooleanFromSharedPrefs("notify_on_like", true));
        ((CheckBox) findViewById(R.id.toggler4)).setChecked(Utils.getBooleanFromSharedPrefs("notify_on_comments", true));
        ((CheckBox) findViewById(R.id.toggler5)).setChecked(Utils.getBooleanFromSharedPrefs("notify_on_mention", true));
        ((CheckBox) findViewById(R.id.toggler6)).setChecked(Utils.getBooleanFromSharedPrefs("notification_lights", true));
        ((CheckBox) findViewById(R.id.toggler7)).setChecked(Utils.getBooleanFromSharedPrefs("notification_vibration", true));
        ((CheckBox) findViewById(R.id.toggler8)).setChecked(Utils.getBooleanFromSharedPrefs("notification_sound", true));
        ((CheckBox) findViewById(R.id.toggler9)).setChecked(Utils.getBooleanFromSharedPrefs("notify_on_smalltalk", true));
    }

    public void toggleOnSup(View view) {
        Utils.putBooleanInSharedPrefs("notify_on_sup", !Utils.getBooleanFromSharedPrefs("notify_on_sup", true));
    }

    public void toggleOnSmalltalk(View view) {
        Utils.putBooleanInSharedPrefs("notify_on_smalltalk", !Utils.getBooleanFromSharedPrefs("notify_on_smalltalk", true));
    }

    public void toggleOnRespond(View view) {
        Utils.putBooleanInSharedPrefs("notify_on_respond", !Utils.getBooleanFromSharedPrefs("notify_on_respond", true));
    }

    public void toggleLike(View view) {
        Utils.putBooleanInSharedPrefs("notify_on_like", !Utils.getBooleanFromSharedPrefs("notify_on_like", true));
    }

    public void toggleComments(View view) {
        Utils.putBooleanInSharedPrefs("notify_on_comments", !Utils.getBooleanFromSharedPrefs("notify_on_comments", true));
    }

    public void toggleMention(View view) {
        Utils.putBooleanInSharedPrefs("notify_on_mention", !Utils.getBooleanFromSharedPrefs("notify_on_mention", true));
    }

    public void toggleLights(View view) {
        Utils.putBooleanInSharedPrefs("notification_lights", !Utils.getBooleanFromSharedPrefs("notification_lights", true));
    }

    public void toggleVibration(View view) {
        Utils.putBooleanInSharedPrefs("notification_vibration", !Utils.getBooleanFromSharedPrefs("notification_vibration", true));
    }

    public void toggleSound(View view) {
        Utils.putBooleanInSharedPrefs("notification_sound", !Utils.getBooleanFromSharedPrefs("notification_sound", true));
    }
}
