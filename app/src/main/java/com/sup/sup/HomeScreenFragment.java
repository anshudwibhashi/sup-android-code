package com.sup.sup;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.File;

public class HomeScreenFragment extends Fragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HomeScreenFragment() {
    }

    public static HomeScreenFragment newInstance() {
        HomeScreenFragment fragment = new HomeScreenFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    HomeAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final ListView view = (ListView)inflater.inflate(R.layout.fragment_home, container, false);
        view.setItemsCanFocus(true);
        view.setFocusable(false);
        view.setFocusableInTouchMode(false);

        if(Utils.MainDataSet.names.isEmpty()) {
            view.setSelector(android.R.color.transparent);
            EmptyAdapter adapter = new EmptyAdapter(getContext());
            adapter.title = "Nothing to see here!";
            adapter.message = "Get started by adding your friends. Click the button down there to find them!";
            view.setAdapter(adapter);
        } else {

            adapter = new HomeAdapter(getContext());
            view.setAdapter(adapter);

            view.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view2, final int position, long id) {
                    if(Utils.MainDataSet.names.isEmpty()) {
                        return false;
                    }
                    if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
                        if (Utils.getGroupOwner(Utils.MainDataSet.tokens.get(position)).equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
                            File photo = new File(Utils.MainDataSet.photos.get(position));
                            Utils.deleteGroupFromServer(getContext(), Utils.MainDataSet.tokens.get(position), null, photo);
                        } else {
                            File photo = new File(Utils.MainDataSet.photos.get(position));
                            Utils.leaveGroup(getContext(), Utils.MainDataSet.tokens.get(position), null, photo);
                        }
                    } else {
                        Utils.deleteContact(getContext(), Utils.MainDataSet.tokens.get(position), adapter, view);
                    }
                    return true;
                }

            });

            view.setOnItemClickListener(new ContactScreenClickListener(getContext()));

        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
