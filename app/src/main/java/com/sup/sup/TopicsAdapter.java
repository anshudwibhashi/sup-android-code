package com.sup.sup;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabWidget;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;

public class TopicsAdapter  extends BaseAdapter {
    private LayoutInflater mInflater;
    private Context context;
    private JSONArray topics;

    public TopicsAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        try {
            if (context instanceof NewsKeywordsActivity) {
                topics = new JSONArray(Utils.getStringFromSharedPrefs("news_keywords", "[]"));
            } else if (context instanceof StocksMenuActivity){
                topics = new JSONArray(Utils.getStringFromSharedPrefs("stock_tickers", "[]"));
            } else if (context instanceof PackageTrackerActivity){
                topics = new JSONArray(Utils.getStringFromSharedPrefs("package_tracking_ids", "[]"));
            } else if (context instanceof YoutubeChannelActivity){
                topics = new JSONArray(Utils.getStringFromSharedPrefs("youtube_channel_names", "[]"));
            } else if (context instanceof FlightChannelActivity) {
                topics = new JSONArray(Utils.getStringFromSharedPrefs("flight_codes", "[]"));
            } else if (context instanceof RedditChannelActivity) {
                topics = new JSONArray(Utils.getStringFromSharedPrefs("subreddits", "[]"));
            } else if (context instanceof TwitterChannelActivity) {
                topics = new JSONArray(Utils.getStringFromSharedPrefs("twitter_handles", "[]"));
            } else if (context instanceof SportsChannelActivity) {
                topics = new JSONArray(Utils.getStringFromSharedPrefs("sports_teams_abbrs", "[]"));
            }
        } catch (Exception e){e.printStackTrace();}
    }

    @Override
    public int getCount() {
        return topics.length();
    }

    @Override
    public String getItem(int position) {
        return null; // useless to us
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_item_topics, null);
            holder.topicTV = (TextView) convertView.findViewById(R.id.topicTV);
            holder.deleteIV = (ImageView) convertView.findViewById(R.id.deleteIV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.topicTV.setText(topics.getString(position));
            holder.deleteIV.setColorFilter(Color.parseColor("#757575"));
            holder.deleteIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONArray newTopics = new JSONArray();
                        for (int i = 0; i < topics.length(); i++) {
                            if (i != position) {
                                newTopics.put(topics.getString(i));
                            }
                        }
                        ListView lv;
                        if (context instanceof  NewsKeywordsActivity) {
                            Utils.putStringInSharedPrefs("news_keywords", newTopics.toString());
                            lv = (ListView) ((Activity) context).findViewById(R.id.topicsLV);
                        } else if (context instanceof StocksMenuActivity) {
                            Utils.putStringInSharedPrefs("stock_tickers", newTopics.toString());
                            lv = (ListView) ((Activity) context).findViewById(R.id.tickerLV);
                        } else if (context instanceof PackageTrackerActivity) {
                            Utils.putStringInSharedPrefs("package_tracking_ids", newTopics.toString());
                            lv = (ListView) ((Activity) context).findViewById(R.id.tickerLV);
                        } else if (context instanceof SportsChannelActivity) {
                            Utils.putStringInSharedPrefs("sports_teams_abbrs", newTopics.toString());
                            lv = (ListView) ((Activity) context).findViewById(R.id.tickerLV);

                            JSONArray newLeagues = new JSONArray();
                            JSONArray leagues = new JSONArray(Utils.getStringFromSharedPrefs("sports_teams_leagues", "[]"));
                            for (int i = 0; i < leagues.length(); i++) {
                                if (i != position) {
                                    newLeagues.put(leagues.getString(i));
                                }
                            }
                            Utils.putStringInSharedPrefs("sports_teams_leagues", newLeagues.toString());
                        } else if (context instanceof YoutubeChannelActivity) {
                            Utils.putStringInSharedPrefs("youtube_channel_names", newTopics.toString());
                            lv = (ListView) ((Activity) context).findViewById(R.id.tickerLV);

                            JSONArray newIds = new JSONArray();
                            JSONArray ids = new JSONArray(Utils.getStringFromSharedPrefs("youtube_channel_ids", "[]"));
                            for (int i = 0; i < ids.length(); i++) {
                                if (i != position) {
                                    newIds.put(ids.getString(i));
                                }
                            }
                            Utils.putStringInSharedPrefs("youtube_channel_ids", newIds.toString());
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("youtube_"+ids.get(position));
                        } else if (context instanceof FlightChannelActivity) {
                            Utils.putStringInSharedPrefs("flight_codes", newTopics.toString());
                            lv = (ListView) ((Activity) context).findViewById(R.id.tickerLV);
                        } else if (context instanceof RedditChannelActivity) {
                            Utils.putStringInSharedPrefs("subreddits", newTopics.toString());
                            lv = (ListView) ((Activity) context).findViewById(R.id.tickerLV);
                        } else if (context instanceof TwitterChannelActivity) {
                            Utils.putStringInSharedPrefs("twitter_handles", newTopics.toString());
                            lv = (ListView) ((Activity) context).findViewById(R.id.tickerLV);
                            final String handle = topics.getString(position);
                            Utils.volleyStringCall(context, "http://35.202.114.198/sup/?operation=rem&value=" + handle, new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    FirebaseMessaging.getInstance().unsubscribeFromTopic("twitter_"+handle); // UnSubscribe from FCM topic
                                }
                            });
                        }  else {
                            lv = null; // shouldn't ever happen
                        }

                        lv.setAdapter(new TopicsAdapter(context));
                        if (newTopics.length() == 0) {
                            ((Activity) context).findViewById(R.id.frame).setVisibility(View.VISIBLE);

                            // Delete channel contacts
                            if (context instanceof StocksMenuActivity) {
                                // They've just deleted their last symbol
                                Utils.dbDelete(context, "CHANNEL_STOCKS");
                                Utils.populateMainDataSet(context);
                            } else if (context instanceof WeatherChannelActivity) {
                                Utils.dbDelete(context, "CHANNEL_WEATHER");
                                Utils.populateMainDataSet(context);
                            } else if (context instanceof FlightChannelActivity) {
                                Utils.dbDelete(context, "CHANNEL_FLIGHTS");
                                Utils.populateMainDataSet(context);
                            } else if (context instanceof SportsChannelActivity) {
                                Utils.dbDelete(context, "CHANNEL_SPORTS");
                                Utils.populateMainDataSet(context);
                            }

                            // Make LVs invisible
                            if (context instanceof NewsKeywordsActivity) {
                                ((Activity) context).findViewById(R.id.topicsLV).setVisibility(View.INVISIBLE);
                            } else if (!(context instanceof WeatherChannelActivity)){
                                ((Activity) context).findViewById(R.id.tickerLV).setVisibility(View.INVISIBLE);
                            }
                        }
                    } catch (Exception e) {e.printStackTrace();}
                }
            });
        } catch (Exception e) {e.printStackTrace();}


        convertView.setEnabled(false);
        convertView.setOnClickListener(null);

        return convertView;
    }
    public static class ViewHolder {
        public TextView topicTV;
        public ImageView deleteIV;
    }
}