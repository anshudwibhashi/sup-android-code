package com.sup.sup;

/**
 * Created by anshudwibhashi on 16/01/17.
 */
public class Constants {
    // All globally required constants are defined in this file
    public final static int RC_SIGN_IN = 100;
    public final static int REQUEST_CODE_PICKER = 101;
    public final static int REQUEST_CAMERA_IMAGE = 102;
    public final static int REQUEST_CAMERA_VIDEO = 103;
    public final static int MY_PERMISSIONS_REQUEST_VIDEO_RESPONSE = 105;
    public final static int MY_PERMISSIONS_REQUEST_IMAGE_RESPONSE = 107;
    public final static int MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE = 108;
    public final static int MY_PERMISSIONS_REQUEST_AUDIO_RESPONSE = 109;
    public final static byte CONTENT_TYPE_VOICE = 110;
    public final static int ADOBE_IMAGE_EDITOR = 111;
    public final static int MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE2 = 112;
    public final static int PLACE_PICKER_REQUEST = 113;
    public final static int REQUEST_CAPTURE_MEDIA = 114;
    public final static byte CONTENT_TYPE_VIDEO = 115;
    public final static int MY_PERMISSIONS_REQUEST_LOCATION_FOR_WEATHER = 116;
    public final static int PACKAGE_NOTIFICATION_ID = 999;
    public final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 117;
}
