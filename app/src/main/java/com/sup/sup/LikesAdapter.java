package com.sup.sup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;


public class LikesAdapter  extends BaseAdapter {
    private LayoutInflater mInflater;
    Context context; Likes likes;

    public LikesAdapter(Context context, Likes likes) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.likes = likes;
    }

    @Override
    public int getCount() {
        return likes.users.size();
    }

    @Override
    public String getItem(int position) {
        return null; // useless to us
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_item_main, null);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.usernameTV = (TextView) convertView.findViewById(R.id.taglineTV);
            holder.photoIV = (ImageView) convertView.findViewById(R.id.photoIV);
            holder.microChatIV = (ImageView) convertView.findViewById(R.id.microChatIV);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.microChatIV.setVisibility(View.GONE);

        holder.nameTV.setText(likes.users.get(position).name);
        holder.usernameTV.setText("@"+likes.users.get(position).username);

        if (Utils.getStringFromSharedPrefs("self_following", "[]").contains(likes.users.get(position).token)) {
            // Already a contact. Load image from disk.
            int position2;
            for (position2 = 0; position2 < Utils.MainDataSet.names.size(); position2++) {
                if (Utils.MainDataSet.tokens.get(position2).equals(likes.users.get(position).token))
                    break;
            }

            Glide.with(context)
                    .load(Utils.MainDataSet.photos.get(position2))
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(context))
                    .placeholder(R.drawable.ic_default_picture)
                    .into(holder.photoIV);
        } else if(likes.users.get(position).token.equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
            Glide.with(context)
                    .load(new File(context.getFilesDir(), "self_photo.png"))
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(context))
                    .placeholder(R.drawable.ic_default_picture)
                    .into(holder.photoIV);
        } else {
            Glide.with(context)
                    .load(likes.users.get(position).photoUrl)
                    .centerCrop()
                    .override(100, 100)
                    .transform(new CircleTransform(context))
                    .placeholder(R.drawable.ic_default_picture)
                    .into(holder.photoIV);

        }

        convertView.setEnabled(false);
        convertView.setOnClickListener(null);

        return convertView;
    }
    public static class ViewHolder {
        public TextView nameTV, usernameTV;
        public ImageView photoIV, microChatIV;
    }
}