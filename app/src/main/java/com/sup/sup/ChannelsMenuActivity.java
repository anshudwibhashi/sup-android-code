package com.sup.sup;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ChannelsMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channels_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ((ListView) findViewById(R.id.channelsLV)).setAdapter(new ChannelMenuAdapter(this));
        ((ListView) findViewById(R.id.channelsLV)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(ChannelsMenuActivity.this, NewsKeywordsActivity.class));
                        break;
                    case 8:
                        startActivity(new Intent(ChannelsMenuActivity.this, WeatherChannelActivity.class));
                        break;
                    case 7:
                        startActivity(new Intent(ChannelsMenuActivity.this, StocksMenuActivity.class));
                        break;
                    case 6:
                        startActivity(new Intent(ChannelsMenuActivity.this, PackageTrackerActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(ChannelsMenuActivity.this, YoutubeChannelActivity.class));
                        break;
                    case 4:
                        startActivity(new Intent(ChannelsMenuActivity.this, TwitterChannelActivity.class));
                        break;
                    case 5:
                        startActivity(new Intent(ChannelsMenuActivity.this, FlightChannelActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(ChannelsMenuActivity.this, RedditChannelActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(ChannelsMenuActivity.this, SportsChannelActivity.class));
                        break;
                }
            }
        });
    }

}
