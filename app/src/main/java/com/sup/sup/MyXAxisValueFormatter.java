package com.sup.sup;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

/**
 * Created by anshudwibhashi on 09/10/17.
 */

public class MyXAxisValueFormatter implements IAxisValueFormatter {
    // for intraday

    @Override
    public String getFormattedValue(float v, AxisBase axis) {
        // "value" represents the position of the label on the axis (x or y)
        int value = (int) v;
        return Utils.timeFrom24to12(String.valueOf(value).substring(0,2)+":"+String.valueOf(value).substring(2));
    }
}