package com.sup.sup;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONObject;

public class StocksMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stocks_menu);
        try {
            JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("stock_tickers", "[]"));
            if(pre.length() > 0) {
                // we have some tickers
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                ListView lv = (ListView) findViewById(R.id.tickerLV);
                lv.setAdapter(new TopicsAdapter(this));
            }
        } catch(Exception e) {e.printStackTrace();}
    }
    public void addTicker(View v) {
        new MaterialDialog.Builder(this)
                .title("Enter symbol")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        // Do something
                        try {
                            String keywords = input.toString();
                            if(keywords != null && !keywords.isEmpty()) {
                                JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("stock_tickers", "[]"));
                                pre.put(keywords);
                                Utils.putStringInSharedPrefs("stock_tickers", pre.toString());
                                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                                ListView lv = (ListView) findViewById(R.id.tickerLV);
                                lv.setAdapter(new TopicsAdapter(StocksMenuActivity.this));

                                // If they've just add their first ticker, create a new contact
                                if (pre.length() == 1) {
                                    JSONObject profile = new JSONObject();
                                    profile.put("token", "CHANNEL_STOCKS");
                                    profile.put("name", "");
                                    profile.put("tagline", "");
                                    profile.put("photoUrl", "");
                                    profile.put("username", "");
                                    profile.put("online_status", "");
                                    Utils.dbAdd(StocksMenuActivity.this, profile, "CHANNEL_STOCKS", new VolleyCallback() {
                                        @Override
                                        public void onResponse(Object result) {

                                        }
                                    });
                                }
                            }
                        } catch (Exception e) {e.printStackTrace();}
                    }
                }).show();
    }
}
