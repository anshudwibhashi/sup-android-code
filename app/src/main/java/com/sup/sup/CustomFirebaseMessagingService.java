package com.sup.sup;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CustomFirebaseMessagingService extends FirebaseMessagingService {

    // Maps to keep track of notifications
    public static  Map<String, NotificationCompat.Builder> supNotificationsBuilders = new HashMap<>();
    public static  Map<String, Integer> supNotificationsIds = new HashMap<>();

    public static  Map<String, NotificationCompat.Builder> responseNotificationsBuilders = new HashMap<>();
    public static  Map<String, Integer> responseNotificationsIds = new HashMap<>();

    public static  Map<String, NotificationCompat.Builder> smalltalkNotificationBuilders = new HashMap<>();
    public static  Map<String, Integer> smalltalkNotificationsIds = new HashMap<>();

    public static  Map<String, NotificationCompat.Builder> broadcastNotificationBuilders = new HashMap<>();
    public static  Map<String, Integer> broadcastNotificationIds = new HashMap<>();

    // START: members to help interact with UI
    static final public String SERVICE_RESULT = "com.sup.sup.CustomFirebaseMessagingService.REQUEST_PROCESSED";
    static final public String SERVICE_MESSAGE = "com.sup.sup.CustomFirebaseMessagingService.MSG";
    static final public String SERVICE_TOKEN = "com.sup.sup.CustomFirebaseMessagingService.TKN";
    private LocalBroadcastManager broadcaster;
    public void sendResult(String message) {
        Intent intent = new Intent(SERVICE_RESULT);
        if(message != null)
            intent.putExtra(SERVICE_MESSAGE, message);
        broadcaster.sendBroadcast(intent);
    }
    public void sendResult2(String message, String user_token) {
        Intent intent = new Intent(SERVICE_RESULT);
        if(message != null && user_token != null) {
            intent.putExtra(SERVICE_MESSAGE, message);
            intent.putExtra(SERVICE_TOKEN, user_token);
        }
        broadcaster.sendBroadcast(intent);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Utils.mSharedPreferences = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        broadcaster = LocalBroadcastManager.getInstance(this);
    }
    // END

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        final Map<String,String> data = remoteMessage.getData();

        // Ignore messages if no one's signed in
        if(!Utils.getBooleanFromSharedPrefs("signed_in", false)) return;

        String sender = (data.get("sender") != null ? data.get("sender") : (data.get("token") != null ? data.get("token") : data.get("user")));
        if (data.get("type").equals("comment")) {
            sender = ((Comments.Commenter)Utils.deserialize(data.get("commenter"), Comments.Commenter.class)).token;
        }
        if ((data.get("type").equals("broadcast_liked")) || data.get("type").equals("broadcast_unliked")) {
            sender = ((Likes.User) Utils.deserialize(data.get("sender"), Likes.User.class)).token;
        }
        if ((data.get("type").equals("group_created"))) {
            sender = data.get("owner");
        }
        Log.d("Message received", data.toString());

        Utils.isUserBlocked(this, sender, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                if (result.equals(false)) {
                    switch(data.get("type")){
                        case "refresh":
                            if(data.containsKey("online_status")) {
                                dbUpdateOnlineStatus(CustomFirebaseMessagingService.this, data.get("online_status"), data.get("token")); // bypassing another request
                            } else {
                                refresh(CustomFirebaseMessagingService.this, data.get("token"));
                            }
                            break;
                        case "sup":
                            // Someone sent a sup
                            handleSup(data); // Adds sup to db, refreshes UI, notifies user
                            break;
                        case "text_response":
                        case "image_response":
                        case "video_response":
                        case "audio_response":
                        case "location_response":
                            handleResponse(data.get("type"), data); // Adds response to db, downloads media, refreshes UI, notifies user
                            break;
                        case "smalltalk_message":
                            handleSmalltalk(data);
                            break;
                        case "text_broadcast":
                        case "image_broadcast":
                        case "video_broadcast":
                        case "audio_broadcast":
                        case "location_broadcast":
                            handleBroadcast(data.get("type"), data); // Adds bcast to db, downloads media, refreshes UI, notifies user
                            break;
                        case "broadcast_liked":
                            handleLike(data);
                            break;
                        case "broadcast_unliked":
                            handleUnlike(data);
                            break;
                        case "comment":
                            handleComment(data);
                            break;
                        case "die":
                            Utils.putBooleanInSharedPrefs("kill", true);
                            break;
                        case "deleted":
                            handleUserDeleted(data);
                            break;
                        case "package_channel":
                            handlePackage(data);
                            break;
                        case "youtube_channel":
                            handleYoutube(data);
                            break;
                        case "twitter":
                            handleTwitter(data);
                            break;
                        case "group_created":
                            handleGroupCreated(data);
                            break;
                        case "group_deleted":
                            handleGroupDeleted(data);
                            break;
                        case "group_edited":
                            handleGroupEdited(data);
                            break;
                        case "group_sup":
                            handleGroupSup(data);
                            break;
                        case "group_response":
                            try {
                                handleGroupResponse(new JSONObject(data.get("response_data")).getString("type"), data);
                            } catch (Exception e) {e.printStackTrace();}
                            break;
                        case "done_group_response":
                            handleGroupDone(data);
                            break;
                        case "group_smalltalk_message":
                            handleGroupSmalltalk(data);
                            break;
                        case "scheduled_notification":
                            handleScheduledNotification(data);
                    }
                }
            }
        });
    }

    private void handleScheduledNotification(final Map<String, String> data) {
        int notificationId = Integer.valueOf(data.get("notification_id"));
        String title = Utils.getStringFromSharedPrefs("notification_"+notificationId+"_title","");
        String content = Utils.getStringFromSharedPrefs("notification_"+notificationId+"_message","");
        int icon = Utils.getIntegerFromSharedPrefs("notification_"+notificationId+"_icon", R.drawable.ic_launcher_alternative_hi_res);
        String activityToLaunch = Utils.getStringFromSharedPrefs("notification_"+notificationId+"_activitytoopen", "");
        androidx.core.app.NotificationCompat.Builder builder = new androidx.core.app.NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_notification)
                .setLargeIcon(((BitmapDrawable) this.getResources().getDrawable(icon)).getBitmap())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setChannelId("channel_id");

        if (!activityToLaunch.equals("")) {
            try {
                Intent intent = new Intent(this, Class.forName(activityToLaunch));
                intent.putExtra("launchedFromScheduledNotif", true);
                PendingIntent activity = PendingIntent.getActivity(this, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                builder.setContentIntent(activity);
            } catch (Exception e) {e.printStackTrace();}
        }

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId, builder.build());
        Log.d("NOtif handler", "dyin ghere");
    }

    private void handleGroupDone(final Map<String, String> data) {
        try {
            SQLiteDatabase dbReadable = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
            Cursor result = dbReadable.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{data.get("groupName")});
            result.moveToFirst();
            JSONArray responseList = new JSONObject(result.getString(result.getColumnIndexOrThrow("response"))).getJSONArray("responseList");
            for (int i = 0; i < responseList.length(); i++) {
                JSONObject response = new JSONObject(responseList.getString(i));
                if (response.has("response")) {
                    Utils.deleteFromFCS(new JSONObject(responseList.getString(i)).getString("response"), new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            // don't care
                        }
                    });
                }
            }

            ContentValues cv = new ContentValues();
            JSONObject response = new JSONObject();
            response.put("sup_sender", ""); // Done
            response.put("responseList", new JSONArray());
            cv.put("response", response.toString());
            cv.put("sup_count", 0);
            Log.d("Done", "Handling group done");
            SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
            dbWritable.update("connections", cv, "token=?", new String[]{data.get("groupName")});
            refreshUI();
        } catch (Exception e) {e.printStackTrace();}
    }

    private void handleGroupSup(final Map<String, String> data) {
        final String supSender = data.get("sup_sender");
        final String groupName = data.get("group_name");

        // First check if this user is in connections db
        final SQLiteDatabase dbReadable = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
        final Cursor c = dbReadable.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{groupName});
        c.moveToFirst();

        final SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(this).getWritableDatabase();

        // Update connection
        int supCount = c.getInt(c.getColumnIndexOrThrow("sup_count"));

        // Update connection
        boolean notify = true;
        if (supCount == 1 && !Utils.getBooleanFromSharedPrefs("keep_sup_count", true)) {
            notify = false;
            supCount = 1;
        } else {
            supCount++;
        }

        try {

            ContentValues cv = new ContentValues();
            cv.put("sup_count", supCount);
            cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
            JSONObject response = new JSONObject();
            response.put("sup_sender", supSender);
            response.put("responseList", new JSONArray());
            cv.put("response", response.toString());
            dbWritable.update("connections", cv, "token=?", new String[]{groupName});

            refreshUI();
        } catch (Exception e) {e.printStackTrace();}

        String message;
        if (supCount == 1) {
            message = "@"+supSender+" just supped the group";
        } else {
            message = "@"+supSender+" supped the group "+supCount + " times";
        }

        // Notify
        if (Utils.getBooleanFromSharedPrefs("notify_on_sup", true) && notify) {

            if (supNotificationsBuilders.containsKey(groupName)) { // update
                androidx.core.app.NotificationCompat.Builder mBuilder = (androidx.core.app.NotificationCompat.Builder) supNotificationsBuilders.get(groupName);

                mBuilder = Utils.prepareNotificationForSup(mBuilder, this, message, supNotificationsIds.get(groupName), groupName, false);

                _notify(mBuilder, supNotificationsIds.get(groupName));
            } else { // new notification
                Cursor profile = dbReadable.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{groupName});
                if (profile.getCount() > 0) {
                    profile.moveToFirst();
                    String name = profile.getString(profile.getColumnIndexOrThrow("name"));
                    String photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));

                    Bitmap iconLarge;
                    if (photoUrl.isEmpty() || photoUrl == null) {
                        iconLarge = null;
                    } else {
                        iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), groupName + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                    }
                    Bundle extra = new Bundle();
                    extra.putString("delete_sup_notification", groupName);

                    androidx.core.app.NotificationCompat.Builder mBuilder = buildNotification2(name, null, HomeActivity.class, extra, iconLarge, "sup", groupName);
                    int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                    mBuilder = Utils.prepareNotificationForSup(mBuilder, this, message, mNotificationId, groupName, true);
                    _notify(mBuilder, mNotificationId);

                    supNotificationsBuilders.put(groupName, mBuilder);
                    supNotificationsIds.put(groupName, mNotificationId);
                }
            }
        }
    }

    private void handleGroupDeleted(final Map<String, String> data) {
        Utils.deleteGroup(this, data.get("groupName"), new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                Utils.populateMainDataSet(CustomFirebaseMessagingService.this);
                refreshUI();
            }
        });
    }

    private void handleGroupEdited(final Map<String, String> data) {
        try {
            final JSONObject group = new JSONObject(data.get("group"));
            Utils.downloadImageAndSave(this, group.getString("photoUrl"), group.getString("groupName") + "_photo.png", new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    // Now update database
                    try {
                        Utils.updateGroup(CustomFirebaseMessagingService.this, group.getString("groupName"), group.getString("groupDispName"),
                                group.getString("groupDesc"), group.getString("participants"),
                                group.getString("photoUrl"), new VolleyCallback() {
                                    @Override
                                    public void onResponse(Object result) {
                                        // refresh glide cache
                                        new AsyncTask<Void, Void, Void>() {
                                            @Override
                                            protected void onPreExecute() {
                                                super.onPreExecute();
                                                Log.d("", "Starting Async Glide refresh");
                                            }

                                            @Override
                                            protected Void doInBackground(Void... voids) {
                                                Glide.get(CustomFirebaseMessagingService.this).clearDiskCache();
                                                return null;
                                            }

                                            @Override
                                            protected void onPostExecute(Void aVoid) {
                                                Glide.get(CustomFirebaseMessagingService.this).clearMemory();
                                                refreshUI();
                                                Log.d("", "Ending Async Glide refresh");
                                            }
                                        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) { e.printStackTrace();}
    }

    private void handleGroupCreated(final Map<String, String> data) {
        final SQLiteDatabase dbReadable = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
        final Cursor c = dbReadable.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{data.get("owner")});
        c.moveToFirst();
        boolean contact = c.getCount() == 1;

        try {
            final JSONObject group = new JSONObject(data.get("group"));
            if (contact || !Utils.getBooleanFromSharedPrefs("restricted_profile", false)) {
                // Either they're a contact, or you don't have a restricted profile
                Utils.downloadImageAndSave(this, group.getString("photoUrl"), group.getString("groupName") + "_photo.png", new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        // Now update database
                        try {
                            final String filepath = group.getString("groupName") + "_photo.png";
                            final String dispName = group.getString("groupDispName");
                            Utils.updateGroup(CustomFirebaseMessagingService.this, group.getString("groupName"), group.getString("groupDispName"),
                                    group.getString("groupDesc"), group.getString("participants"),
                                    group.getString("photoUrl"), new VolleyCallback() {
                                        @Override
                                        public void onResponse(Object result) {
                                            refreshUI();

                                            NotificationCompat.Builder mBuilder = CustomFirebaseMessagingService.this.buildNotification(dispName,
                                                    "You were added to the group", null, null,
                                                    BitmapFactory.decodeFile(filepath), null, null);

                                            _notify(mBuilder, Constants.PACKAGE_NOTIFICATION_ID);
                                        }
                                    });
                        } catch (Exception e) {e.printStackTrace();}
                    }
                });
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    private void handleTwitter(final Map<String, String> data) {
        try {
            JSONObject tweet = new JSONObject(data.get("tweet"));
            SQLiteDatabase broadcastsDBR = BroadcastsDBHelper.getHelper(this).getReadableDatabase();
            Cursor result = broadcastsDBR.rawQuery("SELECT * FROM broadcasts WHERE id=?", new String[]{"CHANNEL_TWITTER_" + tweet.get("id_str")});
            result.moveToFirst();
            if(result.getCount() == 0) {
                // add to broadcastsDB
                ContentValues cv = new ContentValues();

                String time = System.currentTimeMillis() + "";
                cv.put("timestamp", time);
                cv.put("id", "CHANNEL_TWITTER_" + tweet.get("id_str"));
                cv.put("text", tweet.toString());

                SQLiteDatabase broadcastsDBW = BroadcastsDBHelper.getHelper(this).getWritableDatabase();
                broadcastsDBW.insert("broadcasts", null, cv);

                refreshUI2("");
                if(Utils.getBooleanFromSharedPrefs("show_twitter_channel_notifications", true)) {
                    NotificationCompat.Builder mBuilder = this.buildNotification("Sup Twitter Update",
                            tweet.getJSONObject("user").getString("name") + " tweeted: \"" + tweet.getString("text") + "\"", null, null,
                            BitmapFactory.decodeResource(getResources(), R.drawable.ic_twitter_channel), null, null);

                    _notify(mBuilder, Constants.PACKAGE_NOTIFICATION_ID);
                }
            }

        } catch (Exception e) {e.printStackTrace();}
    }

    private void handleYoutube(final Map<String, String> data) {
        try {
            SQLiteDatabase broadcastsDBR = BroadcastsDBHelper.getHelper(this).getReadableDatabase();
            Cursor result = broadcastsDBR.rawQuery("SELECT * FROM broadcasts WHERE id=?", new String[]{"CHANNEL_YOUTUBE_" + data.get("videoid")});
            result.moveToFirst();
            if(result.getCount() == 0) {
                // add to broadcastsDB
                ContentValues cv = new ContentValues();

                JSONObject video = new JSONObject();
                video.put("title", data.get("title"));
                video.put("link", data.get("link"));
                video.put("channel", data.get("channel"));
                video.put("videoid", data.get("videoid"));
                video.put("channelid", data.get("channelid"));

                String time = System.currentTimeMillis() + "";
                cv.put("timestamp", time);
                cv.put("id", "CHANNEL_YOUTUBE_" + data.get("videoid"));
                cv.put("text", video.toString());

                SQLiteDatabase broadcastsDBW = BroadcastsDBHelper.getHelper(this).getWritableDatabase();
                broadcastsDBW.insert("broadcasts", null, cv);

                refreshUI2("");
                if(Utils.getBooleanFromSharedPrefs("show_youtube_channel_notifications", true)) {
                    NotificationCompat.Builder mBuilder = this.buildNotification("Sup YouTube Update",
                            "New video by " + data.get("channel"), null, null,
                            BitmapFactory.decodeResource(getResources(), R.drawable.ic_youtube_channel), null, null);

                    _notify(mBuilder, Constants.PACKAGE_NOTIFICATION_ID);
                }

            }

        } catch (Exception e) {e.printStackTrace();}
    }

    private void handlePackage(final Map<String, String> data) {
        try {
            JSONObject response = new JSONObject(data.get("info"));
            // add to broadcastsDB
            ContentValues cv = new ContentValues();

            String time = System.currentTimeMillis() + "";
            cv.put("timestamp", time);
            cv.put("id", "CHANNEL_PACKAGE_" + response.getString("trackingid"));
            cv.put("text", data.get("info"));

            SQLiteDatabase broadcastsDBW = BroadcastsDBHelper.getHelper(this).getWritableDatabase();
            broadcastsDBW.insert("broadcasts", null, cv);

            refreshUI2("");
            NotificationCompat.Builder mBuilder = this.buildNotification("Sup Package Update",
                    data.get("info"), null, null,
                    BitmapFactory.decodeResource(getResources(), R.drawable.ic_package_channel), null, null);

            _notify(mBuilder, Constants.PACKAGE_NOTIFICATION_ID);
        } catch (Exception e) {e.printStackTrace();}
    }

    private void handleUserDeleted(final Map<String, String> data) {
        String userToken = data.get("user");
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
        db.delete("connections", "token=?", new String[]{userToken});
        try {
            JSONArray a = new JSONArray(Utils.getStringFromSharedPrefs("self_following", "[]"));
            JSONArray b = new JSONArray();
            for (int i = 0; i < a.length(); i++) {
                if (a.getString(i).equals(userToken)) continue;
                b.put(a.getString(i));
            }
            Utils.putStringInSharedPrefs("self_following", b.toString());
        } catch (Exception e) {e.printStackTrace();}
        Utils.populateMainDataSet(this);
        refreshUI();
    }

    private void handleComment(final Map<String, String> data) {
        String id = data.get("id");
        Comments.Commenter commenter = (Comments.Commenter)Utils.deserialize(data.get("commenter"), Comments.Commenter.class);
        if(commenter.username.equals(Utils.getStringFromSharedPrefs("self_username", ""))) {
            return;
        }

        Cursor bcast = BroadcastsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM broadcasts WHERE id=?", new String[]{id});
        bcast.moveToFirst();
        if(bcast.getCount() > 0) {
            String owner_token = bcast.getString(bcast.getColumnIndexOrThrow("owner_token"));

            Comments comments = (Comments) Utils.deserialize(bcast.getString(bcast.getColumnIndexOrThrow("comments")), Comments.class);
            comments.users.add(commenter);

            ContentValues cv = new ContentValues();
            cv.put("comments", Utils.serialize(comments));
            BroadcastsDBHelper.getHelper(this).getWritableDatabase().update("broadcasts", cv, "id=?", new String[]{id});

            refreshUI2(id);

            // Notify

            if (Utils.getBooleanFromSharedPrefs("notify_on_comments", true)) {

                Cursor profile = ConnectionsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM connections WHERE token=?", new String[]{commenter.username});
                profile.moveToFirst();

                String name = commenter.name;
                String photoUrl, token;

                if (profile.getCount() == 0) {
                    photoUrl = "";
                    token = "";
                } else {
                    photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));
                    token = profile.getString(profile.getColumnIndexOrThrow("token"));
                }

                Bitmap iconLarge;
                if (photoUrl == null || photoUrl.isEmpty()) {
                    iconLarge = null;
                } else {
                    iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), token + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                }

                if (Utils.containsUsername(commenter.comment)) {
                    if (Utils.getBooleanFromSharedPrefs("notify_on_mention", true)) {
                        String title = name;
                        String content = "Mentioned you in a comment";
                        Bundle extra = new Bundle();
                        extra.putString("delete_broadcast_notification", id);

                        if (broadcastNotificationBuilders.containsKey(id)) { // update
                            NotificationCompat.Builder mBuilder = broadcastNotificationBuilders.get(id);

                            mBuilder.setContentText(content);
                            _notify(mBuilder, broadcastNotificationIds.get(id));
                        } else {
                            NotificationCompat.Builder mBuilder = buildNotification(title, content, HomeActivity.class, extra, iconLarge, "broadcast", id);

                            int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                            _notify(mBuilder, mNotificationId);

                            broadcastNotificationBuilders.put(id, mBuilder);
                            broadcastNotificationIds.put(id, mNotificationId);
                        }
                    }

                } else if (owner_token.equals(Utils.getStringFromSharedPrefs("self_token", ""))) {

                    Bundle extra = new Bundle();
                    extra.putString("delete_broadcast_notification", id);

                    if (broadcastNotificationBuilders.containsKey(id)) { // update
                        androidx.core.app.NotificationCompat.Builder mBuilder = Utils.prepareNotificationForComment(this, comments, false, Utils.serialize(comments), id, owner_token, broadcastNotificationIds.get(id));
                        _notify(mBuilder, broadcastNotificationIds.get(id));
                    } else {
                        int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                        androidx.core.app.NotificationCompat.Builder mBuilder = Utils.prepareNotificationForComment(this, comments, false, Utils.serialize(comments), id, owner_token, mNotificationId);
                        _notify(mBuilder, mNotificationId);

                        broadcastNotificationBuilders.put(id, mBuilder);
                        broadcastNotificationIds.put(id, mNotificationId);
                    }
                }
            }
        }

    }

    private void handleLike(final Map<String, String> data) {
        Likes.User user = (Likes.User) Utils.deserialize(data.get("sender"), Likes.User.class);
        if(user.username.equals(Utils.getStringFromSharedPrefs("self_username", ""))) {
            return;
        }
        String id = data.get("id");

        Cursor bcast = BroadcastsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM broadcasts WHERE id=?", new String[]{id});
        bcast.moveToFirst();
        if(bcast.getCount() > 0) {
            String owner_token = bcast.getString(bcast.getColumnIndexOrThrow("owner_token"));
            Likes likes = (Likes) Utils.deserialize(bcast.getString(bcast.getColumnIndexOrThrow("likes")), Likes.class);
            likes.users.add(user);

            ContentValues cv = new ContentValues();
            cv.put("likes", Utils.serialize(likes));
            BroadcastsDBHelper.getHelper(this).getWritableDatabase().update("broadcasts", cv, "id=?", new String[]{id});

            refreshUI2(id);

            // Notify
            if (Utils.getBooleanFromSharedPrefs("notify_on_like", true)) {
                if (owner_token.equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
                    Cursor profile = ConnectionsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM connections WHERE token=?", new String[]{user.token});
                    profile.moveToFirst();

                    String name = user.name;
                    String photoUrl, token;

                    if (profile.getCount() == 0) {
                        photoUrl = "";
                        token = "";
                    } else {
                        photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));
                        token = profile.getString(profile.getColumnIndexOrThrow("token"));
                    }

                    Bitmap iconLarge;
                    if (photoUrl == null || photoUrl.isEmpty()) {
                        iconLarge = null;
                    } else {
                        iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), token + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                    }

                    String title, content;
                    if (likes.users.size() == 1) {
                        title = name;
                    } else if (likes.users.size() == 2) {
                        title = name + " and another person";
                    } else {
                        title = name + " and " + (likes.users.size() - 1) + " other people";
                    }

                    if (likes.users.size() <= 2) {
                        content = "Likes your broadcast";
                    } else {
                        content = "Like your broadcast";
                    }

                    Bundle extra = new Bundle();
                    extra.putString("delete_broadcast_notification", id);

                    if (broadcastNotificationBuilders.containsKey(id)) { // update
                        NotificationCompat.Builder mBuilder = broadcastNotificationBuilders.get(id);

                        mBuilder.setContentText(content);
                        _notify(mBuilder, broadcastNotificationIds.get(id));
                    } else {
                        NotificationCompat.Builder mBuilder = buildNotification(title, content, HomeActivity.class, extra, iconLarge, "broadcast", id);

                        int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                        _notify(mBuilder, mNotificationId);

                        broadcastNotificationBuilders.put(id, mBuilder);
                        broadcastNotificationIds.put(id, mNotificationId);
                    }
                }
            }
        }
    }

    private void handleUnlike(final Map<String, String> data) {
        Likes.User user = (Likes.User) Utils.deserialize(data.get("sender"), Likes.User.class);
        if(user.username.equals(Utils.getStringFromSharedPrefs("self_username", ""))) {
            return;
        }
        String id = data.get("id");

        Cursor bcast = BroadcastsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM broadcasts WHERE id=?", new String[]{id});
        bcast.moveToFirst();
        if(bcast.getCount() > 0) {
            Likes likes = (Likes) Utils.deserialize(bcast.getString(bcast.getColumnIndexOrThrow("likes")), Likes.class);

            Likes newLikes = new Likes();
            for (int i = 0; i < likes.users.size(); i++) {
                if (likes.users.get(i).username.equals(user.username)) {
                    continue;
                }
                newLikes.users.add(likes.users.get(i));
            }

            ContentValues cv = new ContentValues();
            cv.put("likes", Utils.serialize(newLikes));
            BroadcastsDBHelper.getHelper(this).getWritableDatabase().update("broadcasts", cv, "id=?", new String[]{id});

            refreshUI2(id);
        }
    }

    private void handleBroadcast (final String type, final Map<String, String> data) {
        Cursor profile = ConnectionsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM connections WHERE token=?", new String[]{data.get("sender")});
        profile.moveToFirst();
        String name = profile.getString(profile.getColumnIndexOrThrow("name"));

        final String id = data.get("id");

        // Add to db
        Utils.addBroadcastToDb(this, id, name, data.get("sender"), data.get("text"), data.get("media"), new Likes(), new Comments(), 0);

        // Download any associated media
        if(type.equals("image_broadcast") || type.equals("video_broadcast") || type.equals("audio_broadcast")) {
            Utils.downloadFromFCS(this, data.get("media"), new VolleyCallback(){
                @Override
                public void onResponse(Object result) {
                    if(type.equals("video_broadcast")) {
                        // Create a snapshot of the video
                        try {
                            File output = new File(getFilesDir(), "snapshot_" + data.get("media").replace(".mp4", ".png"));
                            output.createNewFile();
                            Utils.snapshot(new File(getFilesDir(), data.get("media")), output, CustomFirebaseMessagingService.this);
                        } catch (Exception e){e.printStackTrace();}
                    }
                    refreshUI2(id);
                }
            });
        } else if (type.equals("location_broadcast")) {
            // Download a static map
            double lat = Double.valueOf(data.get("media").split("_")[0]);
            double lon = Double.valueOf(data.get("media").split("_")[1]);

            Utils.downloadImageAndSave(CustomFirebaseMessagingService.this, Utils.prepareStaticMapUrl(CustomFirebaseMessagingService.this, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    refreshUI2(id);
                }
            });
        } else if (type.equals("text_broadcast")) {
            refreshUI2(id);
        }

        // Notify of mention
        if (Utils.getBooleanFromSharedPrefs("notify_on_mention", true)) {
            if (Utils.containsUsername(data.get("text"))) {
                String title = name;
                String content = "Mentioned you in a broadcast";
                Bundle extra = new Bundle();
                extra.putString("delete_broadcast_notification", id);

                String photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));
                String token = profile.getString(profile.getColumnIndexOrThrow("token"));

                Bitmap iconLarge;
                if (photoUrl == null || photoUrl.isEmpty()) {
                    iconLarge = null;
                } else {
                    iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), token + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                }

                if (broadcastNotificationBuilders.containsKey(id)) { // update
                    NotificationCompat.Builder mBuilder = broadcastNotificationBuilders.get(id);

                    mBuilder.setContentText(content);
                    _notify(mBuilder, broadcastNotificationIds.get(id));
                } else {
                    NotificationCompat.Builder mBuilder = buildNotification(title, content, HomeActivity.class, extra, iconLarge, "broadcast", id);

                    int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                    _notify(mBuilder, mNotificationId);

                    broadcastNotificationBuilders.put(id, mBuilder);
                    broadcastNotificationIds.put(id, mNotificationId);
                }
            }
        }
    }

    private void handleGroupSmalltalk(final Map<String, String> data) {
        final String sender_token = data.get("sender");
        final String message = data.get("message");
        if (data.get("messageType").equals("text")) {
            // no media to download
            updateConnectionAfterGroupSmalltalk(sender_token, message, data);
        } else if (data.get("messageType").equals("image_response") || data.get("messageType").equals("video_response") || data.get("messageType").equals("audio_response")){
            Utils.downloadFromFCS(this, data.get("message"), new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    if (data.get("messageType").equals("video_response")) {
                        // Create a snapshot of the video
                        try {
                            File output = new File(getFilesDir(), "snapshot_" + data.get("message").replace(".mp4", ".png"));
                            output.createNewFile();
                            Utils.snapshot(new File(getFilesDir(), data.get("message")), output, CustomFirebaseMessagingService.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    updateConnectionAfterGroupSmalltalk(sender_token, message, data);
                }
            });
        } else {
            // Download a static map
            double lat = Double.valueOf(data.get("message").split("_")[0]);
            double lon = Double.valueOf(data.get("message").split("_")[1]);

            Utils.downloadImageAndSave(CustomFirebaseMessagingService.this, Utils.prepareStaticMapUrl(CustomFirebaseMessagingService.this, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    updateConnectionAfterGroupSmalltalk(sender_token, message, data);
                }
            });
        }
    }

    private void handleSmalltalk(final Map<String, String> data) {
        final String sender_token = data.get("sender");
        final String message = data.get("message");

        // First check if this user is in connections db
        final SQLiteDatabase dbReadable = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
        final Cursor c = dbReadable.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{sender_token});
        c.moveToFirst();
        int exists = c.getCount();

        downloadMedia(data, exists, sender_token, message);
    }

    private void downloadMedia(final Map<String, String> data, final int exists, final String sender_token, final String message) {
        if (data.get("messageType").equals("text")) {
            // no media to download
            addContactIfNotAlready(exists, sender_token, message, data);
        } else if (data.get("messageType").equals("image_response") || data.get("messageType").equals("video_response") || data.get("messageType").equals("audio_response")){
            Utils.downloadFromFCS(this, data.get("message"), new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    if (data.get("messageType").equals("video_response")) {
                        // Create a snapshot of the video
                        try {
                            File output = new File(getFilesDir(), "snapshot_" + data.get("message").replace(".mp4", ".png"));
                            output.createNewFile();
                            Utils.snapshot(new File(getFilesDir(), data.get("message")), output, CustomFirebaseMessagingService.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    Utils.deleteFromFCS(data.get("message"), new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            // don't care
                        }
                    });
                    addContactIfNotAlready(exists, sender_token, message, data);
                }
            });
        } else {
            // Download a static map
            double lat = Double.valueOf(data.get("message").split("_")[0]);
            double lon = Double.valueOf(data.get("message").split("_")[1]);

            Utils.downloadImageAndSave(CustomFirebaseMessagingService.this, Utils.prepareStaticMapUrl(CustomFirebaseMessagingService.this, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    addContactIfNotAlready(exists, sender_token, message, data);
                }
            });
        }
    }

    private void addContactIfNotAlready(int exists, final String sender_token, final String message, final Map<String, String> data) {
        if(exists > 0) {
            // already a contact
            updateConnectionAfterSmalltalk(sender_token, message, data);
        } else {
            // add contact first
            if (!Utils.getBooleanFromSharedPrefs("restricted_profile", false)) {
                Utils.downloadProfile(CustomFirebaseMessagingService.this, sender_token, new VolleyCallback() {
                    @Override
                    public void onResponse(Object res) {
                        JSONObject profile = (JSONObject) res;

                        Utils.follow(CustomFirebaseMessagingService.this, profile, sender_token, true, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                // Update connection
                                updateConnectionAfterSmalltalk(sender_token, message, data);
                            }
                        });
                    }
                });
            }
        }
    }

    private void updateConnectionAfterGroupSmalltalk(String sender_token, String message, Map<String, String> data) {
        Cursor result = ConnectionsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM Connections WHERE token=?", new String[]{data.get("groupName")});
        result.moveToFirst();
        Smalltalk.SmalltalkMessage smalltalkMessage = new Gson().fromJson(result.getString(result.getColumnIndexOrThrow("smalltalk")), Smalltalk.SmalltalkMessage.class);
        Smalltalk.MyMessage m;
        if(data.get("senderUsername").trim().isEmpty()) {
            m = new Smalltalk.MyMessage("" + new Random().nextInt(), message, new Smalltalk.MyUser("receiver", ""), new Date());
        } else {
            m = new Smalltalk.MyMessage("" + new Random().nextInt(), message, new Smalltalk.MyUser("receiver", data.get("senderUsername")), new Date());
        }
        smalltalkMessage = Utils.addMessageToGroupSmalltalk(smalltalkMessage, m);

        if(data.get("messageType").equals("image_response")) {
            m.setType("image_response");
            m.setImgPath(data.get("message"));
        } else if (data.get("messageType").equals("video_response")) {
            m.setType("video_response");
            m.setImgPath("snapshot_" + data.get("message").replace(".mp4", ".png"));
        } else if (data.get("messageType").equals("location_response")) {
            m.setType("location_response");
            m.setImgPath(data.get("message")+".png");
        } else if (data.get("messageType").equals("audio_response")) {
            m.setType("audio_response");
            m.setImgPath(data.get("message"));
        }

        // update db
        ContentValues cv = new ContentValues();
        if(!Utils.getStringFromSharedPrefs("smalltalk_open", "").equals(data.get("groupName"))) {
            cv.put("microtext_read", 0);
        }
        cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
        cv.put("smalltalk", new Gson().toJson(smalltalkMessage));
        SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
        dbWritable.update("connections", cv, "token=?", new String[]{data.get("groupName")});

        // refresh UI
        refreshUI3(data.get("groupName"));
        // Notify
        if (Utils.getBooleanFromSharedPrefs("notify_on_smalltalk", true)) {
            if (!Utils.getStringFromSharedPrefs("smalltalk_open", "").equals(data.get("groupName"))) {
                if (smalltalkNotificationBuilders.containsKey(data.get("groupName"))) { // update
                    androidx.core.app.NotificationCompat.Builder mBuilder = (androidx.core.app.NotificationCompat.Builder) smalltalkNotificationBuilders.get(data.get("groupName"));
                    mBuilder = Utils.prepareNotificationForSmalltalk(this, smalltalkMessage, mBuilder, result.getString(result.getColumnIndexOrThrow("name")), smalltalkNotificationsIds.get(data.get("groupName")), data.get("groupName"), false);
                    _notify(mBuilder, smalltalkNotificationsIds.get(data.get("groupName")));
                } else { // new notification
                    Cursor profile = ConnectionsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM connections WHERE token=?", new String[]{data.get("groupName")});
                    if (profile.getCount() > 0) {
                        profile.moveToFirst();
                        String name = profile.getString(profile.getColumnIndexOrThrow("name"));
                        String photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));

                        Bitmap iconLarge;
                        if (photoUrl.isEmpty()) {
                            iconLarge = null;
                        } else {
                            iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), data.get("groupName") + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                        }
                        Bundle extra = new Bundle();
                        extra.putString("delete_mircotext_notification", sender_token);

                        androidx.core.app.NotificationCompat.Builder mBuilder = buildNotification2("Message to group", null, HomeActivity.class, extra, iconLarge, "response", data.get("groupName"));

                        int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                        mBuilder = Utils.prepareNotificationForSmalltalk(this, smalltalkMessage, mBuilder, name, mNotificationId, data.get("groupName"), true);

                        _notify(mBuilder, mNotificationId);

                        smalltalkNotificationBuilders.put(sender_token, mBuilder);
                        smalltalkNotificationsIds.put(sender_token, mNotificationId);
                    }
                }
            }
        }
    }

    private void updateConnectionAfterSmalltalk(String sender_token, String message, Map<String, String> data){
        Cursor result = ConnectionsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM Connections WHERE token=?", new String[]{sender_token});
        result.moveToFirst();
        Smalltalk.SmalltalkMessage smalltalkMessage = new Gson().fromJson(result.getString(result.getColumnIndexOrThrow("smalltalk")), Smalltalk.SmalltalkMessage.class);
        Smalltalk.MyMessage m;
        if(data.get("senderUsername").trim().isEmpty()) {
            m = new Smalltalk.MyMessage("" + new Random().nextInt(), message, new Smalltalk.MyUser("receiver", ""), new Date());
        } else {
            m = new Smalltalk.MyMessage("" + new Random().nextInt(), message, new Smalltalk.MyUser("receiver", data.get("senderUsername")), new Date());
        }
        smalltalkMessage = Utils.addMessageToSmalltalk(smalltalkMessage, m);

        if(data.get("messageType").equals("image_response")) {
            m.setType("image_response");
            m.setImgPath(data.get("message"));
        } else if (data.get("messageType").equals("video_response")) {
            m.setType("video_response");
            m.setImgPath("snapshot_" + data.get("message").replace(".mp4", ".png"));
        } else if (data.get("messageType").equals("location_response")) {
            m.setType("location_response");
            m.setImgPath(data.get("message")+".png");
        } else if (data.get("messageType").equals("audio_response")) {
            m.setType("audio_response");
            m.setImgPath(data.get("message"));
        }

        // update db
        ContentValues cv = new ContentValues();
        if(!Utils.getStringFromSharedPrefs("smalltalk_open", "").equals(sender_token)) {
            cv.put("microtext_read", 0);
        }
        cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
        cv.put("smalltalk", new Gson().toJson(smalltalkMessage));
        SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
        dbWritable.update("connections", cv, "token=?", new String[]{sender_token});

        // refresh UI
        refreshUI3(sender_token);

        try {
            // Notify
            if (Utils.getBooleanFromSharedPrefs("notify_on_smalltalk", true)) {
                if (!Utils.getStringFromSharedPrefs("smalltalk_open", "").equals(sender_token)) {
                    if (smalltalkNotificationBuilders.containsKey(sender_token)) { // update
                        androidx.core.app.NotificationCompat.Builder mBuilder = (androidx.core.app.NotificationCompat.Builder) smalltalkNotificationBuilders.get(sender_token);
                        mBuilder = Utils.prepareNotificationForSmalltalk(this, smalltalkMessage, mBuilder, result.getString(result.getColumnIndexOrThrow("name")), smalltalkNotificationsIds.get(sender_token), sender_token, false);
                        _notify(mBuilder, smalltalkNotificationsIds.get(sender_token));
                    } else { // new notification
                        Cursor profile = ConnectionsDBHelper.getHelper(this).getReadableDatabase().rawQuery("SELECT * FROM connections WHERE token=?", new String[]{sender_token});
                        if (profile.getCount() > 0) {
                            profile.moveToFirst();
                            String name = profile.getString(profile.getColumnIndexOrThrow("name"));
                            String photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));

                            Bitmap iconLarge;
                            if (photoUrl.isEmpty()) {
                                iconLarge = null;
                            } else {
                                iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), sender_token + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                            }
                            Bundle extra = new Bundle();
                            extra.putString("delete_mircotext_notification", sender_token);

                            androidx.core.app.NotificationCompat.Builder mBuilder = buildNotification2("Smalltalk", null, HomeActivity.class, extra, iconLarge, "response", sender_token);

                            int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                            mBuilder = Utils.prepareNotificationForSmalltalk(this, smalltalkMessage, mBuilder, name,mNotificationId, sender_token, true);

                            _notify(mBuilder, mNotificationId);

                            smalltalkNotificationBuilders.put(sender_token, mBuilder);
                            smalltalkNotificationsIds.put(sender_token, mNotificationId);
                        }
                    }
                }
            }
        } catch(Exception e) {e.printStackTrace();}
    }

    private void refresh(final Context context, final String token) {
        /**
         * This function refreshes a contact's profile. (name, tagline, photo, online_status, username)
         */

        Utils.downloadProfile(context, token, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                final JSONObject profile = (JSONObject) result;
                try {
                    // Download image to <token>_photo.png
                    if (profile.getString("photoUrl") != null && !profile.getString("photoUrl").isEmpty()) {
                        Utils.downloadImageAndSave(context, profile.getString("photoUrl"), token + "_photo.png", new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                // Now update database
                                dbUpdate(context, profile, token);
                            }
                        });
                    } else {
                        FileOutputStream outputStream;
                        File file = new File(context.getFilesDir(), token + "_photo.png");
                        file.createNewFile();
                        outputStream = new FileOutputStream(file);
                        BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_default_picture).compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                        outputStream.close();

                        // Now update database
                        dbUpdate(context, profile, token);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void dbUpdate(Context context, JSONObject profile, String token) {
        /**
         * Method to be called by refresh() after downloading contact
         */
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("name", profile.getString("name"));
            values.put("tagline", profile.getString("tagline"));
            values.put("username", profile.getString("username"));

            db.update(
                    "connections",
                    values,
                    "token=?",
                    new String[]{token});
            refreshUI(); // dp, name, tagline, username
            refreshUI3(token); // name, dp
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dbUpdateOnlineStatus(Context context, String online_status, String token) {
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("online_status", online_status);
            db.update(
                    "connections",
                    values,
                    "token=?",
                    new String[]{token});
            refreshUI3(token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshUI() {
        sendResult("update_ui"); // refresh contacts list
    }

    private void refreshUI3(String token) {
        sendResult2("update_ui3", token); // refresh active smalltalk windows
    }

    private void refreshUI2(String token) {
        sendResult2("update_ui2", token); // refresh feed upon new broadcast
    }

    private void handleGroupResponse(final String type, final Map<String, String> data) {
        // Download any associated media
        try {
            final String responseString = new JSONObject(data.get("response_data")).getString("response");
            if (type.equals("image_response") || type.equals("video_response") || type.equals("audio_response")) {
                Utils.downloadFromFCS(this, responseString, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        if (type.equals("video_response")) {
                            // Create a snapshot of the video
                            try {
                                File output = new File(getFilesDir(), "snapshot_" + responseString.replace(".mp4", ".png"));
                                output.createNewFile();
                                Utils.snapshot(new File(getFilesDir(), responseString), output, CustomFirebaseMessagingService.this);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        updateConnectionAfterGroupResponse(type, data);
                    }
                });
            } else if (type.equals("location_response")) {
                // Download a static map
                double lat = Double.valueOf(responseString.split("_")[0]);
                double lon = Double.valueOf(responseString.split("_")[1]);

                Utils.downloadImageAndSave(CustomFirebaseMessagingService.this, Utils.prepareStaticMapUrl(CustomFirebaseMessagingService.this, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        updateConnectionAfterGroupResponse(type, data);
                    }
                });
            } else if (type.equals("text_response")) {
                updateConnectionAfterGroupResponse(type, data);
            } else if (type.equals("ignored")) {
                try {
                    SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
                    Cursor results = dbR.rawQuery("SELECT * FROM Connections WHERE token=?", new String[]{data.get("group_name")});
                    results.moveToFirst();
                    int participantCount = new JSONArray(results.getString(results.getColumnIndexOrThrow("username"))).length();
                    JSONArray responseList = new JSONObject(results.getString(results.getColumnIndexOrThrow("response"))).getJSONArray("responseList");
                    int ignoreCount = 0;
                    for (int i = 0; i < responseList.length(); i++) {
                        if (new JSONObject(responseList.getString(i)).getString("type").equals("ignored")) {
                            ignoreCount++;
                        }
                    }
                    if (ignoreCount >= participantCount - 1) {
                        // All ignored
                        String url = "http://black-abode-2709.appspot.com/done_group_response?to_group=" + URLEncoder.encode(data.get("group_name"))
                                + "&sender=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_username", ""));

                        Utils.volleyStringCall(this, url, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                            }
                        });

                        ContentValues cv = new ContentValues();
                        JSONObject response = new JSONObject();
                        response.put("sup_sender", ""); // Done
                        response.put("responseList", new JSONArray());
                        cv.put("sup_count", 0);
                        cv.put("response", response.toString());

                        SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
                        dbWritable.update("connections", cv, "token=?", new String[]{data.get("groupName")});

                        refreshUI();
                    } else {
                        updateConnectionAfterGroupResponse(type, data);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    private void updateConnectionAfterGroupResponse(String type, Map<String, String> data) {

        // Now add to database
        try {
            String token = data.get("group_name");
            JSONObject responseJSON = new JSONObject(data.get("response_data")).put("sender_username", data.get("sender"));
            String receivedResponse = responseJSON.toString();

            SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
            Cursor results = dbR.rawQuery("SELECT * FROM Connections WHERE token=?", new String[]{token});
            results.moveToFirst();
            JSONObject response = new JSONObject(results.getString(results.getColumnIndexOrThrow("response")));
            response.put("responseList", response.getJSONArray("responseList").put(receivedResponse));

            SQLiteDatabase db = ConnectionsDBHelper.getHelper(this).getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("response", response.toString());
            values.put("last_modified", String.valueOf(System.currentTimeMillis()));
            db.update("connections", values, "token=?", new String[]{token});

            // Now refresh UI
            refreshUI();

            // Notify
            if (Utils.getBooleanFromSharedPrefs("notify_on_respond", true) && !((String) responseJSON.get("type")).equals("ignored")) {
                Cursor profile = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                if (profile.getCount() > 0) {
                    profile.moveToFirst();
                    String name = profile.getString(profile.getColumnIndexOrThrow("name"));
                    String photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));

                    Bitmap iconLarge;
                    if (photoUrl.isEmpty() || photoUrl == null) {
                        iconLarge = null;
                    } else {
                        iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), token + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                    }
                    Bundle extra = new Bundle();
                    extra.putString("delete_response_notification", token);

                    NotificationCompat.Builder mBuilder = buildNotification(name, "Responded to the group", HomeActivity.class, extra, iconLarge, "response", token);
                    mBuilder.setSubText("@"+data.get("sender")+" responded to the group");
                    switch ((String) responseJSON.get("type")) {
                        case "text_response":
                            mBuilder.setContentText(responseJSON.getString("response"));
                            break;
                        case "image_response":
                            SpannableString i = new SpannableString("Image");
                            i.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Image".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            mBuilder.setContentText(i);
                            break;
                        case "video_response":
                            SpannableString v = new SpannableString("Video");
                            v.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Video".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            mBuilder.setContentText(v);
                            break;
                        case "audio_response":
                            SpannableString a= new SpannableString("Audio");
                            a.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Audio".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            mBuilder.setContentText(a);
                            break;
                        case "location_response":
                            SpannableString l = new SpannableString("Location");
                            l.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Location".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            mBuilder.setContentText(l);
                            break;

                    }

                    int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                    _notify(mBuilder, mNotificationId);

                    responseNotificationsBuilders.put(token, mBuilder);
                    responseNotificationsIds.put(token, mNotificationId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

        private void handleResponse(final String type, final Map<String, String> data) {
        // Download any associated media
        if(type.equals("image_response") || type.equals("video_response") || type.equals("audio_response")) {
            Utils.downloadFromFCS(this, data.get("response"), new VolleyCallback(){
                @Override
                public void onResponse(Object result) {
                    if(type.equals("video_response")) {
                        // Create a snapshot of the video
                        try {
                            File output = new File(getFilesDir(), "snapshot_" + data.get("response").replace(".mp4", ".png"));
                            output.createNewFile();
                            Utils.snapshot(new File(getFilesDir(), data.get("response")), output, CustomFirebaseMessagingService.this);
                        } catch (Exception e){e.printStackTrace();}
                    }
                    Utils.deleteFromFCS(data.get("response"), new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            // don't care
                        }
                    });
                    updateConnectionAfterResponse(type, data);
                }
            });
        } else if (type.equals("location_response")) {
            // Download a static map
            double lat = Double.valueOf(data.get("response").split("_")[0]);
            double lon = Double.valueOf(data.get("response").split("_")[1]);

            Utils.downloadImageAndSave(CustomFirebaseMessagingService.this, Utils.prepareStaticMapUrl(CustomFirebaseMessagingService.this, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    updateConnectionAfterResponse(type, data);
                }
            });
        } else if (type.equals("text_response")) {
            updateConnectionAfterResponse(type, data);
        }
    }

    private void updateConnectionAfterResponse(String type, Map<String, String> data) {

        // Now add to database
        try {
            String token = data.get("sender");
            JSONObject responseJSON = new JSONObject().put("type", type).put("response", data.get("response"));
            String response = responseJSON.toString();

            final SQLiteDatabase dbW = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
            final SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(this).getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("response", response);
            cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
            dbW.update("connections", cv, "token=?", new String[]{token});

            // Now refresh UI
            refreshUI();


            // Notify
            if (Utils.getBooleanFromSharedPrefs("notify_on_respond", true)) {
                if (responseNotificationsBuilders.containsKey(token)) { // update
                    NotificationCompat.Builder mBuilder = responseNotificationsBuilders.get(token);
                    mBuilder.setSubText("Responded to your sup");
                    switch ((String) responseJSON.get("type")) {
                        case "text_response":
                            mBuilder.setContentText(responseJSON.getString("response"));
                            break;
                        case "image_response":
                            SpannableString i = new SpannableString("Image");
                            i.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Image".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            mBuilder.setContentText(i);
                            break;
                        case "video_response":
                            SpannableString v = new SpannableString("Video");
                            v.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Video".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            mBuilder.setContentText(v);
                            break;
                        case "audio_response":
                            SpannableString a= new SpannableString("Audio");
                            a.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Audio".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            mBuilder.setContentText(a);
                            break;
                        case "location_response":
                            SpannableString l = new SpannableString("Location");
                            l.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Location".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            mBuilder.setContentText(l);
                            break;

                    }
                    _notify(mBuilder, responseNotificationsIds.get(token));
                } else { // new notification
                    Cursor profile = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                    if (profile.getCount() > 0) {
                        profile.moveToFirst();
                        String name = profile.getString(profile.getColumnIndexOrThrow("name"));
                        String photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));

                        Bitmap iconLarge;
                        if (photoUrl.isEmpty() || photoUrl == null) {
                            iconLarge = null;
                        } else {
                            iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), token + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                        }
                        Bundle extra = new Bundle();
                        extra.putString("delete_response_notification", token);

                        NotificationCompat.Builder mBuilder = buildNotification(name, "Responded to your sup", HomeActivity.class, extra, iconLarge, "response", token);
                        mBuilder.setSubText("Responded to your sup");
                        switch ((String) responseJSON.get("type")) {
                            case "text_response":
                                mBuilder.setContentText(responseJSON.getString("response"));
                                break;
                            case "image_response":
                                SpannableString i = new SpannableString("Image");
                                i.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Image".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                                mBuilder.setContentText(i);
                                break;
                            case "video_response":
                                SpannableString v = new SpannableString("Video");
                                v.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Video".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                                mBuilder.setContentText(v);
                                break;
                            case "audio_response":
                                SpannableString a= new SpannableString("Audio");
                                a.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Audio".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                                mBuilder.setContentText(a);
                                break;
                            case "location_response":
                                SpannableString l = new SpannableString("Location");
                                l.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Location".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                                mBuilder.setContentText(l);
                                break;

                        }

                        int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                        _notify(mBuilder, mNotificationId);

                        responseNotificationsBuilders.put(token, mBuilder);
                        responseNotificationsIds.put(token, mNotificationId);
                    }
                }
            }

        } catch (Exception e) {e.printStackTrace();}
    }

    private void handleSup(Map<String, String> data) {
        /**
         * Adds sup to db, refreshes UI, notifies user
         */
        final String from_token = data.get("sender");

        // First check if this user is in connections db
        final SQLiteDatabase dbReadable = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
        final Cursor c = dbReadable.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{from_token});
        c.moveToFirst();
        int exists = c.getCount();

        final SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(this).getWritableDatabase();
        if (exists > 0) {
            // Update connection
            final int supCount = c.getInt(c.getColumnIndexOrThrow("sup_count"));
            updateConnectionAfterSup(dbWritable, dbReadable, supCount, from_token);
        } else {
            if (!Utils.getBooleanFromSharedPrefs("restricted_profile", false)) {
                // Add new connection
                Utils.downloadProfile(this, from_token, new VolleyCallback() {
                    @Override
                    public void onResponse(Object res) {
                        JSONObject profile = (JSONObject) res;

                        Utils.follow(CustomFirebaseMessagingService.this, profile, from_token, true, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                // Update connection
                                updateConnectionAfterSup(dbWritable, dbReadable, 0, from_token);
                            }
                        });
                    }
                });
            }
        }
    }

    private void updateConnectionAfterSup(SQLiteDatabase dbW, SQLiteDatabase dbR, int supCount, String token) {
        // Update connection
        boolean notify = true;
        if (supCount == 1 && !Utils.getBooleanFromSharedPrefs("keep_sup_count", true)) {
            notify = false;
            supCount = 1;
        } else {
            supCount++;
        }
        ContentValues cv = new ContentValues();
        cv.put("sup_count", supCount);
        cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
        dbW.update("connections", cv, "token=?", new String[]{token});

        refreshUI();

        String message;
        if (supCount == 1) {
            message = "Just supped you";
        } else {
            message = "Supped you " + supCount + " times";
        }

        // Notify
        if (Utils.getBooleanFromSharedPrefs("notify_on_sup", true) && notify) {

            if (supNotificationsBuilders.containsKey(token)) { // update
                androidx.core.app.NotificationCompat.Builder mBuilder = (androidx.core.app.NotificationCompat.Builder) supNotificationsBuilders.get(token);

                mBuilder = Utils.prepareNotificationForSup(mBuilder, this, message, supNotificationsIds.get(token), token, false);

                _notify(mBuilder, supNotificationsIds.get(token));
            } else { // new notification
                Cursor profile = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                if (profile.getCount() > 0) {
                    profile.moveToFirst();
                    String name = profile.getString(profile.getColumnIndexOrThrow("name"));
                    String photoUrl = profile.getString(profile.getColumnIndexOrThrow("photoUrl"));

                    Bitmap iconLarge;
                    if (photoUrl.isEmpty() || photoUrl == null) {
                        iconLarge = null;
                    } else {
                        iconLarge = ImageManip.getRoundedCornerBitmap(ImageManip.decodeSampledBitmapFromFile(new File(CustomFirebaseMessagingService.this.getFilesDir(), token + "_photo.png").getAbsolutePath(), 100, 100), 1000);
                    }
                    Bundle extra = new Bundle();
                    extra.putString("delete_sup_notification", token);

                    androidx.core.app.NotificationCompat.Builder mBuilder = buildNotification2(name, null, HomeActivity.class, extra, iconLarge, "sup", token);
                    int mNotificationId = (int) System.currentTimeMillis() % 100000000;
                    mBuilder = Utils.prepareNotificationForSup(mBuilder, this, message, mNotificationId, token, true);
                    _notify(mBuilder, mNotificationId);

                    supNotificationsBuilders.put(token, mBuilder);
                    supNotificationsIds.put(token, mNotificationId);
                }
            }
        }
    }

    public static NotificationCompat.Builder buildNotification(Context context, String title, String content, Bitmap iconBmp) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_notification)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setLargeIcon(iconBmp)
                        .setColor(Color.parseColor("#673AB7"))
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX);

        if (Utils.getBooleanFromSharedPrefs("notification_lights", true)) {
            mBuilder.setLights(Color.parseColor("#E040FB"), 1000, 200);
        }

        if (Utils.getBooleanFromSharedPrefs("notification_sound", true)) {
            mBuilder.setSound(RingtoneManager.getDefaultUri(2));
        }

        if (Utils.getBooleanFromSharedPrefs("notification_vibration", true)) {
            mBuilder.setVibrate(new long[]{0L, 1000L, 500L});
        }

        return mBuilder;
    }


    private NotificationCompat.Builder buildNotification(String title, String content, Class<?> cls, Bundle extra, Bitmap iconBmp, String purpose, String token) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_notification)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setLargeIcon(iconBmp)
                        .setColor(Color.parseColor("#673AB7"))
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX);

        if (Utils.getBooleanFromSharedPrefs("notification_lights", true)) {
            mBuilder.setLights(Color.parseColor("#E040FB"), 1000, 200);
        }

        if (Utils.getBooleanFromSharedPrefs("notification_sound", true)) {
            mBuilder.setSound(RingtoneManager.getDefaultUri(2));
        }

        if (Utils.getBooleanFromSharedPrefs("notification_vibration", true)) {
            mBuilder.setVibrate(new long[]{0L, 1000L, 500L});
        }

        if(cls != null) {
            Intent resultIntent = new Intent(this, cls);
            resultIntent.putExtra("extra", extra);
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            mBuilder.setContentIntent(resultPendingIntent);
        }

        if(purpose != null && !purpose.isEmpty()) {
            Intent intent = new Intent(NOTIFICATION_DELETED_ACTION);
            intent.putExtra("delete_" + purpose + "_notifiation", token);
            PendingIntent deletePendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

            registerReceiver(receiver, new IntentFilter(NOTIFICATION_DELETED_ACTION));

            mBuilder.setDeleteIntent(deletePendingIntent);
        }

        return mBuilder;
    }

    private androidx.core.app.NotificationCompat.Builder buildNotification2(String title, String content, Class<?> cls, Bundle extra, Bitmap iconBmp, String purpose, String token) {
        androidx.core.app.NotificationCompat.Builder mBuilder =
                new androidx.core.app.NotificationCompat.Builder(this);
        mBuilder
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentTitle(title)
                .setContentText(content)
                .setLargeIcon(iconBmp).setChannelId("channel_id")
                .setColor(Color.parseColor("#673AB7"))
                .setAutoCancel(true).setChannelId("channel_id")
                .setPriority(NotificationCompat.PRIORITY_MAX);

        if (Utils.getBooleanFromSharedPrefs("notification_lights", true)) {
            mBuilder.setLights(Color.parseColor("#E040FB"), 1000, 200);
        }

        if (Utils.getBooleanFromSharedPrefs("notification_sound", true)) {
            mBuilder.setSound(RingtoneManager.getDefaultUri(2));
        }

        if (Utils.getBooleanFromSharedPrefs("notification_vibration", true)) {
            mBuilder.setVibrate(new long[]{0L, 1000L, 500L});
        }

        if(cls != null) {
            Intent resultIntent = new Intent(this, cls);
            resultIntent.putExtra("extra", extra);
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            mBuilder.setContentIntent(resultPendingIntent);
        }

        if(purpose != null && !purpose.isEmpty()) {
            Intent intent = new Intent(NOTIFICATION_DELETED_ACTION);
            intent.putExtra("delete_" + purpose + "_notifiation", token);
            PendingIntent deletePendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

            registerReceiver(receiver, new IntentFilter(NOTIFICATION_DELETED_ACTION));

            mBuilder.setDeleteIntent(deletePendingIntent);
        }

        return mBuilder;
    }

    public static void _notify(NotificationCompat.Builder mBuilder, int mNotificationId, Context context) {
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    private void _notify(NotificationCompat.Builder mBuilder, int mNotificationId) {
        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    /**
     * The following members are for deleting the builder stored in the map
     * once the user cancels the notification.
     */
    private static final String NOTIFICATION_DELETED_ACTION = "NOTIFICATION_DELETED";

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getStringExtra("delete_sup_notification") != null ){
                supNotificationsBuilders.remove(intent.getStringExtra("delete_sup_notification"));
                supNotificationsIds.remove(intent.getStringExtra("delete_sup_notification"));
            } else if (intent.getStringExtra("delete_response_notification") != null ) {
                responseNotificationsBuilders.remove(intent.getStringExtra("delete_sup_notification"));
                responseNotificationsIds.remove(intent.getStringExtra("delete_sup_notification"));
            } else if (intent.getStringExtra("delete_mircotext_notification") != null ) {
                smalltalkNotificationBuilders.remove(intent.getStringExtra("delete_mircotext_notification"));
                smalltalkNotificationsIds.remove(intent.getStringExtra("delete_mircotext_notification"));
            }
            unregisterReceiver(this);
        }
    };

}
