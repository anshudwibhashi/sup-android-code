package com.sup.sup;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.HashMap;

/**
 * Created by anshudwibhashi on 09/10/17.
 */

public class MyXAxisValueFormatter2 implements IAxisValueFormatter {
    // for week

    HashMap<Float, String> xToDays;
    public MyXAxisValueFormatter2(HashMap<Float, String> xToDays) {
        this.xToDays = xToDays;
    }

    @Override
    public String getFormattedValue(float v, AxisBase axis) {
        // "value" represents the position of the label on the axis (x or y)
        return xToDays.get(v);
    }
}