package com.sup.sup;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class WebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        setTitle(getIntent().getStringExtra("title"));

        try {
            StringBuilder buf = new StringBuilder();
            InputStream json = getAssets().open(getIntent().getStringExtra("filename"));
            BufferedReader in =
                    new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String str;

            while ((str = in.readLine()) != null) {
                buf.append(str);
            }

            in.close();

            String data = buf.toString();
            ((WebView) findViewById(R.id.web_view)).loadDataWithBaseURL("file:///android_asset/", data, "text/html","utf-8", null);
        } catch ( Exception e) {e.printStackTrace();}

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
