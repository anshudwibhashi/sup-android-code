package com.sup.sup;

import android.content.DialogInterface;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;

public class SportsChannelActivity extends AppCompatActivity {
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                overrideNavigation();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        overrideNavigation();
    }

    private void overrideNavigation() {
        if(getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            finish();
            startActivity(new Intent(this, HomeActivity.class));
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports_channel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().getBooleanExtra("launchedFromScheduledNotif", false)) {
            Utils.populateMainDataSet(this); Utils.populateFeedDataSet(this);
        }


        try {
            JSONArray pre = new JSONArray(Utils.getStringFromSharedPrefs("sports_teams_abbrs", "[]"));
            if(pre.length() > 0) {
                // we have some tickers
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                findViewById(R.id.tickerLV).setVisibility(View.VISIBLE);
                ListView lv = (ListView) findViewById(R.id.tickerLV);
                lv.setAdapter(new TopicsAdapter(this));
            }
        } catch(Exception e) {e.printStackTrace();}
    }
    public void addSportsTeams(View view) {
        new MaterialDialog.Builder(this)
                .title("Search for team")
                .content("Like \"LAL\", \"Celtics\", or \"Chicago Bulls\"")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(final MaterialDialog dialog, CharSequence input) {
                        // Do something
                        try {
                            final String keywords = input.toString();
                            if(keywords != null && !keywords.isEmpty()) {
                                Utils.volleyJSONCall2(SportsChannelActivity.this, "http://black-abode-2709.appspot.com/searchSportsTeam?q=" + keywords, new VolleyCallback() {
                                    @Override
                                    public void onResponse(Object result) {
                                        try {
                                            final JSONArray results = ((JSONArray) result);

                                            dialog.dismiss();

                                            // Now show new dialog with list in it

                                            MaterialDialog dialog = new MaterialDialog.Builder(SportsChannelActivity.this)
                                                    .title("Search Results")
                                                    // second parameter is an optional layout manager. Must be a LinearLayoutManager or GridLayoutManager.
                                                    .adapter(new SportsResultsAdapter(SportsChannelActivity.this, results), null)
                                                    .build();

                                            ((SportsResultsAdapter)dialog.getRecyclerView().getAdapter()).dialog = dialog;

                                            dialog.show();

                                        } catch (Exception e) {e.printStackTrace();}
                                    }
                                });
                            }
                        } catch (Exception e) {e.printStackTrace();}
                    }
                }).show();
    }
}
