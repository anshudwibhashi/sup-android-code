package com.sup.sup;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.Executors;

/**
 * Created by anshudwibhashi on 04/01/18.
 * For groups
 */

public class MicroChatClickListener2 implements View.OnClickListener {
    Context context; int position; HomeAdapter.ViewHolder5 holder;
    public MicroChatClickListener2(Context context, int position, HomeAdapter.ViewHolder5 holder) {
        this.context = context; this.position = position; this.holder = holder;
    }
    @Override
    public void onClick(View v) {
        final LinearLayout ll = (LinearLayout) (Utils.getRequiredActivity(v)).getLayoutInflater().inflate(R.layout.bottom_sheet_smalltalk, null);

        MessageHolders.ContentChecker<Smalltalk.MyMessage> checker = new MessageHolders.ContentChecker<Smalltalk.MyMessage>() {
            @Override
            public boolean hasContentFor(Smalltalk.MyMessage message, byte type) {
                switch (type) {
                    case Constants.CONTENT_TYPE_VOICE:
                        return (message.getType() != null && message.getType().equals("audio_response"));
                    case Constants.CONTENT_TYPE_VIDEO:
                        return (message.getType() != null && message.getType().equals("video_response"));
                }
                return false;
            }
        };

        MessageHolders holders = new MessageHolders()
                .registerContentType(
                        Constants.CONTENT_TYPE_VOICE,
                        IncomingVoiceMessageViewHolder.class,
                        R.layout.item_custom_incoming_voice_message,
                        OutgoingVoiceMessageViewHolder.class,
                        R.layout.item_custom_outgoing_voice_message,
                        checker);

                /*if (url.contains("snapshot_") && playButton != null) {
                                            playButton.setVisibility(View.VISIBLE);
                                        } else {
                                            if (playButton != null)
                                                playButton.setVisibility(View.GONE);
                                        }*/

        final MessagesList messagesList = (MessagesList) ll.findViewById(R.id.messagesList);
        final MessagesListAdapter<Smalltalk.MyMessage> adapter = new MessagesListAdapter<>("sender", holders, new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url) {
                Glide.with(context.getApplicationContext())
                        .load(new File(context.getFilesDir(), url))
                        .centerCrop()
                        .override(Utils.getScreenWidth(context), ((int) (Utils.getScreenWidth(context) * (9.0 / 16.0))))
                        .into(imageView);
            }
        });

        final Dialog mBottomSheetDialog = new Dialog (context,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (ll);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
        mBottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                ((HomeActivity)context).activeAdapter = adapter;
                ((HomeActivity)context).activeMessageList = messagesList;
                ((HomeActivity)context).activePosition = position;

                ContentValues cv = new ContentValues();
                cv.put("microtext_read", 1);
                SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
                dbWritable.update("connections", cv, "token=?", new String[]{Utils.MainDataSet.tokens.get(position)});

                holder.microChatIV.setColorFilter(Color.parseColor("#757575"));

                if (CustomFirebaseMessagingService.smalltalkNotificationBuilders.containsKey(Utils.MainDataSet.tokens.get(position))) {
                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(CustomFirebaseMessagingService.smalltalkNotificationsIds.get(Utils.MainDataSet.tokens.get(position)));

                    CustomFirebaseMessagingService.smalltalkNotificationBuilders.remove(Utils.MainDataSet.tokens.get(position));
                    CustomFirebaseMessagingService.smalltalkNotificationsIds.remove(Utils.MainDataSet.tokens.get(position));
                }
                Utils.putStringInSharedPrefs("smalltalk_open", Utils.MainDataSet.tokens.get(position));
                ((HomeActivity)context).activePhotoIV = (ImageView) ll.findViewById(R.id.photoIV);
                ((HomeActivity)context).activeNameTV = (TextView) ll.findViewById(R.id.nameTV);
                ((HomeActivity)context).activeOnlineStatusTV = (TextView) ll.findViewById(R.id.onlineStatusTV);

            }
        });
        mBottomSheetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((HomeActivity)context).activeAdapter = null;
                ((HomeActivity)context).activeMessageList = null;
                ((HomeActivity)context).activePosition = 0;
                if (CustomFirebaseMessagingService.smalltalkNotificationBuilders.containsKey(Utils.MainDataSet.tokens.get(position))) {
                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(CustomFirebaseMessagingService.smalltalkNotificationsIds.get(Utils.MainDataSet.tokens.get(position)));

                    CustomFirebaseMessagingService.smalltalkNotificationBuilders.remove(Utils.MainDataSet.tokens.get(position));
                    CustomFirebaseMessagingService.smalltalkNotificationsIds.remove(Utils.MainDataSet.tokens.get(position));
                }
                Utils.putStringInSharedPrefs("smalltalk_open", "");
            }
        });
        mBottomSheetDialog.show ();

        ImageView help = (ImageView) ll.findViewById(R.id.helpButton);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showDialogMessage(context, "What is Smalltalk?", "Smalltalk lets you follow-up on sups. Sup will store group Smalltalk messages for one hour before deleting them.");
            }
        });
        ((TextView)ll.findViewById(R.id.nameTV)).setText(Utils.MainDataSet.names.get(position));

        Glide.with(context)
                .load(Utils.MainDataSet.photos.get(position))
                .centerCrop()
                .override(100, 100)
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.ic_default_picture)
                .into((ImageView) ll.findViewById(R.id.photoIV));

        ((TextView)ll.findViewById(R.id.onlineStatusTV)).setVisibility(View.GONE);

        messagesList.setAdapter(adapter);
        adapter.setOnMessageClickListener(new MessagesListAdapter.OnMessageClickListener<Smalltalk.MyMessage>() {
            @Override
            public void onMessageClick(Smalltalk.MyMessage message) {
                if(message.getType().equals("image_response")) {
                    Intent i = new Intent(context, ImageViewer.class);
                    i = i.putExtra("path", new File(context.getFilesDir(), message.getImageUrl()).getAbsolutePath()).putExtra("delete", false);
                    context.startActivity(i);
                } else if (message.getType().equals("video_response")) {
                    context.startActivity(new Intent(context, VideoPlayer.class).putExtra("filename", message.getText()));
                } else if (message.getType().equals("location_response")) {
                    double lat = Double.valueOf(message.getText().split("_")[0]);
                    double lon = Double.valueOf(message.getText().split("_")[1]);
                    // context.startActivity(new Intent(context, MapsActivity.class).putExtra("lat", lat).putExtra("lon", lon));
                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lat, lon);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    context.startActivity(intent);
                }
            }
        });

        Smalltalk.SmalltalkMessage smalltalk = Utils.MainDataSet.smallTalkMessages.get(position);
        ArrayList<Smalltalk.MyMessage> messages = (ArrayList<Smalltalk.MyMessage>) smalltalk.messages.getMessages().clone();
        Collections.reverse(messages);
        adapter.addToEnd(messages, false);

        MessageInput inputView = (MessageInput) ll.findViewById(R.id.input);
        inputView.setInputListener(new MessageInput.InputListener() {
            @Override
            public boolean onSubmit(CharSequence input) {
                if (!Utils.isNetworkAvailable(context)) {
                    Utils.showToast(context, "No internet connection");
                    return false;
                }

                if (input.toString().trim() == null || input.toString().trim().isEmpty())
                    return false;

                // prepare message:
                Smalltalk.MyUser user = new Smalltalk.MyUser("sender", ""); // third param null, because we don't want do display our own pic
                Smalltalk.MyMessage message = new Smalltalk.MyMessage(""+new Random().nextInt(), input.toString().trim(), user, new Date());

                HomeAdapter.sendSmalltalk(context, position, message, adapter, messagesList, input.toString().trim(), "text");
                try {
                    Utils.sendSmalltalkToGroup(input.toString().trim(), "text", Utils.MainDataSet.tokens.get(position), context);
                } catch (Exception e) {e.printStackTrace();}
                return true;
            }
        });

        inputView.setAttachmentsListener(new MessageInput.AttachmentsListener() {
            @Override
            public void onAddAttachments() {
                ((HomeActivity)context).saveMedia = true;
                ((HomeActivity)context).refreshCallback = new VolleyCallback(){
                    @Override
                    public void onResponse(Object result) {

                        try {
                            // prepare message:
                            Smalltalk.MyUser user = new Smalltalk.MyUser("sender", ""); // third param null, because we don't want do display our own pic
                            final Smalltalk.MyMessage message = new Smalltalk.MyMessage("" + new Random().nextInt(), ((JSONObject) result).getString("response"), user, new Date());
                            message.setType(((JSONObject) result).getString("type"));

                            switch (((JSONObject) result).getString("type")) {
                                case "image_response":
                                    message.setImgPath(((JSONObject) result).getString("response"));
                                    HomeAdapter.sendSmalltalk(context, position, message, adapter, messagesList, ((JSONObject) result).getString("response"), "image_response");
                                    break;
                                case "audio_response":
                                    message.setImgPath(((JSONObject) result).getString("response"));
                                    HomeAdapter.sendSmalltalk(context, position, message, adapter, messagesList, ((JSONObject) result).getString("response"), "audio_response");
                                    break;
                                case "video_response":
                                    File output = new File(context.getFilesDir(), "snapshot_" + ((JSONObject) result).getString("response").replace(".mp4", ".png"));
                                    output.createNewFile();
                                    Utils.snapshot(new File(context.getFilesDir(), ((JSONObject) result).getString("response")), output, context);
                                    message.setImgPath("snapshot_" + ((JSONObject) result).getString("response").replace(".mp4", ".png"));
                                    HomeAdapter.sendSmalltalk(context, position, message, adapter, messagesList, ((JSONObject) result).getString("response"), "video_response");
                                    break;
                                case "location_response":
                                    final float lat = Float.valueOf(((JSONObject) result).getString("response").split("_")[0]);
                                    final float lon = Float.valueOf(((JSONObject) result).getString("response").split("_")[1]);
                                    message.setImgPath(lat + "_" + lon + ".png");

                                    final ProgressDialog pd = new ProgressDialog(context);
                                    pd.setCancelable(false);
                                    pd.setCanceledOnTouchOutside(false);
                                    pd.setIndeterminate(true);
                                    pd.setMessage("Downloading map...");
                                    pd.show();
                                    Utils.downloadImageAndSave(context, Utils.prepareStaticMapUrl(context, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                                        @Override
                                        public void onResponse(Object result) {
                                            pd.dismiss();
                                            try {
                                                HomeAdapter.sendSmalltalk(context, position, message, adapter, messagesList, lat+"_"+lon, "location_response");
                                            } catch (Exception e) {e.printStackTrace();}
                                        }
                                    });
                            }
                        } catch (Exception e) {e.printStackTrace();}
                    }
                };
                if (!Utils.isNetworkAvailable(context)) {
                    Utils.showToast(context, "No internet connection");
                    return;
                }
                Utils.obtainResponseFromUser((Activity)context, Utils.MainDataSet.names.get(position), "Attach", new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        try {
                            Utils.sendSmalltalkToGroup(((JSONObject) result).getString("response"), ((JSONObject) result).getString("type"), Utils.MainDataSet.tokens.get(position), context);
                        } catch (Exception e) {e.printStackTrace();}
                    }
                }, ll);
            }
        });
    }
}
