package com.sup.sup;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ShowStockInfoActivity extends AppCompatActivity {

    JSONObject stock_info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_stock_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();

        try {
            stock_info = new JSONObject(intent.getStringExtra("stock_info"));
            getSupportActionBar().setTitle(stock_info.getString("symbol"));

            if (stock_info.has("low") && !stock_info.getString("low").equals("null")) {
                ((TextView) findViewById(R.id.lowTV)).setText(stock_info.getString("low"));
            } else {
                ((TextView) findViewById(R.id.lowTV)).setText(" - ");
            }
            if (stock_info.has("high") && !stock_info.getString("high").equals("null")) {
                ((TextView) findViewById(R.id.highTV)).setText(stock_info.getString("high"));
            } else {
                ((TextView) findViewById(R.id.highTV)).setText(" - ");
            }
            if (stock_info.has("open") && !stock_info.getString("open").equals("null")) {
                ((TextView) findViewById(R.id.openTV)).setText(stock_info.getString("open"));
            } else {
                ((TextView) findViewById(R.id.openTV)).setText(" - ");
            }
            if (stock_info.has("peratio") && !stock_info.getString("peratio").equals("null")) {
                ((TextView) findViewById(R.id.peTV)).setText(stock_info.getString("peratio"));
            } else {
                ((TextView) findViewById(R.id.peTV)).setText(" - ");
            }
            if (stock_info.has("mktcap") && !stock_info.getString("mktcap").equals("null")) {
                ((TextView) findViewById(R.id.mktTV)).setText(stock_info.getString("mktcap"));
            } else {
                ((TextView) findViewById(R.id.mktTV)).setText(" - ");
            }
            if (stock_info.has("divyield") && !stock_info.getString("divyield").equals("null")) {
                ((TextView) findViewById(R.id.divTV)).setText(stock_info.getString("divyield")+"%");
            } else {
                ((TextView) findViewById(R.id.divTV)).setText(" - ");
            }

            if (stock_info.has("name") && !stock_info.getString("name").equals("null")) {
                ((TextView) findViewById(R.id.nameTV)).setText(stock_info.getString("name"));
            } else {
                ((TextView) findViewById(R.id.nameTV)).setText(" - ");
            }

            if (stock_info.has("price") && !stock_info.getString("price").equals("null")) {
                ((TextView) findViewById(R.id.priceTV)).setText(stock_info.getString("price"));
            } else {
                ((TextView) findViewById(R.id.priceTV)).setText(" - ");
            }

            if (stock_info.has("currency") && !stock_info.getString("currency").equals("null")) {
                ((TextView) findViewById(R.id.currencyTV)).setText(stock_info.getString("currency"));
            } else {
                ((TextView) findViewById(R.id.currencyTV)).setText(" - ");
            }

            if (stock_info.has("change_abs") && !stock_info.getString("change_abs").equals("null")) {
                if (Double.valueOf(stock_info.getString("change_abs")) > 0) {
                    ((TextView) findViewById(R.id.nameTV)).setText(((TextView) findViewById(R.id.nameTV)).getText()+" "+Html.fromHtml("&#x25B2;"));
                } else {
                    ((TextView) findViewById(R.id.nameTV)).setText(((TextView) findViewById(R.id.nameTV)).getText()+" "+Html.fromHtml("&#x25BC;"));
                }
                ((TextView) findViewById(R.id.changeTV)).setText(stock_info.getString("change_abs")
                        + " / " + stock_info.getString("change_pct"));
            } else {
                ((TextView) findViewById(R.id.changeTV)).setText(" - / - ");
            }


            // Now graph

            intraday(null);

        } catch (Exception e) {e.printStackTrace(); stock_info = null;}
    }

    public void week(View view) {
        if (view != null) {
            ((Button) view).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.day)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month3)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month6)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.year)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
        }


        try {
            final LineChart chart = (LineChart) findViewById(R.id.chart);
            chart.setVisibility(View.GONE);
            ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

            Utils.volleyJSONCall(this, "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" + stock_info.getString("symbol") + "&interval=30min&outputsize=full&apikey=EV7KQSJQRFPRIHS0", new VolleyCallback2() {
                @Override
                public void onError(Object error) {
                    ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                    if(error instanceof String)
                        chart.setNoDataText((String) error);
                    else
                        chart.setNoDataText("Something went wrong. Try again.");
                    Log.d("Stocks error", error.toString());
                    chart.setNoDataTextColor(R.color.colorAccent);
                    chart.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResponse(Object result) {
                    try {
                        JSONObject response = ((JSONObject) result).getJSONObject("Time Series (30min)");
                        Iterator<?> keys = response.keys();

                        ArrayList<Float> yValues = new ArrayList<>();
                        ArrayList<Float> xValues = new ArrayList<>();
                        HashMap<Float, String> xToDays = new HashMap<>();

                        String months[] = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
                        "Sep", "Oct", "Nov", "Dec"};

                        int x = response.length();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            int dayToday = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split(" ")[0].split("-")[2]);
                            if (dayToday - Integer.valueOf(key.split(" ")[0].split("-")[2]) < 7) {
                                yValues.add(Float.valueOf(response.getJSONObject(key).getString("4. close")));
                                xValues.add((float) x);
                                xToDays.put((float) x, months[Integer.valueOf(key.split(" ")[0].split("-")[1])]+" "+key.split(" ")[0].split("-")[2]);
                                x--;
                            }
                        }

                        List<Entry> entries = new ArrayList<>();
                        for (int i = 0; i < xValues.size(); i++) {
                            entries.add(new Entry(xValues.get(i), yValues.get(i)));
                        }
                        Collections.sort(entries, new EntryXComparator());
                        chart.getLegend().setEnabled(false);
                        chart.getDescription().setEnabled(false);
                        chart.getAxisLeft().setDrawAxisLine(false);
                        chart.getAxisRight().setEnabled(false);
                        chart.setTouchEnabled(false);

                        XAxis xAxis = chart.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setTextSize(10f);
                        xAxis.setDrawAxisLine(true);
                        xAxis.setDrawGridLines(false);
                        xAxis.setDrawAxisLine(false);

                        xAxis.setValueFormatter(new MyXAxisValueFormatter2(xToDays));

                        LineDataSet dataSet = new LineDataSet(entries, ""); // add entries to dataset
                        dataSet.setColor(R.color.colorPrimary);
                        dataSet.setValueTextColor(R.color.white);
                        dataSet.setDrawCircles(false);
                        dataSet.setLineWidth(2f);
                        dataSet.setColor(R.color.colorAccent);


                        LineData lineData = new LineData(dataSet);
                        lineData.setDrawValues(false);

                        chart.setData(lineData);
                        chart.invalidate(); // refresh

                        ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                        chart.setVisibility(View.VISIBLE);


                    }  catch (Exception e) {
                        this.onError("Data unavailable at this moment. Try again in a few seconds.");
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {e.printStackTrace();}

    }

    public void month(View view) {
        if (view != null) {
            ((Button) view).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.week)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.day)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month3)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month6)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.year)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
        }

        try {
            final LineChart chart = (LineChart) findViewById(R.id.chart);
            chart.setVisibility(View.GONE);
            ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

            Utils.volleyJSONCall(this, "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&outputsize=full&symbol=" + stock_info.getString("symbol") + "&apikey=EV7KQSJQRFPRIHS0", new VolleyCallback2() {
                @Override
                public void onError(Object error) {
                    ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                    if(error instanceof String)
                        chart.setNoDataText((String) error);
                    else
                        chart.setNoDataText("Something went wrong. Try again.");
                    Log.d("Stocks error", error.toString());
                    chart.setNoDataTextColor(R.color.colorAccent);
                    chart.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResponse(Object result) {
                    try {
                        JSONObject response = ((JSONObject) result).getJSONObject("Time Series (Daily)");
                        Iterator<?> keys = response.keys();

                        ArrayList<Float> yValues = new ArrayList<>();
                        ArrayList<Float> xValues = new ArrayList<>();
                        HashMap<Float, String> xToDays = new HashMap<>();

                        String months[] = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
                                "Sep", "Oct", "Nov", "Dec"};

                        int x = response.length();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            String lastRef[] = ((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-");
                            int lastRefreshedDay = Integer.valueOf(lastRef[2].substring(lastRef[2].indexOf(" ")));
                            int lastRefreshedMonth = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[1]);
                            int lastRefreshedYear = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[0]);

                            int thisDay = Integer.valueOf(key.split("-")[2]);
                            int thisMonth = Integer.valueOf(key.split("-")[1]);
                            int thisYear = Integer.valueOf(key.split("-")[0]);

                            if ((thisYear == lastRefreshedYear) &&
                                    ((thisMonth == lastRefreshedMonth && thisDay <= lastRefreshedDay)
                                            || (thisMonth == lastRefreshedMonth-1 && thisDay > lastRefreshedDay))) {
                                yValues.add(Float.valueOf(response.getJSONObject(key).getString("4. close")));
                                xValues.add((float) x);
                                xToDays.put((float) x, months[Integer.valueOf(key.split("-")[1])]+" "+key.split("-")[2]);
                                x--;
                            }
                        }

                        List<Entry> entries = new ArrayList<>();
                        for (int i = 0; i < xValues.size(); i++) {
                            entries.add(new Entry(xValues.get(i), yValues.get(i)));
                        }
                        Collections.sort(entries, new EntryXComparator());
                        chart.getLegend().setEnabled(false);
                        chart.getDescription().setEnabled(false);
                        chart.getAxisLeft().setDrawAxisLine(false);
                        chart.getAxisRight().setEnabled(false);
                        chart.setTouchEnabled(false);

                        XAxis xAxis = chart.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setTextSize(10f);
                        xAxis.setDrawAxisLine(true);
                        xAxis.setDrawGridLines(false);
                        xAxis.setDrawAxisLine(false);

                        xAxis.setValueFormatter(new MyXAxisValueFormatter2(xToDays));

                        LineDataSet dataSet = new LineDataSet(entries, ""); // add entries to dataset
                        dataSet.setColor(R.color.colorPrimary);
                        dataSet.setValueTextColor(R.color.white);
                        dataSet.setDrawCircles(false);
                        dataSet.setLineWidth(2f);
                        dataSet.setColor(R.color.colorAccent);


                        LineData lineData = new LineData(dataSet);
                        lineData.setDrawValues(false);

                        chart.setData(lineData);
                        chart.invalidate(); // refresh

                        ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                        chart.setVisibility(View.VISIBLE);


                    }  catch (Exception e) {
                        this.onError("Data unavailable at this moment. Try again in a few seconds.");
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {e.printStackTrace();}

    }

    public void month3(View view) {
        if (view != null) {
            ((Button) view).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.week)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.day)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month6)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.year)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
        }

        try {
            final LineChart chart = (LineChart) findViewById(R.id.chart);
            chart.setVisibility(View.GONE);
            ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

            Utils.volleyJSONCall(this, "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&outputsize=full&symbol=" + stock_info.getString("symbol") + "&apikey=EV7KQSJQRFPRIHS0", new VolleyCallback2() {
                @Override
                public void onError(Object error) {
                    ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                    if(error instanceof String)
                        chart.setNoDataText((String) error);
                    else
                        chart.setNoDataText("Something went wrong. Try again.");
                    Log.d("Stocks error", error.toString());
                    chart.setNoDataTextColor(R.color.colorAccent);
                    chart.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResponse(Object result) {
                    try {
                        JSONObject response = ((JSONObject) result).getJSONObject("Time Series (Daily)");
                        Iterator<?> keys = response.keys();

                        ArrayList<Float> yValues = new ArrayList<>();
                        ArrayList<Float> xValues = new ArrayList<>();
                        HashMap<Float, String> xToDays = new HashMap<>();

                        String months[] = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
                                "Sep", "Oct", "Nov", "Dec"};

                        int x = response.length();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            int lastRefreshedDay = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[2]);
                            int lastRefreshedMonth = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[1]);
                            int lastRefreshedYear = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[0]);

                            int thisDay = Integer.valueOf(key.split("-")[2]);
                            int thisMonth = Integer.valueOf(key.split("-")[1]);
                            int thisYear = Integer.valueOf(key.split("-")[0]);

                            if ((thisYear == lastRefreshedYear) &&
                                    ((thisMonth == lastRefreshedMonth && thisDay <= lastRefreshedDay)
                                            || (thisMonth == lastRefreshedMonth-1)
                                            || (thisMonth == lastRefreshedMonth-2)
                                    || (thisMonth == lastRefreshedMonth-3 && thisDay > lastRefreshedDay))) {
                                yValues.add(Float.valueOf(response.getJSONObject(key).getString("4. close")));
                                xValues.add((float) x);
                                xToDays.put((float) x, months[Integer.valueOf(key.split("-")[1])]+" "+key.split("-")[2]);
                                x--;
                            }
                        }

                        List<Entry> entries = new ArrayList<>();
                        for (int i = 0; i < xValues.size(); i++) {
                            entries.add(new Entry(xValues.get(i), yValues.get(i)));
                        }
                        Collections.sort(entries, new EntryXComparator());
                        chart.getLegend().setEnabled(false);
                        chart.getDescription().setEnabled(false);
                        chart.getAxisLeft().setDrawAxisLine(false);
                        chart.getAxisRight().setEnabled(false);
                        chart.setTouchEnabled(false);

                        XAxis xAxis = chart.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setTextSize(10f);
                        xAxis.setDrawAxisLine(true);
                        xAxis.setDrawGridLines(false);
                        xAxis.setDrawAxisLine(false);

                        xAxis.setValueFormatter(new MyXAxisValueFormatter2(xToDays));

                        LineDataSet dataSet = new LineDataSet(entries, ""); // add entries to dataset
                        dataSet.setColor(R.color.colorPrimary);
                        dataSet.setValueTextColor(R.color.white);
                        dataSet.setDrawCircles(false);
                        dataSet.setLineWidth(2f);
                        dataSet.setColor(R.color.colorAccent);


                        LineData lineData = new LineData(dataSet);
                        lineData.setDrawValues(false);

                        chart.setData(lineData);
                        chart.invalidate(); // refresh

                        ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                        chart.setVisibility(View.VISIBLE);


                    } catch (Exception e) {
                        this.onError("Data unavailable at this moment. Try again in a few seconds.");
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public void month6(View view) {
        if (view != null) {
            ((Button) view).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.week)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month3)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.day)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.year)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
        }

        try {
            final LineChart chart = (LineChart) findViewById(R.id.chart);
            chart.setVisibility(View.GONE);
            ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

            Utils.volleyJSONCall(this, "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&outputsize=full&symbol=" + stock_info.getString("symbol") + "&apikey=EV7KQSJQRFPRIHS0", new VolleyCallback2() {
                @Override
                public void onError(Object error) {
                    ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                    if(error instanceof String)
                        chart.setNoDataText((String) error);
                    else
                        chart.setNoDataText("Something went wrong. Try again.");
                    Log.d("Stocks error", error.toString());
                    chart.setNoDataTextColor(R.color.colorAccent);
                    chart.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResponse(Object result) {
                    try {
                        JSONObject response = ((JSONObject) result).getJSONObject("Time Series (Daily)");
                        Iterator<?> keys = response.keys();

                        ArrayList<Float> yValues = new ArrayList<>();
                        ArrayList<Float> xValues = new ArrayList<>();
                        HashMap<Float, String> xToDays = new HashMap<>();

                        String months[] = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
                                "Sep", "Oct", "Nov", "Dec"};

                        int x = response.length();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            int lastRefreshedDay = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[2]);
                            int lastRefreshedMonth = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[1]);
                            int lastRefreshedYear = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[0]);

                            int thisDay = Integer.valueOf(key.split("-")[2]);
                            int thisMonth = Integer.valueOf(key.split("-")[1]);
                            int thisYear = Integer.valueOf(key.split("-")[0]);

                            if ((thisYear == lastRefreshedYear) &&
                                    ((thisMonth == lastRefreshedMonth && thisDay <= lastRefreshedDay)
                                            || (thisMonth == lastRefreshedMonth-1)
                                            || (thisMonth == lastRefreshedMonth-2)
                                            || (thisMonth == lastRefreshedMonth-3)
                                            || (thisMonth == lastRefreshedMonth-4)
                                            || (thisMonth == lastRefreshedMonth-5)
                                            || (thisMonth == lastRefreshedMonth-6 && thisDay > lastRefreshedDay))) {
                                yValues.add(Float.valueOf(response.getJSONObject(key).getString("4. close")));
                                xValues.add((float) x);
                                xToDays.put((float) x, months[Integer.valueOf(key.split("-")[1])]+" "+key.split("-")[0]);
                                x--;
                            }
                        }

                        List<Entry> entries = new ArrayList<>();
                        for (int i = 0; i < xValues.size(); i++) {
                            entries.add(new Entry(xValues.get(i), yValues.get(i)));
                        }
                        Collections.sort(entries, new EntryXComparator());
                        chart.getLegend().setEnabled(false);
                        chart.getDescription().setEnabled(false);
                        chart.getAxisLeft().setDrawAxisLine(false);
                        chart.getAxisRight().setEnabled(false);
                        chart.setTouchEnabled(false);

                        XAxis xAxis = chart.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setTextSize(10f);
                        xAxis.setDrawAxisLine(true);
                        xAxis.setDrawGridLines(false);
                        xAxis.setDrawAxisLine(false);

                        xAxis.setValueFormatter(new MyXAxisValueFormatter2(xToDays));

                        LineDataSet dataSet = new LineDataSet(entries, ""); // add entries to dataset
                        dataSet.setColor(R.color.colorPrimary);
                        dataSet.setValueTextColor(R.color.white);
                        dataSet.setDrawCircles(false);
                        dataSet.setLineWidth(2f);
                        dataSet.setColor(R.color.colorAccent);


                        LineData lineData = new LineData(dataSet);
                        lineData.setDrawValues(false);

                        chart.setData(lineData);
                        chart.invalidate(); // refresh

                        ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                        chart.setVisibility(View.VISIBLE);


                    } catch (Exception e) {
                        this.onError("Data unavailable at this moment. Try again in a few seconds.");
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public void year(View view) {
        if (view != null) {
            ((Button) view).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.week)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month3)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month6)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.day)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
        }

        try {
            final LineChart chart = (LineChart) findViewById(R.id.chart);
            chart.setVisibility(View.GONE);
            ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

            Utils.volleyJSONCall(this, "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&outputsize=full&symbol=" + stock_info.getString("symbol") + "&apikey=EV7KQSJQRFPRIHS0", new VolleyCallback2() {
                @Override
                public void onError(Object error) {
                    ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                    if(error instanceof String)
                        chart.setNoDataText((String) error);
                    else
                        chart.setNoDataText("Something went wrong. Try again.");
                    Log.d("Stocks error", error.toString());
                    chart.setNoDataTextColor(R.color.colorAccent);
                    chart.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResponse(Object result) {
                    try {
                        JSONObject response = ((JSONObject) result).getJSONObject("Time Series (Daily)");
                        Iterator<?> keys = response.keys();

                        ArrayList<Float> yValues = new ArrayList<>();
                        ArrayList<Float> xValues = new ArrayList<>();
                        HashMap<Float, String> xToDays = new HashMap<>();

                        String months[] = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
                                "Sep", "Oct", "Nov", "Dec"};

                        int x = response.length();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            int lastRefreshedDay = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[2]);
                            int lastRefreshedMonth = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[1]);
                            int lastRefreshedYear = Integer.valueOf(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split("-")[0]);

                            int thisDay = Integer.valueOf(key.split("-")[2]);
                            int thisMonth = Integer.valueOf(key.split("-")[1]);
                            int thisYear = Integer.valueOf(key.split("-")[0]);

                            if ((thisYear == lastRefreshedYear && thisMonth == lastRefreshedMonth && thisDay <= lastRefreshedDay)
                                    || (thisYear == lastRefreshedYear && thisMonth < lastRefreshedMonth)
                                    || (thisYear == lastRefreshedYear-1 && thisMonth >= lastRefreshedMonth && thisDay > lastRefreshedDay)) {
                                yValues.add(Float.valueOf(response.getJSONObject(key).getString("4. close")));
                                xValues.add((float) x);
                                xToDays.put((float) x, months[Integer.valueOf(key.split("-")[1])]+" "+key.split("-")[0]);
                                x--;
                            }
                        }

                        List<Entry> entries = new ArrayList<>();
                        for (int i = 0; i < xValues.size(); i++) {
                            entries.add(new Entry(xValues.get(i), yValues.get(i)));
                        }
                        Collections.sort(entries, new EntryXComparator());
                        chart.getLegend().setEnabled(false);
                        chart.getDescription().setEnabled(false);
                        chart.getAxisLeft().setDrawAxisLine(false);
                        chart.getAxisRight().setEnabled(false);
                        chart.setTouchEnabled(false);

                        XAxis xAxis = chart.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setTextSize(10f);
                        xAxis.setDrawAxisLine(true);
                        xAxis.setDrawGridLines(false);
                        xAxis.setDrawAxisLine(false);

                        xAxis.setValueFormatter(new MyXAxisValueFormatter2(xToDays));

                        LineDataSet dataSet = new LineDataSet(entries, ""); // add entries to dataset
                        dataSet.setColor(R.color.colorPrimary);
                        dataSet.setValueTextColor(R.color.white);
                        dataSet.setDrawCircles(false);
                        dataSet.setLineWidth(2f);
                        dataSet.setColor(R.color.colorAccent);


                        LineData lineData = new LineData(dataSet);
                        lineData.setDrawValues(false);

                        chart.setData(lineData);
                        chart.invalidate(); // refresh

                        ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                        chart.setVisibility(View.VISIBLE);


                    } catch (Exception e) {
                        this.onError("Data unavailable at this moment. Try again in a few seconds.");
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public void intraday(View view) {
        if (view != null) {
            ((Button) view).setTextColor(getResources().getColor(R.color.colorAccent));
            ((Button) findViewById(R.id.week)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month3)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.month6)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
            ((Button) findViewById(R.id.year)).setTextColor(getResources().getColor(R.color.colorPrimaryText));
        }
        try {
            final LineChart chart = (LineChart) findViewById(R.id.chart);
            chart.setVisibility(View.GONE);
            ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

            Utils.volleyJSONCall(this, "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" + stock_info.getString("symbol") + "&interval=5min&outputsize=full&apikey=EV7KQSJQRFPRIHS0", new VolleyCallback2() {
                @Override
                public void onError(Object error) {
                    ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                    if(error instanceof String)
                        chart.setNoDataText((String) error);
                    else
                        chart.setNoDataText("Something went wrong. Try again.");
                    Log.d("Stocks error", error.toString());
                    chart.setNoDataTextColor(R.color.colorAccent);
                    chart.setVisibility(View.VISIBLE);
                }

                @Override
                public void onResponse(Object result) {
                    try {
                        JSONObject response = ((JSONObject) result).getJSONObject("Time Series (5min)");
                        Iterator<?> keys = response.keys();

                        ArrayList<Float> yValues = new ArrayList<>();
                        ArrayList<Float> xValues = new ArrayList<>();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            if (key.startsWith(((JSONObject) result).getJSONObject("Meta Data").getString("3. Last Refreshed").split(" ")[0])) {
                                // this is an entry for the last available day
                                yValues.add(Float.valueOf(response.getJSONObject(key).getString("4. close")));
                                xValues.add(Float.valueOf(key.split(" ")[1].replaceAll(":", "").substring(0, 4)));
                            }
                        }

                        List<Entry> entries = new ArrayList<>();
                        for (int i = 0; i < xValues.size(); i++) {
                            entries.add(new Entry(xValues.get(i), yValues.get(i)));
                        }
                        Collections.sort(entries, new EntryXComparator());
                        chart.getLegend().setEnabled(false);
                        chart.getDescription().setEnabled(false);
                        chart.getAxisLeft().setDrawAxisLine(false);
                        chart.getAxisRight().setEnabled(false);
                        chart.setTouchEnabled(false);

                        XAxis xAxis = chart.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setTextSize(10f);
                        xAxis.setDrawAxisLine(true);
                        xAxis.setDrawGridLines(false);
                        xAxis.setDrawAxisLine(false);

                        xAxis.setValueFormatter(new MyXAxisValueFormatter());

                        LineDataSet dataSet = new LineDataSet(entries, ""); // add entries to dataset
                        dataSet.setColor(R.color.colorPrimary);
                        dataSet.setValueTextColor(R.color.white);
                        dataSet.setDrawCircles(false);
                        dataSet.setLineWidth(2f);
                        dataSet.setColor(R.color.colorAccent);


                        LineData lineData = new LineData(dataSet);
                        lineData.setDrawValues(false);

                        chart.setData(lineData);
                        chart.invalidate(); // refresh

                        ShowStockInfoActivity.this.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                        chart.setVisibility(View.VISIBLE);


                    } catch (Exception e) {
                        this.onError("Data unavailable at this moment. Try again in a few seconds.");
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }
}
