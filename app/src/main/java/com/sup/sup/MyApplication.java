package com.sup.sup;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.google.firebase.FirebaseApp;
import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static com.sup.sup.CustomFirebaseMessagingService.SERVICE_RESULT;
import static com.sup.sup.NotificationActionActivity.SERVICE_MESSAGE;
import static com.sup.sup.NotificationActionActivity.SERVICE_TOKEN;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);

        Utils.mSharedPreferences = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        MyJobCreator jc = new MyJobCreator();
        JobManager.create(this).addJobCreator(jc);
    }

}

class MyJobCreator implements JobCreator {

    @Override
    public Job create(String tag) {
        switch (tag) {
            case NewsArticlesJob.TAG:
                return new NewsArticlesJob();
            case RedditJob.TAG:
                return new RedditJob();
            default:
                return null;
        }
    }
}

class RedditJob extends Job {
    public static final String TAG = "job_demo_tag_2";

    @Override
    @NotNull
    protected Result onRunJob(Params params) {
        Pusher pusher = new Pusher("50ed18dd967b455393ed");
        pusher.connect();

        while(true) {
            try {
                JSONArray subreddits = new JSONArray(Utils.getStringFromSharedPrefs("subreddits", "[]"));
                JSONArray subscribed_subreddits = new JSONArray(Utils.getStringFromSharedPrefs("subscribed_subreddits", "[]"));
                for(int i = 0; i < subreddits.length(); i++) {
                    if(!contains(subscribed_subreddits, subreddits.getString(i))) {
                        // New subreddit. Subscribe!
                        Channel channel = pusher.subscribe(subreddits.getString(i));
                        channel.bind("new-listing", new SubscriptionEventListener() {
                            @Override
                            public void onEvent(String channelName, String eventName, final String data_str) {
                                try {
                                    SQLiteDatabase broadcastsDBR = BroadcastsDBHelper.getHelper(getContext()).getReadableDatabase();
                                    JSONObject data = new JSONObject(data_str);
                                    Cursor result = broadcastsDBR.rawQuery("SELECT * FROM broadcasts WHERE id=?", new String[]{"CHANNEL_REDDIT_" + data.getString("id")});
                                    result.moveToFirst();
                                    if (result.getCount() == 0 && contains(new JSONArray(Utils.getStringFromSharedPrefs("subreddits", "[]").toLowerCase()), data.get("subreddit").toString().toLowerCase())) {
                                        // add to broadcastsDB
                                        ContentValues cv = new ContentValues();

                                        String time = System.currentTimeMillis() + "";
                                        cv.put("timestamp", time);
                                        cv.put("id", "CHANNEL_REDDIT_" + data.getString("id"));
                                        cv.put("text", data_str);

                                        SQLiteDatabase broadcastsDBW = BroadcastsDBHelper.getHelper(getContext()).getWritableDatabase();
                                        broadcastsDBW.insert("broadcasts", null, cv);

                                        // refresh UI
                                        Intent intent = new Intent(SERVICE_RESULT);
                                        intent.putExtra(CustomFirebaseMessagingService.SERVICE_MESSAGE, "update_ui2");
                                        intent.putExtra(CustomFirebaseMessagingService.SERVICE_TOKEN, "");

                                        try {
                                            if(Utils.getBooleanFromSharedPrefs("show_reddit_channel_notifications", true)) {
                                                NotificationCompat.Builder mBuilder = CustomFirebaseMessagingService.buildNotification(getContext(), "Sup Reddit Update",
                                                        "New post: \"" + new JSONObject(data_str).getString("title") + "\"",
                                                        BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_reddit_channel));

                                                CustomFirebaseMessagingService._notify(mBuilder, Constants.PACKAGE_NOTIFICATION_ID, getContext());
                                            }
                                        } catch ( Exception e) {e.printStackTrace();}

                                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                                    }
                                } catch (Exception e) {e.printStackTrace();}
                            }
                        });
                        subscribed_subreddits.put(subreddits.getString(i));
                    }
                }
                JSONArray newSubscribedSR = new JSONArray();
                for(int i = 0; i < subscribed_subreddits.length(); i++) {
                    if(!contains(subreddits, subscribed_subreddits.getString(i))) {
                        pusher.unsubscribe(subscribed_subreddits.getString(i));
                    } else {
                        newSubscribedSR.put(subscribed_subreddits.getString(i));
                    }
                }
                Utils.putStringInSharedPrefs("subscribed_subreddits", newSubscribedSR.toString());
                Thread.sleep(10000); // Sleep for ten seconds
            } catch (Exception e) {}
        }
    }
    private boolean contains(JSONArray a, Object b) {
        try {
            for (int i = 0; i < a.length(); i++) {
                if (a.get(i).equals(b))
                    return true;
            }
        } catch (Exception e){}
        return false;
    }

    public static void scheduleJob() {
        new JobRequest.Builder(RedditJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15))
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }
}

class NewsArticlesJob extends Job {

    public static final String TAG = "job_demo_tag";

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        try {
            final JSONArray keywords = new JSONArray(Utils.getStringFromSharedPrefs("news_keywords", "[]"));
            for (int i = 0; i < keywords.length(); i++) {
                final String q = keywords.getString(i);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Ocp-Apim-Subscription-Key", "2ef43afb06d84160a817cd03273f2110");

                Utils.volleyJSONCall(this.getContext(), "https://api.cognitive.microsoft.com/bing/v7.0/news/search?q="+ URLEncoder.encode(q, "UTF-8")+"&mkt=en-us&textDecorations=true&textFormat=HTML", headers, new VolleyCallback() {
                    @Override
                    public void onResponse(Object r) {
                        try {
                            JSONObject result = (JSONObject) r;
                            int articleIndex = 0;
                            long end = System.currentTimeMillis();
                            long offset = end-(24*60*60*1000); // one day ago
                            Log.d("NewsHere1", "YO");

                            SQLiteDatabase broadcastsDBW = BroadcastsDBHelper.getHelper(getContext()).getWritableDatabase();
                            while (articleIndex < 10) {
                                Log.d("NewsHere2", "YO");
                                JSONObject article = result.getJSONArray("value").getJSONObject(articleIndex);
                                ArrayList<String> names = Utils.JSONArrayToArrayList(new JSONArray(Utils.getStringFromSharedPrefs("newsArticlesPossessed", "[]")));
                                if(!names.contains(article.getString("name"))) {
                                    // we don't have this article already. add it.
                                    names.add(article.getString("name"));
                                    Utils.putStringInSharedPrefs("newsArticlesPossessed", Utils.ArrayListToJSONArray(names).toString());

                                    // now add to broadcastsDB
                                    ContentValues cv2 = new ContentValues();

                                    end = Utils.generateRandomTime(offset, end);
                                    offset = end-(24*60*60*1000); // one day ago

                                    //String time = String.valueOf(end);

                                    cv2.put("timestamp", String.valueOf(Utils.convertISOToMillis(article.getString("datePublished"))));
                                    cv2.put("id", "CHANNEL_NEWS_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + System.currentTimeMillis());

                                    JSONObject repr = new JSONObject();
                                    repr.put("title", article.getString("name"));
                                    repr.put("snippet", article.getString("description"));
                                    repr.put("source", article.getJSONArray("provider").getJSONObject(0).getString("name"));
                                    try {
                                        repr.put("image", article.getJSONObject("image").getJSONObject("thumbnail").getString("contentUrl"));
                                    } catch (Exception e) {
                                        repr.put("image", "");
                                    }
                                    repr.put("url", article.getString("url"));
                                    repr.put("datePublished", Utils.convertISOToMillis(article.getString("datePublished")));

                                    cv2.put("text", repr.toString());
                                    broadcastsDBW.insert("broadcasts", null, cv2);

                                    Log.d("NewsHere3", "YO");
                                } else {

                                }
                                articleIndex++;
                            }
                            broadcastsDBW.close();
                            Utils.populateMainDataSet(NewsArticlesJob.this.getContext());
                            Intent intent = new Intent(SERVICE_RESULT);
                            intent.putExtra(SERVICE_MESSAGE, "update_ui2");
                            intent.putExtra(SERVICE_TOKEN, "");
                            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return Result.RESCHEDULE;
        }
    }

    public static void scheduleJob() {
        new JobRequest.Builder(NewsArticlesJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(60))
                .setPersisted(true)
                .setUpdateCurrent(true)
                .build()
                .schedule();
    }

}