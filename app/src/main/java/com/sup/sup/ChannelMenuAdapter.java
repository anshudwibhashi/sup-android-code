package com.sup.sup;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class ChannelMenuAdapter extends BaseAdapter {
    String texts[] = {"News",
            "Sports",
            "Reddit",
            "YouTube",
            "Twitter",
            "Track Flights",
            "Track Packages",
            "Stocks",
            "Weather"
    };
    String descs[] = {
            "Sends you broadcasts with real-time news-updates on topics of your choice from over 130 sources.",
            "Gives you on-demand real-time score updates and play-by-play info for teams of your choice.",
            "Sends you broadcasts with real-time updates of subreddits of your choice.",
            "Sends you broadcasts each time your favorite YouTube channels publish videos.",
            "Sends you broadcasts each time your favorite Twitter handles tweet.",
            "Gives you on-demand real-time updates of flights of your choice.",
            "Sends you broadcasts with real-time updates on the status of your packages.",
            "Gives you on-demand stock updates of companies of your choice.",
            "Gives you on-demand weather updates of your current location."
    };
    int icons[] = {R.drawable.ic_news_channel,
            R.drawable.ic_sports_channel,
            R.drawable.ic_reddit_channel,
            R.drawable.ic_youtube_channel,
            R.drawable.ic_twitter_channel,
            R.drawable.ic_flight_channel,
            R.drawable.ic_package_channel,
            R.drawable.ic_stocks_alt,
            R.drawable.ic_weather_channel
    };

    Activity context;
    public ChannelMenuAdapter(Activity context) {
        this.context = context;
    }
    @Override
    public int getCount() {
        return 9;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null || convertView.getTag() == null) {
            holder = new ViewHolder();
            convertView =  context.getLayoutInflater().inflate(R.layout.list_item_channels_menu, parent, false);
            holder.icon = (ImageView) convertView.findViewById(R.id.iconIV);
            holder.text = (TextView) convertView.findViewById(R.id.nameTV);
            holder.desc = (TextView) convertView.findViewById(R.id.descTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(texts[position]);
        holder.icon.setImageResource(icons[position]);
        holder.desc.setText(descs[position]);

        return convertView;
    }

    public static class ViewHolder {
        public ImageView icon;
        public TextView text, desc;
    }
}
