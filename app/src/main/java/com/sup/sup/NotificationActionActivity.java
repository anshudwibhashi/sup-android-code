package com.sup.sup;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.RemoteInput;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.FirebaseApp;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class NotificationActionActivity extends AppCompatActivity {

    static final public String SERVICE_RESULT = "com.sup.sup.CustomFirebaseMessagingService.REQUEST_PROCESSED";
    static final public String SERVICE_MESSAGE = "com.sup.sup.CustomFirebaseMessagingService.MSG";
    static final public String SERVICE_TOKEN = "com.sup.sup.CustomFirebaseMessagingService.TKN";
    private LocalBroadcastManager broadcaster;
    public void sendResult(String message) {
        Intent intent = new Intent(SERVICE_RESULT);
        if(message != null)
            intent.putExtra(SERVICE_MESSAGE, message);
        broadcaster.sendBroadcast(intent);
    }
    public void sendResult2(String message, String user_token) {
        Intent intent = new Intent(SERVICE_RESULT);
        if(message != null && user_token != null) {
            intent.putExtra(SERVICE_MESSAGE, message);
            intent.putExtra(SERVICE_TOKEN, user_token);
        }
        broadcaster.sendBroadcast(intent);
    }

    public void refreshUI() {
        sendResult("update_ui"); // refresh contacts list
    }

    private void refreshUI3(String token) {
        sendResult2("update_ui3", token); // refresh active smalltalk windows
    }

    private void refreshUI2(String token) {
        sendResult2("update_ui2", token); // refresh feed upon new broadcast
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Global initializations
        broadcaster = LocalBroadcastManager.getInstance(this);
        FirebaseApp.initializeApp(this);
        Utils.mSharedPreferences = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        Utils.mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .build();

        Intent intent = getIntent();
        switch (intent.getStringExtra("ACTION")) {
            case "COMMENT_FROM_NOTIFICATION":
                handleComments(intent);
                break;
            case "SMALLTALK_FROM_NOTIFICATION":
                handleSmalltalk(intent);
                break;
            case "SUP_FROM_NOTIFICATION":
                handleSup(intent);
        }
    }

    int notificationId, position; MaterialDialog y;

    private void handleSup(Intent intent) {
        String to = intent.getStringExtra("to");
        notificationId = intent.getIntExtra("notificationId", -1);


        Utils.populateMainDataSet(this);
        int p;
        for (p = 0; p < Utils.MainDataSet.tokens.size();) {
            if (Utils.MainDataSet.tokens.get(p).equals(to)) break;
            p++;
        }

        position = p;

        final LinearLayout view = (LinearLayout) getLayoutInflater().inflate(R.layout.sup_payload, null);


        MaterialDialog.Builder x = new MaterialDialog.Builder(this)
                .title("Respond to Sup")
                .customView(view, false)
                .neutralText("Cancel")
                .cancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                })
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                });
        y = x.build();


        view.findViewById(R.id.sup_payload).setVisibility(View.VISIBLE);
        ((View)view.findViewById(R.id.cancel_action).getParent()).setVisibility(View.GONE);

        view.findViewById(R.id.text_response).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final VolleyCallback callback = new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        y.dismiss();
                        Utils.deleteSups(NotificationActionActivity.this, Utils.MainDataSet.tokens.get(position));
                        Utils.populateMainDataSet(NotificationActionActivity.this);
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.cancel(notificationId);
                        finish();
                        refreshUI();
                        if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
                            HomeAdapter.processGroupResponse(result, null, position, NotificationActionActivity.this);
                        } else {
                            HomeAdapter.processResponse(result, null, position, NotificationActionActivity.this);
                        }
                    }
                };

                Utils.obtainTextResponse((Activity) NotificationActionActivity.this, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        callback.onResponse(Utils.packageResponse("text_response", result.toString()));
                    }
                }, "Respond with");
            }
    });
        view.findViewById(R.id.image_response).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final VolleyCallback callback = new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        y.dismiss();
                        Utils.deleteSups(NotificationActionActivity.this, Utils.MainDataSet.tokens.get(position));
                        Utils.populateMainDataSet(NotificationActionActivity.this);
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.cancel(notificationId);
                        finish();
                        refreshUI();
                        if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
                            HomeAdapter.processGroupResponse(result, null, position, NotificationActionActivity.this);
                        } else {
                            HomeAdapter.processResponse(result, null, position, NotificationActionActivity.this);
                        }
                    }
                };

                Utils.activity_result_callback = callback;
                ((NotificationActionActivity)NotificationActionActivity.this).requestingPosition = position;
                ((NotificationActionActivity)NotificationActionActivity.this).purpose = "";
                ((NotificationActionActivity) NotificationActionActivity.this).usersName = Utils.MainDataSet.names.get(position);

                Context context = NotificationActionActivity.this;
                if(!(ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(context,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions((Activity)context,
                            new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            Constants.MY_PERMISSIONS_REQUEST_IMAGE_RESPONSE);
                } else {
                    new SandriosCamera((Activity) NotificationActionActivity.this, Constants.REQUEST_CAPTURE_MEDIA)
                            .setShowPicker(true)
                            .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                            .enableImageCropping(false) // Default is false.
                            .launchCamera();
                    // The result will be delivered in the onActivityResult() method of this activity
                }
            }
        });
        view.findViewById(R.id.video_response).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final VolleyCallback callback = new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        y.dismiss();
                        Utils.deleteSups(NotificationActionActivity.this, Utils.MainDataSet.tokens.get(position));
                        Utils.populateMainDataSet(NotificationActionActivity.this);
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.cancel(notificationId);
                        finish();
                        refreshUI();
                        if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
                            HomeAdapter.processGroupResponse(result, null, position, NotificationActionActivity.this);
                        } else {
                            HomeAdapter.processResponse(result, null, position, NotificationActionActivity.this);
                        }
                    }
                };

                Utils.activity_result_callback = callback;
                ((NotificationActionActivity)NotificationActionActivity.this).requestingPosition = position;
                ((NotificationActionActivity)NotificationActionActivity.this).purpose = "";
                ((NotificationActionActivity) NotificationActionActivity.this).usersName = Utils.MainDataSet.names.get(position);

                if(!(ContextCompat.checkSelfPermission(NotificationActionActivity.this,
                        android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(NotificationActionActivity.this,
                        android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions((Activity)NotificationActionActivity.this,
                            new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO},
                            Constants.MY_PERMISSIONS_REQUEST_VIDEO_RESPONSE);
                } else {
                    /*new MaterialCamera( (Activity) NotificationActionActivity.this)
                            .saveDir(NotificationActionActivity.this.getFilesDir())
                            .countdownSeconds(10f)
                            .showPortraitWarning(false)
                            .start(Constants.REQUEST_CAMERA_VIDEO);*/
                    // The result will be delivered in the onActivityResult() method of MainActivity
                }
            }
        });
        view.findViewById(R.id.location_response).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final VolleyCallback callback = new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        y.dismiss();
                        Utils.deleteSups(NotificationActionActivity.this, Utils.MainDataSet.tokens.get(position));
                        Utils.populateMainDataSet(NotificationActionActivity.this);
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.cancel(notificationId);
                        finish();
                        refreshUI();
                        if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
                            HomeAdapter.processGroupResponse(result, null, position, NotificationActionActivity.this);
                        } else {
                            HomeAdapter.processResponse(result, null, position, NotificationActionActivity.this);
                        }
                    }
                };

                ((NotificationActionActivity) NotificationActionActivity.this).usersName = Utils.MainDataSet.names.get(position);

                if (!(ContextCompat.checkSelfPermission(NotificationActionActivity.this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(NotificationActionActivity.this,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                    Utils.activity_result_callback = callback;

                    ActivityCompat.requestPermissions((Activity) NotificationActionActivity.this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE);
                } else {
                    Location location = Utils.getLastKnownLocation(NotificationActionActivity.this);

                    if (location != null) {
                        double lat = location.getLatitude();
                        double lon = location.getLongitude();
                        callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                    } else {
                        Utils.showToast(NotificationActionActivity.this, "Location Unavailable");
                    }
                }
            }
        });

        view.findViewById(R.id.audio_response).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final VolleyCallback callback = new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        y.dismiss();
                        Utils.deleteSups(NotificationActionActivity.this, Utils.MainDataSet.tokens.get(position));
                        Utils.populateMainDataSet(NotificationActionActivity.this);
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.cancel(notificationId);
                        finish();
                        refreshUI();
                        if(Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
                            HomeAdapter.processGroupResponse(result, null, position, NotificationActionActivity.this);
                        } else {
                            HomeAdapter.processResponse(result, null, position, NotificationActionActivity.this);
                        }
                    }
                };

                ((NotificationActionActivity) NotificationActionActivity.this).usersName = Utils.MainDataSet.names.get(position);

                // Slide-in the audio portion
                View pre = view.findViewById(R.id.attachLayout);
                View post = view.findViewById(R.id.audioRecordLayout);

                Utils.animateIn(pre, post, null);

                Utils.requestingPosition = position;
                Utils.inNotification = true;
                Utils.y = y; Utils.position = position; Utils.notificationId = notificationId;
                Utils.obtainAudioResponse((Activity) NotificationActionActivity.this, Utils.MainDataSet.names.get(position), new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        callback.onResponse(result);
                    }
                }, view.findViewById(R.id.sup_payload), null, "Respond with");
            }
        });

        y.show();

    }

    File fileToBeDeleted = null;
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final String fileName;
        fileName = "response_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis() % 100000000);

        // Received recording or error from MaterialCamera
        if (requestCode == Constants.REQUEST_CAPTURE_MEDIA) {
            if (resultCode == RESULT_OK) {
                String path = data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH);
                path = path.substring(path.indexOf("/storage"));
                File result = new File(path);
                String mime = Utils.getMimeType(Uri.fromFile(result), NotificationActionActivity.this);
                if (mime.startsWith("image/")) {
                    fileToBeDeleted = result;

                    try {

                        Uri editedImageUri = Uri.fromFile(fileToBeDeleted);
                        InputStream is = getContentResolver().openInputStream(editedImageUri);
                        final File finalFile = new File(getFilesDir(), fileName);
                        finalFile.createNewFile();
                        Utils.copyInputStreamToFile(is, finalFile);
                        byte[] bytes = Utils.fileToBytes(finalFile);

                        String message;
                        if (usersName != null) {
                            message = "Sending image to " + usersName;
                        } else {
                            message = null;
                        }

                        y.dismiss();
                        Utils.deleteSups(NotificationActionActivity.this, Utils.MainDataSet.tokens.get(position));
                        Utils.populateMainDataSet(NotificationActionActivity.this);
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.cancel(notificationId);
                        refreshUI();
                        finish();

                        Utils.uploadToFCS(this, message, fileName + ".png", bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                Utils.activity_result_callback.onResponse(Utils.packageResponse("image_response", fileName + ".png"));
                            }
                        });

                        if (fileToBeDeleted != null) {
                            fileToBeDeleted.delete();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (mime.startsWith("video/")) {
                    try {
                        File f = new File(data.getStringExtra(CameraConfiguration.Arguments.FILE_PATH));
                        byte[] bytes = Utils.fileToBytes(f);
                        f.delete();

                        if (saveMedia) {
                            saveMedia = false;
                            f = new File(getFilesDir(), fileName + ".mp4");
                            FileOutputStream os = new FileOutputStream(f);
                            os.write(bytes);
                            os.close();

                            File output = new File(NotificationActionActivity.this.getFilesDir(), "snapshot_" + fileName+ ".png");
                            output.createNewFile();
                            Utils.snapshot(new File(NotificationActionActivity.this.getFilesDir(), fileName+".mp4"), output, NotificationActionActivity.this);
                        }

                        String message;
                        if (usersName != null) {
                            message = "Sending video to " + usersName;
                        } else {
                            message = null;
                        }

                        y.dismiss();
                        Utils.deleteSups(NotificationActionActivity.this, Utils.MainDataSet.tokens.get(position));
                        Utils.populateMainDataSet(NotificationActionActivity.this);
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.cancel(notificationId);
                        refreshUI();
                        finish();

                        Utils.uploadToFCS(this, message, fileName + ".mp4", bytes, new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                Utils.activity_result_callback.onResponse(Utils.packageResponse("video_response", fileName + ".mp4"));
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (data != null) {
                Bundle bundle = data.getExtras();
                for (String key : bundle.keySet()) {
                    Object value = bundle.get(key);
                }
            }
        }
    }

    int requestingPosition = -1; String purpose = ""; String usersName = ""; LinearLayout layoutForAttach = null; boolean saveMedia = false;
    boolean forBroadcast = false;

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.MY_PERMISSIONS_REQUEST_IMAGE_RESPONSE) {
            if(Arrays.equals(grantResults, new int[] {PackageManager.PERMISSION_GRANTED})) {
                /*new MaterialCamera(this)
                        .saveDir(getFilesDir())
                        .stillShot()
                        .start(Constants.REQUEST_CAMERA_IMAGE);*/
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        } else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_VIDEO_RESPONSE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED || (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                /*new MaterialCamera(this)
                        .saveDir(getFilesDir())
                        .showPortraitWarning(false)
                        .countdownSeconds(10f)
                        .start(Constants.REQUEST_CAMERA_VIDEO);*/
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        } else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_AUDIO_RESPONSE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED || (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                Utils.recordAudio(layoutForAttach, usersName, this, purpose, null);
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        } else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                Location location = Utils.getLastKnownLocation(this);
                if(location == null) {
                    Utils.showToast(this, "Location Unavailable");
                } else {
                    final double lat = location.getLatitude();
                    final double lon = location.getLongitude();

                    if (!saveMedia) {
                        Utils.activity_result_callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                    } else {
                        final ProgressDialog pd = new ProgressDialog(this);
                        pd.setCancelable(false);
                        pd.setCanceledOnTouchOutside(false);
                        pd.setIndeterminate(true);
                        pd.setMessage("Downloading map...");
                        pd.show();
                        Utils.downloadImageAndSave((Activity) this, Utils.prepareStaticMapUrl(this, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                pd.dismiss();
                                try {
                                    if (forBroadcast) {
                                        // getCaption();
                                        Utils.activity_result_callback.onResponse(Utils.packageBroadcast("location_broadcast", "", lat + "_" + lon));
                                    } else {
                                        Utils.activity_result_callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        }  else if (requestCode == Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE2) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED && (grantResults.length == 2 && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    this.startActivityForResult(builder.build(this), Constants.PLACE_PICKER_REQUEST);
                } catch (Exception e) {e.printStackTrace();}
            } else {
                Utils.showDialogMessage(this, "Required permissions denied", "Please provide the required permissions to use this feature.");
            }
        }
    }


    private void handleSmalltalk(Intent intent) {
        final String to = intent.getStringExtra("to");
        final int notificationId = intent.getIntExtra("notificationId", -1);
        Utils.putStringInSharedPrefs("smalltalk_open", "");

        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            String reply = remoteInput.getCharSequence("KEY_TEXT_REPLY").toString();
            processReply(reply, to);
            finish();
            Utils.showToast(this, "Reply sent");

            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.cancel(notificationId);
        } else {
            // Show dialog to get comment
            new MaterialDialog.Builder(this)
                    .title("Reply")
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .cancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            finish();
                        }
                    })
                    .input(null, null, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, CharSequence input) {
                            // Do something
                            String reply = input.toString();
                            dialog.dismiss();
                            processReply(reply, to);
                            finish();

                            Utils.showToast(NotificationActionActivity.this, "Reply sent");
                            NotificationManager mNotifyMgr =
                                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            // Builds the notification and issues it.
                            mNotifyMgr.cancel(notificationId);
                        }
                    }).show();
        }
    }

    private void processReply(String reply, String to) {
        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToast(this, "No internet connection");
            return;
        }

        if (to.startsWith("GROUP_")) {
            if (reply.trim().isEmpty())
                return;

            // prepare message:
            Smalltalk.MyUser user = new Smalltalk.MyUser("sender", ""); // third param null, because we don't want do display our own pic
            Smalltalk.MyMessage message = new Smalltalk.MyMessage(""+new Random().nextInt(), reply.toString().trim(), user, new Date());

            Utils.populateMainDataSet(this);
            int position;
            for (position = 0; position < Utils.MainDataSet.tokens.size(); ) {
                if (Utils.MainDataSet.tokens.get(position).equals(to)) break;
                position++;
            }

            HomeAdapter.sendSmalltalk(this, position, message, null, null, null, null);
            try {
                refreshUI3(to);
                Utils.sendSmalltalkToGroup(reply.toString().trim(), "text", Utils.MainDataSet.tokens.get(position), this);
            } catch (Exception e) {e.printStackTrace();}
        } else {
            if (reply.trim().isEmpty())
                return;

            // prepare message:
            Smalltalk.MyUser user = new Smalltalk.MyUser("sender", ""); // third param null, because we don't want do display our own pic
            Smalltalk.MyMessage message = new Smalltalk.MyMessage("" + new Random().nextInt(), reply, user, new Date());

            Utils.populateMainDataSet(this);
            int position;
            for (position = 0; position < Utils.MainDataSet.tokens.size(); ) {
                if (Utils.MainDataSet.tokens.get(position).equals(to)) break;
                position++;
            }

            HomeAdapter.sendSmalltalk(this, position, message, null, null, null, null);
            try {
                refreshUI3(to);
                HomeAdapter.sendSmalltalkToOtherUser(reply, "text", position, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handleComments(Intent intent) {
        final Comments comments = (Comments) Utils.deserialize(intent.getStringExtra("comments"), Comments.class);
        final String id = intent.getStringExtra("id");
        final String owner_token = intent.getStringExtra("owner_token");
        final int notificationId = intent.getIntExtra("notificationId", -1);

        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            String comment = remoteInput.getCharSequence("KEY_TEXT_REPLY").toString();
            processComment(comment, comments, id, owner_token);
            finish();
            Utils.showToast(this, "Comment sent");

            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.cancel(notificationId);
        } else {
            // Show dialog to get comment
            new MaterialDialog.Builder(this)
                    .title("Reply to comment")
                    .inputType(InputType.TYPE_CLASS_TEXT)
                    .cancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            finish();
                        }
                    })
                    .input(null, null, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, CharSequence input) {
                            // Do something
                            String comment = input.toString();
                            dialog.dismiss();
                            processComment(comment, comments, id, owner_token);
                            finish();

                            Utils.showToast(NotificationActionActivity.this, "Comment sent");
                            NotificationManager mNotifyMgr =
                                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            // Builds the notification and issues it.
                            mNotifyMgr.cancel(notificationId);
                        }
                    }).show();
        }
    }

    private Comments processComment(String comment, Comments comments, String id, String owner_token) {
        Comments.Commenter user = new Comments.Commenter();
        user.comment = comment;
        user.name = Utils.getStringFromSharedPrefs("self_name", "");
        user.username = Utils.getStringFromSharedPrefs("self_username", "");
        user.timeStamp = ""+System.currentTimeMillis();
        user.photoUrl = Utils.getStringFromSharedPrefs("self_photoUrl", "").replace("_photo.png","_photo_mini.png");
        user.token = Utils.getStringFromSharedPrefs("self_token", "");

        comments.users.add(user);

        Utils.addCommentToDb(this, id, comments); // add comment
        refreshUI2(id);
        Utils.sendComment(this, user, id, owner_token); // send comment

        return comments;
    }
}
