package com.sup.sup;


import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.RemoteInput;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationCompat;

import android.os.SystemClock;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.net.ssl.HttpsURLConnection;
import rm.com.audiowave.AudioWaveView;
import rm.com.audiowave.OnSamplingListener;

/**
 * Created by anshudwibhashi on 24/05/17.
 */
public class Utils {
    public static SharedPreferences mSharedPreferences;
    public static FirebaseStorage storage = FirebaseStorage.getInstance();
    public static GoogleApiClient mGoogleApiClient;

    public static String convertISOToMillis(String ISO) {
        try {
            DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date result1 = df1.parse(ISO);
            long millis = result1.getTime();
            return String.valueOf(millis);
        } catch (Exception e) {e.printStackTrace(); return "";}
    }

    public static long generateRandomTime(long offset, long end) {
        long diff = end - offset + 1;
        return new Timestamp(offset + (long)(Math.random() * diff)).getTime();
    }

    public static ArrayList<String> JSONArrayToArrayList(JSONArray jsonArray) {
        ArrayList<String> list = new ArrayList<>();
        if (jsonArray != null) {
            int len = jsonArray.length();
            for (int i = 0; i < len; i++) {
                try {
                    list.add(jsonArray.get(i).toString());
                } catch (Exception e) {e.printStackTrace();}
            }
        }
        return list;
    }

    public static JSONArray ArrayListToJSONArray(ArrayList<String> myCustomList) {
        JSONArray jsonArray = new JSONArray();
        for (int i=0; i < myCustomList.size(); i++) {
            jsonArray.put(myCustomList.get(i));
        }
        return jsonArray;
    }


    public static long getMillisFromTime(String time) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Date dt = formatter.parse(time);
        Calendar cal = Calendar.getInstance();
        int hours = dt.getHours();
        if (hours > cal.getTime().getHours()) {
            hours = (int)((cal.getTime().getHours() * hours)/24.0);
        }
        cal.set(Calendar.HOUR, hours);
        cal.set(Calendar.MINUTE, dt.getMinutes());
        cal.set(Calendar.SECOND, dt.getSeconds());
        return cal.getTimeInMillis();
    }


    public static boolean getBooleanFromSharedPrefs(String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    public static int getIntegerFromSharedPrefs(String key, int defaultValue) {
        return mSharedPreferences.getInt(key, defaultValue);
    }

    public static void putIntegerInSharedPrefs(String key, int value) {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putInt(key, value);
        mEditor.apply();
    }

    public static void putStringInSharedPrefs(String key, String value) {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putString(key, value);
        mEditor.apply();
    }

    public static void putBooleanInSharedPrefs(String key, boolean value) {
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean(key, value);
        mEditor.apply();
    }

    public static String getStringFromSharedPrefs(String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel("channel_id",
                "The only channel",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("The only channel");
        notificationManager.createNotificationChannel(channel);
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void scheduleNotification(Context context, long delay, int notificationId, String title,
                                     String message, int icon, Class activityToOpen) {//delay is after how much time(in millis) from current time you want to schedule the notification
        Utils.putStringInSharedPrefs("notification_"+String.valueOf(notificationId)+"_title", title);
        Utils.putStringInSharedPrefs("notification_"+String.valueOf(notificationId)+"_message", message);
        Utils.putIntegerInSharedPrefs("notification_"+String.valueOf(notificationId)+"_icon", icon);
        if(activityToOpen != null)
            Utils.putStringInSharedPrefs("notification_"+String.valueOf(notificationId)+"_activitytoopen", activityToOpen.getName());
        String url = "http://black-abode-2709.appspot.com/scheduleNotification?token=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", ""))
                +"&notification_id="+notificationId+"&delay="+delay;
        Utils.volleyStringCall(context, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                // dont care
            }
        });
    }

    public static void volleyStringCall(Context context, String url, final VolleyCallback callback) {
        if(!Utils.isNetworkAvailable(context)){
            Utils.showToast(context, "No internet connection");
            return;
        }
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        // Add the request to the RequestQueue.
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    public static void volleyJSONCall(Context context, final String url, final HashMap<String, String> headers, final VolleyCallback callback) {
        if(!Utils.isNetworkAvailable(context)){
            Utils.showToast(context, "No internet connection");
            return;
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("News success", response.toString());
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("news error", error.toString());
                    }
                }){
                    /**
                     * Passing some request headers
                     */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Log.d("News log", headers.toString());
                        return headers;
                    }
                };
                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        50000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(jsObjRequest);
    }


    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public static String syncAddressBook(String data) {
        HashMap<String, String> postDataParams = new HashMap<String, String>();
        postDataParams.put("data", data);

        String requestURL  = "http://black-abode-2709.appspot.com/syncAddressBook";

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(1500000);
            conn.setConnectTimeout(150000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else {
                response="";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public static void volleyJSONCall(Context context, final String url, final VolleyCallback callback) {
        if(!Utils.isNetworkAvailable(context)){
            Utils.showToast(context, "No internet connection");
            return;
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(jsObjRequest);
    }

    public static void volleyJSONCall(Context context, final String url, final VolleyCallback2 callback) {
        if(!Utils.isNetworkAvailable(context)){
            Utils.showToast(context, "No internet connection");
            return;
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(jsObjRequest);
    }


    public static void volleyJSONCall2(Context context, final String url, final VolleyCallback callback) {
        if(!Utils.isNetworkAvailable(context)){
            Utils.showToast(context, "No internet connection");
            return;
        }
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(context).add(jsonArrayRequest);
    }


    /**
     * Downloads the JSON object for the profile
     */
    public static void downloadProfile(Context context, String token, final VolleyCallback callback) {
        String url = "http://black-abode-2709.appspot.com/download_profile?token=" + URLEncoder.encode(token);
        Utils.volleyJSONCall(context, url, callback);
    }

    public static void downloadProfileByUsername(Context context, String username, final VolleyCallback callback) {
        String url = "http://black-abode-2709.appspot.com/download_profile?username=" + URLEncoder.encode(username);
        Utils.volleyJSONCall(context, url, callback);
    }

    // NOTE: we can retrive group owner anytime from groupID (aka groupName) because grouID = "GROUP_<owner_token>_<timestamp>"
    public static String getGroupOwner(String groupName) {
        return groupName.split("_")[1];
    }

    public static void deleteGroup(Context context, String groupName, VolleyCallback callback) {
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
        db.delete("connections", "token=?", new String[]{groupName});
        populateMainDataSet(context);
        callback.onResponse(null);
    }

    public static void updateGroup(Context context, String groupName, String groupDispName, String groupDesc, String participants, String photoUrl
    , VolleyCallback callback) {
        SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
        Cursor results = dbR.rawQuery("SELECT * FROM Connections WHERE token=?", new String[]{groupName});
        if (results.getCount() == 0) {
            // Create group
            Smalltalk.SmalltalkMessage smalltalkMessage = new Smalltalk.SmalltalkMessage();
            smalltalkMessage.messages = new Smalltalk.MessageBlock();

            try {
                SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put("token", groupName);
                values.put("name", groupDispName);
                values.put("tagline", groupDesc);
                values.put("photoUrl", photoUrl);
                values.put("sup_count", 0);
                JSONObject response = new JSONObject();
                response.put("sup_sender", "");
                response.put("responseList", new JSONArray());
                values.put("response", response.toString());
                values.put("smalltalk", serialize(smalltalkMessage));
                values.put("microtext_read", 1);
                values.put("username", participants);
                values.put("last_modified", String.valueOf(System.currentTimeMillis()));
                values.put("online_status", 0);
                db.insert("connections", null, values);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // Update group

            try {
                SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put("name", groupDispName);
                values.put("tagline", groupDesc);
                values.put("photoUrl", photoUrl);
                values.put("username", participants);
                values.put("last_modified", String.valueOf(System.currentTimeMillis()));
                db.update("connections", values, "token=?", new String[]{groupName});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        populateMainDataSet(context);
        callback.onResponse(null);
    }


    public static void saveSelfProfile(String name, String token, String photoUrl, String tagline, String following, String username, String settings, String phone_number) {
        Utils.putStringInSharedPrefs("self_name", name);
        Utils.putStringInSharedPrefs("self_token", token);
        Utils.putStringInSharedPrefs("self_photoUrl", photoUrl);
        Utils.putStringInSharedPrefs("self_tagline", tagline);
        Utils.putStringInSharedPrefs("self_following", following);
        Utils.putStringInSharedPrefs("self_username", username);
        Utils.putStringInSharedPrefs("self_phone_number", phone_number);

        Utils.saveSelfSettings(settings);
    }

    public static void updateSelfProfile(String name, String photoUrl, String tagline, String username) {
        Utils.putStringInSharedPrefs("self_tagline", tagline);
        Utils.putStringInSharedPrefs("self_name", name);
        Utils.putStringInSharedPrefs("self_photoUrl", photoUrl);
        Utils.putStringInSharedPrefs("self_username", username);
    }

    public static void downloadImageAndSave(final Context context, final String imgUrl, final String filename, final VolleyCallback callback) {
        /**
         * Downloads image, saves to internal storage.
         */

        ImageRequest request = new ImageRequest(imgUrl,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        // Save this bitmap to internal storage
                        FileOutputStream outputStream;
                        try {
                            File file = new File(context.getFilesDir(), filename);
                            file.createNewFile();
                            Log.d("File created", file.getAbsolutePath());
                            outputStream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                            outputStream.close();
                            callback.onResponse(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.onResponse(null);
                    }
                });
        Volley.newRequestQueue(context).add(request);
    }

    /**
     * Download profiles of all those in "following" and add to database
     * For each profile call follow()
     */
    public static void downloadContacts(final Context context, JSONObject profile, final VolleyCallback finalCallback) {
        try {
            if (new JSONArray(profile.getString("following")).length() == 0) {
                finalCallback.onResponse(null); // No one to download
                return;
            }
            for (int i = 0; i < new JSONArray(profile.getString("following")).length(); i++) {
                final String following_token = new JSONArray(profile.getString("following")).getString(i);
                // FCM Follow this user's topic
                Utils.downloadProfile(context, following_token, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        Utils.follow(context, (JSONObject) result, following_token, false, finalCallback);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void downloadGroups(final Context context, String token, final VolleyCallback callback) {
        volleyJSONCall2(context, "http://black-abode-2709.appspot.com/get_all_groups?user=" + token, new VolleyCallback() {
            @Override
            public void onResponse(Object r) {
                final JSONArray result = (JSONArray) r;
                for(int i = 0; i < result.length(); i++) {
                    try {
                        final String name = result.getJSONObject(i).getString("groupName");
                        final String dispName = result.getJSONObject(i).getString("groupDispName");
                        final String desc = result.getJSONObject(i).getString("groupDesc");
                        final String participants = result.getJSONObject(i).getString("participants");
                        final String photoUrl = result.getJSONObject(i).getString("photoUrl");
                        final int index = i;
                        downloadImageAndSave(context, photoUrl, name + "_photo.png", new VolleyCallback() {
                            @Override
                            public void onResponse(Object r) {
                                updateGroup(context, name, dispName, desc, participants, photoUrl, new VolleyCallback() {
                                    @Override
                                    public void onResponse(Object r) {
                                        if (index == result.length() - 1) {
                                            callback.onResponse(null);
                                        }
                                    }
                                });
                            }
                        });
                    } catch (Exception e){e.printStackTrace();}
                }
                if(result.length() == 0) {
                    callback.onResponse(null); // If the user doesn't have any groups and it didn't run!
                }
            }
        });
    }

    public static void leaveGroup(final Context context, final String token, final Dialog mBottomSheetDialog, final File photo) {
        Utils.confirmDialog(context, "Leave group?", new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                if (((boolean) result)) {
                    Utils.volleyStringCall(context, "http://black-abode-2709.appspot.com/leave_group?groupID=" + token + "&user="+
                            URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", "")), new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                        }
                    });

                    // delete images from FCS
                    Utils.deleteFromFCS(token + "_photo.png", new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            // dont care
                        }
                    });
                    Utils.deleteFromFCS(token + "_photo_mini.png", new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            // dont care
                        }
                    });
                    photo.delete(); // delete the image
                    Utils.deleteGroup(context, token, new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            if (mBottomSheetDialog != null)
                                mBottomSheetDialog.dismiss();
                            if (((ListView) ((Activity) context).findViewById(R.id.mainLV)) != null && ((ListView) ((Activity) context).findViewById(R.id.mainLV)).getAdapter() instanceof HomeAdapter) {
                                ((BaseAdapter) ((ListView) ((Activity) context).findViewById(R.id.mainLV)).getAdapter()).notifyDataSetInvalidated();
                                ((BaseAdapter) ((ListView) ((Activity) context).findViewById(R.id.mainLV)).getAdapter()).notifyDataSetChanged();
                            }

                        }
                    });
                }
            }
        });
    }

    public static void deleteGroupFromServer(final Context context, final String token, final Dialog mBottomSheetDialog, final File photo) {
        Utils.confirmDialog(context, "Delete group? All participants will be affected.", new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        if (((boolean) result)) {
                            Utils.volleyStringCall(context, "http://black-abode-2709.appspot.com/delete_group?groupID=" + token, new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    // don't care
                                }
                            });

                            // delete images from FCS
                            Utils.deleteFromFCS(token + "_photo.png", new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    // dont care
                                }
                            });
                            Utils.deleteFromFCS(token + "_photo_mini.png", new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    // dont care
                                }
                            });
                            photo.delete(); // delete the image

                            Utils.deleteGroup(context, token, new VolleyCallback() {
                                @Override
                                public void onResponse(Object result) {
                                    if (mBottomSheetDialog != null)
                                        mBottomSheetDialog.dismiss();
                                    if (((ListView) ((Activity) context).findViewById(R.id.mainLV)) != null && ((ListView) ((Activity) context).findViewById(R.id.mainLV)).getAdapter() instanceof HomeAdapter) {
                                        ((BaseAdapter) ((ListView) ((Activity) context).findViewById(R.id.mainLV)).getAdapter()).notifyDataSetInvalidated();
                                        ((BaseAdapter) ((ListView) ((Activity) context).findViewById(R.id.mainLV)).getAdapter()).notifyDataSetChanged();
                                    }

                                }
                            });
                        }
                    }
                });
    }

    /**
     * follow() follows a person, downloads their picture.
     * Calls dbAdd() to add person to db.
     */
    public static void follow(final Context context, final JSONObject profile, final String token, boolean newContact, final VolleyCallback finalCallback) {
        // If this is a new contact, first add them to db
        FirebaseMessaging.getInstance().subscribeToTopic(token);
        if (newContact) {
            try {
                JSONArray a = new JSONArray(Utils.getStringFromSharedPrefs("self_following", "[]"));
                if(!a.toString().contains(token)) {
                    a.put(token);
                    Utils.putStringInSharedPrefs("self_following", a.toString());
                    Utils.volleyStringCall(context, "http://black-abode-2709.appspot.com/update_profile?token=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", "")) + "&following=" + URLEncoder.encode(a.toString()), new VolleyCallback() {
                        @Override
                        public void onResponse(Object result) {
                            // Don't care..
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Now download their picture
        try {
            // Download image to <token>_photo.png
            if (profile.getString("photoUrl") != null && !profile.getString("photoUrl").isEmpty()) {
                Utils.downloadImageAndSave(context, profile.getString("photoUrl"), token + "_photo.png", new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        // Now add to database
                        Utils.dbAdd(context, profile, token, finalCallback);
                    }
                });
            } else {
                FileOutputStream outputStream;
                File file = new File(context.getFilesDir(), token + "_photo.png");
                file.createNewFile();
                outputStream = new FileOutputStream(file);
                BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_default_picture).compress(Bitmap.CompressFormat.PNG, 100, outputStream);
                outputStream.close();

                Utils.dbAdd(context, profile, token, finalCallback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String serialize(Object myObject) {
        // serialize the object
        return new Gson().toJson(myObject);
    }

    public static Object deserialize(String serializedObject, Type classOf) {
        // deserialize the object
        return new Gson().fromJson(serializedObject, classOf);
    }

    /**
     * Method to be called by follow() after downloading the picture
     */
    public static void dbAdd(Context context, JSONObject profile, String token, VolleyCallback finalCallback) {

        SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
        Cursor result = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
        if(result.getCount() > 0) {
            finalCallback.onResponse(null);
            return;
        }

        Smalltalk.SmalltalkMessage smalltalkMessage = new Smalltalk.SmalltalkMessage();
        smalltalkMessage.messages = new Smalltalk.MessageBlock();

        try {
            SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("token", profile.getString("token"));
            values.put("name", profile.getString("name"));
            values.put("tagline", profile.getString("tagline"));
            values.put("photoUrl", profile.getString("photoUrl"));
            values.put("sup_count", 0);
            values.put("response", "{}");
            values.put("smalltalk", serialize(smalltalkMessage));
            values.put("microtext_read", 1);
            values.put("username", profile.getString("username"));
            values.put("last_modified", String.valueOf(System.currentTimeMillis()));
            values.put("online_status", profile.getString("online_status"));
            db.insert("connections", null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Now execute finalCallback's onResponse
        finalCallback.onResponse(null);
    }

    public static int getConnectionsCount(Context context) {
        String countQuery = "SELECT * FROM connections";
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        return cnt;
    }

    public static void populateMainDataSet(Context context) {
        MainDataSet.names = new ArrayList<>();
        MainDataSet.taglines = new ArrayList<>();
        MainDataSet.tokens = new ArrayList<>();
        MainDataSet.photoUrls = new ArrayList<>();
        MainDataSet.photos = new ArrayList<>();
        MainDataSet.supCounts = new ArrayList<>();
        MainDataSet.responses = new JSONArray();
        MainDataSet.microtextRead = new ArrayList<>();
        MainDataSet.usernames = new ArrayList<>();
        MainDataSet.smallTalkMessages = new ArrayList<>();
        MainDataSet.online_statuses = new ArrayList<>();
        MainDataSet.responses = new JSONArray();

        try {
            SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
            Cursor results = db.rawQuery("SELECT * FROM connections ORDER BY last_modified DESC", null, null);
            for (results.moveToFirst(); !results.isAfterLast(); results.moveToNext()) {
                String token = results.getString(results.getColumnIndexOrThrow("token"));

                MainDataSet.names.add(results.getString(results.getColumnIndexOrThrow("name")));
                MainDataSet.taglines.add(results.getString(results.getColumnIndexOrThrow("tagline")));
                MainDataSet.tokens.add(token);
                MainDataSet.supCounts.add(results.getInt(results.getColumnIndexOrThrow("sup_count")));
                MainDataSet.responses.put(new JSONObject(results.getString(results.getColumnIndexOrThrow("response"))));
                MainDataSet.photos.add(new File(context.getFilesDir(), token + "_photo.png").getAbsolutePath());
                MainDataSet.microtextRead.add(1 == (results.getInt(results.getColumnIndexOrThrow("microtext_read"))));
                MainDataSet.photoUrls.add(results.getString(results.getColumnIndexOrThrow("photoUrl")));
                MainDataSet.usernames.add(results.getString(results.getColumnIndexOrThrow("username")));
                MainDataSet.online_statuses.add(results.getString(results.getColumnIndexOrThrow("online_status")));

                MainDataSet.smallTalkMessages.add((Smalltalk.SmalltalkMessage)deserialize(results.getString(results.getColumnIndexOrThrow("smalltalk")), Smalltalk.SmalltalkMessage.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestWhenInternetAvailable(String url) {
        try {
            JSONArray urls = new JSONArray(Utils.getStringFromSharedPrefs("pending_requests", "[]"));
            urls.put(url);
            Utils.putStringInSharedPrefs("pending_requests", urls.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class MainDataSet {
        /**
         * The dataset used to populate the ListView on the main screen
         */
        public static ArrayList<String> names, taglines, tokens, photoUrls, usernames, online_statuses;
        public static ArrayList<String> photos;
        public static ArrayList<Integer> supCounts;
        public static ArrayList<Boolean> microtextRead;
        public static ArrayList<Smalltalk.SmalltalkMessage> smallTalkMessages;
        public static JSONArray responses; // [{'type':'<type>', 'response':{<response>}},...]
    }

    public static void deleteFromFCS(String filename, final VolleyCallback callback) {
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReferenceFromUrl("gs://supapp-1270.appspot.com");

        // Create a reference to the file to delete
        StorageReference fileRef = storageRef.child(filename);

        // Delete the file
        fileRef.delete().addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(Object o) {
                callback.onResponse(o);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                exception.printStackTrace();
            }
        });
    }

    public static NotificationCompat.Builder obtainUploadNotificationBuilder(Context context, String message) {
        if (message == null) {
            return null;
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle("Uploading")
                .setContentText(message)
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setAutoCancel(false)
                .setOngoing(true)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), android.R.drawable.stat_sys_upload))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setProgress(100, 0, false);

        return mBuilder;
    }

    public static void uploadToFCS(final Context context, final String uploadMessage, final String filename, final byte[] data, final VolleyCallback callback) {
        if (!Utils.isNetworkAvailable(context)) {
            Utils.showToast(context, "No internet connection");
            return;
        }
        StorageReference storageRef = Utils.storage.getReferenceFromUrl("gs://supapp-1270.appspot.com");
        StorageReference imageRef = storageRef.child(filename);

        final NotificationCompat.Builder mBuilder = Utils.obtainUploadNotificationBuilder(context, uploadMessage);;
        final NotificationManager mNotifyManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        final int mNotificationId = (int) System.currentTimeMillis() % 100000000;

        if (uploadMessage != null) {
            mNotifyManager.notify(mNotificationId, mBuilder.build());
        }

        final UploadTask uploadTask = imageRef.putBytes(data);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                exception.printStackTrace();
                if (uploadMessage != null)
                mNotifyManager.cancel(mNotificationId);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                if (uploadMessage != null)
                mNotifyManager.cancel(mNotificationId);
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                if (uploadMessage != null) {
                    mBuilder.setProgress(100, (int) progress, false);
                    mNotifyManager.notify(mNotificationId, mBuilder.build());
                }
            }
        });

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return imageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    callback.onResponse(downloadUri.toString());
                } else {
                    // Handle failures
                    // ...
                }
            }
        });
    }

    @SuppressWarnings({"MissingPermission"})
    public static Location getLastKnownLocation(Context context) {
        LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public static VolleyCallback activity_result_callback;

    public static JSONObject packageBroadcast(String type, String text, String media) {
        try {
            JSONObject data = new JSONObject();
            data.put("type", type);
            data.put("sender", Utils.getStringFromSharedPrefs("self_token", ""));
            data.put("id", Utils.getStringFromSharedPrefs("self_token", "")+"_"+System.currentTimeMillis());
            if (media != null) {
                data.put("media", media);
            }
            data.put("text", text);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject packageResponse(String type, String response) {
        try {
            JSONObject data = new JSONObject();
            data.put("type", type);
            data.put("sender", Utils.getStringFromSharedPrefs("self_token", ""));
            data.put("response", response);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] fileToBytes(File file) throws IOException {
        // Open file
        RandomAccessFile f = new RandomAccessFile(file, "r");
        try {
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            if (length != longlength)
                throw new IOException("File size >= 2 GB");
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            return data;
        } finally {
            f.close();

        }
    }

    public static String standardisePhone(Context context, String phone) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            return phoneUtil.format(phoneUtil.parse(phone, context.getResources().getConfiguration().locale.getCountry()), PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (Exception e) {
            return "";
        }
    }

    public static void confirmDialog(Context context, String title, String message, final VolleyCallback callback) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setTitle(title)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        callback.onResponse(true);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        callback.onResponse(false);
                    }
                }).show();
    }

    public static void confirmDialog2(Context context, String title, String message, final VolleyCallback callback) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setTitle(title)
                .setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        callback.onResponse(true);
                    }
                }).show();
    }

    public static void confirmDialog(Context context, String message, final VolleyCallback callback) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        callback.onResponse(true);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        callback.onResponse(false);
                    }
                }).show();
    }

    public static JSONArray deleteFromJSONArray(JSONArray from, Object what) {
        JSONArray result = new JSONArray();
        for (int i = 0; i < from.length(); i++) {
            try {
                if (from.get(i).equals(what)) continue;
                result.put(from.get(i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private static void getStockInfoFromYahoo(Context context, final VolleyCallback callback) {
        String symbols = Utils.getStringFromSharedPrefs("stock_tickers", "[]");
        String url = "http://black-abode-2709.appspot.com/getStockInfo?symbols="+symbols;
        Utils.volleyJSONCall2(context, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                JSONArray responses = (JSONArray) result;
                callback.onResponse(responses);
            }
        });
    }

    private static void getFlightsFromServer(Context context, final VolleyCallback callback) {
        String codes = Utils.getStringFromSharedPrefs("flight_codes", "[]");
        Date date = new Date(); // your date
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH)+1; // need to add 1 because start at 0
        int day = cal.get(Calendar.DAY_OF_MONTH);
        try {
            String url = "http://black-abode-2709.appspot.com/getFlightStats?codes=" + URLEncoder.encode(codes, "UTF-8") +
                    "&year=" + year + "&month=" + month + "&day=" + day;
            Utils.volleyJSONCall2(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    JSONArray responses = (JSONArray) result;
                    callback.onResponse(responses);
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public static String timeFrom24to12(String time) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            final Date dateObj = sdf.parse(time);
            return new SimpleDateFormat("hh:mm").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void fetchFlights(final Context context, final LinearLayout ll) {
        ll.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        getFlightsFromServer(context, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                try {
                    renderFlights((LinearLayout) ll.findViewById(R.id.payload_container), (JSONArray) result);
                    ContentValues cv = new ContentValues();
                    cv.put("response", new JSONObject().put("flights", result).toString());
                    // cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
                    Utils.putBooleanInSharedPrefs("update_flights_last_seen", true);
                    ConnectionsDBHelper.getHelper(context).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_FLIGHTS"});
                } catch (Exception e){e.printStackTrace();}
                ll.findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
            }
        });
    }

    public static void fetchSports(final Context context, final LinearLayout ll) {
        ll.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        getSportsScoresFromServer(context, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                try {
                    if(((JSONArray)result).length() != 0) {
                        renderSports((LinearLayout) ll.findViewById(R.id.payload_container), (JSONArray) result);
                        ContentValues cv = new ContentValues();
                        cv.put("response", new JSONObject().put("sports", result).toString());
                        // cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
                        Utils.putBooleanInSharedPrefs("update_sports_last_seen", true);
                        ConnectionsDBHelper.getHelper(context).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_SPORTS"});
                        Log.d("Sports results", result.toString());
                    } else {
                        Utils.showDialogMessage(context, "No games today", "None of the teams you're following are playing today.");
                    }
                } catch (Exception e){e.printStackTrace();}
                ll.findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
            }
        });
    }

    public static void getSportsScoresFromServer(final Context context, final VolleyCallback callback) {
        String leagues = Utils.getStringFromSharedPrefs("sports_teams_leagues", "[]");
        String teams = Utils.getStringFromSharedPrefs("sports_teams_abbrs", "[]");
        try {
            String url = "http://black-abode-2709.appspot.com/querySports?leagues=" + URLEncoder.encode(leagues, "UTF-8") +
                    "&teams="+URLEncoder.encode(teams, "UTF-8");
            Utils.volleyJSONCall2(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    JSONArray responses = (JSONArray) result;
                    callback.onResponse(responses);
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }
    public static void getSportsScoresFromServer2(final Context context, String league, String team, final VolleyCallback callback) {
        String leagues = new JSONArray().put(league).toString();
        String teams = new JSONArray().put(team).toString();
        try {
            String url = "http://black-abode-2709.appspot.com/querySports?leagues=" + URLEncoder.encode(leagues, "UTF-8") +
                    "&teams="+URLEncoder.encode(teams, "UTF-8");
            Utils.volleyJSONCall2(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    JSONArray responses = (JSONArray) result;
                    callback.onResponse(responses);
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void renderFlights(final LinearLayout payloadContainer, JSONArray flights) {
        try {
            payloadContainer.setVisibility(View.VISIBLE);
            ListView lv = (ListView) payloadContainer.findViewById(R.id.flightListingsLV);
            lv.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            lv.setOnItemClickListener(new FlightListClickListener(flights, payloadContainer.getContext()));

            payloadContainer.findViewById(R.id.doneButton1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues cv = new ContentValues();
                    cv.put("response", "{}");
                    ConnectionsDBHelper.getHelper(payloadContainer.getContext()).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_FLIGHTS"});
                    payloadContainer.setVisibility(View.GONE);
                    Utils.populateMainDataSet(v.getContext());
                }
            });

            lv.setAdapter(new FlightListAdapter(flights, (Activity) payloadContainer.getContext()));
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void renderFlights(final HomeAdapter.ViewHolder4 viewHolder4, JSONArray flights) {
        try {
            viewHolder4.payloadContainer.setVisibility(View.VISIBLE);
            viewHolder4.listView.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            viewHolder4.listView.setOnItemClickListener(new FlightListClickListener(flights, viewHolder4.payloadContainer.getContext()));

            viewHolder4.doneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues cv = new ContentValues();
                    cv.put("response", "{}");
                    ConnectionsDBHelper.getHelper(viewHolder4.payloadContainer.getContext()).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_FLIGHTS"});
                    Utils.populateMainDataSet(v.getContext());
                    viewHolder4.payloadContainer.setVisibility(View.GONE);
                }
            });

            viewHolder4.listView.setAdapter(new FlightListAdapter(flights, (Activity) viewHolder4.payloadContainer.getContext()));
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void renderSports(final LinearLayout payloadContainer, JSONArray sports) {
        try {
            payloadContainer.setVisibility(View.VISIBLE);
            ListView lv = (ListView) payloadContainer.findViewById(R.id.sportsListingsLV);
            lv.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            lv.setOnItemClickListener(new SportsListClickListener(sports, payloadContainer.getContext()));

            payloadContainer.findViewById(R.id.doneButton1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues cv = new ContentValues();
                    cv.put("response", "{}");
                    ConnectionsDBHelper.getHelper(payloadContainer.getContext()).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_SPORTS"});
                    payloadContainer.setVisibility(View.GONE);
                    Utils.populateMainDataSet(v.getContext());
                }
            });

            lv.setAdapter(new SportsListAdapter(sports, (Activity) payloadContainer.getContext()));
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void renderSports(final HomeAdapter.ViewHolder6 viewHolder6, JSONArray sports) {
        try {
            viewHolder6.payloadContainer.setVisibility(View.VISIBLE);
            viewHolder6.listView.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            viewHolder6.listView.setOnItemClickListener(new SportsListClickListener(sports, viewHolder6.payloadContainer.getContext()));

            viewHolder6.doneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues cv = new ContentValues();
                    cv.put("response", "{}");
                    ConnectionsDBHelper.getHelper(viewHolder6.payloadContainer.getContext()).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_SPORTS"});
                    Utils.populateMainDataSet(v.getContext());
                    viewHolder6.payloadContainer.setVisibility(View.GONE);
                }
            });

            viewHolder6.listView.setAdapter(new SportsListAdapter(sports, (Activity) viewHolder6.payloadContainer.getContext()));
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void fetchStocks(final Context context, final LinearLayout ll) {
        ll.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        getStockInfoFromYahoo(context, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                try {
                    renderStocks((LinearLayout) ll.findViewById(R.id.payload_container), (JSONArray) result);
                    ContentValues cv = new ContentValues();
                    cv.put("response", new JSONObject().put("stocks", result).toString());
                    // cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
                    Utils.putBooleanInSharedPrefs("update_stocks_last_seen", true);
                    ConnectionsDBHelper.getHelper(context).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_STOCKS"});
                } catch (Exception e){e.printStackTrace();}
                ll.findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
            }
        });
    }

    public static void fetchWeather(final Context context, final LinearLayout ll) {
        if (!(ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            ((HomeActivity) context).activeLL = ll;
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    Constants.MY_PERMISSIONS_REQUEST_LOCATION_FOR_WEATHER);
        } else {
            Location location = getLastKnownLocation(context);
            if (location == null) {
                Utils.showToast(context, "Location Unavailable");
            } else {
                final double lat = location.getLatitude();
                final double lon = location.getLongitude();
                String loc = lat + "," + lon;
                ll.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
                Utils.volleyJSONCall(context, "http://api.wunderground.com/api/57ddfb2794c5b167/conditions/astronomy/forecast/q/" + loc + ".json", new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        try {
                            JSONObject response = (JSONObject) result;
                            JSONObject weather = new JSONObject();
                            weather.put("city", response.getJSONObject("current_observation").getJSONObject("display_location").getString("city"));
                            weather.put("temp_f", String.valueOf(response.getJSONObject("current_observation").get("temp_f")));
                            weather.put("icon", String.valueOf(response.getJSONObject("current_observation").get("icon")));
                            weather.put("temp_c", String.valueOf(response.getJSONObject("current_observation").get("temp_c")));
                            weather.put("weather", response.getJSONObject("current_observation").getString("weather"));
                            weather.put("icon", response.getJSONObject("current_observation").getString("icon"));
                            weather.put("high_c", response.getJSONObject("forecast").getJSONObject("simpleforecast").getJSONArray("forecastday").getJSONObject(0).getJSONObject("high").getString("celsius"));
                            weather.put("high_f", response.getJSONObject("forecast").getJSONObject("simpleforecast").getJSONArray("forecastday").getJSONObject(0).getJSONObject("high").getString("fahrenheit"));
                            weather.put("low_c", response.getJSONObject("forecast").getJSONObject("simpleforecast").getJSONArray("forecastday").getJSONObject(0).getJSONObject("low").getString("celsius"));
                            weather.put("low_f", response.getJSONObject("forecast").getJSONObject("simpleforecast").getJSONArray("forecastday").getJSONObject(0).getJSONObject("low").getString("fahrenheit"));
                            weather.put("sunrise_hour", Integer.parseInt(response.getJSONObject("sun_phase").getJSONObject("sunrise").getString("hour")));
                            weather.put("sunrise_minute", Integer.parseInt(response.getJSONObject("sun_phase").getJSONObject("sunrise").getString("minute")));
                            weather.put("sunset_hour", Integer.parseInt(response.getJSONObject("sun_phase").getJSONObject("sunset").getString("hour")));
                            weather.put("sunset_minute", Integer.parseInt(response.getJSONObject("sun_phase").getJSONObject("sunset").getString("minute")));
                            ContentValues cv = new ContentValues();
                            cv.put("response", weather.toString());
                            // cv.put("last_modified", String.valueOf(System.currentTimeMillis()));
                            Utils.putBooleanInSharedPrefs("update_weather_last_seen", true);
                            ConnectionsDBHelper.getHelper(context).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_WEATHER"});
                            ll.findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);

                            renderWeather((LinearLayout) ll.findViewById(R.id.payload_container), weather);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    public static void renderStocks(final LinearLayout payloadContainer, JSONArray stocks) {
        try {
            payloadContainer.setVisibility(View.VISIBLE);
            ListView lv = (ListView) payloadContainer.findViewById(R.id.stockListingsLV);
            lv.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            lv.setOnItemClickListener(new StockPriceListingsClickListener(stocks, payloadContainer.getContext()));

            payloadContainer.findViewById(R.id.doneButton1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues cv = new ContentValues();
                    cv.put("response", "{}");
                    ConnectionsDBHelper.getHelper(payloadContainer.getContext()).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_STOCKS"});
                    payloadContainer.setVisibility(View.GONE);Utils.populateMainDataSet(v.getContext());
                }
            });

            lv.setAdapter(new StockPriceAdapter(stocks, (Activity) payloadContainer.getContext()));
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void renderStocks(final HomeAdapter.ViewHolder3 viewHolder3, JSONArray stocks) {
        try {
            viewHolder3.payloadContainer.setVisibility(View.VISIBLE);
            viewHolder3.listView.setOnTouchListener(new ListView.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                    // Handle ListView touch events.
                    v.onTouchEvent(event);
                    return true;
                }
            });

            viewHolder3.listView.setOnItemClickListener(new StockPriceListingsClickListener(stocks, viewHolder3.payloadContainer.getContext()));

            viewHolder3.doneButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues cv = new ContentValues();
                    cv.put("response", "{}");
                    ConnectionsDBHelper.getHelper(viewHolder3.payloadContainer.getContext()).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_STOCKS"});
                    viewHolder3.payloadContainer.setVisibility(View.GONE);Utils.populateMainDataSet(v.getContext());
                }
            });

            viewHolder3.listView.setAdapter(new StockPriceAdapter(stocks, (Activity) viewHolder3.payloadContainer.getContext()));
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void renderWeather(final LinearLayout payloadContainer, JSONObject weather) {
        try {
            payloadContainer.setVisibility(View.VISIBLE);
            ((TextView) payloadContainer.findViewById(R.id.cityTV)).setText(weather.getString("city"));
            ((TextView) payloadContainer.findViewById(R.id.descTV)).setText(weather.getString("weather"));

            if (Utils.getBooleanFromSharedPrefs("use_metric_units_weather", false)) {
                ((TextView) payloadContainer.findViewById(R.id.currentTempTV)).setText(Html.fromHtml(weather.getString("temp_c")+"&deg;"));
                ((TextView) payloadContainer.findViewById(R.id.hiloTV)).setText(weather.getString("low_c")+" / "+weather.getString("high_c"));
            } else {
                ((TextView) payloadContainer.findViewById(R.id.currentTempTV)).setText(Html.fromHtml(weather.getString("temp_f")+"&deg;"));
                ((TextView) payloadContainer.findViewById(R.id.hiloTV)).setText(weather.getString("low_f")+" / "+weather.getString("high_f"));
            }

            int icon = (Utils.mapIconToBmp(weather.getString("icon"), weather));
            if (icon == -1) {
                ((ImageView) payloadContainer.findViewById(R.id.weatherIV)).setVisibility(View.GONE);
            } else {
                ((ImageView) payloadContainer.findViewById(R.id.weatherIV)).setImageBitmap(BitmapFactory.decodeResource(payloadContainer.getContext().getResources(), icon));
            }

            payloadContainer.findViewById(R.id.doneButton1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues cv = new ContentValues();
                    cv.put("response", "{}");
                    ConnectionsDBHelper.getHelper(payloadContainer.getContext()).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_WEATHER"});
                    payloadContainer.setVisibility(View.GONE);
                    Utils.populateMainDataSet(v.getContext());
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void renderWeather(final HomeAdapter.ViewHolder2 viewHolder2, JSONObject weather) {
        try {
            viewHolder2.payloadContainer.setVisibility(View.VISIBLE);
            viewHolder2.cityTV.setText(weather.getString("city"));
            viewHolder2.descTV.setText(weather.getString("weather"));

            if (Utils.getBooleanFromSharedPrefs("use_metric_units_weather", false)) {
                viewHolder2.currentTempTV.setText(Html.fromHtml(weather.getString("temp_c")+"&deg;"));
                viewHolder2.hiloTV.setText(weather.getString("low_c")+" / "+weather.getString("high_c"));
            } else {
                viewHolder2.currentTempTV.setText(Html.fromHtml(weather.getString("temp_f")+"&deg;"));
                viewHolder2.hiloTV.setText(weather.getString("low_f")+" / "+weather.getString("high_f"));
            }

            int icon = (Utils.mapIconToBmp(weather.getString("icon"), weather));
            if (icon == -1) {
                viewHolder2.weatherIV.setVisibility(View.GONE);
            } else {
                viewHolder2.weatherIV.setImageBitmap(BitmapFactory.decodeResource(viewHolder2.payloadContainer.getContext().getResources(), icon));
            }

            viewHolder2.doneButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ContentValues cv = new ContentValues();
                    cv.put("response", "{}");
                    ConnectionsDBHelper.getHelper(viewHolder2.doneButton1.getContext()).getWritableDatabase().update("connections", cv, "token=?", new String[]{"CHANNEL_WEATHER"});
                    viewHolder2.payloadContainer.setVisibility(View.GONE);
                    Utils.populateMainDataSet(v.getContext());
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    public static int mapIconToBmp(String icon, JSONObject weather) {
        try {
            Calendar sunrise = Calendar.getInstance();
            sunrise.set(Calendar.HOUR_OF_DAY, weather.getInt("sunrise_hour"));
            sunrise.set(Calendar.MINUTE, weather.getInt("sunrise_minute"));

            Calendar sunset = Calendar.getInstance();
            sunset.set(Calendar.HOUR_OF_DAY, weather.getInt("sunset_hour"));
            sunset.set(Calendar.MINUTE, weather.getInt("sunset_minute"));

            Calendar rightNow = Calendar.getInstance();

            switch (icon) {
                case "chanceflurries":
                case "flurries":
                    return R.drawable.ic_flurries;
                case "chancerain":
                case "rain":
                    return R.drawable.ic_rainy;
                case "chancesleet":
                case "sleet":
                case "chancesnow":
                case "snow":
                    return R.drawable.ic_snow;
                case "chancestorms":
                case "tstorms":
                    return R.drawable.ic_thunderstorm;
                case "clear":
                case "sunny":
                    if (rightNow.after(sunrise) && rightNow.before(sunset))
                        return R.drawable.ic_day_clear;
                    else
                        return R.drawable.ic_night_clear;
                case "cloudy":
                case "fog":
                case "hazy":
                case "overcast":
                    return R.drawable.ic_cloudy_foggy_hazy;
                case "mostlycloudy":
                case "mostlysunny":
                case "partlycloudy":
                case "partlysunny":
                    if (rightNow.after(sunrise) && rightNow.before(sunset))
                        return R.drawable.ic_day_cloudy;
                    else
                        return R.drawable.ic_night_cloudy;
                case "unknown":
                default:
                    return -1;
            }
        } catch (Exception e) {e.printStackTrace(); return -1;}
    }

    public static void sendSup(final Context context, final String token, final LinearLayout itemView) {
        if (!Utils.isNetworkAvailable(context)) {
            Utils.showToast(context, "No internet connection");
            return;
        }
        try {
            JSONObject data = new JSONObject();
            data.put("type", "sup");
            data.put("sender", Utils.getStringFromSharedPrefs("self_token", ""));
            itemView.findViewById(R.id.microChatIV).setVisibility(View.INVISIBLE);
            itemView.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);

            String url = "http://black-abode-2709.appspot.com/send_message?to_user=" + URLEncoder.encode(token) + "&data=" + URLEncoder.encode(data.toString());
            Utils.volleyStringCall(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    Utils.showToast(context, "Sup sent");
                    itemView.findViewById(R.id.microChatIV).setVisibility(View.VISIBLE);
                    itemView.findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
                    try {
                        SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
                        Cursor res = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                        res.moveToFirst();
                        final String type;
                        final String response;
                        if (!res.getString(res.getColumnIndexOrThrow("response")).equals("{}")) {
                            type = new JSONObject(res.getString(res.getColumnIndexOrThrow("response"))).getString("type");
                            response = new JSONObject(res.getString(res.getColumnIndexOrThrow("response"))).getString("response");
                        } else {
                            type = response = null;
                        }
                        Utils.deleteResponse(context, token, type, response);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendGroupSup(final Context context, final String token, final LinearLayout itemView) {
        if (!Utils.isNetworkAvailable(context)) {
            Utils.showToast(context, "No internet connection");
            return;
        }
        try {
            itemView.findViewById(R.id.microChatIV).setVisibility(View.INVISIBLE);
            itemView.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
            String selfUsername = Utils.getStringFromSharedPrefs("self_username", "");
            String url = "http://black-abode-2709.appspot.com/send_group_sup?to_group=" + URLEncoder.encode(token) + "&from=" + URLEncoder.encode(selfUsername);
            Utils.volleyStringCall(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    Utils.showToast(context, "Sup sent");
                    itemView.findViewById(R.id.microChatIV).setVisibility(View.VISIBLE);
                    itemView.findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
                    try {
                        SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
                        Cursor res = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
                        res.moveToFirst();;
                        final JSONArray responses;
                        if (!res.getString(res.getColumnIndexOrThrow("response")).equals("{}")) {
                            responses = new JSONObject(res.getString(res.getColumnIndexOrThrow("response"))).getJSONArray("responseList");
                        } else {
                            responses = new JSONArray(); // Length 0, loop in deleteGroupResponse won't run.
                        }
                        Utils.deleteGroupResponse(context, token, responses, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getScreenHeight(Context context) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int getScreenWidth(Context context) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }
    public static void dbDelete(Context context, String token) {
        SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
        db.delete("connections", "token=?", new String[]{token});
    }

    public static Bitmap overlayBitmapToCenter(Bitmap bitmap1, Bitmap bitmap2) {
        int bitmap1Width = bitmap1.getWidth();
        int bitmap1Height = bitmap1.getHeight();
        int bitmap2Width = bitmap2.getWidth();
        int bitmap2Height = bitmap2.getHeight();

        float marginLeft = (float) (bitmap1Width * 0.5 - bitmap2Width * 0.5);
        float marginTop = (float) (bitmap1Height * 0.5 - bitmap2Height * 0.5);

        Bitmap overlayBitmap = Bitmap.createBitmap(bitmap1Width, bitmap1Height, bitmap1.getConfig());
        Canvas canvas = new Canvas(overlayBitmap);
        canvas.drawBitmap(bitmap1, new Matrix(), null);
        canvas.drawBitmap(bitmap2, marginLeft, marginTop, null);
        return overlayBitmap;
    }

    public static void snapshot(File video, File snapshot, Context context) {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

            mediaMetadataRetriever.setDataSource(video.getAbsolutePath());
            Bitmap snap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);
            // Bitmap bmp2 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_play_circle_filled_white_48dp);
            // Bitmap fin = Utils.overlayBitmapToCenter(snap, bmp2);
            // fin.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(snapshot));
            snap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(snapshot));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void downloadFromFCS(Context context, final String filename, final VolleyCallback callback) {
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReferenceFromUrl("gs://supapp-1270.appspot.com");

        // Create a reference to the file to delete
        StorageReference fileRef = storageRef.child(filename);

        File localFile = new File(context.getFilesDir(), filename);
        fileRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                callback.onResponse(null);
            }
        });

        fileRef.getFile(localFile).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    public static String prepareStaticMapUrl(Context context, double lat, double lon) {
        String size = Utils.getScreenWidth(context) + "x" + ((int) (Utils.getScreenWidth(context) * (9.0 / 16.0)));
        String url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon + "&zoom=17&size=" + size + "&maptype=roadmap" +
                "&markers=color:blue%7C" + lat + "," + lon +
                "&key=AIzaSyBuAQyC6wXWQqp_XPVl-0MTJIAXapqSTek";
        return url;
    }

    public static void deleteResponse(final Context context, final String token, final String type, final String response) {
        try {
            if (type != null && response != null) {
                // First delete any associated media
                if (type.startsWith("image") || type.startsWith("video") || type.startsWith("audio")) {
                    new File(context.getFilesDir(), response).delete();
                    // If it's a video, there's an additional snapshot image file to delete
                    if (type.startsWith("video")) {
                        new File(context.getFilesDir(), "snapshot_" + response.replace(".mp4", ".png")).delete();
                    }
                } else if (type.startsWith("location")) {
                    new File(context.getFilesDir(), response + ".png").delete();
                }
            }

            // Now delete response from db
            ContentValues cv = new ContentValues();
            cv.put("response", "{}");

            SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
            dbWritable.update("connections", cv, "token=?", new String[]{token});

            // Delete notification
            // Delete notification
            if (CustomFirebaseMessagingService.responseNotificationsBuilders.containsKey(token)) {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(CustomFirebaseMessagingService.responseNotificationsIds.get(token));

                CustomFirebaseMessagingService.responseNotificationsBuilders.remove(token);
                CustomFirebaseMessagingService.responseNotificationsIds.remove(token);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        populateMainDataSet(context);
        ListView lv = ((ListView)((HomeActivity)context).findViewById(R.id.mainLV));
        if (lv != null )
            ((BaseAdapter)lv.getAdapter()).notifyDataSetChanged();
    }

    public static void deleteGroupResponse(final Context context, final String token, final JSONArray responses, final boolean afterSup) {
        // The third param will be true if deleting responses right after this user sent a sup, false if just deleting sups otherwise
        // It makes a difference because if true, we not only delete responses but set the "sup_sender" to this user's token
        try {
            for (int i = 0; i < responses.length(); i++) {
                String type = new JSONObject(responses.getString(i)).getString("type");
                String response = new JSONObject(responses.getString(i)).getString("response");
                if (type != null && response != null) {
                    // First delete any associated media
                    if (type.startsWith("image") || type.startsWith("video") || type.startsWith("audio")) {
                        new File(context.getFilesDir(), response).delete();
                        // If it's a video, there's an additional snapshot image file to delete
                        if (type.startsWith("video")) {
                            new File(context.getFilesDir(), "snapshot_" + response.replace(".mp4", ".png")).delete();
                        }
                    } else if (type.startsWith("location")) {
                        new File(context.getFilesDir(), response + ".png").delete();
                    }
                }
            }

            if (afterSup) {
                // Now delete response from db
                ContentValues cv = new ContentValues();
                JSONObject response = new JSONObject();
                response.put("sup_sender", Utils.getStringFromSharedPrefs("self_username", ""));
                response.put("responseList", new JSONArray());
                cv.put("response", response.toString());

                SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
                dbWritable.update("connections", cv, "token=?", new String[]{token});
            } else {
                // Now delete response from db
                ContentValues cv = new ContentValues();
                SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
                Cursor results = dbR.rawQuery("SELECT * FROM Connections WHERE token=?", new String[]{token});
                results.moveToFirst();
                JSONObject response = new JSONObject(results.getString(results.getColumnIndexOrThrow("response")));
                if(response.getString("sup_sender").equals(Utils.getStringFromSharedPrefs("self_username", ""))) {
                    response.put("sup_sender", ""); // Done with own sup
                }
                response.put("responseList", new JSONArray());
                cv.put("response", response.toString());

                SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
                dbWritable.update("connections", cv, "token=?", new String[]{token});
            }

            // Delete notification
            if (CustomFirebaseMessagingService.responseNotificationsBuilders.containsKey(token)) {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(CustomFirebaseMessagingService.responseNotificationsIds.get(token));

                CustomFirebaseMessagingService.responseNotificationsBuilders.remove(token);
                CustomFirebaseMessagingService.responseNotificationsIds.remove(token);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        populateMainDataSet(context);
        ListView lv = ((ListView)((HomeActivity)context).findViewById(R.id.mainLV));
        if (lv != null )
            ((BaseAdapter)lv.getAdapter()).notifyDataSetChanged();
    }

    public static void deleteSups(final Context context, final String token) {
        ContentValues cv = new ContentValues();
        cv.put("sup_count", 0);

        SQLiteDatabase dbWritable = ConnectionsDBHelper.getHelper(context).getWritableDatabase();
        dbWritable.update("connections", cv, "token=?", new String[]{token});
        dbWritable.close();

        if (CustomFirebaseMessagingService.supNotificationsBuilders.containsKey(token)) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(CustomFirebaseMessagingService.supNotificationsIds.get(token));

            CustomFirebaseMessagingService.supNotificationsBuilders.remove(token);
            CustomFirebaseMessagingService.supNotificationsIds.remove(token);
        }
        populateMainDataSet(context);
    }

    public static void renderTextResponse(TextView tv, JSONObject response) {
        try {
            Typeface font = Typeface.createFromAsset(tv.getContext().getAssets(), "RobotoCondensed-Light.ttf");
            tv.setTypeface(font);
            tv.setText(response.getString("response"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void renderImageResponse(final ImageView iv, final JSONObject response, boolean async) {
        try {
            ((View)iv.getParent()).setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (Utils.getScreenWidth(iv.getContext()) * (9.0 / 16.0))));

            Glide.with(iv.getContext())
                    .load(new File(iv.getContext().getFilesDir(), response.getString("response")).getAbsolutePath())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(Utils.getScreenWidth(iv.getContext()), ((int) (Utils.getScreenWidth(iv.getContext()) * (9.0 / 16.0))))
                    .into(iv);

            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        iv.getContext().startActivity(new Intent(iv.getContext(), ImageViewer.class).putExtra("path", new File(iv.getContext().getFilesDir(), response.getString("response")).getAbsolutePath()));
                    } catch (Exception e) {e.printStackTrace();}
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void renderVideoResponse(final ImageView iv, final JSONObject response, boolean async, ImageView playButton) {
        try {
            ((View)iv.getParent()).setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (Utils.getScreenWidth(iv.getContext()) * (9.0 / 16.0))));
            View.OnClickListener ocl = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        iv.getContext().startActivity(new Intent(iv.getContext(), VideoPlayer.class).putExtra("filename", response.getString("response")));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            iv.setOnClickListener(ocl);
            playButton.setOnClickListener(ocl);


            Glide.with(iv.getContext())
                    .load(new File(iv.getContext().getFilesDir(), "snapshot_" + response.getString("response").replace(".mp4", ".png")).getAbsolutePath())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(Utils.getScreenWidth(iv.getContext()), ((int) (Utils.getScreenWidth(iv.getContext()) * (9.0 / 16.0))))
                    .into(iv);


            playButton.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void renderLocationResponse(final ImageView iv, final JSONObject response, boolean async) {
        try {
            final double lat = Double.valueOf(response.getString("response").split("_")[0]);
            final double lon = Double.valueOf(response.getString("response").split("_")[1]);
            Log.d("File exists", new File(iv.getContext().getFilesDir(), response.getString("response") + ".png").getAbsolutePath()+"_"+new File(iv.getContext().getFilesDir(), response.getString("response") + ".png").exists()+"");
            Glide.with(iv.getContext())
                    .load(new File(iv.getContext().getFilesDir(), response.getString("response") + ".png").getAbsolutePath())
                    .centerCrop()
                    .override(Utils.getScreenWidth(iv.getContext()), ((int) (Utils.getScreenWidth(iv.getContext()) * (9.0 / 16.0))))
                    .into(iv);
            ((View)iv.getParent()).setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (Utils.getScreenWidth(iv.getContext()) * (9.0 / 16.0))));
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // iv.getContext().startActivity(new Intent(iv.getContext(), MapsActivity.class).putExtra("lat", lat).putExtra("lon", lon));
                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lat, lon);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    iv.getContext().startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void renderAudioResponse(final AudioWaveView wave, final View playButton, final View cancelButton, final JSONObject response) {

        // a way to keep track of progress
        class MediaObserver implements Runnable {
            private AtomicBoolean stop = new AtomicBoolean(false);

            public void stop() {
                stop.set(true);
            }
            public void start() {
                stop.set(false);
            }

            @Override
            public void run() {
                while (!stop.get()) {
                    try {
                        if(mediaPlayer == null) break;
                        wave.setProgress(100*((float)mediaPlayer.getCurrentPosition())/(float)mediaPlayer.getDuration());
                        Thread.sleep(100);
                    } catch ( Exception e) {e.printStackTrace();}
                }
            }
        }

        final MediaObserver observer = new MediaObserver();
        try {
            byte[] data = Utils.fileToBytes(new File(playButton.getContext().getApplicationContext().getFilesDir(), response.getString("response")));
            wave.setRawData(data, new OnSamplingListener() {
                @Override
                public void onComplete() {
                    // After the audio has been downsampled, set listener for the play and cancel buttons.
                    playButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // firstly make the play button go, and the cancel button appear
                            ((View)playButton.getParent()).setVisibility(View.GONE);
                            ((View)cancelButton.getParent()).setVisibility(View.VISIBLE);

                            // set progress to 0
                            wave.setProgress(0f);

                            // now play the clip
                            mediaPlayer = new MediaPlayer();
                            try {
                                mediaPlayer.setDataSource(new File(playButton.getContext().getFilesDir(), response.getString("response")).getAbsolutePath());
                                mediaPlayer.prepare();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
                                @Override
                                public void onCompletion(MediaPlayer mPlayer) {
                                    observer.stop();
                                    mediaPlayer = null;
                                    wave.setProgress(100*((float)mPlayer.getCurrentPosition())/(float)mPlayer.getDuration());
                                    ((View)cancelButton.getParent()).setVisibility(View.GONE);
                                    ((View)playButton.getParent()).setVisibility(View.VISIBLE);
                                }
                            });

                            mediaPlayer.start();
                            observer.start();
                            new Thread(observer).start();
                        }
                    });

                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // firstly make the cancel button go, and the play button appear
                            ((View)cancelButton.getParent()).setVisibility(View.GONE);
                            ((View)playButton.getParent()).setVisibility(View.VISIBLE);

                            // now stop playing the clip
                            wave.setProgress(0);
                            if(mediaPlayer != null){
                                mediaPlayer.stop();
                                mediaPlayer.release();
                                observer.stop();
                                mediaPlayer = null;
                            }
                        }
                    });

                    wave.setOnProgressListener(new rm.com.audiowave.OnProgressListener() {
                        @Override
                        public void onStartTracking(float v) {

                        }

                        @Override
                        public void onStopTracking(float v) {

                        }

                        @Override
                        public void onProgressChanged(float v, boolean b) {
                            if(b && mediaPlayer!=null) {
                                mediaPlayer.seekTo((int) (mediaPlayer.getDuration() * (v/100.0)));
                            } else if(b) {
                                mediaPlayer = new MediaPlayer();
                                try {
                                    mediaPlayer.setDataSource(new File(playButton.getContext().getFilesDir(), response.getString("response")).getAbsolutePath());
                                    mediaPlayer.prepare();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
                                    @Override
                                    public void onCompletion(MediaPlayer mPlayer) {
                                        observer.stop();
                                        mediaPlayer = null;
                                        wave.setProgress(100*((float)mPlayer.getCurrentPosition())/(float)mPlayer.getDuration());
                                        ((View)cancelButton.getParent()).setVisibility(View.GONE);
                                        ((View)playButton.getParent()).setVisibility(View.VISIBLE);
                                    }
                                });

                                mediaPlayer.start();
                                mediaPlayer.seekTo((int) (mediaPlayer.getDuration() * (v/100.0)));
                                observer.start();
                                new Thread(observer).start();
                            }
                        }
                    });
                }
            });
        } catch ( Exception e) {e.printStackTrace();}
    }

    public static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    public static void deleteContact(final Context context, final String token, final BaseAdapter adapter, final ListView view) {
        Utils.confirmDialog(context, "Remove?", new VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                if (((boolean) result)) {
                    // They clicked yes
                    if (!Utils.isNetworkAvailable(context)) {
                        Utils.showToast(context, "No internet connection");
                        return;
                    }

                    if (token.equals("CHANNEL_STOCKS")) {
                        Utils.putStringInSharedPrefs("stock_tickers", "[]");
                    }

                    try {
                        final ProgressDialog pd = new ProgressDialog(context);
                        pd.setCancelable(false);
                        pd.setCanceledOnTouchOutside(false);
                        pd.setMessage("Please wait...");
                        pd.setIndeterminate(true);
                        pd.show();

                        // Remove from self_following
                        JSONArray following = new JSONArray(Utils.getStringFromSharedPrefs("self_following", "[]"));
                        following = Utils.deleteFromJSONArray(following, token);
                        Utils.putStringInSharedPrefs("self_following", following.toString());

                        // Delete profile picture from internal storage
                        new File(context.getFilesDir(), token + "_photo.png").delete();

                        // Delete record from connections
                        Utils.dbDelete(context, token);

                        // FCM unsubscribe
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(token);

                        // Tell server
                        Utils.volleyStringCall(context, "http://black-abode-2709.appspot.com/update_profile?token=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", "")) + "&following=" + URLEncoder.encode(following.toString()), new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                Utils.populateMainDataSet(context);
                                pd.dismiss();
                                if (Utils.MainDataSet.names.size() > 0) {
                                    adapter.notifyDataSetChanged();
                                } else {
                                    EmptyAdapter a = new EmptyAdapter(context);
                                    a.title = "You don't have any contacts yet!";
                                    a.message = "Get started by adding your friends. Click the button down there to find them!";
                                    view.setAdapter(a);
                                    if (Utils.MainDataSet.names.isEmpty()) {
                                        view.setOnItemClickListener(null);
                                        view.setOnItemLongClickListener(null);
                                    }
                                }
                                Utils.showToast(context, "Removed");
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static void animateIn(final View pre, final View post, View container) {
        // Prepare the View for the animation
        pre.setVisibility(View.VISIBLE);
        pre.animate()
                .translationXBy(-pre.getWidth())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        pre.setVisibility(View.GONE);


                        // Prepare the View for the animation
                        post.setVisibility(View.VISIBLE);
                        post.setAlpha(0.0f);

                        // Start the animation
                        post.animate()
                                .alpha(1.0f)
                                .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        animation.removeAllListeners();
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });

                        animation.removeAllListeners();
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
    }

    public static void animateOut(final View pre, final View post, View container) {
        // Prepare the View for the animation
        pre.setVisibility(View.VISIBLE);
        pre.setAlpha(1.0f);

        // Start the animation

        pre.animate()
                .alpha(0.0f)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        pre.setVisibility(View.GONE);


                        // Prepare the View for the animation
                        post.setVisibility(View.VISIBLE);

                        // Start the animation
                        post.animate()
                                .translationXBy(post.getWidth())
                                .setListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        animation.removeAllListeners();
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                });

                        animation.removeAllListeners();
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
    }

    private static View layoutForAttach;
    // The following function is called obtain"Reponse"FromUser for legacy reasons, but is also used by smalltalk and broadcasts
    public static void obtainResponseFromUser(final Activity context, final String usersName, final String purpose, final VolleyCallback callback, final View lfa){
        View text_response, image_response, video_response, location_response, location_response2, cancel_action, audio_response;
        layoutForAttach = lfa;
        ((HomeActivity) context).purpose = purpose;
        ((HomeActivity) context).usersName = usersName;
        final Dialog mBottomSheetDialog;
        if(purpose.equals("Broadcast")) {
            View view = context.getLayoutInflater().inflate(R.layout.bottom_sheet_respond, null);
            layoutForAttach = view;
            text_response =  view.findViewById(R.id.text_response);
            image_response =  view.findViewById(R.id.image_response);
            video_response =  view.findViewById(R.id.video_response);
            location_response =  view.findViewById(R.id.location_response);
            location_response2 =  view.findViewById(R.id.location_response2);
            audio_response =  view.findViewById(R.id.audio_response);
            cancel_action = null;

            ((HomeActivity) context).saveMedia = true;
            ((HomeActivity) context).forBroadcast = true;

            ((TextView) text_response.findViewById(R.id.textTV)).setText(purpose + " a text");
            ((TextView) image_response.findViewById(R.id.imageTV)).setText(purpose + " an image or video");
            ((TextView) video_response.findViewById(R.id.videoTV)).setText(purpose + " a video");
            ((TextView) location_response2.findViewById(R.id.locationTV2)).setText(purpose + " your current location");
            ((TextView) location_response.findViewById(R.id.locationTV)).setText(purpose + " a location");
            ((TextView) audio_response.findViewById(R.id.audioTV)).setText(purpose + " an audio clip");


            mBottomSheetDialog = new Dialog(context,
                    R.style.MaterialDialogSheet);
            mBottomSheetDialog.setContentView(view);
            mBottomSheetDialog.setCancelable(true);
            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
            mBottomSheetDialog.show();
        } else {
            mBottomSheetDialog = null;
            text_response = layoutForAttach.findViewById(R.id.text_response);
            image_response = layoutForAttach.findViewById(R.id.image_response);
            video_response = layoutForAttach.findViewById(R.id.video_response);
            location_response = layoutForAttach.findViewById(R.id.location_response);
            location_response2 = layoutForAttach.findViewById(R.id.location_response2);
            cancel_action = layoutForAttach.findViewById(R.id.cancel_action);
            audio_response = layoutForAttach.findViewById(R.id.audio_response);

            ((HomeActivity) context).saveMedia = true;
            ((HomeActivity) context).forBroadcast = false;
            ((View)text_response.getParent()).setVisibility(View.GONE); // makes the containing framelayout disappear

            // Now slide-animate in
            View pre = layoutForAttach.findViewById(R.id.input);
            View post = layoutForAttach.findViewById(R.id.attachLayout);
            View container = layoutForAttach.findViewById(R.id.bottomContainer);
            Utils.animateIn(pre, post, container);
        }

        if(cancel_action != null) {
            cancel_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                // Now slide-animate out
                View post = layoutForAttach.findViewById(R.id.input);
                View pre = layoutForAttach.findViewById(R.id.attachLayout);
                View container = layoutForAttach.findViewById(R.id.bottomContainer);
                Utils.animateOut(pre, post, container);
                }
            });
        }

        text_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                } else {
                    // Now slide-animate out
                    View post = layoutForAttach.findViewById(R.id.input);
                    View pre = layoutForAttach.findViewById(R.id.attachLayout);
                    View container = layoutForAttach.findViewById(R.id.bottomContainer);
                    Utils.animateOut(pre, post, container);
                }
                obtainTextResponse(context, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        JSONObject broadcast = Utils.packageBroadcast("text_broadcast", result.toString(), "");
                        if (purpose.equals("Broadcast")) {
                            callback.onResponse(broadcast);
                        } else {
                            callback.onResponse(Utils.packageResponse("text_response", result.toString()));
                        }
                    }
                }, purpose);
            }
        });

        image_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                } else {
                    // Now slide-animate out
                    View post = layoutForAttach.findViewById(R.id.input);
                    View pre = layoutForAttach.findViewById(R.id.attachLayout);
                    View container = layoutForAttach.findViewById(R.id.bottomContainer);
                    Utils.animateOut(pre, post, container);
                }

                Utils.activity_result_callback = callback;
                if(!(ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(context,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions((Activity)context,
                            new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            Constants.MY_PERMISSIONS_REQUEST_IMAGE_RESPONSE);
                } else {
                    new SandriosCamera((Activity) context, Constants.REQUEST_CAPTURE_MEDIA)
                            .setShowPicker(true)
                            .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                            .enableImageCropping(false) // Default is false.
                            .launchCamera();

                    // The result will be delivered in the onActivityResult() method of HomeActivity
                }
            }
        });
        video_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                } else {
                    // Now slide-animate out
                    View post = layoutForAttach.findViewById(R.id.input);
                    View pre = layoutForAttach.findViewById(R.id.attachLayout);
                    View container = layoutForAttach.findViewById(R.id.bottomContainer);
                    Utils.animateOut(pre, post, container);
                }

                Utils.activity_result_callback = callback;
                if(!(ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(context,
                            new String[]{android.Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                            Constants.MY_PERMISSIONS_REQUEST_VIDEO_RESPONSE);
                } else {
                    /*new MaterialCamera( context)
                            .saveDir(context.getFilesDir())
                            .showPortraitWarning(false)
                            .countdownSeconds(10f)
                            .start(Constants.REQUEST_CAMERA_VIDEO);*/
                    // The result will be delivered in the onActivityResult() method of MainActivity
                }
            }
        });
        location_response2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                } else {
                    // Now slide-animate out
                    View post = layoutForAttach.findViewById(R.id.input);
                    View pre = layoutForAttach.findViewById(R.id.attachLayout);
                    View container = layoutForAttach.findViewById(R.id.bottomContainer);
                    Utils.animateOut(pre, post, container);
                }

                if (!(ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(context,
                                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                    Utils.activity_result_callback = callback;

                    ActivityCompat.requestPermissions(context,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE);
                } else {
                    Location location = getLastKnownLocation(context);
                        if(location == null) {
                            Utils.showToast(context, "Location Unavailable");
                        } else {
                            final double lat = location.getLatitude();
                            final double lon = location.getLongitude();

                        final ProgressDialog pd = new ProgressDialog(context);
                        pd.setCancelable(false);
                        pd.setCanceledOnTouchOutside(false);
                        pd.setIndeterminate(true);
                        pd.setMessage("Downloading map...");
                        pd.show();
                        Utils.downloadImageAndSave((Activity) context, Utils.prepareStaticMapUrl(context, lat, lon), lat + "_" + lon + ".png", new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                pd.dismiss();
                                if (purpose.startsWith("Attach") && ((HomeActivity)context).refreshCallback != null) {
                                    ((HomeActivity)context).refreshCallback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                                }
                                JSONObject broadcast = Utils.packageBroadcast("location_broadcast", "", lat + "_" + lon);
                                if (purpose.startsWith("Broadcast") && ((HomeActivity)context).refreshCallback != null) {
                                    ((HomeActivity)context).refreshCallback.onResponse(broadcast);
                                }
                                try {
                                    if (purpose.equals("Broadcast")) {
                                        ((HomeActivity) context).getCaption(broadcast);
                                        callback.onResponse(broadcast);
                                    } else {
                                        callback.onResponse(Utils.packageResponse("location_response", lat + "_" + lon));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }

            }
        });
        location_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                } else {
                    // Now slide-animate out
                    View post = layoutForAttach.findViewById(R.id.input);
                    View pre = layoutForAttach.findViewById(R.id.attachLayout);
                    View container = layoutForAttach.findViewById(R.id.bottomContainer);
                    Utils.animateOut(pre, post, container);
                }

                if (!(ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(context,
                                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                    Utils.activity_result_callback = callback;

                    ActivityCompat.requestPermissions(context,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            Constants.MY_PERMISSIONS_REQUEST_LOCATION_RESPONSE2);
                } else {
                    // Pick location
                    try {
                        Utils.activity_result_callback = callback;
                        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                        context.startActivityForResult(builder.build(context), Constants.PLACE_PICKER_REQUEST);
                    } catch ( Exception e){e.printStackTrace();}
                }
            }
        });

        audio_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Slide-in the audio portion
                View pre = layoutForAttach.findViewById(R.id.attachLayout);
                View post = layoutForAttach.findViewById(R.id.audioRecordLayout);

                View container = layoutForAttach.findViewById(R.id.bottomContainer);
                Utils.animateIn(pre, post, container);

                obtainAudioResponse(context, usersName, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        callback.onResponse(result);
                    }
                }, layoutForAttach, mBottomSheetDialog, purpose);
            }
        });

    }

    public static void obtainAudioResponse(final Activity context,final String usersName, final VolleyCallback callback, final View layoutForAttach, final Dialog mBottomSheetDialog, final String purpose) {
        View cancel_action = layoutForAttach.findViewById(R.id.cancel_action2);
        cancel_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Now slide-animate out
                View post = layoutForAttach.findViewById(R.id.attachLayout);
                View pre = layoutForAttach.findViewById(R.id.audioRecordLayout);
                View container = layoutForAttach.findViewById(R.id.bottomContainer);
                Utils.animateOut(pre, post, container);
            }
        });

        View record_action = layoutForAttach.findViewById(R.id.record_action);
        record_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.activity_result_callback = callback;
                if (context instanceof HomeActivity) {
                    ((HomeActivity) context).layoutForAttach = layoutForAttach;
                    ((HomeActivity) context).purpose = purpose;
                } else {
                    ((NotificationActionActivity) context).layoutForAttach = (LinearLayout) layoutForAttach;
                    ((NotificationActionActivity) context).purpose = purpose;
                }
                if(!(ContextCompat.checkSelfPermission(context,
                        android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(context,
                            new String[]{Manifest.permission.RECORD_AUDIO},
                            Constants.MY_PERMISSIONS_REQUEST_AUDIO_RESPONSE);
                } else {
                    recordAudio(layoutForAttach, usersName, context, purpose, mBottomSheetDialog);
                }
            }
        });
    }

    private static MediaRecorder mediaRecorder;
    private static MediaPlayer mediaPlayer;
    static int time = 0;
    public static void recordAudio(final View layoutForAttach, final String usersName, final Activity context, final String purpose, final Dialog dialog) {
        try {
            View cancel_action = layoutForAttach.findViewById(R.id.cancel_action2);
            final View send_action = layoutForAttach.findViewById(R.id.send_action);
            final View record_action = layoutForAttach.findViewById(R.id.record_action);

            ((View)record_action.getParent()).setVisibility(View.GONE);
            ((View)send_action.getParent()).setVisibility(View.VISIBLE);

            final String fileName;
            if (context instanceof HomeActivity && ((HomeActivity) context).forBroadcast) {
                fileName = "broadcast_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis() % 100000000);
            } else {
                fileName = "response_" + Utils.getStringFromSharedPrefs("self_token", "") + "_" + String.valueOf(System.currentTimeMillis() % 100000000);
            }

            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
            mediaRecorder.setOutputFile(new File(context.getFilesDir(), fileName + ".3gpp").getAbsolutePath());

            mediaRecorder.prepare();
            mediaRecorder.start();

            final ImageView blink = (ImageView) layoutForAttach.findViewById(R.id.blinkIV);
            blink.setVisibility(View.VISIBLE);
            blink.setAlpha(1f);
            final Animation animation = new AlphaAnimation(1, 0);
            animation.setDuration(1000);
            animation.setInterpolator(new LinearInterpolator());
            animation.setRepeatCount(Animation.INFINITE);
            animation.setRepeatMode(Animation.REVERSE);
            blink.startAnimation(animation);

            final Handler mHandler = new Handler();
            final Runnable x = new Runnable() {
                @Override
                public void run() {
                    String dispTime = String.format("%02d:%02d",
                            TimeUnit.MILLISECONDS.toMinutes(time*1000),
                            TimeUnit.MILLISECONDS.toSeconds(time*1000) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time*1000))
                    );

                    ((TextView) layoutForAttach.findViewById(R.id.timeTV)).setText(dispTime);
                    if (time < 3600) {
                        time++;
                        mHandler.postDelayed(this, 1000);
                    } else {
                        blink.clearAnimation();
                        blink.setVisibility(View.INVISIBLE);
                        mHandler.removeCallbacks(this);
                        try {
                            mediaRecorder.stop();
                        } catch (Exception e) {e.printStackTrace();}
                        sendAudio(fileName, usersName, context, record_action, send_action, layoutForAttach, purpose, dialog);
                    }
                }
            };
            x.run();

            send_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        mediaRecorder.stop();
                    } catch (Exception e) {e.printStackTrace();}
                    blink.setVisibility(View.INVISIBLE);
                    blink.clearAnimation();
                    mHandler.removeCallbacks(x);
                    sendAudio(fileName, usersName, context, record_action, send_action, layoutForAttach, purpose, dialog);
                }
            });

            cancel_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Now slide-animate out
                    ((View)record_action.getParent()).setVisibility(View.VISIBLE);
                    ((View)send_action.getParent()).setVisibility(View.GONE);
                    Utils.showToast(context, "Recording cancelled");
                    try {
                        mediaRecorder.stop();
                    } catch (Exception e) {e.printStackTrace();}
                    mHandler.removeCallbacks(x);
                    new File(context.getFilesDir(), fileName + ".3gpp").delete();
                    ((TextView) layoutForAttach.findViewById(R.id.timeTV)).setText("00:00");
                    time = 0;
                    blink.setVisibility(View.INVISIBLE);
                    blink.clearAnimation();

                    View post = layoutForAttach.findViewById(R.id.attachLayout);
                    View pre = layoutForAttach.findViewById(R.id.audioRecordLayout);
                    View container = layoutForAttach.findViewById(R.id.bottomContainer);
                    Utils.animateOut(pre, post, container);
                }
            });

        } catch ( Exception e) {e.printStackTrace();}
    }

    public static int requestingPosition = -1;
    public static boolean inNotification = false;
    public static HomeAdapter.ViewHolder holder = null;
    public static HomeAdapter.ViewHolder5 groupHolder = null;
    private static void sendAudio(final String fileName, final String usersName, final Context context, View record_action, View send_action, View layoutForAttach, String purpose, final Dialog dialog) {
        ((View)record_action.getParent()).setVisibility(View.VISIBLE);
        ((View)send_action.getParent()).setVisibility(View.GONE);
        ((TextView) layoutForAttach.findViewById(R.id.timeTV)).setText("00:00");
        time = 0;
        if (purpose.startsWith("Attach") && ((HomeActivity)context).refreshCallback != null) {
            ((HomeActivity)context).refreshCallback.onResponse(Utils.packageResponse("audio_response", fileName+".3gpp"));
        }
        final JSONObject broadcast = Utils.packageBroadcast("audio_broadcast", "", fileName+".3gpp");

        if (inNotification) {
            y.dismiss();
            Utils.deleteSups(context, Utils.MainDataSet.tokens.get(position));
            Utils.populateMainDataSet(context);
            NotificationManager mNotifyMgr =
                    (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.cancel(notificationId);
            ((NotificationActionActivity)context).refreshUI();
            ((Activity)context).finish();
            y = null; position = -1; notificationId = -1;
        }

        try {

            Utils.uploadToFCS(context, "Sending audio to " + usersName, fileName+".3gpp", fileToBytes(new File(context.getFilesDir(), fileName+".3gpp")), new VolleyCallback() {
                @Override
                public void onResponse(Object result) {
                    if (!inNotification && context instanceof HomeActivity && ((HomeActivity) context).forBroadcast) {
                        Utils.activity_result_callback.onResponse(broadcast);
                    } else {
                        Utils.activity_result_callback.onResponse(Utils.packageResponse("audio_response", fileName+".3gpp"));
                    }
                }
            });
        } catch (Exception e ){e.printStackTrace();}

        if (!inNotification) {
            View post = layoutForAttach.findViewById(R.id.attachLayout);
            View pre = layoutForAttach.findViewById(R.id.audioRecordLayout);
            View container = layoutForAttach.findViewById(R.id.bottomContainer);
            Utils.animateOut(pre, post, container);

            if (purpose.equals("Respond with") && !inNotification) {
                if (holder != null)
                    HomeAdapter.animateOut(context, holder, requestingPosition);
                else if (groupHolder != null)
                    HomeAdapter.animateOut(context, groupHolder, requestingPosition);
            } else {
                post = layoutForAttach.findViewById(R.id.attachLayout);
                pre = layoutForAttach.findViewById(R.id.audioRecordLayout);
                container = layoutForAttach.findViewById(R.id.bottomContainer);
                Utils.animateOut(pre, post, container);

                if (dialog != null && context instanceof HomeActivity && purpose.startsWith("Broadcast")) {
                    dialog.dismiss();
                    ((HomeActivity) context).getCaption(broadcast);
                }
            }
        }

        if(purpose.equals("Respond with")) {
            new File(context.getFilesDir(), fileName + ".3gpp").delete();
        }
        inNotification = false;
    }

    public static int position, notificationId; public static MaterialDialog y;

    public static void obtainTextResponse(Activity context, final VolleyCallback callback, String purpose) {
        final View view = context.getLayoutInflater ().inflate (R.layout.bottom_sheet_text_response, null);

        ((TextView) view.findViewById(R.id.headerTV)).setText(purpose+" a text");

        try {
            final Object r[] = {"At school", "At the gym", "At the movies", "At work", "Awkward situation. Ttyl...", "Bored to death...", "Can't talk", "Do not disturb. Busy...", "Eating", "Having shower thoughts...", "In a meeting", "In a meeting. Talk to you in a bit.", "In deep thought...", "In urgent life crisis!", "Nothing much...", "Off the grid. Call if urgent.", "Out on a date", "Phone almost dead", "Studying", "Thinking about life", "Too cool for life", "Wondering what I'm doing with my life...", "Zzzz..."};
            ArrayList<Object> choices = new ArrayList<>(Arrays.asList(r));
            JSONArray history = new JSONArray(Utils.getStringFromSharedPrefs("text_history", "[]"));
            for (int i = 0; i<history.length(); i++) {
                choices.add(0, Html.fromHtml("<font color=\"#E040FB\"><b>"+history.getString(i)+"</b></font>"));
            }
            final Object[] responses = choices.toArray();
            ((ListView) view.findViewById(R.id.predefinedLV)).setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, responses));


        final Dialog mBottomSheetDialog = new Dialog (context,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
        ((EditText)view.findViewById(R.id.customET)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String response = v.getText().toString();
                    if (!response.trim().isEmpty()) {
                        mBottomSheetDialog.dismiss();
                        callback.onResponse(response);

                        // add to history if history is enabled
                        if (Utils.getBooleanFromSharedPrefs("save_custom_texts", true)) {
                            try {
                                JSONArray history = new JSONArray(Utils.getStringFromSharedPrefs("text_history", "[]"));
                                history.put(response);
                                Utils.putStringInSharedPrefs("text_history", history.toString());
                            } catch (Exception e) {e.printStackTrace();}
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        ((ListView) view.findViewById(R.id.predefinedLV)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mBottomSheetDialog.dismiss();
                callback.onResponse(responses[position]);
            }
        });
        mBottomSheetDialog.show ();
        } catch( Exception e){e.printStackTrace();}
    }

    public static void showDialogMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(message)
                .setNeutralButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .show();

    }

    public static void refreshChatAdapter(MessagesListAdapter<Smalltalk.MyMessage> adapter, Smalltalk.SmalltalkMessage smalltalk) {
        ArrayList<Smalltalk.MyMessage> messages = (ArrayList<Smalltalk.MyMessage>) smalltalk.messages.getMessages().clone();
        Collections.reverse(messages);
        adapter.addToEnd(messages, false);
    }

    public static void sendSmalltalkMessage(String message, String messageType, final String token, final Context context) {
        try {
            JSONObject data = new JSONObject();
            data.put("type", "smalltalk_message");
            data.put("message", message);
            data.put("senderUsername", "");
            data.put("messageType", messageType);
            data.put("sender", Utils.getStringFromSharedPrefs("self_token", ""));

            String url = "http://black-abode-2709.appspot.com/send_message?to_user=" + URLEncoder.encode(token) + "&data=" + URLEncoder.encode(data.toString());
            Utils.volleyStringCall(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {

                }
            });

            if (context instanceof ShareActivity) {
                // we're in share activity
                Utils.showToast(context, "Sent to selected contacts");
                ((Activity)context).finish();
                context.startActivity(new Intent(context, HomeActivity.class).putExtra("show_feed", false));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendSmalltalkToGroup(String message, String messageType, final String token, final Context context) {
        try {
            String senderToken = Utils.getStringFromSharedPrefs("self_token", "");
            JSONObject data = new JSONObject();
            data.put("type", "group_smalltalk_message");
            data.put("message", message);
            data.put("senderUsername", Utils.getStringFromSharedPrefs("self_username", ""));
            data.put("messageType", messageType);
            data.put("sender", senderToken);
            data.put("groupName", token);

            String url = "http://black-abode-2709.appspot.com/group_smalltalk?to_group=" + URLEncoder.encode(token) + "&data=" + URLEncoder.encode(data.toString()) +
                    "&senderToken="+URLEncoder.encode(senderToken);
            Utils.volleyStringCall(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {

                }
            });

            if (context instanceof ShareActivity) {
                // we're in share activity
                Utils.showToast(context, "Sent to selected contacts");
                ((Activity)context).finish();
                context.startActivity(new Intent(context, HomeActivity.class).putExtra("show_feed", false));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Smalltalk.SmalltalkMessage addMessageToGroupSmalltalk(Smalltalk.SmalltalkMessage smalltalkMessage, Smalltalk.MyMessage message) {
        smalltalkMessage.messages.addMessage(message);
        return smalltalkMessage;
    }

    public static Smalltalk.SmalltalkMessage addMessageToSmalltalk(Smalltalk.SmalltalkMessage smalltalkMessage, Smalltalk.MyMessage message) {
        Smalltalk.SmalltalkMessage newsmalltalkMessage = new Smalltalk.SmalltalkMessage();
        newsmalltalkMessage.messages = new Smalltalk.MessageBlock();
        String userId = message.getUser().getId();

        // First determine if heterogeneous and determine what messages arr to be deleted
        int senderCount=0, receiverCount=0;
        for(Smalltalk.MyMessage i : smalltalkMessage.messages.getMessages()) {
            if(i.getUser().getId().equals("sender")) {
                senderCount++;
            } else {
                receiverCount++;
            }
        }

        boolean heterogeneous = (senderCount != 0 && receiverCount != 0);

        if(!heterogeneous) {
            for (Smalltalk.MyMessage i : smalltalkMessage.messages.getMessages()) {
                newsmalltalkMessage.messages.addMessage(i);
            }
        } else {
            ArrayList<Integer> toBeDeletedIndices = new ArrayList<>();
            int index = 0;
            for (Smalltalk.MyMessage i : smalltalkMessage.messages.getMessages()) {
                if (!i.getUser().getId().equals(userId)) {
                    break;
                } else {
                    toBeDeletedIndices.add(index);
                }
                index++;
            }
            // Now add all but those messages
            if(toBeDeletedIndices.size() == 0) {
                for (Smalltalk.MyMessage i : smalltalkMessage.messages.getMessages()) {
                    newsmalltalkMessage.messages.addMessage(i);
                }
            } else {
                index =0;
                for (Smalltalk.MyMessage i : smalltalkMessage.messages.getMessages()) {
                    if (!toBeDeletedIndices.contains(index)) {
                        newsmalltalkMessage.messages.addMessage(i);
                    }
                    index++;
                }
            }
        }
        newsmalltalkMessage.messages.addMessage(message);

        return newsmalltalkMessage;
    }

    public static class FeedDataSet {
        /**
         * The dataset used to populate the ListView on the feed screen
         */
        public static ArrayList<String> names, timestamps, ids, owners, texts, media, profilePics;
        public static ArrayList<Boolean> liked;
        public static ArrayList<Likes> likes;
        public static ArrayList<Comments> comments;
    }

    public static void populateFeedDataSet(Context context) {
        FeedDataSet.names = new ArrayList<>(); FeedDataSet.timestamps = new ArrayList<>();
        FeedDataSet.ids = new ArrayList<>(); FeedDataSet.owners = new ArrayList<>();
        FeedDataSet.texts = new ArrayList<>(); FeedDataSet.media = new ArrayList<>();
        FeedDataSet.liked = new ArrayList<>(); FeedDataSet.likes = new ArrayList<>();
        FeedDataSet.profilePics = new ArrayList<>(); FeedDataSet.comments = new ArrayList<>();

        try {
            SQLiteDatabase db = BroadcastsDBHelper.getHelper(context).getReadableDatabase();
            Cursor results = db.rawQuery("SELECT * FROM broadcasts ORDER BY timestamp DESC", null);
            for(results.moveToFirst(); !results.isAfterLast(); results.moveToNext()){
                FeedDataSet.ids.add(results.getString(results.getColumnIndexOrThrow("id")));
                FeedDataSet.timestamps.add(results.getString(results.getColumnIndexOrThrow("timestamp")));
                FeedDataSet.texts.add(results.getString(results.getColumnIndexOrThrow("text")));

                if (results.getString(results.getColumnIndexOrThrow("id")).startsWith("CHANNEL_")) {
                    // Add shit for all else
                    FeedDataSet.names.add("");
                    FeedDataSet.owners.add("");
                    FeedDataSet.comments.add(new Comments());
                    FeedDataSet.profilePics.add(""); // doesn't work for self posts
                    FeedDataSet.media.add("");
                    FeedDataSet.liked.add(false);
                    FeedDataSet.likes.add(new Likes());
                } else {
                    String token = results.getString(results.getColumnIndexOrThrow("owner_token"));
                    FeedDataSet.names.add(results.getString(results.getColumnIndexOrThrow("name")));
                    FeedDataSet.owners.add(token);
                    FeedDataSet.comments.add((Comments) deserialize(results.getString(results.getColumnIndexOrThrow("comments")), Comments.class));
                    FeedDataSet.profilePics.add(new File(context.getFilesDir(), token+"_photo.png").getAbsolutePath()); // doesn't work for self posts
                    FeedDataSet.media.add(results.getString(results.getColumnIndexOrThrow("media")));
                    FeedDataSet.liked.add(1 == (results.getInt(results.getColumnIndexOrThrow("liked"))));
                    FeedDataSet.likes.add((Likes) deserialize(results.getString(results.getColumnIndexOrThrow("likes")), Likes.class));
                }
            }
        } catch (Exception e) {e.printStackTrace();}
    }
    public static String timeFromTimeStamp(long timeStamp) {
        /**
         * Converts timestamp into x ago format
         */
        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                timeStamp,
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        return (timeAgo).toString();
    }

    public static void toggleLike(final Context context, ImageView iv, String id, String owner_token, boolean liked, Likes likes, int position) {
        /**
         * Toggle whether this person likes this broadcast or not
         */
        JSONObject data = new JSONObject();
        SQLiteDatabase db = BroadcastsDBHelper.getHelper(context).getWritableDatabase();
        ContentValues cv = new ContentValues();

        try {

            Likes.User user = new Likes.User();
            user.name = Utils.getStringFromSharedPrefs("self_name", "");
            user.username = Utils.getStringFromSharedPrefs("self_username", "");
            user.photoUrl = Utils.getStringFromSharedPrefs("self_photoUrl", "").replace("_photo.png","_photo_mini.png");
            user.token = Utils.getStringFromSharedPrefs("self_token", "");

            if(liked) {
                data.put("type", "broadcast_unliked");
                iv.setColorFilter(Color.parseColor("#757575"));
                Likes newLikes = new Likes();
                for(int i=0;i<likes.users.size();i++){
                    if(likes.users.get(i).name.equals(Utils.getStringFromSharedPrefs("self_name", ""))){
                        continue;
                    }
                    newLikes.users.add(likes.users.get(i));
                }
                cv.put("likes", Utils.serialize(newLikes));
                cv.put("liked", 0);
            } else {
                iv.setColorFilter(Color.parseColor("#E040FB"));
                data.put("type", "broadcast_liked");
                likes.users.add(user);

                cv.put("liked", 1);
                cv.put("likes", Utils.serialize(likes));
            }
            db.update("broadcasts", cv, "id=?", new String[]{id});

            data.put("sender", serialize(user));
            data.put("id", id);

            populateFeedDataSet(context);

            // Now refresh LV
            if (position != -1) {
                // we're in a list
                ListView list = (ListView) ((Activity) iv.getContext()).findViewById(R.id.mainLV2);
                View view = list.getChildAt(position - list.getFirstVisiblePosition());
                list.getAdapter().getView(position, view, list);
                Utils.populateFeedDataSet(context);
            }

            if(!owner_token.equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
                String url = "http://black-abode-2709.appspot.com/send_message?to_user="+ owner_token + "&data=" + URLEncoder.encode(data.toString());
                Utils.volleyStringCall(context, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {

                    }
                });
            }
            String url = "http://black-abode-2709.appspot.com/send_message?to_topic="+ owner_token + "&data=" + URLEncoder.encode(data.toString());
            Utils.volleyStringCall(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {

                }
            });
        } catch(Exception e) {}
    }

    public static void showLikesDialog(final Context context, final Likes likes, final int position) {
        final LinearLayout view = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.bottom_sheet_likes, null);
        ((TextView) view.findViewById(R.id.likesHeaderTV)).setText("People who like this · "+likes.users.size());
        if (likes.users.size()==0) {
            EmptyAdapter adapter = new EmptyAdapter(context);
            adapter.comments = true;
            adapter.title = "Nothing to see here!";
            adapter.message = "Nobody has liked this broadcast yet.";
            ((ListView) view.findViewById(R.id.likesLV)).setAdapter(adapter);
        } else {
            ((ListView) view.findViewById(R.id.likesLV)).setAdapter(new LikesAdapter(context, likes));
        }

        final Dialog mBottomSheetDialog = new Dialog (context,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
        mBottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                ((HomeActivity) context).activeLikesLV =  ((ListView) view.findViewById(R.id.likesLV));
                ((HomeActivity) context).activeHeaderTV =  ((TextView) view.findViewById(R.id.likesHeaderTV));
                ((HomeActivity) context).activeLikesPosition = position;
            }
        });
        mBottomSheetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((HomeActivity) context).activeLikesLV =  null;
                ((HomeActivity) context).activeLikesPosition = -1;
                ((HomeActivity) context).activeHeaderTV =  null;
            }
        });
        mBottomSheetDialog.show ();
    }

    public static void showCommentsDialog(final Context context,final Comments comments, final String id, final String owner_token, final int position) {
        final LinearLayout view = (LinearLayout) ((Activity)context).getLayoutInflater().inflate(R.layout.bottom_sheet_comments, null);
        final TextView header = ((TextView) view.findViewById(R.id.headerTV));
        header.setText("Comments · "+comments.users.size());

        if (comments.users.size()==0) {
            EmptyAdapter adapter = new EmptyAdapter(context);
            adapter.comments = true;
            adapter.title = "Nothing to see here!";
            adapter.message = "Nobody has commented on this broadcast yet.";
            ((ListView) view.findViewById(R.id.commentsLV)).setAdapter(adapter);
        } else {
            ((ListView) view.findViewById(R.id.commentsLV)).setAdapter(new CommentsAdapter(context, comments));
        }


        ((EditText) view.findViewById(R.id.commentET)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    String comment = v.getText().toString().trim();
                    if (!comment.isEmpty()) {
                        Comments.Commenter user = new Comments.Commenter();
                        user.comment = comment;
                        user.name = Utils.getStringFromSharedPrefs("self_name", "");
                        user.username = Utils.getStringFromSharedPrefs("self_username", "");
                        user.timeStamp = ""+System.currentTimeMillis();
                        user.photoUrl = Utils.getStringFromSharedPrefs("self_photoUrl", "").replace("_photo.png","_photo_mini.png");
                        user.token = Utils.getStringFromSharedPrefs("self_token", "");

                        Comments comments = FeedDataSet.comments.get(position);
                        comments.users.add(user);

                        addCommentToDb(context, id, comments); // add comment

                        populateFeedDataSet(context); // refresh dataset
                        ((BaseAdapter)((ListView)((Activity) context).findViewById(R.id.mainLV2)).getAdapter()).notifyDataSetChanged(); // refresh feed LV
                        ((ListView) view.findViewById(R.id.commentsLV)).setAdapter(new CommentsAdapter(context, comments)); // refresh commentsLV

                        sendComment(context, user, id, owner_token); // send comment
                        v.setText("");
                        header.setText("Comments · "+comments.users.size());
                        return true;
                    }
                }
                return false;
            }
        });

        final Dialog mBottomSheetDialog = new Dialog (context,
                R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
        mBottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                ((HomeActivity) context).activeCommentsLV =  ((ListView) view.findViewById(R.id.commentsLV));
                ((HomeActivity) context).activeHeaderTV =  ((TextView) view.findViewById(R.id.headerTV));
                ((HomeActivity) context).activeCommentsPosition = position;
            }
        });
        mBottomSheetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((HomeActivity) context).activeCommentsLV =  null;
                ((HomeActivity) context).activeHeaderTV =  null;
                ((HomeActivity) context).activeCommentsPosition = -1;
            }
        });
        mBottomSheetDialog.show();
    }

    public static void addCommentToDb(Context context, String id, Comments comments) {
        SQLiteDatabase db = BroadcastsDBHelper.getHelper(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("comments", serialize(comments));
        db.update("broadcasts", values, "id=?", new String[]{id});
    }

    public static void sendComment(Context context, Comments.Commenter commenter, String id, String owner_token) {
        try {
            JSONObject data = new JSONObject();
            data.put("id", id);
            data.put("type", "comment");
            data.put("commenter", serialize(commenter));
            String url = "http://black-abode-2709.appspot.com/send_message?&to_topic=" + URLEncoder.encode(owner_token) + "&data=" + URLEncoder.encode(data.toString());

            Utils.volleyStringCall(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {

                }
            });

            if (!owner_token.equals(Utils.getStringFromSharedPrefs("self_token", ""))) {
                url = "http://black-abode-2709.appspot.com/send_message?to_user=" + owner_token + "&data=" + URLEncoder.encode(data.toString());
                Utils.volleyStringCall(context, url, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {

                    }
                });
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void addBroadcastToDb(Context context, String id, String name, String owner_token, String text, String media, Likes likes, Comments comments, int liked) {
        String timeStamp = ""+System.currentTimeMillis();

        SQLiteDatabase db = BroadcastsDBHelper.getHelper(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("name", name);
        values.put("owner_token", owner_token);
        values.put("text", text);
        values.put("media", media);
        values.put("likes", serialize(likes));
        values.put("comments", serialize(comments));
        values.put("liked", liked);
        values.put("timeStamp", timeStamp);
        db.insert("broadcasts", null, values);
    }

    public static void sendBroadcast(Context context, JSONObject broadcast) {
        String url = "http://black-abode-2709.appspot.com/send_message?ttl="+(24*60*60)+"&to_topic=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", "")) + "&data=" + URLEncoder.encode(broadcast.toString());
        Utils.volleyStringCall(context, url, new VolleyCallback() {
            @Override
            public void onResponse(Object result) {

            }
        });
    }

    public static boolean containsUsername(String s) {
        try {
            String letterAfterUsername = "" + s.charAt(s.indexOf(Utils.getStringFromSharedPrefs("self_username", "")) + Utils.getStringFromSharedPrefs("self_username", "").length());
            boolean endsWithNonAlphaNum = letterAfterUsername.matches("^.*[^a-zA-Z0-9\\._\\-].*$");

            return (s.contains("@"+Utils.getStringFromSharedPrefs("self_username", "")) && endsWithNonAlphaNum);
        } catch(StringIndexOutOfBoundsException e) {
            if(!s.contains(Utils.getStringFromSharedPrefs("self_username", ""))) {
                return false;
            }
            return !s.isEmpty();
            // if the letter is out of bounds that means that the text ended with the username
            // but if it's empty, then it has no username
        }
    }

    public static void sendTextBroadcast(JSONObject broadcast, Activity context) {
        // Send broadcast
        // Add to db, send to server, refresh
        try {
            Utils.addBroadcastToDb(context,
                    broadcast.getString("id"),
                    Utils.getStringFromSharedPrefs("self_name", ""),
                    Utils.getStringFromSharedPrefs("self_token", ""),
                    broadcast.getString("text"),
                    broadcast.getString("media"),
                    new Likes(),
                    new Comments(),
                    0);

            if(context instanceof HomeActivity) {
                Utils.populateFeedDataSet(context);
                if ((((ListView) context.findViewById(R.id.mainLV2)).getAdapter()) instanceof FeedAdapter) {
                    ((BaseAdapter) ((ListView) context.findViewById(R.id.mainLV2)).getAdapter()).notifyDataSetChanged();
                } else {
                    final ListView view = ((ListView) context.findViewById(R.id.mainLV2));
                    final FeedAdapter adapter = new FeedAdapter(context);
                    view.setAdapter(adapter);
                }
            }  else  {
                // we're in share activity
                Utils.showToast(context, "Shared via broadcast");
                context.finish();
                context.startActivity(new Intent(context, HomeActivity.class).putExtra("show_feed", true));
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void sendMediaBroadcast(JSONObject broadcast, Activity context){
        // Add to db, send to server, refresh
        try {
            Utils.addBroadcastToDb(context,
                    broadcast.getString("id"),
                    Utils.getStringFromSharedPrefs("self_name", ""),
                    Utils.getStringFromSharedPrefs("self_token", ""),
                    broadcast.getString("text"),
                    broadcast.getString("media"),
                    new Likes(),
                    new Comments(),
                    0);

            if(context instanceof HomeActivity) {
                Utils.populateFeedDataSet(context);
                if ((((ListView) context.findViewById(R.id.mainLV2)).getAdapter()) instanceof FeedAdapter) {
                    ((BaseAdapter) ((ListView) context.findViewById(R.id.mainLV2)).getAdapter()).notifyDataSetChanged();
                } else {
                    final ListView view = ((ListView) context.findViewById(R.id.mainLV2));
                    final FeedAdapter adapter = new FeedAdapter(context);
                    view.setAdapter(adapter);
                }
            }  else  {
                // we're in share activity
                Utils.showToast(context, "Broadcast successfully sent");
                context.finish();
                context.startActivity(new Intent(context, HomeActivity.class).putExtra("show_feed", true));
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void copyFile(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
    public static String getMimeType(Uri uri, Activity context) {
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getApplicationContext().getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    public static void copyInputStreamToFile(InputStream in, File file) {
        OutputStream out = null;

        try {
            out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            // Ensure that the InputStreams are closed even if there's an exception.
            try {
                if ( out != null ) {
                    out.close();
                }

                // If you want to close the "in" InputStream yourself then remove this
                // from here but ensure that you close it yourself eventually.
                in.close();
            }
            catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }

    public static void isUserBlocked(Context context, String token, final VolleyCallback callback) {
        if (token == null) {
            callback.onResponse(false); return;
        }
        try {
            SQLiteDatabase db = ConnectionsDBHelper.getHelper(context).getReadableDatabase();
            Cursor result = db.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{token});
            result.moveToFirst();
            if (result.getCount() != 0) {
                String username = result.getString(result.getColumnIndexOrThrow("username"));
                JSONArray blocked = new JSONArray(Utils.getStringFromSharedPrefs("blocked_users", "[]"));
                for (int i=0; i<blocked.length(); i++) {
                    if (blocked.getString(i).equals(username)) {
                        callback.onResponse(true); return;
                    }
                }
                callback.onResponse(false); return;
            } else {
                Utils.volleyStringCall(context, "http://black-abode-2709.appspot.com/getUsernameFromToken?token=" + token, new VolleyCallback() {
                    @Override
                    public void onResponse(Object result) {
                        try {
                            String username = (String) result;
                            JSONArray blocked = new JSONArray(Utils.getStringFromSharedPrefs("blocked_users", "[]"));
                            for (int i = 0; i < blocked.length(); i++) {
                                if (blocked.getString(i).equals(username)) {
                                    callback.onResponse(true);
                                }
                            }
                            callback.onResponse(false); return;
                        } catch(Exception e) {e.printStackTrace();}
                    }
                });
            }
        } catch (Exception e) {callback.onResponse(false); return;}
    }

    private static String getSettingsRepr() {
        try {
            JSONObject settings = new JSONObject();
            settings.put("blocked_users", Utils.getStringFromSharedPrefs("blocked_users", "[]"));
            settings.put("last_seen", Utils.getBooleanFromSharedPrefs("last_seen", true));
            settings.put("private_profile", Utils.getBooleanFromSharedPrefs("private_profile", false));
            settings.put("restricted_profile", Utils.getBooleanFromSharedPrefs("restricted_profile", false));
            settings.put("notify_on_sup", Utils.getBooleanFromSharedPrefs("notify_on_sup", true));
            settings.put("notify_on_respond", Utils.getBooleanFromSharedPrefs("notify_on_respond", true));
            settings.put("notify_on_like", Utils.getBooleanFromSharedPrefs("notify_on_like", true));
            settings.put("notify_on_comments", Utils.getBooleanFromSharedPrefs("notify_on_comments", true));
            settings.put("notify_on_mention", Utils.getBooleanFromSharedPrefs("notify_on_mention", true));
            settings.put("notification_lights", Utils.getBooleanFromSharedPrefs("notification_lights", true));
            settings.put("notification_vibration", Utils.getBooleanFromSharedPrefs("notification_vibration", true));
            settings.put("notification_sound", Utils.getBooleanFromSharedPrefs("notification_sound", true));
            settings.put("notify_on_smalltalk", Utils.getBooleanFromSharedPrefs("notify_on_smalltalk", true));
            settings.put("save_custom_texts", Utils.getBooleanFromSharedPrefs("save_custom_texts", true));
            settings.put("keep_sup_count", Utils.getBooleanFromSharedPrefs("keep_sup_count", true));
            settings.put("delete_smalltalks_frequency", Utils.getIntegerFromSharedPrefs("delete_smalltalks_frequency", 0));
            return settings.toString();
        } catch (Exception e) {e.printStackTrace(); return null;}
    }

    private static void saveSelfSettings(String s) {
        try {
            JSONObject settings = new JSONObject(s);
            if (settings.length() == 0) return;
            Utils.putStringInSharedPrefs("blocked_users", settings.getString("blocked_users"));
            Utils.putBooleanInSharedPrefs("last_seen", settings.getBoolean("last_seen"));
            Utils.putBooleanInSharedPrefs("private_profile", settings.getBoolean("private_profile"));
            Utils.putBooleanInSharedPrefs("restricted_profile", settings.getBoolean("restricted_profile"));
            Utils.putBooleanInSharedPrefs("notify_on_sup", settings.getBoolean("notify_on_sup"));
            Utils.putBooleanInSharedPrefs("notify_on_respond", settings.getBoolean("notify_on_respond"));
            Utils.putBooleanInSharedPrefs("notify_on_like", settings.getBoolean("notify_on_like"));
            Utils.putBooleanInSharedPrefs("notify_on_comments", settings.getBoolean("notify_on_comments"));
            Utils.putBooleanInSharedPrefs("notify_on_mention", settings.getBoolean("notify_on_mention"));
            Utils.putBooleanInSharedPrefs("notification_lights", settings.getBoolean("notification_lights"));
            Utils.putBooleanInSharedPrefs("notification_vibration", settings.getBoolean("notification_vibration"));
            Utils.putBooleanInSharedPrefs("notification_sound", settings.getBoolean("notification_sound"));
            Utils.putBooleanInSharedPrefs("notify_on_smalltalk", settings.getBoolean("notify_on_smalltalk"));
            Utils.putBooleanInSharedPrefs("save_custom_texts", settings.getBoolean("save_custom_texts"));
            Utils.putBooleanInSharedPrefs("keep_sup_count", settings.getBoolean("keep_sup_count"));
            Utils.putIntegerInSharedPrefs("delete_smalltalks_frequency", settings.getInt("delete_smalltalks_frequency"));
        }catch (Exception e){e.printStackTrace();}
    }

    public static void updateSettingsToServer(Context context) {
        String url = "http://black-abode-2709.appspot.com/update_profile?token="+Utils.getStringFromSharedPrefs("self_token", "")+"&settings="+getSettingsRepr();
        if (Utils.isNetworkAvailable(context)) {
            Utils.volleyStringCall(context, url, new VolleyCallback() {
                @Override
                public void onResponse(Object result) {

                }
            });
        } else {
            Utils.requestWhenInternetAvailable(url);
        }
    }

    public static Activity getRequiredActivity(View req_view) {
        Context context = req_view.getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }


    public static NotificationCompat.Builder prepareNotificationForSmalltalk (Context context, Smalltalk.SmalltalkMessage smalltalk, NotificationCompat.Builder mBuilder, String chatName, int notificationId, String sender_token, boolean newNotification) {
        NotificationCompat.MessagingStyle style = new NotificationCompat.MessagingStyle("Me");
        if(sender_token.startsWith("GROUP_")) {
            style.setConversationTitle("Group chat with " + chatName);
        } else {
            style.setConversationTitle("Chat with " + chatName);
        }
        for (Smalltalk.MyMessage message : smalltalk.messages.getMessages()) {
            switch (message.getType()) {
                case "text":
                    style.addMessage(message.getText(), message.createdAt.getTime(), (message.getUser().getId().equals("sender") ? null : chatName));
                    break;
                case "image_response":
                    SpannableString i = new SpannableString("Image");
                    i.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Image".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    style.addMessage(i, message.createdAt.getTime(), (message.getUser().getId().equals("sender") ? null : chatName));
                    break;
                case "video_response":
                    SpannableString v = new SpannableString("Video");
                    v.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Video".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    style.addMessage(v, message.createdAt.getTime(), (message.getUser().getId().equals("sender") ? null : chatName));
                    break;
                case "audio_response":
                    SpannableString a= new SpannableString("Audio");
                    a.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Audio".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    style.addMessage(a, message.createdAt.getTime(), (message.getUser().getId().equals("sender") ? null : chatName));
                    break;
                case "location_response":
                    SpannableString l = new SpannableString("Location");
                    l.setSpan(new StyleSpan(Typeface.ITALIC), 0, "Location".length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    style.addMessage(l, message.createdAt.getTime(),(message.getUser().getId().equals("sender") ? null : chatName));
                    break;
            }
        }
        mBuilder.setStyle(style);

        if(newNotification) {
            // Key for the string that's delivered in the action's intent.
            String replyLabel = "Reply";
            RemoteInput remoteInput = new RemoteInput.Builder("KEY_TEXT_REPLY")
                    .setLabel(replyLabel)
                    .build();

            Intent resultIntent = new Intent(context, NotificationActionActivity.class);
            resultIntent.putExtra("notificationId", notificationId);
            resultIntent.putExtra("to", sender_token);

            resultIntent.putExtra("ACTION", "SMALLTALK_FROM_NOTIFICATION");

            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            context,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(R.mipmap.ic_reply_white_24dp,
                            "Reply", resultPendingIntent)
                            .addRemoteInput(remoteInput)
                            .build();

            mBuilder.addAction(action);
        }

        return mBuilder;
    }

    public static NotificationCompat.Builder prepareNotificationForSup(NotificationCompat.Builder mBuilder, Context context, String message, int notificationId, String sender_token, boolean newNotification) {
        mBuilder.setContentText(message);

        if (newNotification) {

            Intent resultIntent = new Intent(context, NotificationActionActivity.class);
            resultIntent.putExtra("notificationId", notificationId);
            resultIntent.putExtra("to", sender_token);

            resultIntent.putExtra("ACTION", "SUP_FROM_NOTIFICATION");

            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            context,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(R.mipmap.ic_reply_white_24dp,
                            "Respond", resultPendingIntent)
                            .build();

            mBuilder.addAction(action);
        }
        return mBuilder;
    }

    public static NotificationCompat.Builder prepareNotificationForComment(Context context, Comments comments, boolean updating, String comments2, String id, String owner_token, int notificationId) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.mipmap.ic_notification);
        mBuilder.setColor(Color.parseColor("#673AB7"));

        NotificationCompat.MessagingStyle style = new NotificationCompat.MessagingStyle("Me");
        style.setConversationTitle("Comments on your broadcast");
        for (Comments.Commenter commenter : comments.users) {
            style.addMessage(commenter.comment, Long.parseLong(commenter.timeStamp), (commenter.name.equals(Utils.getStringFromSharedPrefs("self_name", ""))) ? null : commenter.name);
        }
        mBuilder.setStyle(style);

        // Key for the string that's delivered in the action's intent.
        String replyLabel = "Comment";
        RemoteInput remoteInput = new RemoteInput.Builder("KEY_TEXT_REPLY")
                .setLabel(replyLabel)
                .build();

        Intent resultIntent = new Intent(context, NotificationActionActivity.class);
        resultIntent.putExtra("owner_token", owner_token);
        resultIntent.putExtra("id", id);
        resultIntent.putExtra("notificationId", notificationId);
        resultIntent.putExtra("comments", comments2);

        resultIntent.putExtra("ACTION", "COMMENT_FROM_NOTIFICATION");

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(R.mipmap.ic_mode_edit_white_24dp,
                        "Comment", resultPendingIntent)
                        .addRemoteInput(remoteInput)
                        .build();

        mBuilder.addAction(action);

        return mBuilder;
    }
}
