package com.sup.sup;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.appinvite.AppInviteInvitation;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;

import de.psdev.licensesdialog.LicensesDialog;
import de.psdev.licensesdialog.licenses.ApacheSoftwareLicense20;
import de.psdev.licensesdialog.licenses.BSD2ClauseLicense;
import de.psdev.licensesdialog.licenses.MITLicense;
import de.psdev.licensesdialog.model.Notice;
import de.psdev.licensesdialog.model.Notices;

public class SettingsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        ListView mainLV = (ListView) findViewById(R.id.mainLV);
        mainLV.setAdapter(new SettingsAdapter(this));
        mainLV.setOnItemClickListener(this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        LinearLayout card = (LinearLayout) findViewById(R.id.mainCard);
        ((TextView)card.findViewById(R.id.statusTV)).setText(Utils.getStringFromSharedPrefs("self_tagline", ""));
        ((TextView)card.findViewById(R.id.statusTV)).setSelected(true);
        ((TextView)card.findViewById(R.id.nameTV)).setText(Utils.getStringFromSharedPrefs("self_name", ""));
        ((TextView)card.findViewById(R.id.usernameTV)).setText("@"+Utils.getStringFromSharedPrefs("self_username", ""));
        try {
            String filePath = this.getFilesDir() + "/" + "self_photo.png";
            File file = new File(filePath);
            Bitmap picture = BitmapFactory.decodeStream(new FileInputStream(file));
            ((ImageView) card.findViewById(R.id.profilePicture)).setImageBitmap(picture);
            card.findViewById(R.id.edit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(SettingsActivity.this, CreateProfileActivity.class).putExtra("editing", true));
                }
            });
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch(position) {
            case 0:
                invite();
                break;
            case 1:
                showPrivacySettings();
                break;
            case 2:
                showNotificationSettings();
                break;
            case 3:
                showMainSettings();
                break;
            case 4:
                deleteAccount();
                break;
            case 5:
                verifyPhone();
                break;
            case 6:
                showPermissionsActivity();
                break;
            case 7:
                showFAQ();
                break;
            case 8:
                showTnC();
                break;
            case 9:
                sendEmail();
                break;
            case 10:
                showLicenses();
                break;
            case 11:
                showAbout();
                break;
        }
    }

    private void verifyPhone() {
        startActivity(new Intent(this, PhoneVerifDetailsActivity.class));
    }

    private void showPrivacySettings() {
        startActivity(new Intent(this, PrivacySettingsActivity.class));
    }

    private void showNotificationSettings() {
        startActivity(new Intent(this, NotificationSettingsActivity.class));
    }

    private void showMainSettings() {
        startActivity(new Intent(this, MainSettingsActivity.class));
    }

    private void showAbout() {
        startActivity(new Intent(this, AboutActivity.class));
    }

    private void showPermissionsActivity() {
        startActivity(new Intent(this, PermissionsActivity.class));
    }

    private void deleteAccount() {
        new AlertDialog.Builder(this)
                .setTitle("Delete Account?")
                .setMessage("Your profile will be irreversibly deleted.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // First delete pics from servers. (All other pics will be autodeleted)
                        Utils.deleteFromFCS(Utils.getStringFromSharedPrefs("self_token", "") + "_photo_mini.png", new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {

                            }
                        });
                        Utils.deleteFromFCS(Utils.getStringFromSharedPrefs("self_token", ""), new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {

                            }
                        });

                        // Now tell server
                        Utils.volleyStringCall(SettingsActivity.this, "http://black-abode-2709.appspot.com/deleteAccountPermanently?token=" + URLEncoder.encode(Utils.getStringFromSharedPrefs("self_token", "")), new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {

                            }
                        });

                        // Now delete all shit from their device
                        Utils.showToast(SettingsActivity.this, "We're sorry to see your go!");
                        HomeActivity.logout(SettingsActivity.this);
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void sendEmail() {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setType("*/*");
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@yousup.me"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
        startActivity(Intent.createChooser(i, "Send feedback via email"));
    }

    private void showFAQ() {
        startActivity(new Intent(this, WebViewActivity.class).putExtra("filename", "FAQ.html").putExtra("title", "FAQ"));
    }

    private void showTnC() {
        startActivity(new Intent(this, WebViewActivity.class).putExtra("filename", "t&c.html").putExtra("title", "Terms and Conditions"));
    }

    private void invite() {
        final String message = "Hey,\n" +
                "\n" +
                "I just started using this app called Sup on my phone.\n" +
                "\n" +
                "It's not really a messaging app, but an app that lets you update your friends and family with your current status in as few as a single tap.\n" +
                "\n" +
                "It also lets you keep track of a variety of other things, like your twitter or tumblr feed, your fedex packages, etc.\n" +
                "\n" +
                "Could you please download it, so there's someone else on Sup to use it with me? Just go to http://www.yousup.me/";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Invite friends using")
                .setItems(new String[] {"SMS or Email", "A third-party app"}, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                // Firebase invites
                                Intent intent = new AppInviteInvitation.IntentBuilder("Invite friends")
                                        .setCallToActionText("Download Sup")
                                        .build();
                                startActivityForResult(intent, 1);
                                break;
                            case 1:
                                // Traditional invites

                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                        }
                    }
                });
        builder.show();
    }

    private void showLicenses() {
        final Notices notices = new Notices();
        notices.addNotice(new Notice("Android Support Library", null, "(c) Google, Inc.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("Google Play Services Library", null, "(c) Google, Inc., All rights reserved.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("Glide", "https://github.com/bumptech/glide", "(c) Sam Judd, All rights reserved.", new BSD2ClauseLicense()));
        notices.addNotice(new Notice("Firebase SDK", null, "(c) Google, Inc.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("Volley", null, "(c) Google, Inc.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("ImagePicker", "https://github.com/nguyenhoanglam/ImagePicker", "(c) Nguyen Hoang Lam, All rights reserved.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("App Intro Library", "https://github.com/apl-devs/AppIntro", "(c) Paolo Rotolo, Maximilian Narr, All rights reserved.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("DismissableImageView", "https://github.com/dmallcott/DismissibleImageView", "(c) Daniel Mallcott, All rights reserved.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("Sandrios Camera", "https://github.com/sandrios/sandriosCamera", "(c) sandrios studios, All rights reserved.", new MITLicense()));
        notices.addNotice(new Notice("Easy Video Player", "https://github.com/afollestad/easy-video-player", "(c) Aidan Follestad, All rights reserved.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("ChatKit", "http://stfalcon.com", "(c) stfalcon.com, All rights reserved.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("Google Gson", null, "(c) Google, Inc.", new ApacheSoftwareLicense20()));
        notices.addNotice(new Notice("Audiogram", "https://github.com/alxrm/audiowave-progressbar", "(c) Alexey Derbyshev, All rights reserved.", new MITLicense()));

        new LicensesDialog.Builder(this)
                .setNotices(notices)
                .setIncludeOwnLicense(true)
                .build()
                .show();
    }
}
