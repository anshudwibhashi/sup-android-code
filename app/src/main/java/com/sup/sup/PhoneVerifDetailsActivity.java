package com.sup.sup;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PhoneVerifDetailsActivity extends AppCompatActivity {

    private void init() {
        String phone = Utils.getStringFromSharedPrefs("self_phone_number", "0");
        if (phone.equals("0")) {
            findViewById(R.id.removeButton).setVisibility(View.GONE);
            ((Button)findViewById(R.id.addChangeButton)).setText("Add Number");
            ((TextView)findViewById(R.id.phoneNumberDisplay)).setText("None");
        } else {
            ((TextView)findViewById(R.id.phoneNumberDisplay)).setText(phone);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verif_details);
        init();

        findViewById(R.id.addChangeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PhoneVerifDetailsActivity.this, VerifyPhoneActivity.class));
            }
        });

        findViewById(R.id.removeButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.putStringInSharedPrefs("self_phone_number", "0");
                Utils.volleyStringCall(PhoneVerifDetailsActivity.this,
                        "http://black-abode-2709.appspot.com/addPhoneNumber?phone=" +
                                "0" + "&token=" + Utils.getStringFromSharedPrefs("self_token",""),
                        new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {

                            }
                        });
                init();
                Utils.showToast(PhoneVerifDetailsActivity.this, "Phone removed from records");
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        init();
    }
}
