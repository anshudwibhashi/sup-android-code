package com.sup.sup;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import org.json.JSONObject;

public class WeatherChannelActivity extends AppCompatActivity {
    private boolean enabled = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_channel);

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SQLiteDatabase dbR = ConnectionsDBHelper.getHelper(this).getReadableDatabase();
        Cursor c = dbR.rawQuery("SELECT * FROM connections WHERE token=?", new String[]{"CHANNEL_WEATHER"});
        enabled = c.getCount() == 1;
        if (enabled) {
            findViewById(R.id.enableButton).setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
            ((Button)findViewById(R.id.enableButton)).setText("Disable");
        }
        findViewById(R.id.enableButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (enabled) {
                        Utils.dbDelete(WeatherChannelActivity.this, "CHANNEL_WEATHER");
                        findViewById(R.id.enableButton).setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        ((Button)findViewById(R.id.enableButton)).setText("Enable");
                        Utils.showToast(WeatherChannelActivity.this, "Disabled");
                        enabled = false;
                    } else {
                        JSONObject profile = new JSONObject();
                        profile.put("token", "CHANNEL_WEATHER");
                        profile.put("name", "");
                        profile.put("tagline", "");
                        profile.put("photoUrl", "");
                        profile.put("username", "");
                        profile.put("online_status", "");
                        Utils.dbAdd(WeatherChannelActivity.this, profile, "CHANNEL_WEATHER", new VolleyCallback() {
                            @Override
                            public void onResponse(Object result) {
                                findViewById(R.id.enableButton).setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
                                ((Button)findViewById(R.id.enableButton)).setText("Disable");
                                Utils.showToast(WeatherChannelActivity.this, "Enabled");
                                enabled = true;
                            }
                        });
                    }
                } catch (Exception e) {e.printStackTrace();}
            }
        });
        boolean useMetricUnits = Utils.getBooleanFromSharedPrefs("use_metric_units_weather", true);
        if (useMetricUnits) {
            ((Button) findViewById(R.id.switchUnits)).setText("Switch to Fahrenheit");
        } else {
            ((Button) findViewById(R.id.switchUnits)).setText("Switch to Celsius");
        }
    }

    public void switchUnitsForWeatherChannel(View view) {
        boolean useMetricUnits = !Utils.getBooleanFromSharedPrefs("use_metric_units_weather", true);
        Utils.putBooleanInSharedPrefs("use_metric_units_weather", useMetricUnits);
        if (useMetricUnits) {
            ((Button) view).setText("Switch to Fahrenheit");
        } else {
            ((Button) view).setText("Switch to Celsius");
        }
    }
}
