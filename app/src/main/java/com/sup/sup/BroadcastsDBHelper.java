package com.sup.sup;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BroadcastsDBHelper extends SQLiteOpenHelper{
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Broadcasts.db";

    public BroadcastsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        // NOTE: If id starts with CHANNEL_ - Then ignore all else. Save newsarticlesdb identifier in text. Treat this broadcast differently.
        db.execSQL("CREATE TABLE IF NOT EXISTS broadcasts " +
                "(id TEXT, " + // Just a token to identify broadcasts
                "name TEXT, " +
                "owner_token TEXT, " + // The user's token (For profile picture)
                "text TEXT, " + // Text content, if any
                "media TEXT, " + // Filename of the media, if any
                "likes TEXT, " + // Array of (names, usernames and photoUrls) of all those who like this post (Serialized object)
                "comments TEXT, " + // Serialized object: Array of comments, names, usernames, photoUrls
                "liked INT, " + // Effectively boolean. Whether this person likes this post
                "timestamp TEXT); ");
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static BroadcastsDBHelper instance;

    public static synchronized BroadcastsDBHelper getHelper(Context context)
    {
        if (instance == null)
            instance = new BroadcastsDBHelper(context);

        return instance;
    }
}
