package com.sup.sup;

import android.graphics.Bitmap;
import android.graphics.Color;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

/**
 * Created by anshudwibhashi on 20/06/17.
 */
public class ShareAdapter extends BaseAdapter {
    ShareActivity context;
    public ShareAdapter(ShareActivity context) {
        this.context = context;
        Utils.populateMainDataSet(context);
    }

    @Override
    public int getCount() {
        return Utils.MainDataSet.names.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(position == 0)  {
            return context.getLayoutInflater().inflate(R.layout.list_item_share_broadcast, null);
        }
        final ViewHolder holder;
        if (convertView == null || convertView.getTag() == null) {
            holder = new ViewHolder();
            convertView =  context.getLayoutInflater().inflate(R.layout.list_item_share, null);
            holder.nameTV = (TextView) convertView.findViewById(R.id.nameTV);
            holder.usernameTV = (TextView) convertView.findViewById(R.id.usernameTV);
            holder.photoIV = (ImageView) convertView.findViewById(R.id.photoIV);
            holder.checkButton = (ImageView) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        position--;
        holder.nameTV.setText(Utils.MainDataSet.names.get(position));
        if (Utils.MainDataSet.tokens.get(position).startsWith("GROUP_")) {
            holder.usernameTV.setText(Html.fromHtml("<i>Group</i>"));
        } else {
            holder.usernameTV.setText("@" + Utils.MainDataSet.usernames.get(position));
        }
        Glide.with(context).load(Utils.MainDataSet.photos.get(position)).asBitmap().centerCrop()
                .override(100, 100)
                .placeholder(R.drawable.ic_default_picture).into(new BitmapImageViewTarget(holder.photoIV) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.photoIV.setImageDrawable(circularBitmapDrawable);
            }
        });

        if(context.selectedUsers.toString().contains(Utils.MainDataSet.tokens.get(position))) {
            holder.checkButton.setColorFilter(Color.parseColor("#FFFFFF"));
            ((View)holder.checkButton.getParent()).setBackgroundResource(R.drawable.rounded_button3);
        }

        return convertView;
    }

    public static class ViewHolder {
        public ImageView photoIV;
        public TextView nameTV;
        public TextView usernameTV;
        public ImageView checkButton;
    }
}
