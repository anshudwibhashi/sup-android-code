package com.sup.sup;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;

/**
 * Created by anshudwibhashi on 08/10/17.
 */

public class StockPriceAdapter extends BaseAdapter {
    JSONArray stocks; Activity activity;
    public StockPriceAdapter(JSONArray stocks, Activity activity) {
        this.stocks = stocks;
        this.activity = activity;
    }
    @Override
    public int getCount() {
        return stocks.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView =  activity.getLayoutInflater().inflate(R.layout.list_item_stock_price, null);
            holder.tickerTV = (TextView) convertView.findViewById(R.id.tickerTV);
            holder.triangleTV = (TextView) convertView.findViewById(R.id.triangleTV);
            holder.priceTV = (TextView) convertView.findViewById(R.id.priceTV);
            holder.changeTV = (TextView) convertView.findViewById(R.id.changeTV);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.tickerTV.setText(stocks.getJSONObject(position).getString("symbol"));
            if (stocks.getJSONObject(position).has("price") && !stocks.getJSONObject(position).getString("price").equals("null")) {
                holder.priceTV.setText(stocks.getJSONObject(position).getString("price"));
            } else {
                holder.priceTV.setText(" - ");
            }
            if (stocks.getJSONObject(position).has("change_abs") && !stocks.getJSONObject(position).getString("change_abs").equals("null")) {
                if (Double.valueOf(stocks.getJSONObject(position).getString("change_abs")) > 0) {
                    holder.triangleTV.setText(Html.fromHtml("&#x25B2;"));
                } else {
                    holder.triangleTV.setText(Html.fromHtml("&#x25BC;"));
                }
                holder.changeTV.setText(stocks.getJSONObject(position).getString("change_abs")
                        + " / " + stocks.getJSONObject(position).getString("change_pct"));
            } else {
                holder.triangleTV.setVisibility(View.GONE);
                holder.changeTV.setText(" - / - ");
            }
        } catch (Exception e ) {e.printStackTrace();}


        return convertView;
    }

    public static class ViewHolder {
        public TextView tickerTV, triangleTV, priceTV, changeTV;
    }
}
