package com.sup.sup;

import java.util.ArrayList;

/**
 * Created by anshudwibhashi on 03/06/17.
 */
public class Likes {
    public ArrayList<User> users;
    public Likes(){
        users = new ArrayList<>();
    }
    public static class User {
        public String name, username, photoUrl, token;
        public User(){
            name = "";
            username = "";
            photoUrl = "";
            token = "";
        }
    }
}
