# README #

This repository contains the entire code for Sup's Android app frontend.

### That's all you have to know... ###

*For any issues, contact* [Anshu Dwibhashi](mailto:anshu@yousup.me)

*Get the app at* [www.yousup.me](http://www.yousup.me)